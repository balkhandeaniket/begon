<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'is_expense', // Add 'title' to the fillable array
        'title', // Add 'title' to the fillable array
        'type', // Add 'type' to the fillable array
        'status'
    ];
}
