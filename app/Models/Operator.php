<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{

    use HasFactory;
      // Specify the table name
      protected $table = 'operator_attendence';


    protected $guarded = [];

    public function createdBy()
    {
        return $this->hasOne('App\Models\User','id', 'created_by');
    }

    public function updeted_by()
    {
        return $this->hasOne('App\Models\User','id', 'updated_by');
    }
}
