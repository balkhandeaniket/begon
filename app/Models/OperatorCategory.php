<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperatorCategory extends Model
{

    use HasFactory;
      // Specify the table name
      protected $table = 'operator_category';


    protected $guarded = [];

    public function category()
    {
        return $this->hasOne('App\Models\Category','id', 'category_id');
    }

}
