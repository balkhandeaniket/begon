<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerService extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function serviceDueDate()
    {
        return $this->hasMany('App\Models\ServiceDueDate', 'service_id', 'id');
    }

    public function service()
    {
        return $this->hasOne('App\Models\Service','id', 'service_id');
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category','id', 'category_id');
    }

}
