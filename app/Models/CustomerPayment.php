<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class CustomerPayment extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function customerServices()
    {
        return $this->hasMany('App\Models\CustomerService', 'payment_id', 'id')->with('serviceDueDate');
    }

    public function paidTransaction()
    {
        return $this->hasMany('App\Models\PaidTransaction', 'payment_id', 'id');
    }

    public function paymentDisussion()
    {
        return $this->hasOne('App\Models\User','id', 'payment_disussion');
    }

    public function customerDetails()
    {
        return $this->hasOne('App\Models\Customer','id', 'customer_id');
    }

    public function customerPaymentAddress()
    {
        return $this->hasOne('App\Models\CustomerAddress','id', 'customer_address_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User','id', 'created_by');
    }

    public function updeted_by()
    {
        return $this->hasOne('App\Models\User','id', 'updated_by');
    }

    protected static function booted()
    {
        // Add a global scope to apply the is_cancelled="0" condition
        static::addGlobalScope('notCancelled', function ($builder) {
            $builder->where('is_cancelled', '=', "0")->where('is_gst_module','0');
        });
    }
}
