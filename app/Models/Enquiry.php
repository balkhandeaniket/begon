<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    use HasFactory;

    protected $guarded = [];
    // protected $fillable = [
    //     'first_name',
    //     'middle_name',
    //     'last_name',
    //     'contact',
    //     'email',
    //     'services',
    //     'contact_no1',
    //     'contact_no2',
    //     'locality',
    //     'address_line1',
    //     'address_line2',
    //     'landmark',
    //     'road',
    //     'near',
    //     'opposite',
    //     'pin_code',
    //     'area_sq_ft','booked_by','entire','value_of_contact','payment_received','balance_payment','payment_mode'
    //   // Add '_token' to allow mass assignment
    // ];

    public function createdBy()
    {
        return $this->hasOne('App\Models\User','id', 'created_by');
    }

    public function updeted_by()
    {
        return $this->hasOne('App\Models\User','id', 'updated_by');
    }
}
