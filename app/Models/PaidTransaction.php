<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaidTransaction extends Model
{
    use HasFactory;
    protected $table = 'paid_transactions';
    protected $guarded = [];
}
