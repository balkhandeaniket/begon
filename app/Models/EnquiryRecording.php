<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EnquiryRecording extends Model
{
    use HasFactory;
    protected $guarded = [];
}
