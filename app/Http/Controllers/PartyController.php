<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\CustomerPayment;
use App\Models\PaidTransaction;
use App\Models\CustomerService;
use App\Models\ServiceDueDate;
use App\Models\Service;
use App\Models\Settings;
use App\Models\TermsAndConditions;
use App\Models\User;
use DataTables;
use Auth;
use DB;
use Mpdf\Mpdf;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Symfony\Component\HttpFoundation\StreamedResponse;

class PartyController extends Controller
{
    //
    public function index(Request $request)
    {
        return view('party.index');
    }

    function getServerSide(Request $request)
    {

        $customers = Customer::select([
            'customers.id',
            'customers.first_name',
            'customer_addresses.contact',
        ])
        ->join('customer_addresses', 'customer_addresses.customer_id', 'customers.id')->orderBy('customers.id','DESC');

        return DataTables::of($customers)

            ->addColumn('amount', function ($customers) {
                $payment = CustomerPayment::select(DB::raw('sum(total_amount) as amount'),DB::raw('GROUP_CONCAT(id) as ids'))->where('customer_id',$customers->id)->first();

                $paid_cal = 0;
                if (!empty($payment->ids)) {
                    $paidTransaction = PaidTransaction::select([DB::raw('sum(paid_amount) as paid_amount')])->where([['is_delete','0']])->whereRaw('payment_id in ('.$payment->ids.')')->first();

                    $paid_cal = $paidTransaction->paid_amount ?? 0;
                }
               
                $cal = $payment->amount - $paid_cal;
                if ($cal > 0) {
                    return "<span class='badge badge-danger'>".$cal."</span>";
                }else{
                    return '';
                }
            })
            ->addColumn('contact', function ($customers) {
                return $customers->contact;
            })
            ->addColumn('name', function ($customers) {
                return '<a href="javascript:void(0)" style="cursor:pointer;colour:sky-blue;" title="View Details"  onclick="showPartyDetails(' . $customers->id . ');">' . $customers->first_name . '</a>';
            })
            ->filterColumn('contact', function ($query, $keyword) {
                $query->whereRaw("customer_addresses.contact LIKE ?", ["%{$keyword}%"]);
            })
            ->rawColumns(['amount', 'name','contact'])
            ->make(true);
    }

    function getPartyDetails(Request $request)
    {
        $customers = Customer::where('customers.id', $request->customer_id)->first();

        $customersAddress = CustomerAddress::where('customer_id', $customers->id)->get();

        $payment = CustomerPayment::where('is_cancelled','0')->where('is_bill','1')->where('customer_id',$customers->id)->orderBy('id','DESC')->get();

        return response()->json([
            'html' => view('party.party_details')->with([
                'customers' => $customers,
                'customersAddress' => $customersAddress,
                'payment' => $payment,
            ])->render(),
        ]);
    }

    function deletePaymentTrans(Request $request){

        $data = [
            'is_delete' => "1",
        ];
        PaidTransaction::where('id', $request->trans_id)->update($data);

        $trasn_details= PaidTransaction::where('id', $request->trans_id)->first();

        $paidTransaction = PaidTransaction::select([DB::raw('sum(paid_amount) as paid_amount')])->where([['payment_id',$trasn_details->payment_id],['is_delete','0']])->first();

        $data1 = [
            'paid_amount' => $paidTransaction->paid_amount ?? '0',
            'updated_by' => Auth::user()->id,
        ];
        CustomerPayment::where('id', $trasn_details->payment_id)->update($data1);

        return true;
    }

    public function paymentPdf($tran_id) {

        $trasn_details= PaidTransaction::where('id', $tran_id)->first();

        $getCustomerPayment = CustomerPayment::with([
            'customerDetails',
            'customerServices',
            'paymentDisussion:id,name',
            'createdBy:id,name',
            'updeted_by:id,name',
        ])
        ->where('id', $trasn_details->payment_id)
        ->first();

        $getCustomer = Customer::find($getCustomerPayment->customer_id);


        $isContractAmountShow = CustomerService::select(['contract'])->where('payment_id', $getCustomerPayment->id)->whereNotNull('contract')->first();
        $isContractShow = "1";
        if (empty($isContractAmountShow->contract)) {
            $isContractShow = "0";
        }
        $getSetting = Settings::latest()->first();
        $termAndCondition = TermsAndConditions::get();

        $getCustomerAddress = CustomerAddress::find(
            $getCustomerPayment->customer_address_id
        );

        $qrCode = QrCode::size('150')->generate("upi://pay?pa=begonpest03control@okaxis&cu=INR&am=" . $getCustomerPayment->customerServices->sum('gst_amount'));
      
        $data = [
            'title' => 'Welcome to My PDF',
            'content' => 'This is the content of my PDF file.',
            // 'id'=>$id
            'customerDetails' => $getCustomer,
            'customerAddress' => $getCustomerAddress,
            'customerPayment' => $getCustomerPayment,
            'settings' => $getSetting,
            'invoice_type' => '2',
            'termAndCondition' => $termAndCondition,
            'qrCode' => $qrCode,
            'isContractShow' => $isContractShow,
            'trasn_details' => $trasn_details
        ];

        $mpdf = new Mpdf();
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            // 'default_font_size' => 18, // Set the default font size
        ]);
        
        $mpdf->WriteHTML(view('customer.payment_receipt', $data));
       
        $filename = 'payemnt_receipt.pdf';
        $mpdf->Output($filename, 'I'); // 'D' sends the file inline to the browser
        exit();
    }

}
