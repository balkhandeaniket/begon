<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Enquiry;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $enquiry = new Enquiry;
        $total =   $enquiry->select('id')->count();
        $EnquiryOpen =   $enquiry->select('id')->where('status','Open')->count();
        $EnquiryClose =   $enquiry->select('id')->where('status','Close ')->count();
        $EnquiryCustomer =   $enquiry->select('id')->where('status','Converted')->count();
        //dd($EnquiryCustomer);
        $EnquiryNew =   $enquiry->select('id')->where('type','New')->count();
        $EnquiryRenewal =   $enquiry->select('id')->where('type','Renewal')->count();
        $EnquirySociety =   $enquiry->select('id')->where('type','Society')->count();
        return view('home')->with(['totalEnquiry' => $total,'EnquiryOpen'=>$EnquiryOpen,'EnquiryClose'=>$EnquiryClose,'EnquiryCustomer'=>$EnquiryCustomer,'EnquiryNew'=>$EnquiryNew,'EnquiryRenewal'=>$EnquiryRenewal,'EnquirySociety'=>$EnquirySociety]);
    }

    // use App\Http\Controllers\DashboardController;

// Route::middleware(['auth'])->group(function () {

//     Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');


//     // Add more routes as needed

// });

// Route::get('/', function () {
//     return view('auth.login');
// })->middleware(['auth'])->name('dashboard');

//require __DIR__.'/auth.php';


//Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

}
