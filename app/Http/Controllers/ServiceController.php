<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Models\Category;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getService = Service::select('services.*', 'categories.title as category_name')
    ->leftJoin('categories', 'services.category_id', '=', 'categories.id')
    ->get();

        return view('service.index')->with('getService', $getService);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categoryList = Category::where([['status','Active'],['type','Service']])->orderBy('title','asc')->get();

        $data = [
            "action_name" => "Create Service",
            "action" => "/service-add-action",
            "categoryList"=>$categoryList

        ];
        return view('service.create')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // Validate the request data
        // $validatedData = $request->validate([
        //     'title' => 'required',
        //     'status' => 'required',
        //     'category_id' =>"required",
        // ]);

         $validatedData =  array(
            'title' =>  $request->title,
            'status' =>  $request->status,
            'category_id' => $request->category_id,
            'hsn' => $request->hsn,
            'remark' => $request->remark,
         );

        Service::create($validatedData);
        // Optionally, you can return a response or redirect to another page
        return redirect('/service')->with('success', 'Data has been stored successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $categoryList = Category::where([['status','Active'],['type','Service']])->orderBy('title','asc')->get();

        $getService = Service::find($request->id);
        $data = [
            "action_name" => "Edit Service",
            "action" => "/service-update-action/".$request->id,
            "categoryList"=>$categoryList

        ];
        return view('service.create')->with('getService', $getService)->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validatedData =  array(
            'title' =>  $request->title,
            'status' =>  $request->status,
            'category_id' => $request->category_id,
            'hsn' => $request->hsn,
            'remark' => $request->remark,
         );
        // Find the service by ID
        $service = Service::findOrFail($request->id);

        // Update the service with the validated data
        $service->update($validatedData);

        // Optionally, you can return a response or redirect to another page
        return redirect('/service')->with('success', 'Service updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }
}
