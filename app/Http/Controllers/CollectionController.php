<?php

namespace App\Http\Controllers;

use App\Models\CustomerPayment;
use App\Models\Customer;
use App\Models\CustomerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use DataTables;
use Auth;
use DB;

class CollectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $financeYear = CustomerPayment::where('is_cancelled','0')
        ->where('is_bill','1')->groupBy('finance_year')->pluck('finance_year');

        return view('collection.index')->with('financeYear',$financeYear);
    }


    function getServerSide(Request $request)
    {
        $payment = CustomerPayment::select([
            'customer_payments.*','customers.first_name','customer_addresses.address_line1','customer_addresses.contact'
        // ,DB::raw('(select sum(amount) as amount from customer_services where payment_id=customer_payments.id limit 1) as amount')
        ,DB::raw('(select sum(gst_amount) as gst_amount from customer_services where payment_id=customer_payments.id limit 1) as gst_amount')
        // ,DB::raw('(select sum(gst_cal_amount) as gst_cal_amount from customer_services where payment_id=customer_payments.id limit 1) as gst_cal_amount')
        ,DB::raw('(select GROUP_CONCAT(contract_from) as contract_from from customer_services where payment_id=customer_payments.id limit 1) as contract_from')
        ,DB::raw('(select GROUP_CONCAT(contract_to) as contract_to from customer_services where payment_id=customer_payments.id limit 1) as contract_to')
        ,DB::raw('(select GROUP_CONCAT(service_id) as service_id from customer_services where payment_id=customer_payments.id limit 1) as service_id')
       ])
        ->join('customers','customers.id','customer_payments.customer_id')
        ->join('customer_addresses','customer_payments.customer_address_id','customer_addresses.id')->where('is_bill','1');

        if (!empty($request->finance_year) && $request->finance_year !="All") {
            $payment = $payment->where('finance_year', $request->finance_year);
        }

        if (!empty($request->from_date)) {
            $payment = $payment->where('invoice_date','>=', $request->from_date);
        }

        if (!empty($request->to_date)) {
            $payment = $payment->where('invoice_date','<=', $request->to_date);
        }

        if (!empty($request->from_date) && !empty($request->to_date)) {
            $payment = $payment->whereBetween('invoice_date', [$request->from_date,$request->to_date]);
        }
        $payment = $payment->orderBy('id','DESC');
        //$i =1;
        return DataTables::of($payment)
        // ->addColumn('sr_no', function ($payment) use (&$i)  {
        //     return $i++;

        // })
        ->addColumn('name', function ($payment) {
            return '<a href="/customer-show/'.$payment->customer_id.'">'.$payment->first_name.'</a>';
        })
        // ->addColumn('invoice_date', function ($payment) {
        //    // return $payment->invoice_no.'<br>Date : '.date(('d/m/Y'), strtotime($payment->invoice_date));
        //     return $payment->invoice_no;
        // })
        ->addColumn('paymentDiscussion', function ($payment) {
            return $payment->createdBy->name ?? "";
        })
        ->addColumn('gst_amount', function ($payment) {
            return $payment->gst_amount ?? "0";
        })
        // ->addColumn('gst_cal_amount', function ($payment) {
        //     return $payment->gst_cal_amount ?? "0";
        // })
        // ->addColumn('amount', function ($payment) {
        //     return $payment->amount ?? "0";
        // })

        ->addColumn('invoice_no_finance', function ($payment) {
            return $payment->invoice_no;
        })
        ->addColumn('address', function ($payment) {
            return $payment->address_line1;
        })
        ->addColumn('mobile', function ($payment) {
            return $payment->contact;
        })
        ->addColumn('service_id', function ($payment) {
            return $payment->service_id;
        })
        ->addColumn('contract_from', function ($payment) {
            return 'Start : '.$payment->contract_from.'<br> End : '.$payment->contract_to;
        })
        // ->addColumn('contract_to', function ($payment) {
        //     return $payment->contract_to;
        // })
        ->addColumn('action', function ($payment) {
            return  '<button type="button" onclick="customerPaidAmount('.$payment->id.')"  style="margin-top:30px" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-default">Pay</button><br><a onclick="printInvoice('.$payment->id.',1)"
            style="margin:5px"
            class="btn btn-sm btn-update btn-info"
            href="javascript:void(0)"> Invoice</a>
            <a onclick="printInvoice('.$payment->id.',0)"
            style="margin:5px"
            class="btn btn-sm btn-update btn-info"
            href="javascript:void(0)"> GST Invoice</a>
            <a onclick="printContractForm('.$payment->id.')"
            style="margin:5px"
            class="btn btn-sm btn-update btn-primary"
            href="javascript:void(0)">Contract</a>
           
            <a href="/customer-payment-update/'.$payment->customer_id.'/'.$payment->id.'" style="margin:5px" class="btn btn-sm btn-update btn-warning">Edit</a>
            <a onclick="deletePayment('.$payment->id.',1)"
            style="margin:5px"
            class="btn btn-sm btn-update btn-danger"
            href="javascript:void(0)"> Delete</a>
             <a onclick="deletePayment('.$payment->id.',2)"
            style="margin:5px"
            class="btn btn-sm btn-update btn-danger"
            href="javascript:void(0)"> Return / Cancelled</a>
           ';
        })
        ->filterColumn('name', function ($query, $keyword) {
            $query->whereRaw("customers.first_name LIKE ?", ["%{$keyword}%"]);
        })
        ->filterColumn('invoice_date', function ($query, $keyword) {
            $query->whereRaw("customer_payments.invoice_date LIKE ?", ["%{$keyword}%"]);
        })
        ->filterColumn('invoice_no_finance', function ($query, $keyword) {
            $query->whereRaw("customer_payments.invoice_no LIKE ?", ["%{$keyword}%"]);
        })
        ->filterColumn('address', function ($query, $keyword) {
            $query->whereRaw("customer_addresses.address_line1 LIKE ?", ["%{$keyword}%"]);
        })
        ->filterColumn('mobile', function ($query, $keyword) {
            $query->whereRaw("customer_addresses.contact LIKE ?", ["%{$keyword}%"]);
        })
        ->rawColumns(['name','created','gst_cal_amount','gst_amount','action','invoice_date','contract_from','invoice_no_finance'])
        ->make(true);
    }

    public function totalAmountCal(Request $request)
    {
        $customerPaidAmount = CustomerPayment::select([DB::raw('sum(paid_amount) as paid_amount')])->where('is_bill','1');
        if (!empty($request->from_date) && !empty($request->to_date)) {
            $customerPaidAmount = $customerPaidAmount->whereBetween('invoice_date', [$request->from_date,$request->to_date]);
        }
        if (!empty($request->finance_year) && $request->finance_year !="All") {
            $customerPaidAmount = $customerPaidAmount->where('finance_year', $request->finance_year);
        }
        if (!empty($request->from_date)) {
            $customerPaidAmount = $customerPaidAmount->where('invoice_date','>=', $request->from_date);
        }
        if (!empty($request->to_date)) {
            $customerPaidAmount = $customerPaidAmount->where('invoice_date','<=', $request->to_date);
        }

        $customerPaidAmount = $customerPaidAmount->where('is_cancelled','0')
        ->where('is_return','0')
        ->where('is_bill','1')->first();

        $customerServiceAmount = CustomerService::select([DB::raw('sum(amount) as total_amount'),DB::raw('sum(gst_cal_amount) as total_gst_cal_amount'),DB::raw('sum(gst_amount) as total_gst_amount')])
        ->join('customer_payments','customer_payments.id','customer_services.payment_id');
        if (!empty($request->from_date) && !empty($request->to_date)) {
            $customerServiceAmount = $customerServiceAmount->whereBetween('invoice_date', [$request->from_date,$request->to_date]);
        }
        if (!empty($request->finance_year) && $request->finance_year !="All") {
            $customerServiceAmount = $customerServiceAmount->where('finance_year', $request->finance_year);
        }
        if (!empty($request->from_date)) {
            $customerServiceAmount = $customerServiceAmount->where('invoice_date','>=', $request->from_date);
        }
        if (!empty($request->to_date)) {
            $customerServiceAmount = $customerServiceAmount->where('invoice_date','<=', $request->to_date);
        }
        $customerServiceAmount = $customerServiceAmount->where('status','1')->first();

        return response()->json(['paid_amount'=>$customerPaidAmount->paid_amount,'total_amount'=>$customerServiceAmount->total_amount,
        'total_gst_cal_amount'=>$customerServiceAmount->total_gst_cal_amount,
        'total_gst_amount'=>$customerServiceAmount->total_gst_amount]);
    }
}
