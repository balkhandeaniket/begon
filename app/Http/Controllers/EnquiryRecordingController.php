<?php

namespace App\Http\Controllers;

use App\Models\EnquiryRecording;
use Illuminate\Http\Request;

class EnquiryRecordingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EnquiryRecording  $enquiryRecording
     * @return \Illuminate\Http\Response
     */
    public function show(EnquiryRecording $enquiryRecording)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EnquiryRecording  $enquiryRecording
     * @return \Illuminate\Http\Response
     */
    public function edit(EnquiryRecording $enquiryRecording)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EnquiryRecording  $enquiryRecording
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EnquiryRecording $enquiryRecording)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EnquiryRecording  $enquiryRecording
     * @return \Illuminate\Http\Response
     */
    public function destroy(EnquiryRecording $enquiryRecording)
    {
        //
    }
}
