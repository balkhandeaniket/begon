<?php

namespace App\Http\Controllers;


use App\Models\ServiceDueDate;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Models\User;
use DataTables;
use Auth;

class ServiceDueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('servicedue.index');
    }

    function getServerSide(Request $request)
    {
        $servicedue = ServiceDueDate::select(['service_due_dates.id','service_due_dates.service_id','service_due_dates.service_due','service_due_dates.is_completed','services.title','customers.first_name','customer_payments.invoice_no','customer_payments.customer_id','customer_addresses.address_line1','customer_addresses.locality','customer_addresses.landmark','customer_addresses.pin_code','customer_payments.id as paymentId'])
        ->join('customer_services','customer_services.id','service_due_dates.service_id')
        ->join('services','services.id','customer_services.service_id')
        ->join('customer_payments','customer_payments.id','customer_services.payment_id')
        ->join('customer_addresses','customer_addresses.id','customer_payments.customer_address_id')
        ->join('customers','customers.id','customer_payments.customer_id')
        ->where('customer_payments.is_cancelled','0');
        return DataTables::of($servicedue)
        ->addColumn('name', function ($servicedue) {
            return '<a href="/customer-show/'.$servicedue->customer_id.'">'.$servicedue->first_name.'</a>';
        })
        ->addColumn('address', function ($servicedue) {
            return $servicedue->address_line1.' '.$servicedue->locality.' '.$servicedue->landmark.' '.$servicedue->pin_code;
        })
        ->addColumn('due_date', function ($servicedue) {
            $due_date = date(('d M Y'), strtotime($servicedue->service_due));
            $hidden = '<span style="display:none">'.$servicedue->service_due.'</span>';
            if ($servicedue->is_completed=="1") {
                return $hidden.'<button class="btn btn-sm btn-success" onclick="serviceDue('.$servicedue->id.')">'.$due_date.'</label>';
            }else if(date('Y-m-d') < $servicedue->service_due){
                return $hidden.'<button class="btn btn-sm btn-warning" onclick="serviceDue('.$servicedue->id.')">'.$due_date.'</label>';
            }else{
                return $hidden.'<button class="btn btn-sm btn-danger" onclick="serviceDue('.$servicedue->id.')">'.$due_date.'</label>';
            }
        })
        ->addColumn('action', function ($servicedue) {
            return  '<a onclick="printContractForm('.$servicedue->paymentId.')"
            style="float: right;margin:5px"
            class="btn btn-sm btn-update btn-primary"
            href="javascript:void(0)">Contract</a>
        <a onclick="printInvoice('.$servicedue->paymentId.',1)"
            style="float: right;margin:5px"
            class="btn btn-sm btn-update btn-warning"
            href="javascript:void(0)"> Invoice</a>
        <a onclick="printInvoice('.$servicedue->paymentId.',0)"
            style="float: right;margin:5px"
            class="btn btn-sm btn-update btn-warning"
            href="javascript:void(0)"> GST Invoice</a>';
        })
        ->rawColumns(['name','due_date','address','action'])
        ->make(true);
    }

    public function updateStatus(Request $request)
    {
        $serviceDue = ServiceDueDate::find($request->id);

        if ($serviceDue->is_completed == "1") {
            $status = "0";
        } else {
            $status = "1";
        }

        // Toggle the value of is_completed
        $serviceDue->update(['is_completed' => $status]);
     
        return true;
    }
}
