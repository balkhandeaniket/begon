<?php

namespace App\Http\Controllers;

use App\Models\Ip;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use DataTables;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('user.index');
    }

    function getServerSide(Request $request)
    {
        $users = User::select('*')->with('userRole');
        return DataTables::of($users)
            ->addColumn('action', function ($users) {

                $btn = '';
                $btn .=
                    '<a class="btn btn-sm btn-update btn-warning" title="Edit" href="/user-edit/' .
                    $users->id .
                    '"> Edit</a>';
                // $btn .=
                //     '&nbsp; <a class="btn btn-sm btn-update btn-info" href="/user-show/' .
                //     $users->id .
                //     '"> View</a>';
                // $btn .=
                //     '&nbsp; <a class="btn btn-sm btn-update btn-info" onclick="updateStatus(' . $users->id . ')" title="Change Password"><i class="fa fa-key" aria-hidden="true"></i></a>';
                return $btn;

            })
            ->addColumn('name', function ($users) {
                return $users->name;
            })
            ->addColumn('role', function ($users) {
                return $users->userRole->title ?? "";
            })
            ->addColumn('contact', function ($users) {
                return $users->contact;
            })
            ->addColumn('created', function ($users) {
                return date('Y M d', strtotime($users->created_at));
            })
            ->addColumn('status', function ($users) {
                if ($users->status == "1") {
                    $users->status = '<span class="btn btn-success btn-sm"> Active </span>';
                } else {
                    $users->status = ' <span class="btn btn-danger btn-sm"> Inactive </span>';
                }
                return $users->status;
            })
            ->addColumn('email', function ($users) {
                return $users->email;
            })
            ->rawColumns(['action', 'name', 'contact', 'email', 'status', 'role'])
            ->make(true);
    }

    public function create()
    {
        $user_action = "Create";
        $roles = Role::get();
        $user_action_url = "/user-add";
        return view('user.create')->with('user_action', $user_action)->with('user_action_url', $user_action_url)->with('roles', $roles);
    }

    function userIpSave(Request $request){
       
        // $user = Ip::firstOrCreate(
        //     ['ip' => $request->ip] // Attributes to search for
        // );
        return true;
    }

    public function store(Request $request)
    {
        $user = User::where('email',$request->email)->first();

        if (empty($user)) {
            $user = new User();
            $user->name = $request->first_name;
            $user->role_id = $request->role_id;
            $user->status = $request->status;
            $user->email = $request->email;
            $user->contact = $request->contact;
            $user->password = bcrypt($request->input('password'));
            $user->save();
        }
       
        // Redirect back or to a specific route after processing the form
        return redirect()->route('user.index')->with('success', 'User saved successfully');
    }
    public function edit(Request $request)
    {
        $getUser = User::find($request->id);

        $user_action = "Update";

        $user_action_url = "/user-update-action/" . $getUser->id;
        $roles = Role::get();
        return view('user.create')->with('user_action', $user_action)->with('getUser', $getUser)->with('user_action_url', $user_action_url)->with('roles', $roles);
    }
    
    public function update(Request $request)
    {
        $getUser = User::where('email',$request->email)->where('id','!=',$request->id)->first();
        // Find the service by ID
        $user = User::find($request->id);
        if (isset($user) && empty($getUser)) {
            $user->name = $request->first_name;
            $user->role_id = $request->role_id;
            $user->email = $request->email;
            $user->contact = $request->contact;
            $user->status = $request->status;
            if (!empty($request->password)) {
                $user->password = bcrypt($request->password);
            }
            $user->save();
        }
        // Optionally, you can return a response or redirect to another page
        return redirect('/user')->with('success', 'Data has been updeted successfully!');
    }


    public function updateStatus(Request $request)
    {
        $user = User::find($request->id);

        return response()->json([
            'html' => view('user.change_passwor')->with('user', $user)->render(),
        ]);
    }
}
