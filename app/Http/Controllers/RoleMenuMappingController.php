<?php

namespace App\Http\Controllers;

use App\Models\RoleMenuMapping;
use Illuminate\Http\Request;

class RoleMenuMappingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RoleMenuMapping  $roleMenuMapping
     * @return \Illuminate\Http\Response
     */
    public function show(RoleMenuMapping $roleMenuMapping)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RoleMenuMapping  $roleMenuMapping
     * @return \Illuminate\Http\Response
     */
    public function edit(RoleMenuMapping $roleMenuMapping)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RoleMenuMapping  $roleMenuMapping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RoleMenuMapping $roleMenuMapping)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RoleMenuMapping  $roleMenuMapping
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoleMenuMapping $roleMenuMapping)
    {
        //
    }
}
