<?php

namespace App\Http\Controllers;

use App\Models\CustomerAddress;
use Illuminate\Http\Request;

class CustomerAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data2 = array(
            'customer_id' => $request->id,
            'contact' => $request->contact ?? null,
            'email' => $request->email ?? null,
            'contact_no1' => $request->contact_no1 ?? null,
            'contact_no2' => $request->contact_no2 ?? null,
            'locality' => $request->locality ?? null,
            'address_line1' => $request->address_line1 ?? null,
            'address_line2' => $request->address_line2 ?? null,
            'landmark' => $request->landmark ?? null,
            'road' => $request->road ?? null,
            'street' => $request->street ?? null,
            'near' => $request->near ?? null,
            'opposite' => $request->opposite ?? null,
            'pin_code' => $request->pin_code ?? null,
            'created_at' => Auth::user()->id,
            'updated_at' => Auth::user()->id,
        );
        //Find customer addresses
        $grtCustomerAddress = CustomerAddress::where('customer_id', $request->id)->first();

        $grtCustomerAddress->update($data2);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CustomerAddress  $customerAddress
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerAddress $customerAddress)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CustomerAddress  $customerAddress
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerAddress $customerAddress)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CustomerAddress  $customerAddress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerAddress $customerAddress)
    {
        $data2 = array(
            'customer_id' => $request->id,
            'contact' => $request->contact ?? null,
            'email' => $request->email ?? null,
            'contact_no1' => $request->contact_no1 ?? null,
            'contact_no2' => $request->contact_no2 ?? null,
            'locality' => $request->locality ?? null,
            'address_line1' => $request->address_line1 ?? null,
            'address_line2' => $request->address_line2 ?? null,
            'landmark' => $request->landmark ?? null,
            'road' => $request->road ?? null,
            'street' => $request->street ?? null,
            'near' => $request->near ?? null,
            'opposite' => $request->opposite ?? null,
            'pin_code' => $request->pin_code ?? null,
            'created_at' => Auth::user()->id,
            'updated_at' => Auth::user()->id,
        );
        //Find customer addresses
        $grtCustomerAddress = CustomerAddress::where('customer_id', $request->id)->first();

        $grtCustomerAddress->update($data2);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CustomerAddress  $customerAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerAddress $customerAddress)
    {
        //
    }
}
