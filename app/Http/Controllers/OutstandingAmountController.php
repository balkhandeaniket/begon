<?php

namespace App\Http\Controllers;

use App\Models\CustomerPayment;
use App\Models\Customer;
use App\Models\CustomerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use DataTables;
use Auth;
use DB;

class OutstandingAmountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $financeYear = CustomerPayment::where('is_cancelled','0')->where('is_bill','1')->groupBy('finance_year')->pluck('finance_year');

        return view('outstanding.index')->with('financeYear',$financeYear);
    }


    function getServerSide(Request $request)
    {
        $payment = CustomerPayment::select([
            'customer_payments.*','customers.first_name','customer_addresses.address_line1','customer_addresses.contact'
        ,DB::raw('(select sum(amount) as amount from customer_services where payment_id=customer_payments.id limit 1) as amount')
        ,DB::raw('(select sum(gst_amount) as gst_amount from customer_services where payment_id=customer_payments.id limit 1) as gst_amount')
        ,DB::raw('(select sum(gst_cal_amount) as gst_cal_amount from customer_services where payment_id=customer_payments.id limit 1) as gst_cal_amount')
        ,DB::raw('(select sum(gst_amount) as gst_amount from customer_services where payment_id=customer_payments.id limit 1) - (customer_payments.paid_amount) as balance')
       ])
        ->join('customers','customers.id','customer_payments.customer_id')
        ->join('customer_addresses','customer_payments.customer_address_id','customer_addresses.id')
        ->where('is_cancelled','0')->where('is_bill','1');
       // ->havingRaw('balance > 0');
        if (!empty($request->finance_year) && $request->finance_year !="All") {
            $payment = $payment->where('finance_year', $request->finance_year);
        }
     
        if (!empty($request->from_date)) {
            $payment = $payment->where('invoice_date','>=', $request->from_date);
        }

        if (!empty($request->to_date)) {
            $payment = $payment->where('invoice_date','<=', $request->to_date);
        }

        if (!empty($request->from_date) && !empty($request->to_date)) {
            $payment = $payment->whereBetween('invoice_date', [$request->from_date,$request->to_date]);
        }
        $payment->whereRaw('customer_payments.total_amount > customer_payments.paid_amount');
       // $payment->whereRaw('(SELECT SUM(gst_amount) - customer_payments.paid_amount FROM customer_services WHERE payment_id = customer_payments.id) > 0');
        //$payment = $payment->havingRaw('balance > 0');
        $payment = $payment->orderBy('id','DESC');
        return DataTables::of($payment)
        ->addColumn('name', function ($payment) {
            return '<a href="/customer-show/'.$payment->customer_id.'">'.$payment->first_name.'</a>';
        })
        ->addColumn('invoice_date', function ($payment) {
            return date(('d/m/Y'), strtotime($payment->invoice_date));
        })
        ->addColumn('paymentDiscussion', function ($payment) {
            return $payment->createdBy->name ?? "";
        })
        ->addColumn('amount', function ($payment) {
            return $payment->amount ?? "";
        })
        ->addColumn('gst_cal_amount', function ($payment) {
            return $payment->gst_cal_amount ?? "";
        })
        ->addColumn('balance', function ($payment) {
            return $payment->balance ?? "";
        })
        ->addColumn('gst_amount', function ($payment) {
            return $payment->gst_amount ?? "";
        })
        ->addColumn('invoice_no_finance', function ($payment) {
            return $payment->invoice_no;
        })
        ->addColumn('address', function ($payment) {
            return $payment->address_line1;
        })
        ->addColumn('mobile', function ($payment) {
            return $payment->contact;
        })
        ->addColumn('action', function ($payment) {
            return  '<button type="button" onclick="customerPaidAmount('.$payment->id.')"  style="margin-top:30px" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-default">Pay</button><br>
            <a onclick="printInvoice('.$payment->id.',1)"
            style="margin:5px"
            class="btn btn-sm btn-update btn-warning"
            href="javascript:void(0)"> Invoice</a>
        <a onclick="printInvoice('.$payment->id.',0)"
            style="margin:5px"
            class="btn btn-sm btn-update btn-warning"
            href="javascript:void(0)"> GST Invoice</a>
            <a onclick="printContractForm('.$payment->id.')"
            style="margin:5px"
            class="btn btn-sm btn-update btn-primary"
            href="javascript:void(0)">Contract</a>
            <a href="/customer-payment-update/'.$payment->customer_id.'/'.$payment->id.'" style="margin:5px" class="btn btn-sm btn-update btn-danger">Edit</a>';
        })
        ->filterColumn('name', function ($query, $keyword) {
            $query->whereRaw("customers.first_name LIKE ?", ["%{$keyword}%"]);
        })
        ->filterColumn('invoice_date', function ($query, $keyword) {
            $query->whereRaw("customer_payments.invoice_date LIKE ?", ["%{$keyword}%"]);
        })
        ->filterColumn('invoice_no_finance', function ($query, $keyword) {
            $query->whereRaw("customer_payments.invoice_no LIKE ?", ["%{$keyword}%"]);
        })
        ->filterColumn('address', function ($query, $keyword) {
            $query->whereRaw("customer_addresses.address_line1 LIKE ?", ["%{$keyword}%"]);
        })
        ->filterColumn('mobile', function ($query, $keyword) {
            $query->whereRaw("customer_addresses.contact LIKE ?", ["%{$keyword}%"]);
        })
        ->rawColumns(['name','created','invoice_date','amount','gst_cal_amount','gst_amount','balance','action','invoice_no_finance'])
        ->make(true);
    }

    public function totalAmountCal(Request $request)
    {   
        $customerPaidAmount = CustomerService::select([DB::raw('(select paid_amount from customer_payments where id=customer_services.payment_id limit 1) as paid_amount'),'payment_id',DB::raw('sum(amount) as amount'),DB::raw('sum(gst_cal_amount) as gst_cal_amount'),DB::raw('sum(gst_amount) as gst_amount')])
        ->join('customer_payments','customer_payments.id','customer_services.payment_id')
        ->where('customer_payments.is_cancelled','0')
        ->where('is_return','0')
        ->groupBy('payment_id')->havingRaw('gst_amount > paid_amount')->get();

        $paid_amount = 0;
        $total_amount = 0;
        $total_gst_cal_amount = 0;
        $total_gst_amount = 0;
        $outStandingAmount = 0;
        foreach ($customerPaidAmount as $key => $value) {
           
            $total_amount += $value->amount;
            $total_gst_cal_amount += $value->gst_cal_amount;
            $total_gst_amount += $value->gst_amount;
            $paid_amount += $value->paid_amount;
            $outStandingAmount += $value->gst_amount - $value->paid_amount;
        }

        return response()->json([
        'paid_amount'=>$paid_amount,
        'total_amount'=>$total_amount,
        'total_gst_cal_amount'=>$total_gst_cal_amount,
        'total_gst_amount'=>$total_gst_amount,
        'outStandingAmount' => $outStandingAmount]);
    }
}
