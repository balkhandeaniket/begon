<?php

namespace App\Http\Controllers;

use App\Models\Enquiry;
use App\Models\Service;
use App\Models\User;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\EnquiryRecording;
use Illuminate\Broadcasting\Broadcasters\NullBroadcaster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use DataTables;
use Auth;

class EnquiryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $deviceInfo = $request->header('User-Agent');
        $getUsers = User::get();
        return view('enquiry.index')->with('getUsers',$getUsers);
    }

    public function show(Request $request)
    {
        $getEnquiry = Enquiry::find($request->id);
        $services = Service::select('id', 'title')->where('status', 'Active')->get();
        $getUsers = User::where([['status', '1']])->get();
        $getRecordings  = EnquiryRecording::where([['enquiry_id',$request->id]])->orderBy('id','desc')->get();
        return view('enquiry.view')->with(['getEnquiry' => $getEnquiry, 'services' => $services, 'getUsers' => $getUsers,"getRecordings"=>$getRecordings]);
    }
    function getServerSide(Request $request)
    {
        $enquirys = Enquiry::select(['enquiries.*']);
        if (!empty($request->status)) {
            $enquirys = $enquirys->where('status', $request->status);
        }
        if (!empty($request->type)) {
            $enquirys = $enquirys->where('type', $request->type);
        }
        if (!empty($request->created_by)) {
            $enquirys = $enquirys->where('created_by', $request->created_by);
        }
        if (!empty($request->from_date)) {
            $enquirys = $enquirys->whereRaw('left(created_at,10) >="'.$request->from_date.'"');
        }
        if (!empty($request->to_date)) {
            $enquirys = $enquirys->whereRaw('left(created_at,10) <="'.$request->to_date.'"');
        }
        if (!empty($request->date)) {
            $enquirys = $enquirys->whereRaw('left(appointment_date,10) = "'.$request->date.'"');
        }
        $enquirys = $enquirys->orderBy('created_at','DESC');
        return DataTables::of($enquirys)
            ->addColumn('name_concat', function ($enquiry) {
                return $enquiry->first_name . ' ' . $enquiry->middle_name . ' ' . $enquiry->last_name;
            })
            ->addColumn('appointment_date', function ($enquiry) {
                $appointment_date = "";
                if(!empty($enquiry->appointment_date)){
                    $appointment_date = date('d M Y H:i:s',strtotime($enquiry->appointment_date));;
                }
                return $appointment_date;
            })
            ->addColumn('created_by', function ($enquiry) {
              
                return $enquiry->createdBy->name ?? "";
            })
            ->addColumn('status_flag', function ($enquiry) {

                if ($enquiry->status == "Open") {
                    return '<button type="button" onclick="updateStatus(' . $enquiry->id . ')" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modal-default">
                   ' . $enquiry->status . '
                  </button>';
                }
                if ($enquiry->status == "Close") {
                    return '<button type="button" onclick="updateStatus(' . $enquiry->id . ')" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-default">
                   ' . $enquiry->status . '
                  </button>';
                }

                if ($enquiry->status == "Converted") {
                    return '<span class="btn btn-info btn-sm">  ' . $enquiry->status . '  </span>';
                }
                return $enquiry->status;
            })
            ->addColumn('action', function ($enquiry) {
                $btn = "";
                if ($enquiry->status != 'Converted') {
                    $btn .= '<a class="btn btn-sm btn-update btn-warning" href="enquiry-edit/' . $enquiry->id . '" title="Update"> Edit</a>';
                }
              $btn .= '&nbsp; <a class="btn btn-sm btn-update btn-info" href="/enquiry-show/' . $enquiry->id . '" title="  View Details"> View</a>';
                return $btn;
            })
            ->addColumn('created', function ($enquiry) {
                return date(('Y M d'), strtotime($enquiry->created_at));
            })
            ->rawColumns(['name_concat', 'status_flag','appointment_date','created_by','action'])
            ->make(true);
    }

    public function getEnquiry(Request $request)
    {

        $data = array();
        $data['enquiry_list'] = new Enquiry();
        if (!empty($request->status)) {
            $data['enquiry_list'] = $data['enquiry_list']->where('status', $request->status);
        }
        $data['enquiry_list'] = $data['enquiry_list']->get();

        return view('enquiry.enquiry_list_table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addEnquiry(Request $request)
    {
        $getUsers = User::where([['status', '1']])->get();

        $service = Service::select('id', 'title')->where('status', 'Active')->get();
        $enquiry_action = "Create";
        $enquiry_action_url = "/enquiry-add";
        $enquiry_type = $request->type;
        return view('enquiry.add_new_enquiry')->with('services', $service)->with('getUsers', $getUsers)->with('enquiry_action', $enquiry_action)->with('enquiry_action_url', $enquiry_action_url)->with('enquiry_type',$enquiry_type);
    }

    public function addEnquiryAction(Request $request)
    {
        $services = "";
        if (!empty($request->services)) {
            $services = implode(',', $request->services);
        }
        $appointment_date = null;
        if (!empty($request->appointment_date)) {
            $appointment_date = date('Y-m-d H:i:s',strtotime($request->appointment_date));
        }
        $data = array(
            'first_name' => $request->first_name ?? null,
            // 'middle_name' => $request->middle_name ?? null,
            // 'last_name' => $request->last_name ?? null,
            'contact' => $request->contact ?? null,
            'address_line1' => $request->address_line1 ?? null,
            'email' => $request->email ?? null,
            'services' => $services ?? null,
            // 'contact_no1' => $request->contact_no1 ?? null,
            // 'contact_no2' => $request->contact_no2 ?? null,
            // 'locality' => $request->locality ?? null,
            // 'address_line2' => $request->address_line2 ?? null,
            // 'landmark' => $request->landmark ?? null,
            // 'road' => $request->road ?? null,
            // 'street' => $request->street ?? null,
            // 'near' => $request->near ?? null,
            // 'opposite' => $request->opposite ?? null,
            // 'pin_code' => $request->pin_code ?? null,
            'area_sq_ft' => $request->area_sq_ft ?? null,
            'booked_by' => $request->booked_by ?? null,
            'entire' => $request->entire ?? null,
            'value_of_contact' => $request->value_of_contact ?? null,
            'payment_received' => $request->payment_received ?? null,
            'balance_payment' => $request->balance_payment ?? null,
            'payment_mode' => $request->payment_mode ?? null,
            'concern_person' => $request->concern_person ?? null,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
            'remark' => $request->remark ?? null,
            'type' => $request->type,
            'appointment_date' => $request->appointment_date,
        );
        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');

            // Generate a unique filename based on the enquiry ID
            $filename = 'enquiry_' . time() . '.' . $file->getClientOriginalExtension();

            // Specify the storage path
            $storagePath = 'public/recordings/';

            // Store the file in the specified storage path with the desired filename
            $file->storeAs($storagePath, $filename);

            // Update the $data array with the file path
            $data['attachment'] = $filename;
        } else {
            $data['attachment'] = null;
        }

        $request->validate([
            'first_name' => 'required',
        ]);
        // Check if a file is provided in the request


        // Create a new Enquiry model and save it to the database
        $last_id = Enquiry::create($data);

        return $last_id->id;
        //return redirect('/enquiry')->with('success', 'Data has been stored successfully!');

    }

    public function edit(Request $request)
    {

        $getUsers = User::where([['status', '1']])->get();
        $getEnquiry = Enquiry::find($request->id);

        $services = Service::select('id', 'title')->where('status', 'Active')->get();
        $enquiry_action = "Update";

        $enquiry_action_url = "/enquiry-update-action/" . $getEnquiry->id;
        $enquiry_type = $getEnquiry->type;

        return view('enquiry.add_new_enquiry')->with('getUsers', $getUsers)->with('services', $services)->with('enquiry_action', $enquiry_action)->with('getEnquiry', $getEnquiry)->with('enquiry_action_url', $enquiry_action_url)->with('enquiry_type',$enquiry_type);
    }

    public function update(Request $request)
    {
        $services = "";
        if (!empty($request->services)) {
            $services = implode(',', $request->services);
        }
        $appointment_date = null;
        if (!empty($request->appointment_date)) {
            $appointment_date = date('Y-m-d H:i:s',strtotime($request->appointment_date));
        }
        $data = array(
            'first_name' => $request->first_name ?? null,
            'contact' => $request->contact ?? null,
            'address_line1' => $request->address_line1 ?? null,
            'email' => $request->email ?? null,
            'services' => $services ?? null,
            // 'contact_no1' => $request->contact_no1 ?? null,
            // 'contact_no2' => $request->contact_no2 ?? null,
            // 'locality' => $request->locality ?? null,
            // 'address_line2' => $request->address_line2 ?? null,
            // 'landmark' => $request->landmark ?? null,
            // 'road' => $request->road ?? null,
            // 'street' => $request->street ?? null,
            // 'near' => $request->near ?? null,
            // 'opposite' => $request->opposite ?? null,
            // 'pin_code' => $request->pin_code ?? null,
            'area_sq_ft' => $request->area_sq_ft ?? null,
            'booked_by' => $request->booked_by ?? null,
            'entire' => $request->entire ?? null,
            'value_of_contact' => $request->value_of_contact ?? null,
            'payment_received' => $request->payment_received ?? null,
            'balance_payment' => $request->balance_payment ?? null,
            'payment_mode' => $request->payment_mode ?? null,
            'concern_person' => $request->concern_person ?? null,
            //'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
            'remark' => $request->remark ?? null,
            'type' => $request->type,
            'appointment_date' =>  $appointment_date,
        );

        // Handle file upload only if a file is present in the request
        if ($request->hasFile('attachment')) {
            // Handle file upload
            $fileData = $this->handleFileUpload($request);
            // Merge the file data into the existing $data array
            $data = array_merge($data, $fileData);
        }

        // Find the service by ID
        $enquiry = Enquiry::findOrFail($request->id);

        // Update the enquiry with the validated data
        $enquiry->update($data);

        return $request->id;
        // Optionally, you can return a response or redirect to another page
        //return redirect('/enquiry')->with('success', 'Data has been updated successfully!');
    }


    private function handleFileUpload(Request $request)
    {
        $data = [];

        if ($request->hasFile('attachment') && file_exists($request->file('attachment'))) {
            // Get the file from the request
            $file = $request->file('attachment');

            // Check if the file is valid
            if ($file->isValid()) {
                // Generate a unique filename based on the enquiry ID
                $filename = 'enquiry_' . time() . '.' . $file->getClientOriginalExtension();

                // Specify the storage path
                $storagePath = 'public/recordings/';

                // Check if the file already exists in the database
                $existingFilePath = Enquiry::where('id', $request->id)->value('attachment');

                if ($existingFilePath && Storage::exists($storagePath . $existingFilePath)) {
                    // Remove the existing file
                    Storage::delete($storagePath . $existingFilePath);
                }

                // Store the file in the specified storage path
                $file->storeAs($storagePath, $filename);

                // Update the $data array with the new file path
                $data['attachment'] = $filename;
            }
        } elseif ($request->id) {
            // During editing, if no new file is provided, retain the existing file path
            $data['attachment'] = Enquiry::where('id', $request->id)->value('attachment');
        }

        return $data;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */

    public function updateStatus(Request $request)
    {
        $getEnquiry = Enquiry::find($request->id);

        return response()->json([
            'html' => view('enquiry.status_update')->with('getEnquiry', $getEnquiry)->render(),
        ]);
    }

    public function updateStatusAction(Request $request)
    {
        $enquiry = Enquiry::find($request->id);

        if ($request->status == "Converted") {
            $customer_data = array(
                'enquiry_id' => $enquiry->id,
                'first_name' => $enquiry->first_name ?? null,
                'updeted_by' => Auth::user()->id,
                'remark' => $enquiry->remark ?? null,
            );

            // Create a new Enquiry model and save it to the database
            $customer = Customer::create($customer_data);

            $customer_address_data = array(
                'customer_id' => $customer->id ?? 0,
                'contact' => $enquiry->contact ?? null,
                'address_line1' => $enquiry->address_line1 ?? null,
                'email' => $enquiry->email ?? null,
                // 'contact_no1' => $enquiry->contact_no1 ?? null,
                // 'contact_no2' => $enquiry->contact_no2 ?? null,
                // 'locality' => $enquiry->locality ?? null,
                // 'address_line2' => $enquiry->address_line2 ?? null,
                // 'landmark' => $enquiry->landmark ?? null,
                // 'road' => $enquiry->road ?? null,
                // 'street' => $enquiry->street ?? null,
                // 'near' => $enquiry->near ?? null,
                // 'opposite' => $enquiry->opposite ?? null,
                // 'pin_code' => $enquiry->pin_code ?? null,
            );
            CustomerAddress::create($customer_address_data);

        }

        $appointment_date = null;
        if (!empty($request->appointment_date)) {
            $appointment_date = date('Y-m-d H:i:s',strtotime($request->appointment_date));
        }
        $data = array(
            "remark" => $request->remark ?? null,
            "status" => $request->status,
            'appointment_date' =>  $appointment_date,
        );
      
        $enquiry->update($data);
        return true;
    }

    function recordingUpload(Request $request)
    {
        $audioFile = $request->file('audio');
        $filename = time() . '_' . $audioFile->getClientOriginalName();

        // Store the file in the storage disk (configured in filesystems.php)
        $audioFile->storeAs('public/recordings/', $filename);

        // Save file details to the database
        EnquiryRecording::create(['enquiry_id'=>$request->enquiry_id,'file_name' => $filename]);

        return response()->json(['success' => true, 'message' => 'Audio file uploaded successfully.']);
    }


    function deleteRecording($id){

           $getRecording = EnquiryRecording::find($id);
           if(isset($getRecording)){
            // Specify the storage path
            $storagePath = 'public/recordings/'.$getRecording->file_name;

            if ( Storage::exists($storagePath)) {
                // Remove the existing file
                Storage::delete($storagePath);
                $getRecording->delete();
                return response()->json(['success' => true, 'message' => 'Audio file Deleted successfully.']);
            }else{
                return response()->json(['error' => true, 'message' => 'Audio file not found.']);
            }

        }else{
            return response()->json(['error' => true, 'message' => 'Record are not exist.']);
        }

    }

}
