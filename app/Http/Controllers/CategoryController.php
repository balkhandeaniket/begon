<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getCategory = Category::all();
        return view('category.index')->with('getCategory', $getCategory);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            "action_name" => "Create Category",
            "action" => "/category-add-action",
        ];
        return view('category.create')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // Validate the request data
       $validatedData = $request->validate([
            'is_expense' => 'required',
            'type' => 'required',
            'title' => 'required',
            'status' => 'required',
        ]);

        Category::create($validatedData);

        // Optionally, you can return a response or redirect to another page
        return redirect('/category')->with('success', 'Data has been stored successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $getCategory = Category::find($request->id);
        $data = [
            "action_name" => "Edit Category",
            "action" => "/category-update-action/".$request->id,
        ];
        return view('category.create')->with('getCategory', $getCategory)->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'is_expense' => 'required',
            'title' => 'required',
            'type' => 'required',
            'status'=> 'required',
        ]);

        // Find the service by ID
        $service = Category::findOrFail($request->id);

        // Update the service with the validated data
        $service->update($validatedData);

        // Optionally, you can return a response or redirect to another page
        return redirect('/category')->with('success', 'Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $service)
    {
        //
    }
}
