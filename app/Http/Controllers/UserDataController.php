<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\OperatorCategory;
use App\Models\OperatorData;
use Illuminate\Http\Request;

use App\Models\CustomerPayment;
use App\Models\Operator;
use App\Models\Service;
use App\Models\User;
use App\Models\Customer;
use App\Models\CustomerAddress;
use Illuminate\Broadcasting\Broadcasters\NullBroadcaster;
use Illuminate\Support\Facades\Storage;
use App\Models\Settings;
use DataTables;
use Auth;
use DB;
use Mpdf\Mpdf;

class UserDataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('operator.index');
    }

    function getServerSide(Request $request)
    {
        $role_id = Auth::user()->role_id;

        $user = User::select(['*']);
        if ($role_id == "1" || $role_id == "2") {
        } else {
            $user = $user->where('id', Auth::user()->id);
        }
        $user = $user->orderBy('id', 'ASC');
        return DataTables::of($user)
            ->addColumn('action', function ($user) {
                $btn = '';
                $btn .= '&nbsp; <a class="btn btn-sm btn-update btn-info" href="/operator-show/' . $user->id . '/Attendence" title="View Details"> Attendance</a>';
                $btn .= '&nbsp; <a class="btn btn-sm btn-update btn-warning" href="/operator-show/' . $user->id . '/Work" title="View Details"> Work </a>';
                $btn .= '&nbsp; <a class="btn btn-sm btn-update btn-danger" href="/operator-show/' . $user->id . '/Salary" title="View Details"> Salary & Expense </a>';


                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $getUser = User::where('id', $request->id)->first();
        return view('operator.view')->with([
            'getUser' => $getUser,
            'type' => $request->type,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        //
    }

    public function addAttendance(Request $request)
    {

        // Validate input data
        // $request->validate([
        //     'user_id' => 'required|integer|exists:users,id',
        //     'remark' => 'nullable|string',
        //     'duty_time_in' => 'required|date',
        //     'duty_time_out' => 'required|date|after:duty_time_in',

        // ]);
        // Create new attendance record
        Operator::create([
            'user_id' => $request->user_id,
            'duty_time_in' => date('Y-m-d H:i:s', strtotime($request->duty_time_in)),
            'duty_time_out' => date('Y-m-d H:i:s', strtotime($request->duty_time_out)),
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'remark' => $request->remark // Add 'remark' column in the migration and the Model if necessary
        ]);

        // Redirect back with success message
        return redirect('/operator-show/' . $request->user_id . '/Attendence')->with(
            'success',
            'Data has been updeted successfully!'
        );
    }

    public function getServerSideAttendence(Request $request)
    {
        // Select all columns from the operator_attendence table
        $query = Operator::query();
       // $query = $query->select([DB::raw('DISTINCT(left(duty_time_in,10)) as user_attendence'),'operator_attendence.*']);
        $query->where('is_delete', '0');
        // Apply filters if they exist in the request
        if ($request->filled('user_id')) {
            $query->where('user_id', $request->user_id);
        }
        // You can add more filtering logic similar to the 'Enquiry' example here if needed
        if (!empty($request->from_date)) {
            $query = $query->whereRaw('left(duty_time_in,10) >="' . $request->from_date . '"');
        }
        if (!empty($request->to_date)) {
            $query = $query->whereRaw('left(duty_time_in,10) <="' . $request->to_date . '"');
        }
        // $query = $query->groupBy(DB::raw('left(duty_time_in,10)'));
        $query = $query->orderBy('duty_time_in', 'DESC');

        // Return data for DataTable
        return DataTables::of($query)
            // ->addColumn('name', function ($operator) {
            //     return $operator->user->name ?? ''; // Assuming you have a relationship to the User model
            // })
            ->addColumn('duty_in_time', function ($operator) {
                return date('d/M/Y h:i A', strtotime($operator->duty_time_in));
            })
            ->addColumn('duty_out_time', function ($operator) {
                return date('d/M/Y h:i A', strtotime($operator->duty_time_out));
            })
            ->addColumn('created_by', function ($operator) {
                return $operator->createdBy->name ?? ''; // Assuming createdBy relationship
            })
            ->addColumn('remark', function ($operator) {
                return $operator->remark ?? "";
            })
            ->addColumn('is_delete', function ($operator) {
                return $operator->is_delete;
            })
            ->addColumn('action', function ($operator) {
                if ($operator->is_delete == "0") {
                    return ' <button class="btn btn-sm btn-danger" onclick="deleteOperator(' . $operator->id . ')">Delete</button>';
                }
                // <button class="btn btn-sm btn-primary" onclick="editOperator(' . $operator->id . ')">Edit</button>
            })
            ->rawColumns(['duty_in_time', 'duty_out_time', 'remark', 'created_by', 'id_delete', 'action']) // If your action column contains HTML, mark it as raw
            ->make(true);
    }

    public function getServerSideOperatorData(Request $request)
    {

        // Select all columns from the operator_attendence table
        $query = OperatorData::query();

        // Apply filters if they exist in the request
        if ($request->filled('user_id')) {
            $query->where('user_id', $request->user_id);
        }
        // You can add more filtering logic similar to the 'Enquiry' example here if needed
        if (!empty($request->from_date)) {
            $query = $query->whereRaw('left(date,10) >="' . $request->from_date . '"');
        }
        if (!empty($request->to_date)) {
            $query = $query->whereRaw('left(date,10) <="' . $request->to_date . '"');
        }
        $enquirys = $query->where('type', $request->type)->orderBy('id', 'DESC');
        // Return data for DataTable
        return DataTables::of($query)
            // ->addColumn('name', function ($operator) {
            //     return $operator->user->name ?? ''; // Assuming you have a relationship to the User model
            // })
            ->addColumn('date', function ($operator) {
                return date('d/M/Y', strtotime($operator->date));
            })
            ->addColumn('salary', function ($operator) {
                $getData = OperatorCategory::select('operator_category.value as data_value', 'title')
                    ->join('categories', 'categories.id', 'operator_category.category_id')
                    ->where('operator_data_id', $operator->id)->get();
                $string = "";
                $total = "0";
                foreach ($getData as $key => $row) {
                    $stringCheck = ",<br>";
                    if (count($getData) == $key + 1) {
                        $stringCheck = "";
                    }
                    $string .= '<b>' . ucfirst($row->title) . '</b> : ' . $row->data_value . $stringCheck;
                    $total += $row->data_value;
                }
                $string .= '<br><b>Total : ' . $total . ' </b>';
                return $string;
            })

            ->addColumn('created_by', function ($operator) {
                return $operator->createdBy->name ?? ''; // Assuming createdBy relationship
            })
            ->addColumn('remark', function ($operator) {
                return $operator->remark ?? "";
            })
            ->addColumn('is_delete', function ($operator) {
                return $operator->is_delete;
            })
            ->addColumn('action', function ($operator) {
                if ($operator->is_delete == "0") {
                    return ' <button class="btn btn-sm btn-danger" onclick="deleteOperatorSalaryWork(' . $operator->id . ')">Delete</button>';
                }
            })
            ->rawColumns(['date', 'salary', 'remark', 'created_by', 'id_delete', 'action']) // If your action column contains HTML, mark it as raw
            ->make(true);
    }

    public function getServerSideOperatorWorkData(Request $request)
    {

        // Select all columns from the operator_attendence table
        $query = OperatorData::query();
        $query->where('is_delete', '0');
        // Apply filters if they exist in the request
        if ($request->filled('user_id')) {
            $query->where('user_id', $request->user_id);
        }
        // You can add more filtering logic similar to the 'Enquiry' example here if needed
        if (!empty($request->from_date)) {
            $query = $query->whereRaw('left(date,10) >="' . $request->from_date . '"');
        }
        if (!empty($request->to_date)) {
            $query = $query->whereRaw('left(date,10) <="' . $request->to_date . '"');
        }
        $enquirys = $query->where('type', $request->type)->orderBy('id', 'DESC');
        // Return data for DataTable
        return DataTables::of($query)
            // ->addColumn('name', function ($operator) {
            //     return $operator->user->name ?? ''; // Assuming you have a relationship to the User model
            // })
            ->addColumn('date', function ($operator) {
                return date('d/M/Y', strtotime($operator->date));
            })
            ->addColumn('salary', function ($operator) {
                $getData = OperatorCategory::select('operator_category.value as data_value', 'title')
                    ->join('categories', 'categories.id', 'operator_category.category_id')
                    ->where('operator_data_id', $operator->id)->get();
                $string = "";
                $total = "0";

                foreach ($getData as $key => $row) {
                    $stringCheck = ",<br>";
                    if (count($getData) == $key + 1) {
                        $stringCheck = "";
                    }
                    $string .= '<b>' . $row->title . '</b> : ' . $row->data_value . $stringCheck;
                    $total += $row->data_value;
                }
                $string .= '<br><b>Total : ' . $total . ' </b>';

                return $string;
            })

            ->addColumn('created_by', function ($operator) {
                return $operator->createdBy->name ?? ''; // Assuming createdBy relationship
            })
            ->addColumn('remark', function ($operator) {
                return $operator->remark ?? "";
            })
            ->addColumn('is_delete', function ($operator) {
                return $operator->is_delete;
            })
            ->addColumn('action', function ($operator) {
                if ($operator->is_delete == "0") {
                    return ' <button class="btn btn-sm btn-danger" onclick="deleteOperatorSalaryWork(' . $operator->id . ')">Delete</button>';
                }
            })
            ->rawColumns(['date', 'salary', 'remark', 'created_by', 'is_delete', 'action']) // If your action column contains HTML, mark it as raw
            ->make(true);
    }

    public function markAttendance(Request $request)
    {
        $getUser = User::find($request->id);

        $lastAttendance = Operator::where('user_id',$request->id)->orderBy('id','DESC')->first(); 

        if (!empty($lastAttendance->duty_time_in)) {
            $newDate = date("d-m-Y", strtotime($lastAttendance->duty_time_in. " +1 day"));
        }else{
            $newDate = date("d-m-Y");
        }
        return response()->json([
            'html' => view('operator.mark_attendance',compact('newDate'))->with('getUser', $getUser)->render(),
        ]);
    }

    function sheetDataAdd(Request $request)
    {
        $getUser = User::find($request->id);


        return response()->json([
            'html' => view('operator.operator_data_add')->with('getUser', $getUser)->with('type', $request->type)->render(),
        ]);
    }

    function operatorAddMore(Request $request)
    {

        $getCategory = Category::where([['type', $request->type], ['status', 'Active']])->get();

        return response()->json([
            'html' => view('operator.operator_add_more')->with('getCategory', $getCategory)
                ->with('type', $request->type)->render(),
        ]);
    }

    public function summaryData(Request $request)
    {
        $getUser = User::find($request->user_id);
        $from_date= $request->from_date;
        $to_date= $request->to_date;

        $workData = OperatorCategory::select('category_id', DB::raw('sum(value) as value'))->with('category')
            ->join('operator_data', 'operator_data.id', 'operator_category.operator_data_id')
            ->join('categories', 'categories.id', 'operator_category.category_id')
            ->where([['operator_data.user_id', $getUser->id], ['operator_data.type', 'Work'], ['operator_data.is_delete', '0']])
            ->whereRaw('left(operator_data.created_at,10) >="' . $request->from_date . '"')
            ->whereRaw('left(operator_data.created_at,10) <="' . $request->to_date . '"')
            ->groupBy('operator_category.category_id')
            ->get();

        $salaryData = OperatorCategory::select('category_id', DB::raw('sum(value) as value'))->with('category')
            ->join('operator_data', 'operator_data.id', 'operator_category.operator_data_id')
            ->join('categories', 'categories.id', 'operator_category.category_id')
            ->where([['operator_data.user_id', $getUser->id], ['operator_data.type', 'Salary'], ['operator_data.is_delete', '0'],['categories.is_expense','0']])
            ->whereRaw('left(operator_data.created_at,10) >="' . $request->from_date . '"')
            ->whereRaw('left(operator_data.created_at,10) <="' . $request->to_date . '"')
            ->groupBy('operator_category.category_id')
            ->get();

        $expanseData = OperatorCategory::select('category_id', DB::raw('sum(value) as value'))->with('category')
            ->join('operator_data', 'operator_data.id', 'operator_category.operator_data_id')
            ->join('categories', 'categories.id', 'operator_category.category_id')
            ->where([['operator_data.user_id', $getUser->id], ['operator_data.type', 'Salary'], ['operator_data.is_delete', '0'],['categories.is_expense','1']])
            ->whereRaw('left(operator_data.created_at,10) >="' . $request->from_date . '"')
            ->whereRaw('left(operator_data.created_at,10) <="' . $request->to_date . '"')
            ->groupBy('operator_category.category_id')
            ->get();

        $attendence = Operator::where('user_id', $request->user_id)
            ->select(DB::raw('DATE(duty_time_in) as date_only'))
            ->where('is_delete', '0')
            ->whereRaw('left(duty_time_in,10) >="' . $request->from_date . '"')
            ->whereRaw('left(duty_time_in,10) <="' . $request->to_date . '"')
            ->groupBy(DB::raw('DATE(duty_time_in)'))
            ->get()->count();
        return response()->json([
            'html' => view('operator.summary', compact('workData', 'salaryData','expanseData'))->with('getUser', $getUser)->with('attendence', $attendence)->render(),
        ]);
    }

    function operatorAddMoreSave(Request $request)
    {

        $operatorData = new OperatorData();
        $operatorData->date = date('Y-m-d', strtotime($request->category_date));
        $operatorData->remark = $request->remark;
        $operatorData->type = $request->type;
        $operatorData->user_id = $request->operator_user_id;
        $operatorData->created_by = Auth::user()->id;
        $operatorData->updated_by = Auth::user()->id;
        $operatorData->created_at = date('Y-m-d H:i:s');
        $operatorData->updated_at = date('Y-m-d H:i:s');
        $operatorData->save();
        $last_id = $operatorData->id;
        $category_ids = $request->category_ids;

        foreach ($category_ids as $key => $value) {
            if (!empty($value)) {
                $operatorCat = new OperatorCategory();
                $operatorCat->operator_data_id = $last_id;
                $operatorCat->category_id = $value;
                $operatorCat->value = $request->category_values[$key];
                $operatorCat->created_at = date('Y-m-d H:i:s');
                $operatorCat->updated_at = date('Y-m-d H:i:s');
                $operatorCat->save();
            }
        }

        return true;
    }
    function operatorAttendenceDeleteData(Request $request)
    {
        Operator::where('id', $request->id)->update(['is_delete' => "1"]);

        return true;
    }

    function operatorAttendenceDeleteSalaryWork(Request $request)
    {
        OperatorData::where('id', $request->id)->update(['is_delete' => "1"]);

        return true;
    }

    public function salarySlip($user_id,$from_date,$to_date) {

        $getSetting = Settings::latest()->first();
        $get_user = User::find($user_id);

        $salaryData = OperatorCategory::select('category_id', DB::raw('sum(value) as value'))->with('category')
        ->join('operator_data', 'operator_data.id', 'operator_category.operator_data_id')
        ->join('categories', 'categories.id', 'operator_category.category_id')
        ->where([['operator_data.user_id', $get_user->id], ['operator_data.type', 'Salary'], ['operator_data.is_delete', '0'],['categories.is_expense','0']])
        ->whereRaw('left(operator_data.created_at,10) >="' . $from_date . '"')
        ->whereRaw('left(operator_data.created_at,10) <="' . $to_date . '"')
        ->groupBy('operator_category.category_id')
        ->get();

        $expanseData = OperatorCategory::select('category_id', DB::raw('sum(value) as value'))->with('category')
        ->join('operator_data', 'operator_data.id', 'operator_category.operator_data_id')
        ->join('categories', 'categories.id', 'operator_category.category_id')
        ->where([['operator_data.user_id', $get_user->id], ['operator_data.type', 'Salary'], ['operator_data.is_delete', '0'],['categories.is_expense','1']])
        ->whereRaw('left(operator_data.created_at,10) >="' . $from_date . '"')
        ->whereRaw('left(operator_data.created_at,10) <="' . $to_date . '"')
        ->groupBy('operator_category.category_id')
        ->get();
        $data = [
            'title' => 'Welcome to My PDF',
            'content' => 'This is the content of my PDF file.',
            // 'id'=>$id
            'customerDetails' => $get_user,
            'settings'=>$getSetting,
            'salaryData' =>$salaryData,
            'expanseData' =>$expanseData,
            'month' => date('M Y',strtotime($from_date)),
        ];

        $mpdf = new Mpdf();
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => 'A3-L',
            'default_font_size' => "10", // Set the default font size
        ]);
        $mpdf->WriteHTML(view('operator.salary_slip', $data));

        $filename = str_replace(' ','',$get_user->name.'_salary_slip') . '.pdf';
        $mpdf->Output($filename, 'I'); // 'D' sends the file inline to the browser
        exit();
    }

}
