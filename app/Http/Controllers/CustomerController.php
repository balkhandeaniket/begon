<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\CustomerPayment;
use App\Models\CustomerService;
use App\Models\ServiceDueDate;
use App\Models\Service;
use App\Models\Settings;
use App\Models\PaidTransaction;
use App\Models\TermsAndConditions;
use Illuminate\Http\Request;
use App\Models\User;
use DataTables;
use Auth;
use DB;
use Mpdf\Mpdf;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Symfony\Component\HttpFoundation\StreamedResponse;


class CustomerController extends Controller
{
    public function __construct()
    {
        //$this->importOldData();
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('customer.index');
    }

    function getServerSide(Request $request)
    {
        //select(['customers.id','customers.first_name','customers.remark','customers.created_at','customer_addresses.contact','customer_addresses.address_line1'])
        $customers = Customer::select([
            DB::raw('distinct(customers.id)'),
            'customers.first_name',
            'customers.remark',
            'customers.created_at',
            'customers.gst_no',
            'customer_addresses.address_line1 as address_line1',
            'customer_addresses.contact as contact',
            // DB::raw(
            //     '(select customer_addresses.contact from customer_addresses where customer_id=customers.id limit 1) as contact'
            // ),
            // DB::raw(
            //     '(select customer_addresses.address_line1 from customer_addresses where customer_id=customers.id limit 1) as address_line1'
            // ),
            //DB::raw('GROUP_CONCAT(customers.id) as address_line1')
        ])
        ->join('customer_addresses', 'customer_addresses.customer_id', 'customers.id')->orderBy('customers.id','DESC');
        // ->groupBy([
        //     'customers.id',
        // ]);

        return DataTables::of($customers)
            ->addColumn('action', function ($customers) {
                if ($customers->status != 'Converted') {
                    $btn = '';
                    $btn .=
                        '<a href="/customer-payment-create/' . $customers->id . '"
                        class="btn btn-sm btn-success">Payment</a>';
                    $btn .=
                        '&nbsp; <a class="btn btn-sm btn-update btn-warning" href="/customer-edit/' .
                        $customers->id .
                        '"> Edit</a>'.$customers->status;
                    $btn .=
                        '&nbsp; <a class="btn btn-sm btn-update btn-info" href="/customer-show/' .
                        $customers->id .
                        '"> View</a>';

                    $btn .=
                        '&nbsp; <a class="btn btn-sm btn-update btn-primary" href="/customer-payment-create/' .
                        $customers->id .'/0"> Quotation</a>';

                    return $btn;
                }
            })
            ->addColumn('created', function ($customers) {
                return date('Y M d', strtotime($customers->created_at));
            })
            ->filterColumn('contact', function ($query, $keyword) {
                $query->whereRaw("customer_addresses.contact LIKE ?", ["%{$keyword}%"]);
            })
            ->filterColumn('address_line1', function ($query, $keyword) {
                $query->whereRaw("customer_addresses.address_line1 LIKE ?", ["%{$keyword}%"]);
            })
            // ->addColumn('contact', function ($customers) {
            //     return $customers->contact;
            // })
            // ->addColumn('address_line1', function ($customers) {
            //     return $customers->address_line1;
            // })
            ->rawColumns(['name_concat', 'action', 'created'])
            ->make(true);
    }

    public function show(Request $request)
    {
        $getCustomer = Customer::find($request->id);
        $getCustomerAddress = CustomerAddress::where(
            'customer_id',
            $request->id
        )->get();

        $getCustomerPayment = CustomerPayment::with([
            'customerServices',
            'paymentDisussion:id,name',
            'customerPaymentAddress',
            'createdBy:id,name',
            'updeted_by:id,name',
        ])
            ->where('customer_id', $request->id)
            ->orderBy('id', 'DESC')
            ->get();

        return view('customer.view')->with([
            'getCustomer' => $getCustomer,
            'getCustomerAddress' => $getCustomerAddress,
            'getCustomerPayment' => $getCustomerPayment,
        ]);
    }

    function getFinancialYear($date)
    {
        $date = str_replace('/', '-', $date);

        $date = date('Y-m-d', strtotime($date));

        $inputTimestamp = strtotime($date);
        $financialYearStart = strtotime(date('Y-04-01', $inputTimestamp));
        $financialYearEnd = strtotime(
            date('Y-03-31', strtotime('+1 year', $financialYearStart))
        );

        if (
            $inputTimestamp >= $financialYearStart &&
            $inputTimestamp <= $financialYearEnd
        ) {
            $financialYear =
                date('y', $inputTimestamp) .
                '-' .
                (date('y', $inputTimestamp) + 1);
        } else {
            $financialYear =
                date('y', $inputTimestamp) -
                1 .
                '-' .
                date('y', $inputTimestamp);
        }

        return $financialYear;
    }

    public function store(Request $request)
    {
        $customer = new Customer();
        $customer->enquiry_id = "0";
        $customer->first_name = $request->first_name;
        $customer->gst_no = $request->gst_no;
        $customer->remark = $request->remark;
        $customer->save();

        $last_id = $customer->id;

        $address = new CustomerAddress();
        $address->customer_id = $last_id;
        $address->contact = $request->contact ?? null;
        $address->address_line1 = $request->address ?? null;
        $address->save();

        return redirect('/customer-payment-create/' . $last_id)->with(
            'success',
            'Data has been updeted successfully!'
        );
    }

    public function checkNumberDuplication(Request $request)
    {

        $customer = CustomerAddress::where('contact', 'like', '%' . $request->contact . '%')->first();
        if (isset($customer)) {
            return 1;
        } else {
            return 0;
        }

    }

    public function edit(Request $request)
    {
        $getCustomer = Customer::select(['customers.*'])
            ->where('customers.id', $request->id)
            ->first();
        $customer_action = 'Update';
        $customer_action_url = '/customer-update-action/' . $getCustomer->id;

        return view('customer.create')
            ->with('customer_action', $customer_action)
            ->with('getCustomer', $getCustomer)
            ->with('customer_action_url', $customer_action_url);
    }

    public function update(Request $request)
    {
        $data = [
            'first_name' => $request->first_name ?? null,
            'gst_no' => $request->gst_no ?? null,
            'remark' => $request->remark ?? null,
        ];

        $customer = Customer::findOrFail($request->id);
        $customer->update($data);
        return redirect('/customer')->with(
            'success',
            'Data has been updeted successfully!'
        );
    }

    public function createCustomerPayment(Request $request)
    {
        $getUsers = User::where([['status', '1']])->get();
        $getAddress = CustomerAddress::where([
            ['customer_id', $request->id],
        ])->get();
        $getCustomer = Customer::find($request->id);

        //$getAllCustomer = Customer::select(['id', 'first_name'])->get();
        $services = Service::select('id', 'title', 'category_id')
            ->where('status', 'Active')
            ->get();

        $services1 = Service::select('title')
            ->where('status', 'Active')
            ->groupBy('title')
            ->pluck('title')->toArray();

        $paymentMode = ['Cash', 'UPI', 'Cheque', 'Bank Transfer', 'Card Payment','NEFT','Online','Gpay','Phone Pay','Paytm'];

        if($request->is_bill == "0"){
            $customerPayment = CustomerPayment::select('invoice_no', 'finance_year')->where('is_bill','0')->orderBy('id', 'desc')->first();

            $customerPaymentPreviousInvoice = CustomerPayment::select('invoice_no', 'finance_year')
            ->where('is_bill','0')
            ->where('is_return','0')
            ->orderBy('id', 'desc')->first();
        }else{
            $customerPayment = CustomerPayment::select('invoice_no', 'finance_year')->where('is_bill','1')->orderBy('id', 'desc')->first();

            $customerPaymentPreviousInvoice = CustomerPayment::select('invoice_no', 'finance_year')
            ->where('is_bill','1')
            ->where('is_return','0')
            ->orderBy('id', 'desc')->first();
        }

        $getFinancialYear = $this->getFinancialYear(date('Y-m-d'));

        $invoice_no_explode = explode('/', $customerPaymentPreviousInvoice->invoice_no ?? "");
        $invoice_no = "";
        try {
            $invoice_no = $invoice_no_explode[1] + 1;
        } catch (\Throwable $th) {
            //throw $th;
        }

        $category = Category::where('status','Active')->get();

        $getSetting = Settings::latest()->first();
        return view('customer.create_customer_payment')->with([
            'getCustomer' => $getCustomer,
            'is_bill' => $request->is_bill ?? "1",
            //'getAllCustomer' => $getAllCustomer,
            'services' => $services,
            'getSetting' => $getSetting,
            'getUsers' => $getUsers,
            'getAddress' => $getAddress,
            'paymentMode' => $paymentMode,
            'service_html' => $services1,
            'category' => $category,
            'invoice_no' => $getFinancialYear . "/" . $invoice_no,
            'previous_invoice_no' => $customerPaymentPreviousInvoice->invoice_no ?? "",
        ]);
    }

    public function updateCustomerPayment(Request $request)
    {
        $getUsers = User::where([['status', '1']])->get();
        $getAddress = CustomerAddress::where([
            ['customer_id', $request->id],
        ])->get();
        $getCustomer = Customer::find($request->id);
        $getAllCustomer = Customer::select(['id', 'first_name'])->get();
        $services = Service::select('id', 'title', 'category_id')
            ->where('status', 'Active')
            ->get();
        $services1 = Service::select('title')
            ->where('status', 'Active')
            ->groupBy('title')
            ->pluck('title')->toArray();

        $paymentMode = ['Cash', 'UPI', 'Cheque', 'Bank Transfer', 'Card Payment','NEFT','Online','Gpay','Phone Pay','Paytm'];

        $getCustomerPayment = CustomerPayment::with([
            'customerDetails',
            'customerServices',
            'paymentDisussion:id,name',
            'createdBy:id,name',
            'updeted_by:id,name',
        ])
            ->where('id', $request->payment_id)
            ->first();

        $customerPayment = CustomerPayment::select('invoice_no')->where('id', $request->payment_id)->orderBy('id', 'desc')->first();

        $hsn = Service::select('hsn', 'title')
            ->where('status', 'Active')->whereNotNull('hsn')
            ->get();
        $remark = Service::select('remark', 'title')
            ->where('status', 'Active')->whereNotNull('remark')
            ->get();

        $category = Category::where('status','Active')->get();

        return view('customer.create_customer_payment')->with([
            'getCustomer' => $getCustomer,
            'is_bill' => $request->is_bill ?? "1",
            'getAllCustomer' => $getAllCustomer,
            'services' => $services,
            'getUsers' => $getUsers,
            'getAddress' => $getAddress,
            'payment_id' => $request->payment_id,
            'getCustomerPayment' => $getCustomerPayment,
            'paymentMode' => $paymentMode,
            'service_html' => $services1,
            'hsn' => $hsn,
            'remark' => $remark,
            'category' => $category,
            'invoice_no' => $customerPayment->invoice_no ?? "",
            'previous_invoice_no' => $customerPayment->invoice_no ?? "",
        ]);
    }

    public function updateCustomerPaidPayment(Request $request)
    {
        $getUsers = User::where([['status', '1']])->get();

        $paymentMode = ['Cash', 'UPI', 'Cheque', 'Bank Transfer', 'Card Payment','NEFT','Online','Gpay','Phone Pay','Paytm'];

        $getCustomerPayment = CustomerPayment::with([
            'customerDetails',
            'customerServices',
            'paymentDisussion:id,name',
            'createdBy:id,name',
            'updeted_by:id,name',
        ])
            ->where('id', $request->payment_id)
            ->first();

        $payment = CustomerPayment::select([
            'customer_payments.*',
            'customers.first_name'
            ,
            DB::raw('(select sum(amount) as amount from customer_services where payment_id=customer_payments.id limit 1) as amount')
            ,
            DB::raw('(select sum(gst_amount) as gst_amount from customer_services where payment_id=customer_payments.id limit 1) as gst_amount')
            ,
            DB::raw('(select sum(gst_cal_amount) as gst_cal_amount from customer_services where payment_id=customer_payments.id limit 1) as gst_cal_amount')
            ,
            DB::raw('(select sum(gst_amount) as gst_amount from customer_services where payment_id=customer_payments.id limit 1) - (customer_payments.paid_amount) as balance')
        ])
            ->join('customers', 'customers.id', 'customer_payments.customer_id')
            ->where('customer_payments.id', $request->payment_id)->first();

        $customerPayment = CustomerPayment::select('invoice_no')->orderBy('id', 'desc')->first();

        return response()->json([
            'html' => view('customer.customer_paid_payment')->with([
                'getUsers' => $getUsers,
                'payment_id' => $request->payment_id,
                'getCustomerPayment' => $getCustomerPayment,
                'paymentMode' => $paymentMode,
                'payment' => $payment,
                'invoice_no' => $customerPayment->invoice_no ?? "",
            ])
                ->render(),
        ]);
    }

    public function submitCustomerPaidPayment(Request $request)
    {
        if (!empty($request->payment_id)) {
            $payment_transaction = new PaidTransaction();
            $payment_transaction->payment_id = $request->payment_id;
            $payment_transaction->paid_amount = $request->paid_amount ?? '0';
            $payment_transaction->payment_mode = $request->payment_mode;
            $payment_transaction->bank_name = $request->bank_name;
            $payment_transaction->payment_reference = $request->payment_reference;
            $payment_transaction->payment_discussion = $request->payment_disussion ?? null;
            $payment_transaction->remark = $request->remark ?? '';
            $payment_transaction->created_by = Auth::user()->id;
            $payment_transaction->created_at = date('Y-m-d H:i:s');
            $payment_transaction->save();

            $paidTransaction = PaidTransaction::select([DB::raw('sum(paid_amount) as paid_amount')])->where([['payment_id',$request->payment_id],['is_delete','0']])->first();

            $data = [
                'paid_amount' => $paidTransaction->paid_amount ?? '0',
                'payment_date' => date('Y-m-d H:i:s'),
                'payment_mode' => $request->payment_mode,
                'bank_name' => $request->bank_name ?? '',
                'payment_reference' => $request->payment_reference ?? '',
                'payment_disussion' => $request->payment_disussion ?? null,
                'remark' => $request->remark ?? '',
                'updated_by' => Auth::user()->id,
            ];
            CustomerPayment::where('id', $request->payment_id)->update($data);

            $payment_id= CustomerPayment::select('customer_id')->where('id', $request->payment_id)->first();
            return $payment_id->customer_id;
        }
        return false;
    }

    public function paymentCancelled(Request $request)
    {
        if (!empty($request->payment_id)) {
            if ($request->flag == "2") {  // for cancelled and return
                $customer_payment =  CustomerPayment::select('is_return')->where('id', $request->payment_id)->first();

                $is_return = "1";
                if ($customer_payment->is_return=="1") {
                    $is_return = "0";
                }

                $data = [
                    'is_return' => $is_return,
                    'updated_by' => Auth::user()->id,
                ];
            }else{ // for delete
                $data = [
                    'is_cancelled' => '1',
                    'updated_by' => Auth::user()->id,
                ];
            }

            CustomerPayment::where('id', $request->payment_id)->update($data);
        }
        return true;
    }

    public function invoiceCancelled(Request $request)
    {
        if (!empty($request->payment_id)) {
            $data = [
                'is_invoice_cancelled' => '1',
                'updated_by' => Auth::user()->id,
            ];
            CustomerPayment::where('id', $request->payment_id)->update($data);
        }
        return true;
    }

    public function addService(Request $request)
    {
        $categorys = Category::select('id', 'title')
            ->where('status', 'Active')
            ->get();
        $services = Service::select('id', 'title', 'category_id')
            ->where('status', 'Active')
            ->get();

        $hsn = Service::select('hsn', 'title')
            ->where('status', 'Active')->whereNotNull('hsn')
            ->get();
        $remark = Service::select('remark', 'title')
            ->where('status', 'Active')->whereNotNull('remark')
            ->get();
        $rand = rand();

        $getSetting = Settings::latest()->first();
        return response()->json([
            'html' => view('customer.create_service')
                ->with('categorys', $categorys)
                ->with('services', $services)
                ->with('rand', $rand)
                ->with('hsn', $hsn)
                ->with('getSetting', $getSetting)
                ->with('remark', $remark)
                ->render(),
            "random_no" => $rand,
        ]);
    }

    public function getCustomerServices(Request $request)
    {
        $services = Service::select('id', 'title')
            ->where([
                ['category_id', $request->category_id],
                ['status', 'Active'],
            ])
            ->get();

        $html = '';
        $html .= '<option value="">Select</option>';
        foreach ($services as $key => $value) {
            $html .=
                '<option value="' .
                $value->id .
                '">' .
                $value->title .
                '</option>';
        }
        return response()->json([
            'html' => $html,
        ]);
    }

    public function getCustomerAddress(Request $request)
    {
        $services = CustomerAddress::select('id', 'address_line1')->where('customer_id', $request->customer_id)
            ->get();
        $html = '';
        // $html .= '<option value="">Select</option>';
        foreach ($services as $key => $value) {
            $html .=
                '<option value="' .
                $value->id .
                '">' .
                $value->address_line1 .
                '</option>';
        }
        return response()->json([
            'html' => $html,
        ]);
    }

    public function customerPaymentSubmit(Request $request)
    {
        if (!empty($request->payment_id)) {
            $financialYear = $this->getFinancialYear($request->invoice_date);
            $data = [
                'customer_id' => $request->customer_id,
                'customer_address_id' => $request->customer_address_id,
                'category_id' => $request->category_id,
                'invoice_no' => $request->invoice_no ?? "",
                'finance_year' => $financialYear,
                'invoice_date' => $request->invoice_date,
                //"total_amount" => $request->total_amount,
                //"total_gst_amount" => $request->total_gst_amount,
                'paid_amount' => $request->paid_amount ?? '0',
                'payment_mode' => $request->payment_mode,
                'bank_name' => $request->bank_name ?? '',
                'payment_reference' => $request->payment_reference ?? '',
                'payment_disussion' => $request->payment_disussion ?? null,
                'remark' => $request->remark ?? '',
                'is_bill' => $request->is_bill ?? '1',
                'updated_by' => Auth::user()->id,
                'is_gst_module' =>  '0',
            ];

            CustomerPayment::where('id', $request->payment_id)->update($data);
            $getDetails = CustomerPayment::where('id', $request->payment_id)->first();

            $service_ids = CustomerService::where('payment_id', $request->payment_id)->get();
            foreach ($service_ids as $key => $value) {

                $serviceDueDate = new ServiceDueDate();
                $serviceDueDate->where('service_id', $value->id)->delete();

                $serviceId = new CustomerService();
                $serviceId->where('id', $value->id)->delete();
            }

            $is_gst = 1;
            foreach ($request->service_ids as $key => $value) {
                //$getserviceCategory =Service::select(['category_id'])->find($request->service_ids[$key]);
                $services = [
                    'payment_id' => $getDetails->id,
                    // 'category_id' => $request->category_ids[$key] ?? '',
                    'category_id' => 0,
                    'service_id' => $request->service_ids[$key] ?? '0',
                    'hsn' => $request->hsns[$key] ?? '',
                    'remark' => $request->remarks[$key] ?? '',
                    'service_due_text' => $request->service_due_texts[$key] ?? '',
                    'contract_from' => $request->contract_froms[$key] ?? null,
                    'contract_to' => $request->contract_tos[$key] ?? null,
                    'contract' => $request->contracts[$key] ?? null,
                    'period' => $request->periods[$key] ?? null,
                    'price' => $request->prices[$key] ?? '0',
                    'quantity' => $request->quantitys[$key] ?? '0',
                    'amount' => $request->amounts[$key] ?? '0',
                    'gst' => $request->gsts[$key] ?? '0',
                    'gst_cal_amount' => $request->gst_cal_amounts[$key] ?? '0',
                    'gst_amount' => $request->gst_amounts[$key] ?? '0',
                ];
                $servicesDetails = CustomerService::create($services);

                if (!empty($request->gsts[$key]) && $request->gsts[$key] > 0) {
                    $is_gst = 0;
                }

                if (!empty($request->service_due_dates)) {
                    foreach ($request->service_due_dates[$key] as $key1 => $due_date) {
                        $service_due_date = [
                            'service_id' => $servicesDetails->id,
                            'service_due' => $due_date,
                        ];
                        ServiceDueDate::create($service_due_date);
                    }
                }

            }


            $servicesAmountSum = CustomerService::select([DB::raw('sum(gst_amount) as total')])->where('payment_id',$getDetails->id)->first();
            CustomerPayment::where('id', $getDetails->id)->update(['total_amount'=>$servicesAmountSum->total]);

        } else {
            $custPaymentCount = CustomerPayment::count();
            $invoiceNo = $custPaymentCount + 1;
            $financialYear = $this->getFinancialYear($request->invoice_date);
            $data = [
                'customer_id' => $request->customer_id,
                'customer_address_id' => $request->customer_address_id,
                'category_id' => $request->category_id,
                'finance_year' => $financialYear,
                //'invoice_no' => $financialYear . '/' . $invoiceNo,
                'invoice_no' => $request->invoice_no,
                'invoice_date' => $request->invoice_date,
                //"total_amount" => $request->total_amount,
                //"total_gst_amount" => $request->total_gst_amount,
                'paid_amount' => $request->paid_amount ?? '0',
                'payment_mode' => $request->payment_mode,
                'bank_name' => $request->bank_name ?? '',
                'payment_reference' => $request->payment_reference ?? '',
                'payment_disussion' => $request->payment_disussion ?? null,
                'remark' => $request->remark ?? '',
                'is_bill' => $request->is_bill ?? '1',
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
                'is_gst_module' =>  '0',
            ];

            $getDetails = CustomerPayment::create($data);

            $is_gst = 1;
            foreach ($request->service_ids as $key => $value) {
                //$getserviceCategory =Service::select(['category_id'])->find($request->service_ids[$key]);
                $services = [
                    'payment_id' => $getDetails->id,
                    // 'category_id' => $request->category_ids[$key] ?? '',
                    'category_id' => 0,
                    'service_id' => $request->service_ids[$key] ?? '0',
                    'hsn' => $request->hsns[$key] ?? '',
                    'remark' => $request->remarks[$key] ?? '',
                    'service_due_text' => $request->service_due_texts[$key] ?? '',
                    'contract_from' => $request->contract_froms[$key] ?? null,
                    'contract_to' => $request->contract_tos[$key] ?? null,
                    'contract' => $request->contracts[$key] ?? null,
                    'period' => $request->periods[$key] ?? null,
                    'price' => $request->prices[$key] ?? '0',
                    'quantity' => $request->quantitys[$key] ?? '0',
                    'amount' => $request->amounts[$key] ?? '0',
                    'gst' => $request->gsts[$key] ?? '0',
                    'gst_cal_amount' => $request->gst_cal_amounts[$key] ?? '0',
                    'gst_amount' => $request->gst_amounts[$key] ?? '0',
                ];
                $servicesDetails = CustomerService::create($services);

                if (!empty($request->gsts[$key]) && $request->gsts[$key] > 0) {
                    $is_gst = 0;
                }

                if (!empty($request->service_due_dates)) {
                    foreach ($request->service_due_dates[$key] as $key1 => $due_date) {
                        $service_due_date = [
                            'service_id' => $servicesDetails->id,
                            'service_due' => $due_date,
                        ];
                        ServiceDueDate::create($service_due_date);
                    }
                }

            }

            $servicesAmountSum = CustomerService::select([DB::raw('sum(gst_amount) as total')])->where('payment_id',$getDetails->id)->first();
            CustomerPayment::where('id', $getDetails->id)->update(['total_amount'=>$servicesAmountSum->total]);

        }
        //return redirect('/customer-view/'.$request->customer_id)->with('success', 'Data has been updeted successfully!');
        return response()->json([
            'status' => true,
            'customer_id' => $request->customer_id,
            'payment_id' => $getDetails->id,
            'is_gst' => $is_gst,
        ]);
    }

    public function customerAddressForm($id)
    {
        $customer_action = 'Add';
        $customer_action_url = '/customer-address-add-action';
        return view('customer.add_customer_address')
            ->with('customerID', $id)
            ->with('customer_action', $customer_action)
            ->with('customer_action_url', $customer_action_url);
    }
    public function customerAddressAddAction(Request $request)
    {
        $CustomerAddress = new CustomerAddress();
        $CustomerAddress->customer_id = $request->customer_id;
        $CustomerAddress->contact = $request->contact ?? null;
        $CustomerAddress->email = $request->email ?? null;
        // $CustomerAddress->contact_no1 = $request->contact_no1 ?? null;
        // $CustomerAddress->contact_no2 = $request->contact_no2 ?? null;
        $CustomerAddress->locality = $request->locality ?? null;
        $CustomerAddress->address_line1 = $request->address_line1 ?? null;
        //$CustomerAddress->address_line2 = $request->address_line2 ?? null;
        $CustomerAddress->landmark = $request->landmark ?? null;
        $CustomerAddress->road = $request->road ?? null;
        $CustomerAddress->street = $request->street ?? null;
        $CustomerAddress->near = $request->near ?? null;
        $CustomerAddress->opposite = $request->opposite ?? null;
        $CustomerAddress->pin_code = $request->pin_code ?? null;
        $CustomerAddress->save();
        return redirect('/customer-show/' . $request->customer_id)->with(
            'success',
            'Data has been stored successfully!'
        );
    }

    public function editAddress($id)
    {
        $getCustomerAddress = CustomerAddress::find($id);

        $customer_action = 'Update';
        $customer_action_url = '/customer-address-edit-action';
        return view('customer.add_customer_address')
            ->with('customerID', $id)
            ->with('customer_action', $customer_action)
            ->with('customer_action_url', $customer_action_url)
            ->with('getCustomerAddress', $getCustomerAddress);
    }

    public function updateCustomerAddress(Request $request)
    {
        $getCustomerAddress = CustomerAddress::find($request->customer_id);

        // $getCustomerAddress->customer_id = $request->customer_id;
        $getCustomerAddress->contact = $request->contact ?? null;
        $getCustomerAddress->email = $request->email ?? null;
        // $getCustomerAddress->contact_no1 = $request->contact_no1 ?? null;
        // $getCustomerAddress->contact_no2 = $request->contact_no2 ?? null;
        $getCustomerAddress->locality = $request->locality ?? null;
        $getCustomerAddress->address_line1 = $request->address_line1 ?? null;
        // $getCustomerAddress->address_line2 = $request->address_line2 ?? null;
        $getCustomerAddress->landmark = $request->landmark ?? null;
        $getCustomerAddress->road = $request->road ?? null;
        $getCustomerAddress->street = $request->street ?? null;
        $getCustomerAddress->near = $request->near ?? null;
        $getCustomerAddress->opposite = $request->opposite ?? null;
        $getCustomerAddress->pin_code = $request->pin_code ?? null;
        $getCustomerAddress->save();
        $id = $getCustomerAddress->customer_id;

        return redirect('/customer-show/' . $id)->with(
            'success',
            'Data has been stored successfully!'
        );
    }

    public function paymentPdf(
        $payment_id,
        $address_id,
        $type,
        Request $request
    ) {
        $getCustomer = Customer::find($request->id);

        $getCustomerPayment = CustomerPayment::with([
            'customerDetails',
            'customerServices',
            'paymentDisussion:id,name',
            'createdBy:id,name',
            'updeted_by:id,name',
        ])
            ->where('id', $request->payment_id)
            ->first();

        $isContractAmountShow = CustomerService::select(['contract'])->where('payment_id', $getCustomerPayment->id)->whereNotNull('contract')->first();
        $isContractShow = "1";
        if (empty($isContractAmountShow->contract)) {
            $isContractShow = "0";
        }
        $getSetting = Settings::latest()->first();
        $termAndCondition = TermsAndConditions::get();

        $getCustomerAddress = CustomerAddress::find(
            $getCustomerPayment->customer_address_id
        );

        $qrCode = QrCode::size('150')->generate("upi://pay?pa=begonpest03control@okaxis&cu=INR&am=" . $getCustomerPayment->customerServices->sum('gst_amount'));
        //$qrCode = QrCode::size('500')->generate("upi://pay?pa=begonpest03control@okaxis&mc=Q93701293&tid=YourTransactionId&tr=YourTransactionRefId&tn=Invoice&cu=INR&am=" . $getCustomerPayment->customerServices->sum('gst_amount'));
        //$qrCode = QrCode::size('500')->generate("upi://pay?pa=9892072628@ybl&mc=Q93701293&tid=YourTransactionId&tr=YourTransactionRefId&tn=Invoice&cu=INR&am=" . $getCustomerPayment->customerServices->sum('gst_amount'));

        $data = [
            'title' => 'Welcome to My PDF',
            'content' => 'This is the content of my PDF file.',
            // 'id'=>$id
            'customerDetails' => $getCustomer,
            'customerAddress' => $getCustomerAddress,
            'customerPayment' => $getCustomerPayment,
            'settings' => $getSetting,
            'invoice_type' => $type,
            'termAndCondition' => $termAndCondition,
            'qrCode' => $qrCode,
            'isContractShow' => $isContractShow,
        ];

        $mpdf = new Mpdf();
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            // 'default_font_size' => 18, // Set the default font size
        ]);
        //$mpdf->WriteHTML(view('customer.invoice_new', $data));

        if ($type == 0) {
            $mpdf->WriteHTML(view('customer.invoice_gst', $data));
        } else {
            $mpdf->WriteHTML(view('customer.invoice', $data));
        }

        $filename = $getCustomerPayment->invoice_no . '.pdf';
        $mpdf->Output($filename, 'I'); // 'D' sends the file inline to the browser
        exit();
    }

    public function contractForm(
        $payment_id,
        $address_id,
        Request $request
    ) {
        $getCustomer = Customer::find($request->id);

        $getCustomerPayment = CustomerPayment::with([
            'customerDetails',
            'customerServices',
            'paymentDisussion:id,name',
            'createdBy:id,name',
            'updeted_by:id,name',
        ])
            ->where('id', $request->payment_id)
            ->first();

        $getSetting = Settings::latest()->first();
        $termAndCondition = TermsAndConditions::get();

        $getCustomerAddress = CustomerAddress::find(
            $getCustomerPayment->customer_address_id
        );

        $data = [
            'title' => 'Welcome to My PDF',
            'content' => 'This is the content of my PDF file.',
            // 'id'=>$id
            'customerDetails' => $getCustomer,
            'customerAddress' => $getCustomerAddress,
            'customerPayment' => $getCustomerPayment,
            'settings' => $getSetting,
            'termAndCondition' => $termAndCondition,
        ];

        $mpdf = new Mpdf();
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            'default_font_size' => "12", // Set the default font size
        ]);
        $mpdf->WriteHTML(view('customer.contract_form', $data));

        $filename = $getCustomerPayment->invoice_no . '.pdf';
        $mpdf->Output($filename, 'I'); // 'D' sends the file inline to the browser
        exit();
    }

    function importOldData()
    {
        $a = DB::select('SELECT * FROM `CustomerDetails`');

        foreach ($a as $key => $aVal) {

            $customer = new Customer();
            $customer->enquiry_id = "0";
            $customer->first_name = $aVal->CustomerName;
            $customer->gst_no = "";
            $customer->remark = $aVal->Category;
            $customer->save();

            $last_id = $customer->id;

            $address = new CustomerAddress();
            $address->customer_id = $last_id;
            $address->contact = $aVal->ContactNo . ',' . $aVal->ContactNo1;
            $address->address_line1 = $aVal->Address;
            $address->save();

            $last_address_id = $address->id;

            $b = DB::select('SELECT * FROM `CustomerAccounts` where CustomerId=' . $aVal->CustomerId . '     limit 1');
            $ChequeNo = $b[0]->ChequeNo ?? "";
            $BankAccountName = $b[0]->BankAccountName ?? "";
            $data = [
                'customer_id' => $last_id,
                'customer_address_id' => $last_address_id,
                'finance_year' => "",
                'invoice_no' => $aVal->BillNo,
                'invoice_date' => $aVal->ServiceStart,
                'paid_amount' => $aVal->PaidAmount ?? '0',
                'payment_mode' => $b[0]->PaymentType ?? "Cash",
                'payment_date' => $b[0]->DateNTime ?? "",
                'bank_name' => $b[0]->BankName ?? '',
                'payment_reference' => $ChequeNo . ', ' . $BankAccountName,
                'payment_disussion' => 1,
                'remark' => $aVal->Category ?? "",
                'created_by' => 1,
                'updated_by' => 1,
            ];

            $getDetails = CustomerPayment::create($data);

            $c = DB::select('SELECT GROUP_CONCAT(ServiceTitle) as service_id,GROUP_CONCAT(ServicesType) as ServicesType FROM `customerservices`
            left join    on old_services.id=customerservices.ServiceId where CustomerId=' . $aVal->CustomerId . ' group by  CustomerId limit 1');

            $ServicesType = $c[0]->ServicesType ?? "";
            $service_id = $c[0]->service_id ?? "";
            $services = [
                'payment_id' => $getDetails->id,
                // 'category_id' => $request->category_ids[$key] ?? '',
                'category_id' => 0,
                'service_id' => $ServicesType . ',' . $service_id,
                'hsn' => "998531",
                'remark' => '',
                'service_due_text' => '',
                'contract_from' => $aVal->ServiceStart,
                'contract_to' => $aVal->ServiceEnd,
                'contract' => $aVal->GrandTotalAmount,
                'period' => "",
                'price' => $aVal->GrandTotalAmount,
                'quantity' => 1,
                'amount' => $aVal->GrandTotalAmount,
                'gst' => '0',
                'gst_cal_amount' => '0',
                'gst_amount' => '0',
            ];

            $servicesDetails = CustomerService::create($services);
        }

        dd("Done");
    }

    function getCustomer(Request $request)
    {

        $getCustomer = Customer::select(['customers.id', 'customers.first_name', 'contact'])
            ->join('customer_addresses', 'customer_addresses.customer_id', 'customers.id')
            ->whereRaw('first_name like "%' . $request->term . '%"')
            ->orWhereRaw('customer_addresses.contact like "%' . $request->term . '%"')
            ->get();

        $list['result'] = [];
        foreach ($getCustomer as $key => $value) {
            $customer_array = ['id' => $value->id, 'text' => $value->first_name . ' - ' . $value->contact];
            array_push($list['result'], $customer_array);
        }

        $list['pagination'] = ["more" => true];
        return response()->json($list);
    }

    function invoiceUnique(Request $request)
    {

        $getCustomerPayment = CustomerPayment::where('invoice_no', $request->invoice_no)->where('is_bill',$request->is_bill)->where('is_return','0');
        if (!empty($request->payment_id)) {
            $getCustomerPayment = $getCustomerPayment->where('id', '!=', $request->payment_id);
        }
        $getCustomerPayment = $getCustomerPayment->count();

        return $getCustomerPayment;
    }

    public function downloadCSV(Request $request)
    {
        $fileName = 'sales'.date('d-m-Y-His').'.csv';

        $payment = CustomerPayment::select([
            'customer_payments.id','customer_payments.remark','customer_payments.payment_mode','customer_payments.invoice_no','customer_payments.invoice_date','customers.first_name','customers.gst_no','customer_addresses.address_line1','customer_addresses.contact','categories.title'
         ,DB::raw('(select sum(amount) as amount from customer_services where payment_id=customer_payments.id limit 1) as amount')
        ,DB::raw('(select sum(gst_amount) as gst_amount from customer_services where payment_id=customer_payments.id limit 1) as gst_amount')
        ,DB::raw('(select sum(gst_cal_amount) as gst_cal_amount from customer_services where payment_id=customer_payments.id limit 1) as gst_cal_amount')
        ,DB::raw('(select GROUP_CONCAT(contract_from) as contract_from from customer_services where payment_id=customer_payments.id limit 1) as contract_from')
        ,DB::raw('(select GROUP_CONCAT(contract_to) as contract_to from customer_services where payment_id=customer_payments.id limit 1) as contract_to')
        ,DB::raw('(select GROUP_CONCAT(service_id) as service_id from customer_services where payment_id=customer_payments.id limit 1) as service_id')
       ])
        ->join('customers','customers.id','customer_payments.customer_id')
        ->leftJoin('categories','categories.id','customer_payments.category_id')
        ->leftJoin('customer_addresses','customer_payments.customer_address_id','customer_addresses.id');

        if (!empty($request->finance_year) && $request->finance_year !="All") {
            $payment = $payment->where('finance_year', $request->finance_year);
        }

        if (!empty($request->from_date)) {
            $payment = $payment->where('invoice_date','>=', $request->from_date);
        }

        if (!empty($request->to_date)) {
            $payment = $payment->where('invoice_date','<=', $request->to_date);
        }

        if (!empty($request->from_date) && !empty($request->to_date)) {
            $payment = $payment->whereBetween('invoice_date', [$request->from_date,$request->to_date]);
        }
        $payment = $payment->where('is_bill','1')
        ->orderBy('invoice_no','ASC')
        ->limit('1000')->get();

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];

        $columns = ['SR.NO.','DATE','CUSTOMER NAME','BILL NO','BILL AMT','TAX','TOTAL','REMARK','MODE','CATEGORY','GST NUMBER']; // Replace with your actual column names

        $callback = function () use ($payment, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);


            foreach ($payment as $key=>$row) {
                fputcsv($file, [
                    $key+1, // Replace with actual column names
                    $row->invoice_date,
                    $row->first_name,
                    $row->invoice_no,
                    $row->amount,
                    $row->gst_cal_amount,
                    $row->gst_amount,
                    $row->remark,
                    $row->payment_mode,
                    $row->title,
                    $row->gst_no,
                ]);
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }


    public function customerCard(
        $customer_id,
        Request $request
    ) {
        $get_address = CustomerAddress::with('customer')->find($customer_id);
        $data = [
            'title' => 'Welcome to My PDF',
            'content' => 'This is the content of my PDF file.',
            // 'id'=>$id
            'customerDetails' => $get_address,

        ];

        $mpdf = new Mpdf();
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => 'A3-L',
            'default_font_size' => "10", // Set the default font size
        ]);
        $mpdf->WriteHTML(view('customer.customer_card', $data));

        $filename = str_replace(' ','',$get_address->customer->first_name.'_card') . '.pdf';
        $mpdf->Output($filename, 'I'); // 'D' sends the file inline to the browser
        exit();
    }
}
