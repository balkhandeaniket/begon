<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckIpAddress
{
    

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    
    public function handle(Request $request, Closure $next)
    {
        $allowedIps = ['127.0.0.1','2409:40c2:201b:85c5:8c52:d54f:a3a2:f22e','115.96.217.110']; // Add your allowed IP addresses
        if (!in_array($request->ip(), $allowedIps)) {
            // return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $next($request);
    }
}
