
function markAttendance(id) {
    var postData = {
        id: id,
    };
    $.post('/operator-mark-attendance', postData, function(response) {
        // Clear the modal body content
        $('.model2').empty();

        // Check if response.html is not empty
        if (response.html) {
            // Update the modal body with new content
            $('.model2').html(response.html);

            $('.inputMask').inputmask('datetime', {
                inputFormat: "dd-mm-yyyy hh:MM TT",  // Inputmask expects lowercase for minutes (MM → mm) and uppercase for AM/PM (tt → TT)
                placeholder: "DD-MM-YYYY hh:mm AM/PM"
            });
            

            $('#datetimepicker_in').datetimepicker({
                format: 'DD-MM-YYYY hh:mm A',   // Date with AM/PM time format
                sideBySide: true,               // Show date and time side by side
                useCurrent: true,              // Do not auto-select current date/time
                showClose: true,                // Show 'Close' button
                showClear: true,                // Show 'Clear' button
                showTodayButton: true           // Show 'Today' button
            });
            $('#datetimepicker_out').datetimepicker({
                format: 'DD-MM-YYYY hh:mm A',   // Date with AM/PM time format
                sideBySide: true,               // Show date and time side by side
                useCurrent: true,              // Do not auto-select current date/time
                showClose: true,                // Show 'Close' button
                showClear: true,                // Show 'Clear' button
                showTodayButton: true           // Show 'Today' button
            });

           
            // Show the modal
            $('#modal-default').modal('show');
        } else {
            // If response is empty, show an alert
            alert('Failed to load content. Please try again.');
        }
    });
}

function addAccountDetails(id, type) {
    var postData = {
        id: id,
        type: type,
    };
    $.post('/user-sheet-data-add', postData, function(response) {
        // Clear the modal body content
        $('.model2').empty();

        // Check if response.html is not empty
        if (response.html) {
            // Update the modal body with new content
            $('.model2').html(response.html);

            // Show the modal
            $('#modal-default').modal('show');

            add_data();
        } else {
            // If response is empty, show an alert
            alert('Failed to load content. Please try again.');
        }
    });
}

function summary() {
    var postData = {
        user_id: $("#user_id").val(),
        from_date: $("#from_date").val(),
        to_date: $("#to_date").val(),
    };
    $.post('/operator-summary-data', postData, function(response) {
        $('.summaryData').html(response.html);
    });
}
summary()

function addAttendance() {
    $("#modal-default").modal('hide')
    var postData = {
        user_id: $(".mark_attendance #status_update_id").val(),
        remark: $(".mark_attendance #remark").val(),
        duty_time_in: $(".mark_attendance #duty_time_in").val(),
        duty_time_out: $(".mark_attendance #duty_time_Out").val(),

    };
    $.post('/mark-attendance-action', postData, function(response) {
        getOperatorsList()
        summary()
    });
}

function isNumberKey(event) {
    // Check if the pressed key is a number
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        event.preventDefault();
        return false;
    }
    return true;
}


function getOperatorsList() {
    $('#operators-table').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: '/operator-getServerSideAttendence',
            type: 'POST', // Specify POST method
            data: {
                user_id: $("#user_id").val(),
                from_date: $("#from_date").val(),
                to_date: $("#to_date").val(),
            },
        },
        dom: 'Bfrtip', // Add 'B' for buttons
        buttons: [{
                extend: 'excelHtml5',
                text: 'Export to Excel',
                title: 'Attendance Report'
            },
            {
                extend: 'print',
                text: 'Print'
            }
        ],
        columns: [
            // {
            //     data: 'name',
            //     name: 'name'
            // },
            {
                data: 'duty_in_time',
                name: 'duty_in_time',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            }, 
            {
                data: 'duty_out_time',
                name: 'duty_out_time',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            },
            {
                data: 'remark',
                name: 'remark',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            },
            {
                data: 'created_by',
                name: 'created_by',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            }
        ],
        lengthMenu: [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]], // Options for rows per page
        pageLength: 40 // Default number of rows per page
    });
}
getOperatorsList();

function add_data() {
    var postData = {
        'type': $("#type").val()
    };
    $.post('/operator-add-more', postData, function(response) {
        // Clear the modal body content
        $(".dataAdd").append(response.html)

        var count = $('.remove-btn').length;
        if (parseInt(count) > 1) {
            $(".remove-btn").show();
        } else {
            $(".remove-btn").hide();
        }

    });

}

function removeCategory(id) {
    $(".remove_category_" + id).html('')

    var count = $('.remove-btn').length;
    if (parseInt(count) == "1") {
        $(".remove-btn").hide();
    } else {
        $(".remove-btn").show();
    }
}

function addCategoryData() {

    operator_user_id = $("#operator_user_id").val()
    type = $("#type").val()
    category_date = $("#category_date").val()
    remark = $("#remark").val()
    category_ids = [];
    $(".category_id").each(function() {
        category_id = $(this).val()
        category_ids.push(category_id)
    })
    category_values = [];
    $(".category_value").each(function() {
        category_value = $(this).val()
        category_values.push(category_value)
    })
    var postData = {
        'operator_user_id': operator_user_id,
        'type': type,
        'category_date': category_date,
        'remark': remark,
        'category_ids': category_ids,
        'category_values': category_values,
    };
    $.post('/operator-add-more-save', postData, function(response) {
        // Clear the modal body content
        $("#modal-default").modal('hide')
        getServerSideOperatorDataWork('Work');

        getServerSideOperatorData('Salary');
        summary()
    });

}

function getServerSideOperatorData(type) {
    $('#operators-data-table').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: '/operator-getServerSideOperatorData',
            type: 'POST', // Specify POST method
            data: {
                
                type: type,
                user_id: $("#user_id").val(),
                from_date: $("#from_date").val(),
                to_date: $("#to_date").val(),
            },
        },
        dom: 'Bfrtip', // Add 'B' for buttons
                buttons: [{
                        extend: 'excelHtml5',
                        text: 'Export to Excel',
                        title: 'Salary Report'
                    },
                    {
                        extend: 'print',
                        text: 'Print'
                    }
                ],
        columns: [{
                data: 'date',
                name: 'date',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            },
            {
                data: 'salary',
                name: 'salary',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            },
            {
                data: 'remark',
                name: 'remark',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            },
            {
                data: 'created_by',
                name: 'created_by',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            }
        ],
        lengthMenu: [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]], // Options for rows per page
        pageLength: 40 // Default number of rows per page
    });
}
getServerSideOperatorData('Salary');


function getServerSideOperatorDataWork(type) {
    $('#operators-work-table').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: '/operator-getServerSideOperatorWorkData',
            type: 'POST', // Specify POST method
            data: {
                type: type,
                user_id: $("#user_id").val(),
                from_date: $("#from_date").val(),
                to_date: $("#to_date").val(),
            },
        },
        dom: 'Bfrtip', // Add 'B' for buttons
        buttons: [{
                extend: 'excelHtml5',
                text: 'Export to Excel',
                title: 'Work Report'
            },
            {
                extend: 'print',
                text: 'Print',
                title: 'Work Report'
            }
        ],

        columns: [{
                data: 'date',
                name: 'date',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            },
            {
                data: 'salary',
                name: 'salary',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            },
            {
                data: 'remark',
                name: 'remark',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            },
            {
                data: 'created_by',
                name: 'created_by',
                render: function(data, type, row) {
                    if (row.is_delete =='1') {
                        return '<span style="text-decoration: line-through;">' + data + '</span>';
                    }
                    return data;
                }
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            }
        ],
        lengthMenu: [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]], // Options for rows per page
        pageLength: 40 // Default number of rows per page
    });
}
getServerSideOperatorDataWork('Work');

function deleteOperator(id) {
    var result = window.confirm('Are you sure you want to proceed?');
    if (result) {

        var postData = {
            "id": id,
        };
        $.post('/operator-attendence-delete-data', postData, function(response) {
            getOperatorsList()
        }).done(function() {

        });
    }
}

function deleteOperatorSalaryWork(id) {
    var result = window.confirm('Are you sure you want to proceed?');
    if (result) {

        var postData = {
            "id": id,
        };
        $.post('/operator-attendence-delete-salary-work', postData, function(response) {
            getServerSideOperatorDataWork('Work');
            getServerSideOperatorData('Salary');
        }).done(function() {

        });
    }
}

function salarySlip(id){
    user_id = $("#user_id").val()
    from_date = $("#from_date").val()
    to_date = $("#to_date").val()
    var url = "/salary-slip/" + user_id + '/'+from_date+ '/'+to_date;
    window.open(url, '_blank');
}

$(function () {
   
    // $('#datetimepicker').datetimepicker({
    //     format: 'YYYY-MM-DD HH:mm' // Customize format as needed
    // });
    /*
    $('#from_date').datetimepicker({
        format: 'YYYY-MM-DD', // This will show only the date
        icons: {
            time: ''
        } // Removes the clock icon (optional)
    });
    */
});