<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[OP.GG](https://op.gg)**
- **[WebReinvent](https://webreinvent.com/?utm_source=laravel&utm_medium=github&utm_campaign=patreon-sponsors)**
- **[Lendio](https://lendio.com)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


admin@admin.com
admin@123 / 1100

composer install - 2.5.1
npm install
gey generate 
branch fetch
php artisan migrate

php - 7.4
Laravel - 8
node - 18.13.0
npm - 9.8.1

//Db user
databasename:be_gon
username: db_begon
pwd :Begon@21


//Db user
databasename:old_begon // u316566303_old_begon
username: old_begon // u316566303_old_begon
pwd :Old_begon21@

mundokarnilesh.83@gmail.com

//Server credientials
username : mundokarnilesh.83@gmail.com
pwd - Umesh@21
Umesh@21
Aniket21@

ssh credientials
ssh -p 65002 u316566303@178.16.136.164
- Umesh@83

INSERT INTO `menus` (`id`, `title`, `url`, `order_by`) VALUES
(1, 'Dashboard', 'dashboard', 1),
(2, 'Enquiry', 'enquiry', 5),
(3, 'Service', 'service', 4),
(4, 'Customer', 'customer', 6),
(5, 'User', 'user', 2),
(6, 'Service Due', 'servicedue', 7),
(7, 'Collection', 'collection', 8),
(8, 'Category', 'category', 3),
(9, 'Outstanding', 'outstanding', 9);


INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@admin.com', NULL, '$2a$10$j11QVoKqWliVv2Yaru0n6O1Ve54o.66BniLmxWO4aefKpWbNGHLiG', '1', NULL, '2023-11-03 10:46:57', '2023-11-03 10:46:57');



INSERT INTO `settings` (`id`, `name`, `logo`, `bank_name`, `bank_account`, `ifsc_code`, `account_holder_name`, `gst_no`, `hsn_code`, `lic_no`, `address`, `email`, `website`, `state`, `mobile_no`, `created_at`, `updated_at`) VALUES
(1, 'BE-GON PEST CONTROL SERVICES', 'invoice_logo.jpeg', 'UNION BANK OF INDIA, MUMBAI ASHOKVAN', '510101002070765', 'UNBIN0904970', 'BE-GON PEST CONTROL SERVICES', '12345', '998531', '23432144', 'Shop No 23,Sai Shraddha CHS-I Behind Sai Baba Mandir,Ashokvan,Borivali(E),Mumbai-400066', 'be_gon@rediffmail.com/info@begonpestcontrol.com', 'www.begonpestcontrol.com.', '27-Maharashtra', '7303290130/28280898/28282833', NULL, NULL);


Please add the user to the users table
Username- admin@admin.com
Password - 1100
Bcrypt password - $2a$10$j11QVoKqWliVv2Yaru0n6O1Ve54o.66BniLmxWO4aefKpWbNGHLiG

Git setup
- git init
- git config settings
- git remote add origin 
- get fetch --all
- checkout in the main branch

PHP 
- check composer, PHP, and npm version
- PHP artisan migrate
- php artisan make:model Enquiry  -m -c -r for used create model and migration
- php artisan migrate:refresh - delete old migartions and again create table
- php artisan serve

composer dump-autoload


----------------------------------------------------------------------------

GST Project
 
 Model =>
  1] CustomerPayment model set mode => 1 =>>where('is_gst_module','1');
  2] COntroller Customer  function customerPaymentSubmit(Request $request) I that function set  'is_gst_module' =>  '1', in both if else condition.


Domain\
https://begonpestcontrolservices.com
https://begon-in.begonpestcontrolservices.com/

https://begon-thane-com.begonpestcontrolservices.com/
https://begon-thane-com.begonpestcontrolservices.com


Subdomain
-Search subdomain

-add subdoamin and checkbox check create autmatic folder

- after 5-10 minute subdoamin open on link 

// Thane branch crediential
u316566303_begon_thane
u316566303_aniket
Begon@21
