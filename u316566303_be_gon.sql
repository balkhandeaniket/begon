-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 20, 2024 at 07:49 AM
-- Server version: 10.11.7-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u316566303_be_gon`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Flat', 'Active', '2024-01-28 08:42:27', '2024-01-28 08:42:27'),
(4, 'School', 'Active', '2024-01-28 08:42:39', '2024-01-28 08:42:39'),
(5, 'Resident', 'Active', '2024-02-25 16:03:43', '2024-02-25 16:03:43'),
(6, 'Commercial', 'Active', '2024-03-07 15:18:59', '2024-03-07 15:18:59'),
(7, 'Hotel', 'Active', '2024-03-07 15:19:08', '2024-03-07 15:19:08'),
(8, 'Pre & Post Construction', 'Active', '2024-03-07 15:19:34', '2024-03-07 15:19:34'),
(9, 'Society', 'Active', '2024-03-07 15:19:57', '2024-03-07 15:19:57');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `enquiry_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `gst_no` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updeted_by` int(11) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `enquiry_id`, `first_name`, `gst_no`, `created_by`, `updeted_by`, `remark`, `created_at`, `updated_at`) VALUES
(1, 0, 'Test', 'test', NULL, NULL, 'test', '2024-03-09 10:06:39', '2024-03-09 10:06:39'),
(2, 0, 'Akshita Apartment CHS rrr', '1233', NULL, NULL, NULL, '2024-03-17 08:28:55', '2024-03-17 08:53:47'),
(3, 0, 'Mr. Jitendra Singh', NULL, NULL, NULL, NULL, '2024-03-23 12:40:58', '2024-03-23 12:40:58'),
(4, 0, 'Mr. Sanjeev Chaturvedi', NULL, NULL, NULL, NULL, '2024-03-23 12:45:32', '2024-03-23 12:45:32'),
(5, 0, 'Mr. Pravin Vaz', NULL, NULL, NULL, NULL, '2024-03-23 12:51:22', '2024-03-23 12:51:22'),
(6, 0, 'Mr. Ravindra Pawar', NULL, NULL, NULL, NULL, '2024-03-23 13:01:32', '2024-03-23 13:01:32'),
(7, 0, 'Mr. V. N. Jha', NULL, NULL, NULL, NULL, '2024-03-23 13:05:36', '2024-03-23 13:05:36'),
(8, 0, 'Mr. Santosh Thakur', NULL, NULL, NULL, NULL, '2024-04-18 13:35:32', '2024-04-18 13:35:32'),
(9, 0, 'Milind Deodhar', NULL, NULL, NULL, NULL, '2024-04-18 13:35:55', '2024-04-18 13:35:55'),
(10, 0, 'Tapasya School of Classical Dance & Music', NULL, NULL, NULL, NULL, '2024-04-18 13:38:22', '2024-04-18 13:38:22'),
(11, 0, 'Bhavya shetty', NULL, NULL, NULL, NULL, '2024-04-20 06:29:00', '2024-04-20 06:29:00');

-- --------------------------------------------------------

--
-- Table structure for table `customer_addresses`
--

CREATE TABLE `customer_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `locality` varchar(255) DEFAULT NULL,
  `address_line1` varchar(255) DEFAULT NULL,
  `landmark` varchar(255) DEFAULT NULL,
  `road` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `near` varchar(255) DEFAULT NULL,
  `opposite` varchar(255) DEFAULT NULL,
  `pin_code` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_addresses`
--

INSERT INTO `customer_addresses` (`id`, `customer_id`, `contact`, `email`, `locality`, `address_line1`, `landmark`, `road`, `street`, `near`, `opposite`, `pin_code`, `created_at`, `updated_at`) VALUES
(1, 1, '8380828913', NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, '2024-03-09 10:06:39', '2024-03-09 10:06:39'),
(2, 2, '9004690707', NULL, NULL, 'Off Bhatia Hall, Veer Sawarkar Garden, Borivali West', NULL, NULL, NULL, NULL, NULL, NULL, '2024-03-17 08:28:55', '2024-03-17 08:28:55'),
(3, 3, '9969954646', NULL, NULL, 'Flat No. 13/304,\r\nSonam Mayuresh CHS LTD, \r\nMira Bhayander Road, \r\nOld Gold Nest Ph II, \r\nBhayander East.', NULL, NULL, NULL, NULL, NULL, NULL, '2024-03-23 12:40:58', '2024-03-23 12:40:58'),
(4, 4, '9699899666   8879379566', NULL, NULL, 'Flat No. A/904, \r\nBlue Oasis Tower-I, \r\nMahavir Nagar, \r\nBlue Empire Complex, \r\nKandivali West.', NULL, NULL, NULL, NULL, NULL, NULL, '2024-03-23 12:45:32', '2024-03-23 12:45:32'),
(5, 5, '8169457562    8097799939', NULL, NULL, 'Flat No. 203/63-I, \r\nNavgrah CHS, \r\nPoonam Sagar Complex, \r\nMira Road,', NULL, NULL, NULL, NULL, NULL, NULL, '2024-03-23 12:51:22', '2024-03-23 12:51:22'),
(6, 6, '8779169579', NULL, NULL, 'Flat No. 501, \r\nAmogh Bldg, \r\nCharkop, \r\nP. F. Bhavan, \r\nLane To Punjab Natioanl Bank, \r\nKabndivali West.', NULL, NULL, NULL, NULL, NULL, NULL, '2024-03-23 13:01:32', '2024-03-23 13:01:32'),
(7, 7, '9322284044', NULL, NULL, 'Shop No. 5, \r\nMandakini CHS, \r\nRawalpada, \r\nS. V. road, \r\nDahisar East.', NULL, NULL, NULL, NULL, NULL, NULL, '2024-03-23 13:05:36', '2024-03-23 13:05:36'),
(8, 1, '8380828913', NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, '2024-03-30 17:24:23', '2024-03-30 17:24:23'),
(9, 1, '1234567890', NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, '2024-03-30 17:24:43', '2024-03-30 17:24:43'),
(10, 8, '8605537795', NULL, NULL, 'Flat No. A/101, \r\nSiddhant Apt, \r\nZenda Khana, \r\nVasqqai West.', NULL, NULL, NULL, NULL, NULL, NULL, '2024-04-18 13:35:32', '2024-04-18 13:35:32'),
(11, 9, '9892104647', NULL, NULL, 'Flat No. 3 - 105,\r\nAkshayanand, \r\nShailendra Nagar, S.V. Road Dahisar Post Office,\r\nDahisar East.', NULL, NULL, NULL, NULL, NULL, NULL, '2024-04-18 13:35:55', '2024-04-18 13:35:55'),
(12, 10, '9869458290', NULL, NULL, 'Flat No. D/450, \r\nUjjwala Bhavishay 4,\r\nCharkop, Sector Noi. 4, \r\nKandivali West.', NULL, NULL, NULL, NULL, NULL, NULL, '2024-04-18 13:38:22', '2024-04-18 13:38:22'),
(13, 11, '9930432957', NULL, NULL, 'Flat no. E-505, Unique Aurum chs,\r\nPoonam Garden, S.K. Stone, \r\nMira Road.', NULL, NULL, NULL, NULL, NULL, NULL, '2024-04-20 06:29:00', '2024-04-20 06:29:00');

-- --------------------------------------------------------

--
-- Table structure for table `customer_payments`
--

CREATE TABLE `customer_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_address_id` int(11) NOT NULL,
  `finance_year` varchar(255) DEFAULT NULL,
  `invoice_no` int(11) DEFAULT NULL,
  `invoice_date` varchar(100) DEFAULT NULL,
  `paid_amount` int(11) NOT NULL DEFAULT 0,
  `payment_mode` enum('Cash','Cheque','Bank Transfer','Card Payment','Online','UPI') DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `payment_reference` varchar(255) DEFAULT NULL,
  `payment_disussion` int(11) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `is_cancelled` enum('0','1') NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_payments`
--

INSERT INTO `customer_payments` (`id`, `customer_id`, `customer_address_id`, `finance_year`, `invoice_no`, `invoice_date`, `paid_amount`, `payment_mode`, `bank_name`, `payment_reference`, `payment_disussion`, `remark`, `is_cancelled`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 2, 2, '23-24', 2654, '2024-03-17', 0, NULL, '', '', 5, '', '0', 5, 5, '2024-03-17 08:51:38', '2024-04-19 08:12:36'),
(2, 3, 3, '23-24', 3746, '2024-03-24', 0, NULL, '', '', NULL, '', '0', 8, 8, '2024-03-23 12:42:48', '2024-03-23 12:42:48'),
(3, 4, 4, '23-24', 3745, '2024-03-24', 0, NULL, '', '', NULL, '', '0', 8, 8, '2024-03-23 12:47:20', '2024-03-23 12:47:20'),
(4, 5, 5, '23-24', 3744, '2024-03-24', 0, NULL, '', '', NULL, '', '0', 5, 5, '2024-03-23 12:57:03', '2024-03-23 12:57:03'),
(5, 6, 6, '23-24', 3747, '2024-03-24', 0, NULL, '', '', NULL, '', '0', 5, 5, '2024-03-23 13:02:24', '2024-03-23 13:02:24'),
(6, 7, 7, '23-24', 3743, '2024-03-24', 0, NULL, '', '', NULL, '', '0', 5, 5, '2024-03-23 13:06:35', '2024-03-23 13:06:35'),
(7, 1, 1, '24-25', 123, '10/04/2024', 0, NULL, '', 'test', 1, 'Test', '1', 1, 1, '2024-04-03 17:03:13', '2024-04-11 17:24:51'),
(8, 8, 10, '24-25', 49, '19/04/2024', 0, NULL, '', '', NULL, '', '0', 8, 1, '2024-04-18 13:36:29', '2024-04-20 07:17:09'),
(9, 9, 11, '24-25', 370, '19/04/2024', 0, NULL, '', '', 5, '', '0', 4, 1, '2024-04-18 13:37:09', '2024-04-20 07:15:42'),
(10, 10, 12, '24-25', 365, '19/04/2024', 0, NULL, '', '', NULL, '', '0', 8, 1, '2024-04-18 13:39:01', '2024-04-20 07:16:41'),
(11, 11, 13, '24-25', 1, '20/04/2024', 0, NULL, '', '', NULL, '', '0', 5, 1, '2024-04-20 06:31:34', '2024-04-20 07:17:46');

-- --------------------------------------------------------

--
-- Table structure for table `customer_services`
--

CREATE TABLE `customer_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `service_id` text NOT NULL,
  `contract_from` varchar(200) DEFAULT NULL,
  `contract_to` varchar(200) DEFAULT NULL,
  `contract` varchar(500) DEFAULT NULL,
  `period` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `gst` int(11) DEFAULT 0,
  `gst_cal_amount` int(11) NOT NULL DEFAULT 0,
  `gst_amount` int(11) DEFAULT 0,
  `remark` text DEFAULT NULL,
  `service_due_text` varchar(1000) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_services`
--

INSERT INTO `customer_services` (`id`, `payment_id`, `category_id`, `service_id`, `contract_from`, `contract_to`, `contract`, `period`, `price`, `quantity`, `amount`, `gst`, `gst_cal_amount`, `gst_amount`, `remark`, `service_due_text`, `status`, `created_at`, `updated_at`) VALUES
(10, 2, 3, 'General Disinfestations', '2024-03-24', '2025-03-23', NULL, '1 Year', 1500, 1, 1500, 0, 0, 1500, '3 Major Services', '2nd- July, 3rd- November', '1', '2024-03-23 12:42:48', '2024-03-23 12:42:48'),
(11, 3, 3, 'General Disinfestations', '2024-03-24', '2025-03-23', NULL, '1 Year', 2200, 1, 2200, 18, 396, 2596, '3 Major Services', '2nd- July, 3rd- November', '1', '2024-03-23 12:47:20', '2024-03-23 12:47:20'),
(12, 4, 3, 'General Disinfestations', '2024-03-24', '2025-03-23', NULL, '1 Year', 2000, 1, 2000, 0, 0, 2000, '3 Major Services', '2nd July, 3rd November', '1', '2024-03-23 12:57:03', '2024-03-23 12:57:03'),
(13, 5, 3, 'General Disinfestations', '2024-03-24', '2025-03-23', NULL, '1 Year', 2500, 1, 2500, 18, 450, 2950, '3 Major Services', '2nd July, 3rd November', '1', '2024-03-23 13:02:24', '2024-03-23 13:02:24'),
(14, 6, 3, 'General Disinfestations', '2024-03-24', '2025-03-23', NULL, '1 Year', 2000, 1, 2000, 18, 360, 2360, '3 Major Services', '2nd July, 3rd November', '1', '2024-03-23 13:06:35', '2024-03-23 13:06:35'),
(16, 7, 0, 'Rodent Control', '02/04/2024', '02/04/2024', '1000', '3    year', 100, 1, 100, 18, 18, 118, 'test', 'test', '1', '2024-04-03 17:03:43', '2024-04-03 17:03:43'),
(21, 1, 0, 'General Disinfestations', '2024-03-17', '2025-03-16', '5000', '1 Year', 5000, 1, 5000, 18, 900, 5900, '', '2nd June, 3rd September, 4th December', '1', '2024-04-19 08:12:36', '2024-04-19 08:12:36'),
(29, 9, 0, 'General Disinfestations', '19/04/2024', '18/04/2025', NULL, '1 Year', 3000, 1, 3000, 0, 0, 3000, '', '2nd August, 3rd December', '1', '2024-04-20 07:15:42', '2024-04-20 07:15:42'),
(31, 10, 0, 'General Disinfestations', '19/04/2024', '18/04/2024', NULL, '1 Year', 3800, 1, 3800, 18, 684, 4484, '3 Major Services', '2nd August, 3rd December', '1', '2024-04-20 07:16:41', '2024-04-20 07:16:41'),
(32, 8, 0, 'General Disinfestations', '19/04/2024', '17/04/2025', NULL, '1 Year', 2000, 1, 2000, 18, 360, 2360, '3 Major Services', '2nd August, 3rd December', '1', '2024-04-20 07:17:09', '2024-04-20 07:17:09'),
(33, 11, 0, 'General Disinfestations', '20/04/2024', '19/04/2025', '2000', '1 Year', 2000, 1, 2000, 18, 360, 2360, '3 Major Services', '2nd August, 3rd December', '1', '2024-04-20 07:17:46', '2024-04-20 07:17:46');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `services` varchar(255) DEFAULT NULL,
  `address_line1` text DEFAULT NULL,
  `area_sq_ft` varchar(255) DEFAULT NULL,
  `booked_by` int(11) DEFAULT NULL,
  `entire` varchar(255) DEFAULT NULL,
  `value_of_contact` varchar(255) DEFAULT NULL,
  `payment_received` varchar(255) DEFAULT NULL,
  `balance_payment` varchar(255) DEFAULT NULL,
  `payment_mode` varchar(255) DEFAULT NULL,
  `concern_person` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `status` enum('Open','Close','Converted') NOT NULL DEFAULT 'Open',
  `appointment_date` datetime DEFAULT NULL,
  `type` enum('New','Renewal','Society') NOT NULL DEFAULT 'New',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `first_name`, `contact`, `email`, `attachment`, `services`, `address_line1`, `area_sq_ft`, `booked_by`, `entire`, `value_of_contact`, `payment_received`, `balance_payment`, `payment_mode`, `concern_person`, `created_by`, `updated_by`, `remark`, `status`, `appointment_date`, `type`, `created_at`, `updated_at`) VALUES
(3, 'Umesh Mundokar', '981749638', NULL, 'enquiry_1706270694.wav', '', 'Dahisar Mandakini Chs .C-203,\r\nRawalpada , Nera 298 Last Bus Stop ,\r\nDahisar East', '2 bhk', 1, 'Inside', '2950', NULL, NULL, NULL, NULL, 1, 1, NULL, 'Converted', NULL, 'New', '2024-01-26 09:45:08', '2024-01-28 08:53:29'),
(5, 'khushbu', '365612326', NULL, NULL, '1', 'dahisar east', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'ghfh', 'Converted', NULL, 'New', '2024-01-28 09:22:34', '2024-01-28 09:23:00'),
(6, 'Sneha', '1223264', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Finalised in 1200', 'Converted', NULL, 'New', '2024-02-06 15:26:03', '2024-02-06 15:26:49'),
(7, 'Kanchan Mundokar', '9819749638', NULL, NULL, '2', 'Mandakini c 203 rawal pada Dahisar east', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'Converted', NULL, 'New', '2024-02-10 19:41:16', '2024-02-18 10:55:28'),
(8, 'Kanchan Mundokar', '9819749638', NULL, NULL, '2', 'c 203 rawal pada Dahisar east', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 'Close', NULL, 'New', '2024-02-10 19:42:28', '2024-03-22 15:19:02'),
(9, 'Abhishek Gupta', '7700035836', NULL, NULL, '2', 'Shubaha Nagar, Jogeshewari East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'test', 'Close', '2024-03-07 21:51:00', 'New', '2024-03-06 09:37:37', '2024-03-22 15:11:44'),
(10, 'Rakesh Vishwakarma', '8767923554', NULL, NULL, '4', 'Room No. 3, Chagan Daga Chawl, Dahisar East', '180', 1, '2 BHK', '2500', NULL, NULL, NULL, NULL, 1, 1, NULL, 'Converted', '2024-03-07 21:48:00', 'New', '2024-03-07 16:17:32', '2024-03-07 16:18:05'),
(14, 'Khushi', '9852636565', 'admin@admin.com', NULL, '2', 'Ashokvan Dahisar East', '1 Bhk', 5, NULL, '2600', NULL, NULL, NULL, NULL, 1, 1, 'done', 'Close', NULL, 'New', '2024-03-17 08:23:37', '2024-03-22 15:16:53'),
(15, 'Akshita Apartment CHS', '8944522636', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'Open', NULL, 'New', '2024-03-17 08:58:29', '2024-03-17 08:58:29'),
(16, 'Test', '1234567890', NULL, NULL, '', 'Test Address', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'Open', NULL, 'New', '2024-03-19 18:21:13', '2024-03-19 18:21:59'),
(17, 'Mr. Pritam Parab', '922368355', NULL, NULL, '2', 'Flat No. A/203, \r\nJai Devki CHS LTD, Lotus Hospita, \r\nBorivali West.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:12:03', '2024-03-22 06:12:03'),
(18, 'Mr. Paresh Mehta', '9223395792', NULL, NULL, '5', 'Flat No. 42,4thn Floor, \r\nKora Kendra, Kora Kendra Flyover, \r\nBorivali West.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:17:08', '2024-03-22 06:17:08'),
(19, 'Subrata Mahadani', '9320894357', NULL, NULL, '2', 'Flat No. 1601, Dosti Ambrosia CHS, Dosti Acress CHS, S.N. Road, Wadala East.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'He Call After Shifting', 'Open', NULL, 'Renewal', '2024-03-22 06:17:21', '2024-03-22 06:17:21'),
(20, 'vedant patil', '9730049775', NULL, NULL, '2', 'A - 1102 Runwal symphony, vakola pippline road santacruz east', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:18:21', '2024-03-22 06:18:21'),
(21, 'SANDRA ROSE', '983397660', NULL, NULL, '', 'BORIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, 'Open', NULL, 'New', '2024-03-22 06:19:06', '2024-03-22 06:19:06'),
(22, 'Indian Bank', '7738152044', NULL, NULL, '4', 'Ashokvan, \r\nGurukul Universal School Compus, \r\nBorivali East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, '121', 'Open', NULL, 'Renewal', '2024-03-22 06:19:58', '2024-03-22 15:15:32'),
(23, 'Agrawal', '9969124700', NULL, NULL, '4', 'Flat No. 1104, \r\nKent Garden CHS, \r\nBorivali West', '1200 sq ft', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:21:36', '2024-03-22 10:07:24'),
(24, 'Rekha Charles', '9819152747', NULL, NULL, '5', 'Flat No. 701, Natty Villa, I.C. Colony, Holly Cross Road, Borivali West.', NULL, NULL, NULL, '2596', NULL, NULL, NULL, NULL, 5, 5, 'She call in april 1st week', 'Open', NULL, 'Renewal', '2024-03-22 06:23:07', '2024-03-22 06:23:07'),
(25, 'Mr. Shinde', '9969014355', NULL, NULL, '2', 'Flat No. A3/201, \r\nOmkar CHS, Chikuwadi, \r\nBorivali West', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:23:34', '2024-03-22 06:23:34'),
(26, 'Suhdir Pawar', '9819537493', NULL, NULL, '4', '4 Riddhi niwas Shankar Mandir Jagerdev Compound Borivali East', '1 room', 7, NULL, '2124', NULL, NULL, NULL, NULL, 4, 4, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:26:12', '2024-03-22 06:26:12'),
(27, 'Mr. Jignesh', '8169043890', NULL, NULL, '2', NULL, NULL, NULL, '2 BHK', '2360', NULL, NULL, NULL, NULL, 5, 5, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:27:19', '2024-03-22 06:27:19'),
(28, 'Mr. Jignesh', '8169043890', NULL, NULL, '2', NULL, NULL, NULL, '2 BHK', '2360', NULL, NULL, NULL, NULL, 5, 5, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:27:19', '2024-03-22 06:27:19'),
(29, 'Mr. Manoj Mehta', '9867958120', NULL, NULL, '2', 'Flat No. 105, b Wing, \r\nSai Krupa CHS, \r\nPlot No. AMC-3, \r\nGorai II, \r\nS. T. Rock, \r\nBorivali West.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, 'mjhkokk', 'Open', '2024-03-23 22:52:00', 'Renewal', '2024-03-22 06:28:02', '2024-03-22 15:22:53'),
(30, 'Mrs. Harshi Shah', '9819853825', NULL, NULL, '2', 'Flat No. F/84, Gaoutam Nagar, \r\nL. T. Road, \r\nShree Sagar Hotel, \r\nBorivali West.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:31:03', '2024-03-22 06:31:03'),
(31, 'Sunil Devlekar', '9892019122', NULL, NULL, '', 'Flat No. A-603, Shree Ganesh CHS LTD. Rajendra Nagar, Kastubh Platinum, Off Datta Pada Road, \r\nNext To East - West Flyover, Borivali East.', '3 BHK', NULL, NULL, '2596', NULL, NULL, NULL, NULL, 5, 5, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:32:58', '2024-03-22 06:32:58'),
(32, 'Mr. Raaj Thakkar', '9321551757', NULL, NULL, '2', 'Flat No/. 1501, A, \r\nSiddhi Vinayak apt, \r\nChikuwadi,\r\nPhoenix Hospita, \r\nBorivali West.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:35:27', '2024-03-22 06:35:27'),
(33, 'Varsha J. Patel', '9029538776', NULL, NULL, '2', NULL, '2 bhk', NULL, NULL, '2200', NULL, NULL, NULL, NULL, 5, 5, 'She Call After Holi', 'Open', NULL, 'Renewal', '2024-03-22 06:37:27', '2024-03-22 06:37:27'),
(34, 'Mr. Yogesh', '9323179841', NULL, NULL, '4', 'Flat No.904, \r\nKandivali Priti Sangam CJ+HS, \r\nSai Baba Mandir Road, \r\nBorivali West', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:45:04', '2024-03-22 06:45:04'),
(35, 'Mr. R. S. Garg', '9869934351', NULL, NULL, '2', 'Flat No. B/3/198, \r\nGanjawala Apt, \r\nBhagat Singh Nursing Home, \r\nPetrol Pump, \r\nBorivali West,', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-03-22 06:48:07', '2024-03-22 06:48:07'),
(36, 'Mrs. Cindretla Thema', '8850678375', NULL, NULL, '2', 'Flat No. A/306, \r\nVini Garden I, \r\nShivaji Nagar, Chandragi CHS, \r\nBorivali West.', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', '2024-03-31 12:41:00', 'Renewal', '2024-03-31 07:12:49', '2024-03-31 07:12:49'),
(37, 'B P VITTHAL', '9769636884', NULL, NULL, '2', 'GOREGAON WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 06:13:08', '2024-04-04 06:47:40'),
(38, 'DIPEN', '8879391234', NULL, NULL, '2', 'BHAYANDER WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 06:17:26', '2024-04-04 06:48:11'),
(39, 'SHIRISH PALKAR', '9920051026', NULL, NULL, '2', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 06:21:53', '2024-04-04 07:09:26'),
(40, 'DIWAKAR NARINREKAR', '9869529479', NULL, NULL, '2', 'MALAD WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'HE CALL IN HALF & HOUR', 'Open', NULL, 'Renewal', '2024-04-04 06:24:26', '2024-04-04 06:46:05'),
(41, 'ANIL GAWANKAR', '9820114948', NULL, NULL, '2', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 06:27:15', '2024-04-04 06:47:19'),
(42, 'KAMLESH', '9769769657', NULL, NULL, '2', 'KANDIVALI EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 06:31:18', '2024-04-04 06:48:33'),
(43, 'parkar', '7710958933', NULL, NULL, '4', 'Jogeshwari west', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'S.OFF ( MSG SENT)', 'Open', NULL, 'Renewal', '2024-04-04 06:46:59', '2024-04-04 06:46:59'),
(44, 'GOPAL SHARMA', '9987376088', NULL, NULL, '2', 'MALAD WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'NOT NEED', 'Open', '2024-04-04 12:19:00', 'Renewal', '2024-04-04 06:50:49', '2024-04-04 06:50:49'),
(45, 'NILIMA RAI REMNULKAR', '9819512339', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CNR MSG SENT', 'Open', '2024-04-04 12:22:00', 'Renewal', '2024-04-04 06:54:35', '2024-04-04 06:54:35'),
(46, 'SHENOY', '8454958703', NULL, NULL, '2', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'SHE WILL CALL', 'Open', NULL, 'Renewal', '2024-04-04 06:55:30', '2024-04-04 06:55:30'),
(47, 'MR. S. BHATTACHARYA', '9930869774', NULL, NULL, '2', 'MALAD EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'NEXT MONTH CALL', 'Open', '2024-04-04 12:31:00', 'Renewal', '2024-04-04 07:01:49', '2024-04-04 07:01:49'),
(48, 'VIJAY GHATGE', '9867051208', NULL, NULL, '2', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'BUSY MSG SENT', 'Open', NULL, 'New', '2024-04-04 07:02:01', '2024-04-04 07:02:01'),
(49, 'HEMAL PARIKH', '9820690153', NULL, NULL, '2', 'KANDIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL AFTER 10 DAYS', 'Open', '2024-04-14 23:00:00', 'Renewal', '2024-04-04 07:04:52', '2024-04-04 07:11:41'),
(50, 'MR. JAYANTIBHAI SHAH', '9324638255', NULL, NULL, '2', 'MALAD E', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'AFTER 15 TH APRIL CALL', 'Open', '2024-04-04 12:33:00', 'Renewal', '2024-04-04 07:06:16', '2024-04-04 07:06:16'),
(51, 'CHARULATA JOTHAN', '9022252789', NULL, NULL, '2', 'GOREGAON WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'DONE 13/04/24 - 4PM', 'Open', NULL, 'Renewal', '2024-04-04 07:08:29', '2024-04-04 07:08:29'),
(52, 'KAUSHIK SURVE', '9920992005', NULL, NULL, '4', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 07:14:39', '2024-04-04 07:14:39'),
(53, 'MR. SAMIR KARMARKAR', '9892906095', 'sneha@begon.com', NULL, '2', 'MIRA ROAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'NEXT WEEK CALL', 'Open', '2024-04-04 12:47:00', 'Renewal', '2024-04-04 07:17:57', '2024-04-04 07:17:57'),
(54, 'PRABHAKAR KATAKHAND', '9967248939', NULL, NULL, '2', 'NALASOPARA EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CUT CUT MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 07:19:02', '2024-04-04 07:19:02'),
(55, 'MR SUDHIR BHARWAD', '8169462073', NULL, NULL, '2', 'FLAT NO 703,\r\nORCHID PLAZA,\r\nDAHISAR E', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CALL CUT (MSG SENT)', 'Open', '2024-04-04 12:50:00', 'Renewal', '2024-04-04 07:20:40', '2024-04-04 07:22:36'),
(56, 'SANDEEP SHAH', '9820070149', NULL, NULL, '2', 'BHAYANDER WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 07:23:27', '2024-04-04 07:23:27'),
(57, 'RAJARAM KADAM', '9833806168', NULL, NULL, '2', 'BORIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'DONE BY OTHER', 'Open', NULL, 'Renewal', '2024-04-04 07:27:02', '2024-04-04 07:27:02'),
(58, 'BIJIAL PATHAK', '9322887442', 'sneha@begon.com', NULL, '2', 'B-1601/1602,\r\nTHYME CHS,\r\nBORIVALI WEST.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'BUSY', 'Open', '2024-04-04 12:56:00', 'Renewal', '2024-04-04 07:27:22', '2024-04-04 07:27:22'),
(59, 'MIRA ILLE', '7718961437', NULL, NULL, '2', 'BORIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 07:29:51', '2024-04-04 07:29:51'),
(60, 'K L S RAO', '9322060184', NULL, NULL, '4', 'BORIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 07:38:32', '2024-04-04 07:38:32'),
(61, 'TURAKHIA VISION CARE', '9167454550', NULL, NULL, '21,22', 'GOREGAON WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 07:40:27', '2024-04-04 07:40:27'),
(62, 'MR. KULKARNI', '9869447301', 'sneha@begon.com', NULL, '4', 'FLAT NO-A/704,\r\nAVIRAHI CLASSIQUE CHS,\r\nDAHISAR EAST.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CALL CUT (CALL LATER)', 'Open', '2024-04-04 13:10:00', 'Renewal', '2024-04-04 07:40:47', '2024-04-04 07:40:47'),
(63, 'AAKASH SWETTA', '9769950397', NULL, NULL, '2', 'BORIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'SHE WILL CALL IF NEED', 'Open', NULL, 'Renewal', '2024-04-04 07:44:11', '2024-04-04 07:44:11'),
(64, 'MR. J.P SHARMA', '8104211578', 'sneha@begon.com', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CALL LATER', 'Open', '2024-04-04 13:14:00', 'Renewal', '2024-04-04 07:44:53', '2024-04-04 07:44:53'),
(65, 'SARVANAND', '9821341699', 'sneha@begon.com', NULL, '4', 'FLAT NO -D/2103,\r\nOBEROI SPLENDOR CHS,\r\nANDHERI E', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CALL IN EVENING', 'Open', '2024-04-04 13:16:00', 'Renewal', '2024-04-04 07:46:54', '2024-04-04 07:46:54'),
(66, 'MR. K.K SHETTY', '9820568087', NULL, NULL, '2', 'FLAT NO -B-09,\r\nCHANDRAGIRI CHS,\r\nBORIVALI W.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'MONTH END CALL', 'Open', '2024-04-04 13:28:00', 'Renewal', '2024-04-04 07:58:40', '2024-04-04 07:58:40'),
(67, 'HARSHA SHANGARI', '8850157596', 'sneha@begon.com', NULL, '2', 'FLAT NO -A/103,\r\nAKSHITA AVENUE,\r\nBORIVALI W.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'S.OFF (MSG SENT)', 'Open', '2024-04-04 13:31:00', 'Renewal', '2024-04-04 08:02:53', '2024-04-04 08:02:53'),
(68, 'MR. NARENDRA SHETH', '9223432125', 'sneha@begon.com', NULL, '2', 'FLAT NO.A/08,\r\nMULGI NAGAR 2,\r\nBORIVALI WEST.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CNR MSG SENT', 'Open', '2024-04-04 13:36:00', 'Renewal', '2024-04-04 08:08:30', '2024-04-04 08:08:30'),
(69, 'MANISHA RANE', '9769300373', NULL, NULL, '2,4', 'BORIVALI EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 08:11:59', '2024-04-04 08:11:59'),
(70, 'MR. SURESH MEHTA', '8591820739', 'sneha@begon.com', NULL, '2', 'FLAT NO. 501,\r\nKHIMRAJ BLDG,\r\nANDHERI WEST.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'WRONG NO', 'Open', '2024-04-04 13:42:00', 'Renewal', '2024-04-04 08:12:38', '2024-04-04 08:12:38'),
(71, 'VISHAL VERMA', '9833878890', NULL, NULL, '4', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL IN EVNG SHE SAID', 'Open', NULL, 'Renewal', '2024-04-04 08:14:54', '2024-04-04 08:14:54'),
(72, 'MR. TUSHAR BHATT', '9167722043', 'sneha@begon.com', NULL, '4', 'flat no. B-12/301,\r\nHAROM CHS,\r\nDAHISAR E.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'BUSY (MSG SENT)', 'Open', '2024-04-04 13:46:00', 'Renewal', '2024-04-04 08:16:44', '2024-04-04 08:16:44'),
(73, 'SANTOSH VOTAWAT', '9930889939', NULL, NULL, '2', 'DADAR WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 08:17:26', '2024-04-04 08:17:26'),
(74, 'RAJNESG PAWASKAR', '773811011', NULL, NULL, '2', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 08:20:10', '2024-04-04 08:20:10'),
(75, 'SHARVARI S.', '9833723577', 'sneha@begon.com', NULL, '5', 'FLAT NO-A/8,\r\nCHANDRAGIRI CHS,\r\nBORIVALI W.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CNR MSG SENT', 'Open', '2024-04-04 13:50:00', 'Renewal', '2024-04-04 08:21:25', '2024-04-04 08:21:25'),
(76, 'AMAR', '9820330796', NULL, NULL, '4', 'ANDHERI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-04 08:22:44', '2024-04-04 08:22:44'),
(77, 'MR. KEDAR B SHRINGARPURE', '9833160244', 'sneha@begon.com', NULL, '5', 'FLAT NO -D9,\r\nSADANAND PARK CHS,\r\nBORIVALI W.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CNR MSG SENT', 'Open', '2024-04-04 13:53:00', 'Renewal', '2024-04-04 08:24:15', '2024-04-04 08:24:15'),
(78, 'PREMA YADAV', '9987516486', NULL, NULL, '2', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG', 'Open', NULL, 'Renewal', '2024-04-04 08:25:48', '2024-04-04 08:25:48'),
(79, 'Anita Parihar', '8600176634', NULL, NULL, '', 'Flat No. 403/C \r\nKandivali East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', '2024-04-05 11:18:00', 'Renewal', '2024-04-05 05:49:16', '2024-04-05 05:49:16'),
(80, 'CHIRAG DESAI', '9594937072', NULL, NULL, '2', 'KANDIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 06:15:35', '2024-04-05 06:15:35'),
(81, 'GAURAV BAGKAR', '9324771585', NULL, NULL, '2,4', 'KANDIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'S.OFF MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 06:17:43', '2024-04-05 06:17:43'),
(82, 'SHAH PRINTERS & STATIONERS', '8433555299', NULL, NULL, '2', 'KANDIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 06:20:05', '2024-04-05 06:20:05'),
(83, 'PARVEEN KHAN', '9167241930', NULL, NULL, '2', 'MIRA ROAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 06:23:13', '2024-04-05 06:23:13'),
(84, 'Mr. Kaushal Basa', '9930176694', NULL, NULL, '4', 'Flat No. B/46, \r\nGoregaon West', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 06:25:51', '2024-04-05 06:25:51'),
(85, 'VISION WELFARE INSTITU', '9833815215', NULL, NULL, '2,4', 'MIRA ROAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 06:26:42', '2024-04-05 06:26:42'),
(86, 'Vijay Singh', '9820462822', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'Call Cutt', 'Open', NULL, 'Renewal', '2024-04-05 06:26:51', '2024-04-05 06:26:51'),
(87, 'Mr. Vipul Parikh', '9167094824', NULL, NULL, '2', 'Flat No. B/403, \r\nLemont Apt. \r\nMalad East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 06:28:23', '2024-04-05 06:28:23'),
(88, 'Shobha Shidruk', '9167825269', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'CNR', 'Open', NULL, 'Renewal', '2024-04-05 06:29:05', '2024-04-05 06:29:05'),
(89, 'RITESH JHORE', '8652371195', NULL, NULL, '4', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 06:29:11', '2024-04-05 06:29:11'),
(90, 'MEGHA', '8369766328', NULL, NULL, '4', 'BORIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 06:32:19', '2024-04-05 06:32:19'),
(91, 'Mr. Kartik Sanghvi', '9920169050', NULL, NULL, '4', 'Flat No. A/1504, \r\nKadtyani Heights \r\nAndheri East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 06:35:17', '2024-04-05 06:35:17'),
(92, 'DEVANG BHATIA', '7021292695', NULL, NULL, '2', 'BORIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 06:40:34', '2024-04-05 06:40:34'),
(93, 'ESPA FEE PVT. LTD.', '9320024138', NULL, NULL, '2,4', 'ANDHERI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL CUT MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 06:45:09', '2024-04-05 06:45:09'),
(94, 'YAMINI TAPARIA', '9920192766', NULL, NULL, '4', 'MUMBAI CENTRAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 06:49:47', '2024-04-05 06:49:47'),
(95, 'SHOBHANA', '9820138580', NULL, NULL, '2', 'KANDIVALI EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'SHE WILL CALL WHEN NEED', 'Open', NULL, 'Renewal', '2024-04-05 06:54:26', '2024-04-05 06:54:26'),
(96, 'HEMA SHAH', '9820472218', NULL, NULL, '2', 'KANDIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 06:58:38', '2024-04-05 06:58:38'),
(97, 'Mr. Keyur Damani', '8104784931', NULL, NULL, '2', 'Flat No. B/6-303,\r\nMira Road', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 07:01:05', '2024-04-05 07:01:05'),
(98, 'SAMIR SHAH', '9819575879', NULL, NULL, '2', 'ANDHERI EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 07:01:07', '2024-04-05 07:01:07'),
(99, 'Mrs. Varsha Jhunjnuwala', '9820198027', NULL, NULL, '2', 'Flat No. B/305, \r\nSamarpan Bldg, \r\nMalad West', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 07:03:27', '2024-04-05 07:03:27'),
(100, 'RAJ SURANA', '9819408434', NULL, NULL, '2', 'GOREGAON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 07:04:04', '2024-04-05 07:04:04'),
(101, 'Everest fabricators & Anodising industries', '9820860294', NULL, NULL, '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'Call In May', 'Open', NULL, 'Renewal', '2024-04-05 07:06:08', '2024-04-05 07:06:08'),
(102, 'RAJESH', '9967720171', NULL, NULL, '4', 'GOREGAON EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'NO. NOT IN SERVICE', 'Open', NULL, 'Renewal', '2024-04-05 07:06:44', '2024-04-05 07:06:44'),
(103, 'Meeta Gala', '9769055070', NULL, NULL, '4,5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'She call if need', 'Open', NULL, 'Renewal', '2024-04-05 07:08:28', '2024-04-05 07:08:28'),
(104, 'VANDANA SINGH', '9833690353', NULL, NULL, '2', 'SANTACRUZ EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 07:10:45', '2024-04-05 07:10:45'),
(105, 'kamla mali', '7977608663', NULL, NULL, '4', 'Borivali west', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'Not need', 'Open', NULL, 'Renewal', '2024-04-05 07:13:23', '2024-04-05 07:13:23'),
(106, 'SAMRAT', '9322765652', NULL, NULL, '2', 'MIRA ROAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL AT 3PM', 'Open', NULL, 'Renewal', '2024-04-05 07:14:18', '2024-04-05 07:14:18'),
(107, 'Mr. Pradeep Gupta', '8591633823', NULL, NULL, '2', 'Flat No. 63/3A, \r\nKalpatru Estate \r\nAndheri East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 07:16:29', '2024-04-05 07:16:29'),
(108, 'AJAY GALA', '9967735614', NULL, NULL, '2', 'BORIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 07:18:37', '2024-04-05 07:18:37'),
(109, 'RAJESH DOKANIA', '9322123358', NULL, NULL, '2', 'KANDIVALI EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 07:21:42', '2024-04-05 07:21:42'),
(110, 'RUPALI JAMBAULIKAR', '9920825544', NULL, NULL, '2', 'MALAD WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 07:24:35', '2024-04-05 07:24:35'),
(111, 'HIREN BORICHA', '9819973713', NULL, NULL, '2', 'KANDIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'NOT NEED', 'Open', NULL, 'Renewal', '2024-04-05 07:26:31', '2024-04-05 07:26:31'),
(112, 'RITESH', '7738079070', NULL, NULL, '2', 'MIRA ROAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'WHEN NEED HE CALL', 'Open', NULL, 'Renewal', '2024-04-05 07:29:35', '2024-04-05 07:29:35'),
(113, 'kamla mali', '7977608663', NULL, NULL, '4', 'Borivali west', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'Not need', 'Open', NULL, 'Renewal', '2024-04-05 07:30:35', '2024-04-05 07:30:35'),
(114, 'RAJENDRA SHAHANE', '9664636654', NULL, NULL, '2', 'KANDIVALI EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 07:32:37', '2024-04-05 07:32:37'),
(115, 'AMIT RANE', '8796304323', NULL, NULL, '2', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, NULL, 'Open', NULL, 'Renewal', '2024-04-05 07:35:02', '2024-04-05 07:35:02'),
(116, 'Savio', '9820231368', NULL, NULL, '5', 'Borivali West', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'Not Need', 'Open', NULL, 'Renewal', '2024-04-05 07:38:36', '2024-04-05 07:38:36'),
(117, 'SMITA PANCHAL', '9167367271', NULL, NULL, '2', 'BORIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 07:39:47', '2024-04-05 07:39:47'),
(118, 'Tapan wadikar', '9833985998', NULL, NULL, '', 'Borivali east', NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'He Call', 'Open', NULL, 'Renewal', '2024-04-05 07:43:20', '2024-04-05 07:43:20'),
(119, 'L.V. PALANDE', '9372939081', NULL, NULL, '4', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, NULL, 'Open', NULL, 'Renewal', '2024-04-05 07:44:07', '2024-04-05 07:44:07'),
(120, 'PRITI GOHIL', '9819581216', NULL, NULL, '2', 'MIRA ROAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 07:45:45', '2024-04-05 07:45:45'),
(121, 'SAVIO DIAS', '9821959992', NULL, NULL, '2', 'BORIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 07:52:55', '2024-04-05 07:52:55'),
(122, 'UMESH AGARWAKL', '9967070899', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'SHE WILL CALL', 'Open', NULL, 'Renewal', '2024-04-05 07:55:39', '2024-04-05 07:55:39'),
(123, 'RISHIKA ACHARYA', '7738330647', NULL, NULL, '2', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL CUT MSG SENT', 'Open', NULL, 'Renewal', '2024-04-05 08:00:06', '2024-04-05 08:00:06'),
(124, 'Neville D\'souza', '9820132572', NULL, NULL, '2', 'Mira Road', NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '06/04/2024   6:30PM', 'Open', NULL, 'Renewal', '2024-04-05 08:04:13', '2024-04-05 08:04:13'),
(125, 'Radhe Maurya', '9967639556', NULL, NULL, '2', 'Flat No. I-62, \r\nMira Road', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 08:12:35', '2024-04-05 08:12:35'),
(126, 'Dr. Prajakta', '9969084988', NULL, NULL, '2', 'Flat No. C-102, \r\nMira Road', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 08:13:44', '2024-04-05 08:13:44'),
(127, 'Mr. Shitosh Sharma', '9867498460', NULL, NULL, '2', 'Flat No. A/401, \r\nMira Road', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 08:17:36', '2024-04-05 08:17:36'),
(128, 'Mr. Sanjay Dhawane', '9004448002', NULL, NULL, '2', 'Flat No. C-201, D/204\r\nMira Road', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 08:20:05', '2024-04-05 08:20:05'),
(129, 'Dinesh Haswani', '8693033369', NULL, NULL, '2', NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'He Call', 'Open', NULL, 'Renewal', '2024-04-05 08:24:02', '2024-04-05 08:24:02'),
(130, 'Dr. Himanshu Chitre', '9820720340', NULL, NULL, '2', 'Flat No. A-1004, \r\nGoregaon West', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 08:24:17', '2024-04-05 08:24:17'),
(131, 'Mr. Santosh', '9769496354', NULL, NULL, '2,4', 'Flat No. B-402, \r\nDeep Ratna CHS, \r\nAndheri West', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, NULL, 'Open', NULL, 'Renewal', '2024-04-05 08:27:31', '2024-04-05 08:27:31'),
(132, 'Dharmesh Solanki', '9371157332', NULL, NULL, '2', 'MIRA ROAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-07 05:40:38', '2024-04-07 05:40:38'),
(133, 'RAJNESG PAWASKAR', '7738110011', NULL, NULL, '2', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-07 05:44:42', '2024-04-07 05:44:42'),
(134, 'DHAWAL VORA', '8828447520', NULL, NULL, '2', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-07 05:47:50', '2024-04-07 05:47:50'),
(135, 'SANTOSH VOTAWAT', '9930889939', NULL, NULL, '2', 'DADAR WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-07 05:50:20', '2024-04-07 05:50:20'),
(136, 'AKASH SWETTA', '9769950397', NULL, NULL, '2', 'BORIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL CUT MSG SENT', 'Open', NULL, 'Renewal', '2024-04-07 06:03:44', '2024-04-07 06:03:44'),
(137, 'JOSVIN', '9619826613', NULL, NULL, '2', 'KANDIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'HE WILL CALL', 'Open', NULL, 'Renewal', '2024-04-07 06:11:44', '2024-04-07 06:11:44'),
(138, 'AARYAN SETH', '9867271037', NULL, NULL, '', 'KANDIVALI WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'SWITCH OFF MSG SENT', 'Open', NULL, 'Renewal', '2024-04-07 06:14:11', '2024-04-07 06:14:11'),
(139, 'HARSHAD PATIL', '9920349782', NULL, NULL, '2', 'BHAYANDER EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-07 06:16:52', '2024-04-07 06:16:52'),
(140, 'JENAL SHAH', '9819092076', NULL, NULL, '2', 'MALAD EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL CUT MSG SENT', 'Open', NULL, 'Renewal', '2024-04-07 06:19:41', '2024-04-07 06:19:41'),
(141, 'MANISH UPADHAY', '9653216373', NULL, NULL, '4', 'GOREGAON EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-07 06:22:04', '2024-04-07 06:22:04'),
(142, 'VISHAL VERMA', '9833878890', NULL, NULL, '4', 'DAHISAR EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-07 06:24:43', '2024-04-07 06:24:43'),
(143, 'SADHANA BANGADIWALA', '9821922994', NULL, NULL, '2', 'MALAD WEST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CALL BUSY MSG SENT', 'Open', NULL, 'Renewal', '2024-04-07 06:27:56', '2024-04-07 06:27:56'),
(144, 'Daksha Pandit', '7666975289', NULL, NULL, '2', 'Andheri West.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'She Call', 'Open', NULL, 'Renewal', '2024-04-07 06:31:36', '2024-04-07 06:31:36'),
(145, 'Ritu Rawal', '9820051401', NULL, NULL, '4', 'Andheri West.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'She Call', 'Open', NULL, 'Renewal', '2024-04-07 06:34:14', '2024-04-07 06:34:14'),
(146, 'Mary', '9004908763', NULL, NULL, '2', 'Dahisar West', NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'End of may', 'Open', NULL, 'Renewal', '2024-04-07 06:48:02', '2024-04-07 06:48:02'),
(147, 'V.N. Joshi', '9869244510', NULL, NULL, '2', 'Dahisar West.', NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'He Call', 'Open', NULL, 'Renewal', '2024-04-07 06:51:03', '2024-04-07 06:51:03'),
(148, 'VINCENT', '9167644938', 'sneha@begon.com', NULL, '2', 'MALAD (W)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CALL CUT( MSG SENT)', 'Open', '2024-04-11 12:45:00', 'Renewal', '2024-04-11 07:16:22', '2024-04-11 07:16:22'),
(149, 'PRAKASH JAIN', '9969468201', NULL, NULL, '4', 'MALAD (W)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CNR MSG SENT', 'Open', NULL, 'Renewal', '2024-04-11 07:20:18', '2024-04-11 07:20:18'),
(150, 'SUDARSHAN CHAVAN', '9833627029', NULL, NULL, '16,17', 'MALAD EAST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CNR', 'Open', NULL, 'Renewal', '2024-04-11 07:22:17', '2024-04-11 07:22:17'),
(151, 'RAVI NAIR', '9869552591', NULL, NULL, '2', 'FLAT NO-B/201,\r\nPRATHMESH HARMONY,\r\nOURLEM CHURCH,\r\nGAUTAM BUDDHA LANE,\r\nMALAD(W)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'CALL LATER', 'Open', NULL, 'Renewal', '2024-04-11 07:25:09', '2024-04-11 07:25:09'),
(152, 'SAVJI CHHEDA', '9892122146', NULL, NULL, '2', 'FLAT NO -D/504,\r\nPRATAP NAGAR,\r\nPUSHP PARK,\r\nMALAD EAST.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'AFTER 1 MONTH CALL', 'Open', NULL, 'Renewal', '2024-04-11 07:27:19', '2024-04-11 07:27:19'),
(153, 'HEMANGI KADAM', '9167939755', 'sneha@begon.com', NULL, '2', 'FLAT NO :B/42,\r\nSAHYADRI CHS,\r\nAARAY ROAD,\r\nGOREGAON (EAST)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 6, 'NOT NEED', 'Open', NULL, 'Renewal', '2024-04-11 07:32:04', '2024-04-11 07:32:04'),
(154, 'Jessy Sabu', '9892831619', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', '2024-04-18 02:19:00', 'Renewal', '2024-04-18 06:06:53', '2024-04-18 06:06:53'),
(155, 'Mr. Ram Chandra', '9821717121', NULL, NULL, '9', 'Andheri east', NULL, NULL, NULL, '4500', NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'New', '2024-04-18 06:28:02', '2024-04-18 06:28:02'),
(156, 'Mrs. Kranti Sawant', '9820969307', NULL, NULL, '2', 'Dahisar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-04-18 06:49:14', '2024-04-18 06:49:14'),
(157, 'Mr. Dilip Parab', '9819111217', NULL, NULL, '2', 'Dahisar East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-04-18 06:51:41', '2024-04-18 06:51:41'),
(158, 'Mr. Arvind Sane', '9820588095', NULL, NULL, '2', 'Andheri West', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', '2024-04-18 12:52:00', 'Renewal', '2024-04-18 07:22:52', '2024-04-18 07:22:52'),
(159, 'Anju Agrawal', '9920378788', NULL, NULL, '2', 'Andheri West', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-04-18 07:25:17', '2024-04-18 07:25:17'),
(160, 'Subhash Gupta', '9892564585', NULL, NULL, '2', 'Andheri East', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'switch off msg sent', 'Open', NULL, 'New', '2024-04-18 07:33:31', '2024-04-18 07:33:31'),
(161, 'Nair', '8355943640', NULL, NULL, '2', 'Mira Road', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'He call in 2 days', 'Open', NULL, 'Renewal', '2024-04-18 07:37:39', '2024-04-18 07:37:39'),
(162, 'Mr. Prakash', '9920321376', NULL, NULL, '2', 'Borivali West', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-04-18 07:38:20', '2024-04-18 07:38:20'),
(163, 'Mr Abhijit Chatokar', '9323546754', NULL, NULL, '2', 'Dahisar East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-04-18 07:40:26', '2024-04-18 07:40:26'),
(164, 'Dhiru Rathod', '9820516265', NULL, NULL, '2', 'Mira Road', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'CNR msg sent', 'Open', NULL, 'Renewal', '2024-04-18 07:41:37', '2024-04-18 07:41:37'),
(165, 'Charulata Jothan', '9022252789', NULL, NULL, '2', 'Goregaon West', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'cnr Msg sent', 'Open', NULL, 'Renewal', '2024-04-18 07:44:32', '2024-04-18 07:44:32'),
(166, 'Kavya', '9561412320', NULL, NULL, '2', 'Virar', '2bhk', 4, NULL, '1416', NULL, NULL, NULL, NULL, 4, 4, 'She will call', 'Open', NULL, 'New', '2024-04-18 07:46:32', '2024-04-18 07:46:32'),
(167, 'Ajay  Mehta', '8657189777', NULL, NULL, '2', 'Andheri East', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'call busy msg sent', 'Open', NULL, 'Renewal', '2024-04-18 07:52:31', '2024-04-18 07:52:31'),
(168, 'Sanjay Sarin', '9920095614', NULL, NULL, '2', 'Santacruz West', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'out of mumbai he will call when he comes', 'Open', NULL, 'Renewal', '2024-04-18 07:55:11', '2024-04-18 07:55:11'),
(169, 'Mr. Vikas Agarwal', '9819882070', NULL, NULL, '5', 'Goregaon East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-04-19 08:24:37', '2024-04-19 08:24:37'),
(170, 'Mrs, Anushu', '9004599997', NULL, NULL, '2', 'Goregaon East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-04-19 08:27:56', '2024-04-19 08:27:56'),
(171, 'dharmendra', '9224202778', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 'Open', '2024-04-19 13:57:00', 'Renewal', '2024-04-19 08:27:57', '2024-04-19 08:27:57'),
(172, 'Sagar Yadav', '9967697899', NULL, NULL, '', 'Andheri East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-04-19 08:29:27', '2024-04-19 08:29:27'),
(173, 'Tushar Sir', '9820460555', NULL, NULL, '2', NULL, NULL, 1, NULL, '3500', NULL, NULL, NULL, NULL, 5, 5, 'Call By Umesh Sir', 'Open', '2024-04-29 02:00:00', 'New', '2024-04-19 11:38:06', '2024-04-19 11:38:06'),
(174, 'dhanraj jethani', '8850612750', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'Busy', 'Open', NULL, 'Renewal', '2024-04-20 06:09:53', '2024-04-20 06:09:53'),
(175, 'sagar pithadia', '7506363755', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'S off', 'Open', NULL, 'Renewal', '2024-04-20 06:13:37', '2024-04-20 06:13:37'),
(176, 'prakash kaur', '9757309910', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 'Open', NULL, 'Renewal', '2024-04-20 06:19:02', '2024-04-20 06:19:02'),
(177, 'Mr. Nishant Jethi', '8898953333', NULL, NULL, '2', 'Flat No. B/604, \r\nLa Bellezza CHS, \r\nBorivali East.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'New', '2024-04-20 06:23:58', '2024-04-20 06:23:58'),
(178, 'Balkrishna Satam', '8355829005', NULL, NULL, '2', 'Bhayander East', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 'Open', NULL, 'Renewal', '2024-04-20 06:49:18', '2024-04-20 06:49:18'),
(179, 'Wilson Mascarenda', '9819551451', NULL, NULL, '2', 'Mira Road', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 'Open', NULL, 'Renewal', '2024-04-20 06:53:39', '2024-04-20 06:53:39'),
(180, 'Mr. Ashpak', '9664877860', NULL, NULL, '2', 'Malad West', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 8, NULL, 'Open', NULL, 'Renewal', '2024-04-20 06:57:38', '2024-04-20 06:57:38'),
(181, 'Mr Dilip L Ashar.', '8369183505', NULL, NULL, '2', 'Flat no.B-704 Vrajdham. Ram Baug lane. Off SVRd . Behind  Chamunda Garage . Borivali -west. Mumbai - 400092.', '1bhk', 4, NULL, '2000', NULL, NULL, NULL, NULL, 4, 4, 'Done on 20-04-2024', 'Open', NULL, 'New', '2024-04-20 06:58:55', '2024-04-20 06:58:55'),
(182, 'Savio', '9820231368', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, 'Borivali west', 'Open', NULL, 'Renewal', '2024-04-20 07:00:12', '2024-04-20 07:00:12'),
(183, 'Shirish palkar', '9920051026', NULL, NULL, '2', '403 shree krupa chs,\r\nRawalpada s.v Road Dahisar East.', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'Switch off msg sent', 'Open', NULL, 'Renewal', '2024-04-20 07:15:24', '2024-04-20 07:15:24'),
(184, 'Jay teli', '8149645768', NULL, NULL, '2', 'A 304 shiv shakti apt anand nagar near - vartak school vasai west.', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'Call not connected msg sent', 'Open', NULL, 'Renewal', '2024-04-20 07:25:00', '2024-04-20 07:25:00'),
(185, 'Vedant patil', '9730049775', NULL, NULL, '2', 'A 1102 Runwal symphony vakola pipeline road santacruz east', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'Switch off msg sent', 'Open', NULL, 'Renewal', '2024-04-20 07:30:22', '2024-04-20 07:30:22'),
(186, 'Mr. Nand kumar surve', '7276343445', NULL, NULL, '4', 'C 203 shree ji complex 2 moregaon near rajiv gandhi school nalasopara west', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'Call cut msg sent', 'Open', NULL, 'Renewal', '2024-04-20 07:33:11', '2024-04-20 07:33:11'),
(187, 'Mr. Vishal shrivastava', '6388021321', NULL, NULL, '4', '2001 summit chs aarey colony palms 2 near royal palm goregaon east.', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'Call cut msg sent', 'Open', NULL, 'Renewal', '2024-04-20 07:36:24', '2024-04-20 07:36:24'),
(188, 'Mrs. Priya Janbandhu', '9768134647', NULL, 'enquiry_1713598752.m4a', '2', 'Mandakini chs rawalpada 298 last bus stop dahisar east', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'Not need', 'Open', NULL, 'Renewal', '2024-04-20 07:39:12', '2024-04-20 07:39:12'),
(189, 'Mr. Mathew', '9821265461', NULL, NULL, '6', '215/6 Takshashila chs charkop sector no 2 near bmc school kandiwali west', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, 'Call not connected msg', 'Open', NULL, 'Renewal', '2024-04-20 07:43:40', '2024-04-20 07:43:40');

-- --------------------------------------------------------

--
-- Table structure for table `enquiry_recordings`
--

CREATE TABLE `enquiry_recordings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `enquiry_id` int(11) NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `enquiry_recordings`
--

INSERT INTO `enquiry_recordings` (`id`, `enquiry_id`, `file_name`, `created_at`, `updated_at`) VALUES
(27, 3, '1706270448_audio.wav', '2024-01-26 12:00:48', '2024-01-26 12:00:48'),
(28, 3, '1706270601_audio.wav', '2024-01-26 12:03:21', '2024-01-26 12:03:21'),
(29, 1, '1706325943_audio.wav', '2024-01-27 03:25:43', '2024-01-27 03:25:43'),
(30, 4, '1706431799_audio.wav', '2024-01-28 08:49:59', '2024-01-28 08:49:59'),
(31, 4, '1706431831_audio.wav', '2024-01-28 08:50:31', '2024-01-28 08:50:31'),
(32, 4, '1706431895_audio.wav', '2024-01-28 08:51:35', '2024-01-28 08:51:35'),
(34, 4, '1708365038_audio.wav', '2024-02-19 17:50:38', '2024-02-19 17:50:38'),
(36, 9, '1709828846_audio.wav', '2024-03-07 16:27:26', '2024-03-07 16:27:26'),
(38, 9, '1710665925_audio.wav', '2024-03-17 08:58:45', '2024-03-17 08:58:45'),
(40, 4, '1710666083_audio.wav', '2024-03-17 09:01:23', '2024-03-17 09:01:23'),
(41, 9, '1710741550_audio.wav', '2024-03-18 05:59:10', '2024-03-18 05:59:10'),
(42, 15, '1710741551_audio.wav', '2024-03-18 05:59:11', '2024-03-18 05:59:11'),
(52, 17, '1711087923_audio.wav', '2024-03-22 06:12:03', '2024-03-22 06:12:03'),
(53, 18, '1711088228_audio.wav', '2024-03-22 06:17:08', '2024-03-22 06:17:08'),
(54, 19, '1711088242_audio.wav', '2024-03-22 06:17:22', '2024-03-22 06:17:22'),
(55, 20, '1711088301_audio.wav', '2024-03-22 06:18:21', '2024-03-22 06:18:21'),
(56, 22, '1711088398_audio.wav', '2024-03-22 06:19:58', '2024-03-22 06:19:58'),
(57, 23, '1711088497_audio.wav', '2024-03-22 06:21:37', '2024-03-22 06:21:37'),
(58, 24, '1711088588_audio.wav', '2024-03-22 06:23:08', '2024-03-22 06:23:08'),
(59, 25, '1711088614_audio.wav', '2024-03-22 06:23:34', '2024-03-22 06:23:34'),
(60, 26, '1711088772_audio.wav', '2024-03-22 06:26:12', '2024-03-22 06:26:12'),
(61, 27, '1711088840_audio.wav', '2024-03-22 06:27:20', '2024-03-22 06:27:20'),
(62, 28, '1711088840_audio.wav', '2024-03-22 06:27:20', '2024-03-22 06:27:20'),
(63, 29, '1711088882_audio.wav', '2024-03-22 06:28:02', '2024-03-22 06:28:02'),
(64, 30, '1711089063_audio.wav', '2024-03-22 06:31:03', '2024-03-22 06:31:03'),
(65, 31, '1711089178_audio.wav', '2024-03-22 06:32:58', '2024-03-22 06:32:58'),
(66, 32, '1711089327_audio.wav', '2024-03-22 06:35:27', '2024-03-22 06:35:27'),
(67, 33, '1711089447_audio.wav', '2024-03-22 06:37:27', '2024-03-22 06:37:27'),
(68, 34, '1711089904_audio.wav', '2024-03-22 06:45:04', '2024-03-22 06:45:04'),
(69, 35, '1711090087_audio.wav', '2024-03-22 06:48:07', '2024-03-22 06:48:07'),
(73, 16, '1711124940_audio.wav', '2024-03-22 16:29:00', '2024-03-22 16:29:00'),
(74, 36, '1711869169_audio.wav', '2024-03-31 07:12:50', '2024-03-31 07:12:50'),
(75, 38, '1712211447_audio.wav', '2024-04-04 06:17:27', '2024-04-04 06:17:27'),
(76, 39, '1712211713_audio.wav', '2024-04-04 06:21:53', '2024-04-04 06:21:53'),
(77, 40, '1712211866_audio.wav', '2024-04-04 06:24:26', '2024-04-04 06:24:26'),
(78, 41, '1712212035_audio.wav', '2024-04-04 06:27:15', '2024-04-04 06:27:15'),
(79, 42, '1712212278_audio.wav', '2024-04-04 06:31:18', '2024-04-04 06:31:18'),
(80, 43, '1712213219_audio.wav', '2024-04-04 06:46:59', '2024-04-04 06:46:59'),
(81, 44, '1712213449_audio.wav', '2024-04-04 06:50:49', '2024-04-04 06:50:49'),
(82, 45, '1712213675_audio.wav', '2024-04-04 06:54:35', '2024-04-04 06:54:35'),
(83, 46, '1712213731_audio.wav', '2024-04-04 06:55:31', '2024-04-04 06:55:31'),
(84, 47, '1712214109_audio.wav', '2024-04-04 07:01:49', '2024-04-04 07:01:49'),
(85, 48, '1712214121_audio.wav', '2024-04-04 07:02:01', '2024-04-04 07:02:01'),
(86, 49, '1712214292_audio.wav', '2024-04-04 07:04:52', '2024-04-04 07:04:52'),
(87, 50, '1712214376_audio.wav', '2024-04-04 07:06:16', '2024-04-04 07:06:16'),
(88, 51, '1712214510_audio.wav', '2024-04-04 07:08:30', '2024-04-04 07:08:30'),
(89, 52, '1712214879_audio.wav', '2024-04-04 07:14:39', '2024-04-04 07:14:39'),
(90, 53, '1712215077_audio.wav', '2024-04-04 07:17:57', '2024-04-04 07:17:57'),
(91, 54, '1712215142_audio.wav', '2024-04-04 07:19:02', '2024-04-04 07:19:02'),
(92, 55, '1712215241_audio.wav', '2024-04-04 07:20:41', '2024-04-04 07:20:41'),
(93, 56, '1712215407_audio.wav', '2024-04-04 07:23:27', '2024-04-04 07:23:27'),
(94, 58, '1712215642_audio.wav', '2024-04-04 07:27:22', '2024-04-04 07:27:22'),
(95, 59, '1712215791_audio.wav', '2024-04-04 07:29:51', '2024-04-04 07:29:51'),
(96, 60, '1712216312_audio.wav', '2024-04-04 07:38:32', '2024-04-04 07:38:32'),
(97, 62, '1712216447_audio.wav', '2024-04-04 07:40:47', '2024-04-04 07:40:47'),
(98, 63, '1712216651_audio.wav', '2024-04-04 07:44:11', '2024-04-04 07:44:11'),
(99, 64, '1712216694_audio.wav', '2024-04-04 07:44:54', '2024-04-04 07:44:54'),
(100, 65, '1712216814_audio.wav', '2024-04-04 07:46:54', '2024-04-04 07:46:54'),
(101, 66, '1712217520_audio.wav', '2024-04-04 07:58:40', '2024-04-04 07:58:40'),
(102, 67, '1712217773_audio.wav', '2024-04-04 08:02:53', '2024-04-04 08:02:53'),
(103, 68, '1712218110_audio.wav', '2024-04-04 08:08:30', '2024-04-04 08:08:30'),
(104, 69, '1712218319_audio.wav', '2024-04-04 08:11:59', '2024-04-04 08:11:59'),
(105, 70, '1712218358_audio.wav', '2024-04-04 08:12:38', '2024-04-04 08:12:38'),
(106, 71, '1712218494_audio.wav', '2024-04-04 08:14:54', '2024-04-04 08:14:54'),
(107, 72, '1712218604_audio.wav', '2024-04-04 08:16:44', '2024-04-04 08:16:44'),
(108, 74, '1712218810_audio.wav', '2024-04-04 08:20:10', '2024-04-04 08:20:10'),
(109, 75, '1712218885_audio.wav', '2024-04-04 08:21:25', '2024-04-04 08:21:25'),
(110, 76, '1712218964_audio.wav', '2024-04-04 08:22:44', '2024-04-04 08:22:44'),
(111, 77, '1712219055_audio.wav', '2024-04-04 08:24:15', '2024-04-04 08:24:15'),
(112, 78, '1712219148_audio.wav', '2024-04-04 08:25:48', '2024-04-04 08:25:48'),
(113, 79, '1712296156_audio.wav', '2024-04-05 05:49:16', '2024-04-05 05:49:16'),
(114, 80, '1712297736_audio.wav', '2024-04-05 06:15:36', '2024-04-05 06:15:36'),
(115, 81, '1712297863_audio.wav', '2024-04-05 06:17:43', '2024-04-05 06:17:43'),
(116, 82, '1712298005_audio.wav', '2024-04-05 06:20:05', '2024-04-05 06:20:05'),
(117, 83, '1712298193_audio.wav', '2024-04-05 06:23:13', '2024-04-05 06:23:13'),
(118, 84, '1712298351_audio.wav', '2024-04-05 06:25:51', '2024-04-05 06:25:51'),
(119, 85, '1712298402_audio.wav', '2024-04-05 06:26:42', '2024-04-05 06:26:42'),
(120, 86, '1712298411_audio.wav', '2024-04-05 06:26:51', '2024-04-05 06:26:51'),
(121, 87, '1712298503_audio.wav', '2024-04-05 06:28:23', '2024-04-05 06:28:23'),
(122, 88, '1712298546_audio.wav', '2024-04-05 06:29:06', '2024-04-05 06:29:06'),
(123, 89, '1712298551_audio.wav', '2024-04-05 06:29:11', '2024-04-05 06:29:11'),
(124, 90, '1712298739_audio.wav', '2024-04-05 06:32:19', '2024-04-05 06:32:19'),
(125, 91, '1712298917_audio.wav', '2024-04-05 06:35:17', '2024-04-05 06:35:17'),
(126, 92, '1712299234_audio.wav', '2024-04-05 06:40:34', '2024-04-05 06:40:34'),
(127, 93, '1712299509_audio.wav', '2024-04-05 06:45:09', '2024-04-05 06:45:09'),
(128, 94, '1712299788_audio.wav', '2024-04-05 06:49:48', '2024-04-05 06:49:48'),
(129, 95, '1712300066_audio.wav', '2024-04-05 06:54:26', '2024-04-05 06:54:26'),
(130, 96, '1712300318_audio.wav', '2024-04-05 06:58:38', '2024-04-05 06:58:38'),
(131, 97, '1712300465_audio.wav', '2024-04-05 07:01:05', '2024-04-05 07:01:05'),
(132, 98, '1712300467_audio.wav', '2024-04-05 07:01:07', '2024-04-05 07:01:07'),
(133, 99, '1712300607_audio.wav', '2024-04-05 07:03:27', '2024-04-05 07:03:27'),
(134, 100, '1712300644_audio.wav', '2024-04-05 07:04:04', '2024-04-05 07:04:04'),
(135, 101, '1712300768_audio.wav', '2024-04-05 07:06:08', '2024-04-05 07:06:08'),
(136, 102, '1712300804_audio.wav', '2024-04-05 07:06:44', '2024-04-05 07:06:44'),
(137, 103, '1712300909_audio.wav', '2024-04-05 07:08:29', '2024-04-05 07:08:29'),
(138, 104, '1712301045_audio.wav', '2024-04-05 07:10:45', '2024-04-05 07:10:45'),
(139, 105, '1712301203_audio.wav', '2024-04-05 07:13:23', '2024-04-05 07:13:23'),
(140, 106, '1712301258_audio.wav', '2024-04-05 07:14:18', '2024-04-05 07:14:18'),
(141, 107, '1712301390_audio.wav', '2024-04-05 07:16:30', '2024-04-05 07:16:30'),
(142, 108, '1712301517_audio.wav', '2024-04-05 07:18:37', '2024-04-05 07:18:37'),
(143, 109, '1712301702_audio.wav', '2024-04-05 07:21:42', '2024-04-05 07:21:42'),
(144, 110, '1712301875_audio.wav', '2024-04-05 07:24:35', '2024-04-05 07:24:35'),
(145, 111, '1712301991_audio.wav', '2024-04-05 07:26:31', '2024-04-05 07:26:31'),
(146, 112, '1712302175_audio.wav', '2024-04-05 07:29:35', '2024-04-05 07:29:35'),
(147, 114, '1712302357_audio.wav', '2024-04-05 07:32:37', '2024-04-05 07:32:37'),
(148, 115, '1712302502_audio.wav', '2024-04-05 07:35:02', '2024-04-05 07:35:02'),
(149, 116, '1712302716_audio.wav', '2024-04-05 07:38:36', '2024-04-05 07:38:36'),
(150, 117, '1712302787_audio.wav', '2024-04-05 07:39:47', '2024-04-05 07:39:47'),
(151, 118, '1712303000_audio.wav', '2024-04-05 07:43:20', '2024-04-05 07:43:20'),
(152, 119, '1712303047_audio.wav', '2024-04-05 07:44:07', '2024-04-05 07:44:07'),
(153, 120, '1712303145_audio.wav', '2024-04-05 07:45:45', '2024-04-05 07:45:45'),
(154, 121, '1712303575_audio.wav', '2024-04-05 07:52:55', '2024-04-05 07:52:55'),
(155, 122, '1712303739_audio.wav', '2024-04-05 07:55:39', '2024-04-05 07:55:39'),
(156, 123, '1712304006_audio.wav', '2024-04-05 08:00:06', '2024-04-05 08:00:06'),
(157, 124, '1712304253_audio.wav', '2024-04-05 08:04:13', '2024-04-05 08:04:13'),
(158, 125, '1712304755_audio.wav', '2024-04-05 08:12:35', '2024-04-05 08:12:35'),
(159, 126, '1712304824_audio.wav', '2024-04-05 08:13:44', '2024-04-05 08:13:44'),
(160, 127, '1712305056_audio.wav', '2024-04-05 08:17:36', '2024-04-05 08:17:36'),
(161, 128, '1712305205_audio.wav', '2024-04-05 08:20:05', '2024-04-05 08:20:05'),
(162, 129, '1712305442_audio.wav', '2024-04-05 08:24:02', '2024-04-05 08:24:02'),
(163, 130, '1712305457_audio.wav', '2024-04-05 08:24:17', '2024-04-05 08:24:17'),
(164, 131, '1712305651_audio.wav', '2024-04-05 08:27:31', '2024-04-05 08:27:31'),
(165, 132, '1712468438_audio.wav', '2024-04-07 05:40:38', '2024-04-07 05:40:38'),
(166, 133, '1712468682_audio.wav', '2024-04-07 05:44:42', '2024-04-07 05:44:42'),
(167, 134, '1712468870_audio.wav', '2024-04-07 05:47:50', '2024-04-07 05:47:50'),
(168, 136, '1712469824_audio.wav', '2024-04-07 06:03:44', '2024-04-07 06:03:44'),
(169, 137, '1712470304_audio.wav', '2024-04-07 06:11:44', '2024-04-07 06:11:44'),
(170, 138, '1712470451_audio.wav', '2024-04-07 06:14:11', '2024-04-07 06:14:11'),
(171, 139, '1712470612_audio.wav', '2024-04-07 06:16:52', '2024-04-07 06:16:52'),
(172, 140, '1712470781_audio.wav', '2024-04-07 06:19:41', '2024-04-07 06:19:41'),
(173, 141, '1712470924_audio.wav', '2024-04-07 06:22:04', '2024-04-07 06:22:04'),
(174, 142, '1712471083_audio.wav', '2024-04-07 06:24:43', '2024-04-07 06:24:43'),
(175, 144, '1712471496_audio.wav', '2024-04-07 06:31:36', '2024-04-07 06:31:36'),
(176, 145, '1712471654_audio.wav', '2024-04-07 06:34:14', '2024-04-07 06:34:14'),
(177, 146, '1712472483_audio.wav', '2024-04-07 06:48:03', '2024-04-07 06:48:03'),
(178, 147, '1712472663_audio.wav', '2024-04-07 06:51:03', '2024-04-07 06:51:03'),
(179, 148, '1712819782_audio.wav', '2024-04-11 07:16:22', '2024-04-11 07:16:22'),
(180, 149, '1712820018_audio.wav', '2024-04-11 07:20:18', '2024-04-11 07:20:18'),
(181, 150, '1712820137_audio.wav', '2024-04-11 07:22:17', '2024-04-11 07:22:17'),
(182, 151, '1712820309_audio.wav', '2024-04-11 07:25:09', '2024-04-11 07:25:09'),
(183, 152, '1712820440_audio.wav', '2024-04-11 07:27:20', '2024-04-11 07:27:20'),
(184, 153, '1712820725_audio.wav', '2024-04-11 07:32:05', '2024-04-11 07:32:05'),
(185, 155, '1713421682_audio.wav', '2024-04-18 06:28:02', '2024-04-18 06:28:02'),
(186, 156, '1713422954_audio.wav', '2024-04-18 06:49:14', '2024-04-18 06:49:14'),
(187, 157, '1713423101_audio.wav', '2024-04-18 06:51:41', '2024-04-18 06:51:41'),
(188, 159, '1713425117_audio.wav', '2024-04-18 07:25:17', '2024-04-18 07:25:17'),
(189, 160, '1713425611_audio.wav', '2024-04-18 07:33:31', '2024-04-18 07:33:31'),
(190, 161, '1713425859_audio.wav', '2024-04-18 07:37:39', '2024-04-18 07:37:39'),
(191, 162, '1713425900_audio.wav', '2024-04-18 07:38:20', '2024-04-18 07:38:20'),
(192, 163, '1713426026_audio.wav', '2024-04-18 07:40:26', '2024-04-18 07:40:26'),
(193, 165, '1713426273_audio.wav', '2024-04-18 07:44:33', '2024-04-18 07:44:33'),
(194, 168, '1713426911_audio.wav', '2024-04-18 07:55:11', '2024-04-18 07:55:11'),
(195, 170, '1713515276_audio.wav', '2024-04-19 08:27:56', '2024-04-19 08:27:56'),
(196, 171, '1713515277_audio.wav', '2024-04-19 08:27:57', '2024-04-19 08:27:57'),
(197, 172, '1713515367_audio.wav', '2024-04-19 08:29:27', '2024-04-19 08:29:27'),
(198, 174, '1713593393_audio.wav', '2024-04-20 06:09:53', '2024-04-20 06:09:53'),
(199, 175, '1713593618_audio.wav', '2024-04-20 06:13:38', '2024-04-20 06:13:38'),
(200, 176, '1713593943_audio.wav', '2024-04-20 06:19:03', '2024-04-20 06:19:03'),
(201, 177, '1713594238_audio.wav', '2024-04-20 06:23:59', '2024-04-20 06:23:59'),
(202, 178, '1713595758_audio.wav', '2024-04-20 06:49:18', '2024-04-20 06:49:18'),
(203, 179, '1713596020_audio.wav', '2024-04-20 06:53:40', '2024-04-20 06:53:40'),
(204, 182, '1713596412_audio.wav', '2024-04-20 07:00:12', '2024-04-20 07:00:12');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `order_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `url`, `order_by`) VALUES
(1, 'Dashboard', 'dashboard', 1),
(2, 'Enquiry', 'enquiry', 5),
(3, 'Service', 'service', 4),
(4, 'Customer', 'customer', 6),
(5, 'User', 'user', 2),
(6, 'Service Due', 'servicedue', 7),
(7, 'Account', 'collection', 8),
(8, 'Category', 'category', 3),
(9, 'Outstanding', 'outstanding', 9);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(82, '2014_10_12_000000_create_users_table', 1),
(83, '2014_10_12_100000_create_password_resets_table', 1),
(84, '2019_08_19_000000_create_failed_jobs_table', 1),
(85, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(86, '2023_11_02_173854_create_roles_table', 1),
(87, '2023_11_02_174205_create_menus_table', 1),
(88, '2023_11_02_174401_create_role_menu_mappings_table', 1),
(89, '2023_11_02_175639_create_enquiries_table', 1),
(90, '2023_11_04_031821_create_category_table', 1),
(91, '2023_11_04_031821_create_services_table', 1),
(92, '2023_11_13_044208_create_customers_table', 1),
(93, '2023_11_20_174627_create_customer_addresses_table', 1),
(94, '2023_11_23_174136_create_enquiry_recordings_table', 1),
(95, '2023_11_25_173056_create_settings_table', 1),
(96, '2023_11_25_175739_create_customer_payments_table', 1),
(97, '2023_11_25_182834_create_customer_services_table', 1),
(98, '2023_12_06_160158_create_service_due_dates_table', 1),
(99, '2023_12_17_105501_create_terms_and_conditions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`) VALUES
(1, 'Admin'),
(2, 'Employee');

-- --------------------------------------------------------

--
-- Table structure for table `role_menu_mappings`
--

CREATE TABLE `role_menu_mappings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_menu_mappings`
--

INSERT INTO `role_menu_mappings` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 2, 1),
(13, 2, 4),
(15, 2, 6),
(18, 2, 9),
(19, 2, 2),
(20, 2, 7),
(21, 2, 3),
(22, 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `hsn` text DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `category_id`, `title`, `hsn`, `remark`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, 'Mosqito', NULL, NULL, 'Active', '2024-01-07 03:45:35', '2024-03-28 17:22:50'),
(2, 3, 'General Disinfestations', NULL, NULL, 'Active', '2024-02-06 15:24:58', '2024-02-06 15:24:58'),
(4, 5, 'Termite Control', NULL, NULL, 'Active', '2024-02-25 16:03:59', '2024-02-25 16:03:59'),
(5, 6, 'General Disinfestations', NULL, NULL, 'Active', '2024-03-07 15:20:38', '2024-03-07 15:20:38'),
(6, 6, 'Termite Control', NULL, NULL, 'Active', '2024-03-07 15:21:50', '2024-03-07 15:21:50'),
(7, 6, 'Rodent Control', NULL, NULL, 'Active', '2024-03-07 15:22:03', '2024-03-07 15:22:03'),
(8, 6, 'Mosquito Control', NULL, NULL, 'Active', '2024-03-07 15:22:16', '2024-03-07 15:22:16'),
(9, 6, 'Bed Bugs Control', NULL, NULL, 'Active', '2024-03-07 15:22:26', '2024-03-07 15:22:26'),
(10, 8, 'Mosquito Control', NULL, NULL, 'Active', '2024-03-07 15:22:57', '2024-03-07 15:22:57'),
(11, 8, 'Termite Control', NULL, NULL, 'Active', '2024-03-07 15:23:10', '2024-03-07 15:23:10'),
(12, 8, 'Rodent Control', NULL, NULL, 'Active', '2024-03-07 15:23:22', '2024-03-07 15:23:22'),
(13, 3, 'Termite Control', NULL, NULL, 'Active', '2024-03-07 15:24:02', '2024-03-07 15:24:02'),
(14, 3, 'Bed Bugs Control', NULL, NULL, 'Active', '2024-03-07 15:24:20', '2024-03-07 15:24:20'),
(15, 3, 'Wall Worms Treatment', NULL, NULL, 'Active', '2024-03-07 15:24:43', '2024-03-07 15:24:43'),
(16, 9, 'General Disinfestations', NULL, NULL, 'Active', '2024-03-07 15:25:19', '2024-03-07 15:25:19'),
(17, 9, 'Termite Control', NULL, NULL, 'Active', '2024-03-07 15:25:31', '2024-03-07 15:25:31'),
(18, 9, 'Rodent Control', NULL, NULL, 'Active', '2024-03-07 15:25:41', '2024-03-07 15:25:41'),
(19, 9, 'Mosquito Control', NULL, NULL, 'Active', '2024-03-07 15:26:11', '2024-03-07 15:26:11'),
(20, 9, 'Rat Guard Installation', NULL, NULL, 'Active', '2024-03-07 15:26:36', '2024-03-07 15:26:36'),
(21, 7, 'General Disinfestations', NULL, NULL, 'Active', '2024-03-07 15:28:31', '2024-03-07 15:28:31'),
(22, 7, 'Termite Control', NULL, NULL, 'Active', '2024-03-07 15:28:42', '2024-03-07 15:28:42'),
(23, 7, 'Rodent Control', NULL, NULL, 'Active', '2024-03-07 15:28:52', '2024-03-07 15:28:52'),
(24, 7, 'Mosquito Control', NULL, NULL, 'Active', '2024-03-07 15:29:20', '2024-03-07 15:29:20');

-- --------------------------------------------------------

--
-- Table structure for table `service_due_dates`
--

CREATE TABLE `service_due_dates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_id` int(11) NOT NULL,
  `service_due` date NOT NULL,
  `completed_date` date DEFAULT NULL,
  `card_image` varchar(255) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `is_completed` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_due_dates`
--

INSERT INTO `service_due_dates` (`id`, `service_id`, `service_due`, `completed_date`, `card_image`, `remark`, `is_completed`) VALUES
(11, 21, '2024-03-17', NULL, NULL, NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `bank_account` varchar(255) NOT NULL,
  `ifsc_code` varchar(255) NOT NULL,
  `account_holder_name` varchar(255) NOT NULL,
  `gst_no` varchar(255) NOT NULL,
  `hsn_code` varchar(255) NOT NULL,
  `lic_no` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `logo`, `bank_name`, `bank_account`, `ifsc_code`, `account_holder_name`, `gst_no`, `hsn_code`, `lic_no`, `address`, `email`, `website`, `state`, `mobile_no`, `created_at`, `updated_at`) VALUES
(1, 'BE-GON PEST CONTROL SERVICES', 'invoice_logo.jpeg', 'UNION BANK OF INDIA, MUMBAI ASHOKVAN', '510101002070765', 'UNBIN0904970', 'BE-GON PEST CONTROL SERVICES', '12345', '998531', '23432144', 'Shop No 23,Sai Shraddha CHS-I Behind Sai Baba Mandir,Ashokvan,Borivali(E),Mumbai-400066', 'be_gon@rediffmail.com/info@begonpestcontrol.com', 'www.begonpestcontrol.com.', '27-Maharashtra', '7303290130/28280898/28282833', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `terms_and_conditions`
--

CREATE TABLE `terms_and_conditions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terms_and_conditions`
--

INSERT INTO `terms_and_conditions` (`id`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Bill not Paid within one Month will be Subject\r\nto Interest @21%', '2024-01-26 17:40:30', '2024-01-26 17:40:34'),
(2, 'Payment Should be made in the Name of BEGON PEST CONTROL SERVICES', '2024-01-26 17:40:14', '2024-01-26 17:40:14'),
(3, 'Subject to Mumbai Jurisdiction Only.\r\n', '2024-01-26 17:40:14', '2024-01-26 17:40:14'),
(4, 'Payment Within 30 days', '2024-01-26 17:40:14', '2024-01-26 17:40:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact` bigint(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `contact`, `email`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Umesh', 9819749638, 'admin@admin.com', NULL, '$2y$10$g0bkBxAqTQDdqiH4WsXQUOoJ5je13V1Lt/hTDrGUzlXn8eylEy7ki', '1', 'gSKfuYZ77zo4TveWXcfCDvc9PIwZDnDs7CjT0bJ0kePAt7xqio1lLGNUeXVJ', '2023-11-03 05:16:57', '2024-03-09 19:10:51'),
(3, 2, 'Moni Gupta', 8767923554, 'moni@begon.com', NULL, '$2y$10$Iv4cDu1IZA40SgjcjhILOet4XeibA.fnTuaGsfAcWbjZgF2vyNTUm', '1', 'KesQBZCzdhE9BFWwYy5rZiInjx9eK7Qa3CYjTyVVGCKRvnejAtbOFIQoCfE0', '2024-03-07 15:12:50', '2024-03-22 06:09:02'),
(4, 2, 'Urvashi Andre', 7304311138, 'urvashi@begon.com', NULL, '$2y$10$EuKW204dPXcDBFo4f5igEeTjUdYCe1y2m3l76isd1lHJvMS0Fgg5q', '1', 'f7t3r6wNEBbJvxEpEzPdeM2QqmguH47z4QziSAgQHAcQGdergLtbKavCXZCs', '2024-03-07 15:14:14', '2024-03-22 06:06:51'),
(5, 2, 'Khushbu Sahani', 9769625789, 'khushbusahani@begon.com', NULL, '$2y$10$eTPqIufvr0JeOwqbou1WYOYN0Zyhk.rUb5G3tPL1YpYZu.GbPx1Cm', '1', '8ma6PeWjq0auIwZJ2zGagxA0txKPrVp1QnESd8HtVv5dUv7LLddtCZtyZ1n9', '2024-03-07 15:15:56', '2024-03-18 05:54:25'),
(6, 2, 'Sneha', 9867724532, 'sneha@begon.com', NULL, '$2y$10$LcyRMNWIs1WR8M3egFEPTOZsiX/WWDCTIM2Fe6lrdZukn0uMB6ecm', '1', 'Y3Y0ckXaJcvfA2gzMsJmlDmxDodCBMMWE152Zy3M7UCN1TOKHmY2KDAp4CrY', '2024-03-07 15:17:22', '2024-04-04 06:40:56'),
(7, 1, 'Nilesh Mundokar', 9323669191, 'nilesh@begon.com', NULL, '$2y$10$AbSMJrBR7kfU8.7TW5uY.uOjglDi.wVlWZD5XP3ofqC.vBlgivOqm', '1', NULL, '2024-03-07 15:18:28', '2024-03-07 15:18:28'),
(8, 2, 'khushboo gupta', 9324972638, 'khushboog07@be.goncom', NULL, '$2y$10$dXYatycMyKDHg8UBR8LEbOlwJDvqHZN12oOtc1O5lJTkqEo6e.aem', '1', 'o7BjeoOw6aGyMeGa6Gmc5qMg3PgpuzjePurraliiLnV20u6vScfmKQu2x6UE', '2024-03-18 05:43:14', '2024-03-22 05:45:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_payments`
--
ALTER TABLE `customer_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_services`
--
ALTER TABLE `customer_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiry_recordings`
--
ALTER TABLE `enquiry_recordings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_menu_mappings`
--
ALTER TABLE `role_menu_mappings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_due_dates`
--
ALTER TABLE `service_due_dates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_and_conditions`
--
ALTER TABLE `terms_and_conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `customer_payments`
--
ALTER TABLE `customer_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `customer_services`
--
ALTER TABLE `customer_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT for table `enquiry_recordings`
--
ALTER TABLE `enquiry_recordings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_menu_mappings`
--
ALTER TABLE `role_menu_mappings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `service_due_dates`
--
ALTER TABLE `service_due_dates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `terms_and_conditions`
--
ALTER TABLE `terms_and_conditions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
