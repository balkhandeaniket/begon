@extends('main_datatable')

@section('content')
    <style>
        #customer_list {
            overflow-x: auto;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Service Due</h4>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Servic Due List</h3>
                            </div>
                            
                            <!-- /.card-header -->
                            <div class="card-body" id="customer_list">
                              
                                <table id="customer-table" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Invoice No</th>
                                            <th>Name</th>
                                            <th>Service</th>
                                            <th>Address</th>
                                            <th>Service Due</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        
        function getServiceDue() {
            $('#customer-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '/servicedue-getServerSide',
                    type: 'POST', // Specify POST method
                },
                columns: [
                    {
                        data: 'invoice_no',
                        name: 'customer_payments.invoice_no'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'title',
                        name: 'services.title'
                    },
                    {
                        data: 'address',
                        name: 'address'
                    },
                    {
                        data: 'due_date',
                        name: 'due_date'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    },
                ]
            });
        }
        getServiceDue();

        function serviceDue(id) {
           
            var postData = {
                id: id,
            };
            $.post('/servicedue-update-status', postData, function(response) {
                getServiceDue()
            });
        }

        function printInvoice(payment_id, type = 0) {
            //var selectedAddress = $("input[name='address']:checked").val();
            var selectedAddress = "0";
            var url = "/customer-payment-pdf/" + payment_id + '/' + selectedAddress + "/" + type;
            window.open(url, '_blank');
        }

        function printContractForm(payment_id, type = 0) {
            // var selectedAddress = $("input[name='address']:checked").val();
            var selectedAddress = "0";
            var url = "/customer-contract-form/" + payment_id + '/' + selectedAddress + "/";
            window.open(url, '_blank');
        }
    </script>
@endpush
