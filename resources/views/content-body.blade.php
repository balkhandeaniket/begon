<!-- resources/views/main.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
</head>
<body>
    
    <!-- Include Header Component -->
    @include('components.header')

    <!-- Include Left Panel Component -->
    {{-- @include('components.left-panel') --}}

    <!-- Main Content -->
    <div class="main-content">
        @yield('content')
    </div>

    <!-- Include Footer Component -->
    @include('components.footer')
    @stack('scripts')
</body>
</html>
