@extends('main_datatable')

@section('content')
    <style>
        #operator_list {
            overflow-x: auto;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Operator</h4>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Operator List</h3>

                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" id="operator_list">
                                
                                <table id="enquiry-table" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Contact</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
   
@endsection

@push('scripts')
  
    <script>
     
        function getOperator() {
            $('#enquiry-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '/operator-getServerSide',
                    type: 'POST', // Specify POST method
                    data: {
                        status: $('#status').val(),
                        type: $('#type').val(),
                        created_by : $("#created_by").val(),
                        from_date: $('#from_date').val(),
                        to_date: $('#to_date').val(),
                    },
                },
                columns: [
                   
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'contact',
                        name: 'contact'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    },
                   
                ]
            });
        }
        getOperator();
    </script>
@endpush
