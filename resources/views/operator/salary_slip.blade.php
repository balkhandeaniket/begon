<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Salary Slip</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        .container {
            width: 80%;
            margin: auto;
            border: 2px solid #000;
            padding: 20px;
        }

        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            border-bottom: 2px solid #000;
            padding-bottom: 10px;
        }

        .header img {
            width: 100px;
            height: auto;
        }

        .company-details {
            text-align: right;
        }

        .title {
            font-size: 20px;
            font-weight: bold;
        }

        .section {
            margin-top: 15px;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 10px;
        }

        .table th,
        .table td {
            border: 1px solid #000;
            padding: 6px;
            text-align: left;
        }

        .bold {
            font-weight: bold;
        }

        .total {
            background: #f0f0f0;
            font-weight: bold;
        }
    </style>
</head>

<body>

    <div class="container">
        <!-- Header Section -->
        <div class="header">
            <table width="100%">
                <tr>
                    <td width="30%">
                        &nbsp; &nbsp; &nbsp;<img src="images/begon-logo.png" alt="Logo" width="250px" height="100px"
                            class="begon-image">
                    </td>
                    <td width="70%" align="right">
                        <div style="font-size: 16px; font-weight: bold;">{{ $settings->name ?? '' }}</div>
                        <div>{{ $settings->address ?? '' }}</div>
                        <div>Email: {{ $settings->email ?? '' }}</div>
                        <div>Phone: 9892072628/9324972638<br>
                            9323669191/7303290130/28280898/28282833</div>
                    </td>
                </tr>
            </table>
            {{-- <img src="images/contract_form.jpg" alt="Logo" width="150" style="margin-bottom: 10px;">
            <div class="company-details">
                <div class="title">XYZ Pvt Ltd</div>
                <div>123, Business Street, City, Country</div>
                <div>Email: contact@xyz.com</div>
                <div>Phone: +91 98765 43210</div>
            </div> --}}
        </div>

        <!-- Employee Details -->
        <div class="section">
            <table class="table">
                <tr>
                    <td class="bold" width="50%">Employee Name:</td>
                    <td>{{ $customerDetails->name }}</td>
                    {{-- <td class="bold">Employee ID:</td><td>EMP12345</td> --}}
                </tr>
                <tr>
                    {{-- <td class="bold">Designation:</td><td>Software Engineer</td> --}}
                    <td width="50%" class="bold">Month:</td>
                    <td> {{ $month }}</td>
                </tr>
            </table>
        </div>
        <br>
        <br>
        <br>
        <br>
        <table width="100%">
            <tr>
                <td width="50%" valign="top">
                    <div style="font-size: 16px; font-weight: bold; margin-bottom: 5px;">Salary Breakdown</div>
                    <table border="1" cellpadding="5" cellspacing="0" width="100%">
                        <tr>
                            <th>Description</th>
                            <th>Amount (₹)</th>
                        </tr>

                        @php
                            $total = '0';
                        @endphp
                        @foreach ($salaryData as $val)
                            @php
                                $total += $val->value;
                            @endphp
                            <tr>
                                <td><b>{{ $val->category->title }} </b></td>
                                <td><b> ₹ {{ $val->value }} </b></td>
                            </tr>
                        @endforeach
                        <tr class="total">
                            <td style="font-weight: bold;"><b>Total </b></td>
                            <td><b> ₹ {{ $total }} </b></td>
                        </tr>

                    </table>
                </td>
                @if (count($expanseData) > 0)
                    <td width="50%" valign="top">
                        <div style="font-size: 16px; font-weight: bold; margin-bottom: 5px;">Expenses</div>
                        <table border="1" cellpadding="5" cellspacing="0" width="100%">
                            <tr>
                                <th>Description</th>
                                <th>Amount (₹)</th>
                            </tr>
                            @php
                                $total = '0';
                            @endphp
                            @foreach ($expanseData as $val)
                                @php
                                    $total += $val->value;
                                @endphp
                                <tr>
                                    <td><b>{{ $val->category->title }} </b></td>
                                    <td><b> ₹ {{ $val->value }} </b></td>
                                </tr>
                            @endforeach
                            <tr class="total">
                                <td style="text-align: right"><b>Total </b></td>
                                <td><b> ₹ {{ $total }} </b></td>
                            </tr>

                        </table>
                    </td>
                @endif

            </tr>
        </table>
        <!-- Salary Breakdown -->

    </div>

</body>

</html>
