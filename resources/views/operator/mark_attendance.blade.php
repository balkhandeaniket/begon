<h4 class="modal-title">Mark Attendence</h4> <br>
<div class="row mark_attendance">
    <input type="hidden" value="{{ $getUser->id }}" id="status_update_id">
    <div class="col-md-6">
        Name : {{ $getUser->name }}
    </div>
    <div class="col-md-4">
        Contact : {{ $getUser->contact }}
    </div>
    <hr>
    <div class="col-md-6">
        <div class="form-group">
            <label for="duty_time_in">Date Time In</label>
            {{-- <input type="datetime-local" class="form-control inputMask" id="duty_time_in" name="duty_time_in" value="{{ date('Y-m-d\TH:i')}}"> --}}

            <div class="input-group date" id="datetimepicker_in" data-target-input="nearest">
                <?php $currentDateTime = $newDate.'08:30 AM'; ?>
                <input id="duty_time_in" name="duty_time_in" type="text" 
                       value="<?php echo $currentDateTime; ?>"
                       class="form-control datetimepicker-input inputMask" 
                       data-target="#datetimepicker_in" 
                       placeholder="DD-MM-YYYY hh:mm AM/PM" />
                <div class="input-group-append" data-target="#datetimepicker_in" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="duty_time_Out">Date Time Out</label>
            {{-- <input type="datetime-local" class="form-control" id="duty_time_Out" name="duty_time_Out"
                value="{{ \Carbon\Carbon::now()->format('Y-m-d\TH:i') }}"> --}}
            @php
                 $outTime = $newDate.'05:30 PM'; 
            @endphp
            <div class="input-group date" id="datetimepicker_out" data-target-input="nearest">
                <input id="duty_time_Out" name="duty_time_Out" type="text"  value={{$outTime}}
                        class="form-control datetimepicker-input inputMask" 
                        data-target="#datetimepicker_out" 
                        placeholder="DD-MM-YYYY hh:mm AM/PM" />
                <div class="input-group-append" data-target="#datetimepicker_out" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div> 
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="concern_person">Remark</label>
            <textarea name="remark" id="remark" class="form-control"></textarea>
        </div>
    </div>
</div>
<div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick="addAttendance()">Save</button>
</div>




 