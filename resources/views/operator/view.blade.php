@extends('main_datatable')
@section('content')
    <style>
        #enquiry_list {
            overflow-x: auto;
        }

        .custom-border {
            border: 1px solid #ccc;
        }
    </style>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Operator</h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h1 class="card-title"> Operator Details</h3>
                                    <div class="card-tools">
                                        <ul class="nav nav-pills ml-auto">
                                            <li class="nav-item btn-sm">
                                                <a class="btn-sm btn-danger" href="#"
                                                    onclick="history.back(); return false;">Back</a>
                                            </li>
                                        </ul>
                                    </div>
                            </div>
                            <!-- /.card-header -->

                            <div class="card-body">
                                {{-- @csrf --}}
                                <input type="hidden" id="user_id" name="id" value="{{ $getUser->id ?? '' }}">
                                <!-- General Details Section -->
                                <div class="card card-secondary">
                                    <div class="card-header">
                                        <h3 class="card-title">Personal Details</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-4 ">
                                                <div class="form-group">
                                                    <label for="first_name">Name : </label>
                                                    {{ $getUser->name ?? '' }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <div class="form-group">
                                                    <label for="gst_no">Contact No</label>
                                                    {{ $getUser->contact ?? '' }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <div class="form-group">
                                                    <label for="middle_name">Email</label>
                                                    {{ $getUser->email ?? '' }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-success">
                                    <div class="card-header">
                                        <h3 class="card-title">Summary</h3>

                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="card-body">
                                        <div class="card-body" id="enquiry_list">
                                            <div class="row">

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="from_date">From Date</label>
                                                        <input type="date" name="from_date" id="from_date"
                                                            class="form-control" value="{{ date('Y-m-01') }}">
                                                        {{-- <div class="container mt-5">
                                                            
                                                            <div class="form-group">
                                                                <div class="input-group date" id="from_date" data-target-input="nearest">
                                                                    <input type="text" name="from_date"  value="{{ date('Y-m-01') }}" class="form-control datetimepicker-input" data-target="#from_date" />
                                                                    <div class="input-group-append" data-target="#from_date" data-toggle="datetimepicker">
                                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> --}}
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="to_date">To Date</label>
                                                        <input type="date" name="to_date" id="to_date"
                                                            class="form-control" value="{{ date('Y-m-t') }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group" style="margin-top: 34px;">
                                                        <button
                                                            onclick="getOperatorsList();summary();getServerSideOperatorData('Salary');getServerSideOperatorDataWork('Work')"
                                                            class="btn btn-sm btn-info">Go</button>

                                                           
                                                    </div>
                                                </div>
                                               
                                                <div class="summaryData"> </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div @if ($type != 'Attendence') style="display: none" @endif
                                    class="card card-warning">
                                    <div class="card-header">
                                        <h3 class="card-title">Attendence</h3>
                                    </div>
                                    <div class="card-body">
                                        <a class="btn btn-sm btn-info" onclick="markAttendance({{ $getUser->id }});"
                                            style="float: right">Mark Attendance</a>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <table id="operators-table" class="table table-bordered display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    {{-- <th>Name</th> --}}
                                                    <th>Duty In time</th>
                                                    <th>Duty Out time</th>
                                                    <th>Remark</th>
                                                    <th>Created By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>

                                    </div>
                                </div>

                                <div @if ($type != 'Work') style="display: none" @endif class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title">Work Sheet</h3>
                                    </div>
                                    <div class="card-body">
                                        <a class="btn btn-sm btn-info"
                                            onclick="addAccountDetails({{ $getUser->id }},'Work');"
                                            style="float: right">Add Work Data</a>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <table id="operators-work-table" class="table table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    {{-- <th>Name</th> --}}
                                                    <th>Date</th>
                                                    <th>Work Data</th>
                                                    <th>Remark</th>
                                                    <th>Created By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>

                                    </div>
                                </div>

                                <div @if ($type != 'Salary') style="display: none" @endif
                                    class="card card-danger">
                                    <div class="card-header">
                                        <h3 class="card-title">Salery & Expenses</h3>
                                    </div>
                                    <div class="card-body">
                                        @if (Auth::user()->role_id == '1' || Auth::user()->role_id == '2')
                                            <a class="btn btn-sm btn-info"
                                                onclick="addAccountDetails({{ $getUser->id }},'Salary');"
                                                style="float: right">Add Salary & Expenses</a>
                                        @endif

                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <table id="operators-data-table" class="table table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    {{-- <th>Name</th> --}}
                                                    <th>Date</th>
                                                    <th>Salary</th>
                                                    <th>Remark</th>
                                                    <th>Created By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- /.content-wrapper -->
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    {{-- <h4 class="modal-title">Mark Attendence</h4> --}}
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body model2">
                    <p>One fine body&hellip;</p>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
   
@endsection
@push('scripts')
    <script src="{{ asset('js/operator.js') }}"></script>
@endpush

{{-- @push('scripts')
    <script>
        function markAttendance(id) {
            var postData = {
                id: id,
            };
            $.post('/operator-mark-attendance', postData, function(response) {
                // Clear the modal body content
                $('.model2').empty();

                // Check if response.html is not empty
                if (response.html) {
                    // Update the modal body with new content
                    $('.model2').html(response.html);

                    // Show the modal
                    $('#modal-default').modal('show');
                } else {
                    // If response is empty, show an alert
                    alert('Failed to load content. Please try again.');
                }
            });
        }

        function addAccountDetails(id, type) {
            var postData = {
                id: id,
                type: type,
            };
            $.post('/user-sheet-data-add', postData, function(response) {
                // Clear the modal body content
                $('.model2').empty();

                // Check if response.html is not empty
                if (response.html) {
                    // Update the modal body with new content
                    $('.model2').html(response.html);

                    // Show the modal
                    $('#modal-default').modal('show');

                    add_data();
                } else {
                    // If response is empty, show an alert
                    alert('Failed to load content. Please try again.');
                }
            });
        }

        function summary() {
            var postData = {
                user_id: $("#user_id").val(),
                from_date: $("#from_date").val(),
                to_date: $("#to_date").val(),
            };
            $.post('/operator-summary-data', postData, function(response) {
                $('.summaryData').html(response.html);
            });
        }
        summary()

        function addAttendance() {
            $("#modal-default").modal('hide')
            var postData = {
                user_id: $(".mark_attendance #status_update_id").val(),
                remark: $(".mark_attendance #remark").val(),
                duty_time_in: $(".mark_attendance #duty_time_in").val(),
                duty_time_out: $(".mark_attendance #duty_time_Out").val(),

            };
            $.post('/mark-attendance-action', postData, function(response) {
                getOperatorsList()
                summary()
            });
        }

        function isNumberKey(event) {
            // Check if the pressed key is a number
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                event.preventDefault();
                return false;
            }
            return true;
        }


        function getOperatorsList() {
            $('#operators-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '/operator-getServerSideAttendence',
                    type: 'POST', // Specify POST method
                    data: {
                        user_id: $("#user_id").val(),
                        from_date: $("#from_date").val(),
                        to_date: $("#to_date").val(),
                    },
                },
                dom: 'Bfrtip', // Add 'B' for buttons
                buttons: [{
                        extend: 'excelHtml5',
                        text: 'Export to Excel',
                        title: 'Attendance Report'
                    },
                    {
                        extend: 'print',
                        text: 'Print'
                    }
                ],
                columns: [
                    // {
                    //     data: 'name',
                    //     name: 'name'
                    // },
                    {
                        data: 'duty_in_time',
                        name: 'duty_in_time'
                    }, {
                        data: 'duty_out_time',
                        name: 'duty_out_time'
                    },
                    {
                        data: 'remark',
                        name: 'remark'
                    },
                    {
                        data: 'created_by',
                        name: 'created_by'
                    },
                    // {
                    //     data: 'action',
                    //     name: 'action',
                    //     orderable: false,
                    //     searchable: false
                    // }
                ],
                lengthMenu: [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]], // Options for rows per page
                pageLength: 10 // Default number of rows per page
            });
        }
        getOperatorsList();

        function add_data() {
            var postData = {
                'type': $("#type").val()
            };
            $.post('/operator-add-more', postData, function(response) {
                // Clear the modal body content
                $(".dataAdd").append(response.html)

                var count = $('.remove-btn').length;
                if (parseInt(count) > 1) {
                    $(".remove-btn").show();
                } else {
                    $(".remove-btn").hide();
                }

            });

        }

        function removeCategory(id) {
            $(".remove_category_" + id).html('')

            var count = $('.remove-btn').length;
            if (parseInt(count) == "1") {
                $(".remove-btn").hide();
            } else {
                $(".remove-btn").show();
            }
        }



        function addCategoryData() {

            operator_user_id = $("#operator_user_id").val()
            type = $("#type").val()
            category_date = $("#category_date").val()
            remark = $("#remark").val()
            category_ids = [];
            $(".category_id").each(function() {
                category_id = $(this).val()
                category_ids.push(category_id)
            })
            category_values = [];
            $(".category_value").each(function() {
                category_value = $(this).val()
                category_values.push(category_value)
            })
            var postData = {
                'operator_user_id': operator_user_id,
                'type': type,
                'category_date': category_date,
                'remark': remark,
                'category_ids': category_ids,
                'category_values': category_values,
            };
            $.post('/operator-add-more-save', postData, function(response) {
                // Clear the modal body content
                $("#modal-default").modal('hide')
                getServerSideOperatorDataWork('Work');

                getServerSideOperatorData('Salary');
            });

        }

        function getServerSideOperatorData(type) {
            $('#operators-data-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '/operator-getServerSideOperatorData',
                    type: 'POST', // Specify POST method
                    data: {
                       
                        type: type,
                        user_id: $("#user_id").val(),
                        from_date: $("#from_date").val(),
                        to_date: $("#to_date").val(),
                    },
                },
                dom: 'Bfrtip', // Add 'B' for buttons
                        buttons: [{
                                extend: 'excelHtml5',
                                text: 'Export to Excel',
                                title: 'Salary Report'
                            },
                            {
                                extend: 'print',
                                text: 'Print'
                            }
                        ],
                columns: [{
                        data: 'date',
                        name: 'date'
                    },
                    {
                        data: 'salary',
                        name: 'salary'
                    },
                    {
                        data: 'remark',
                        name: 'remark'
                    },
                    {
                        data: 'created_by',
                        name: 'created_by'
                    },
                ],
                lengthMenu: [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]], // Options for rows per page
                pageLength: 10 // Default number of rows per page
            });
        }
        getServerSideOperatorData('Salary');


        function getServerSideOperatorDataWork(type) {
            $('#operators-work-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '/operator-getServerSideOperatorWorkData',
                    type: 'POST', // Specify POST method
                    data: {
                        type: type,
                        user_id: $("#user_id").val(),
                        from_date: $("#from_date").val(),
                        to_date: $("#to_date").val(),
                    },
                },
                dom: 'Bfrtip', // Add 'B' for buttons
                buttons: [{
                        extend: 'excelHtml5',
                        text: 'Export to Excel',
                        title: 'Work Report'
                    },
                    {
                        extend: 'print',
                        text: 'Print',
                        title: 'Work Report'
                    }
                ],

                columns: [{
                        data: 'date',
                        name: 'date'
                    },
                    {
                        data: 'salary',
                        name: 'salary'
                    },
                    {
                        data: 'remark',
                        name: 'remark'
                    },
                    {
                        data: 'created_by',
                        name: 'created_by'
                    },
                ],
                lengthMenu: [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]], // Options for rows per page
                pageLength: 10 // Default number of rows per page
            });
        }
        getServerSideOperatorDataWork('Work');
    </>
@endpush --}}
