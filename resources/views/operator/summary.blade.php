<div class="row col-md-12">
    <br>
    <br>
    <div class="col-m-12">
        <button  onclick="salarySlip();" style="float:left"
        class="btn btn-sm btn-warning">Salary SLip</button>
    </div>
    <div class="col-md-12">
        <h6><b> Total Attendance</b>:  <b>{{ $attendence }}</b> day present.</h6>
        <br>
    </div>
    <div class="col-md-4">
         <h6><b>Total Work Sheet</b></h6>
        <table class="table table-bordered table-striped table-hover table-sm">
            <tr>
                <th>Work</th>
                <th>Amount</th>
            </tr>
            @php
                $total = "0";
            @endphp
            @foreach ($workData as $val)
                @php
                    $total+= $val->value; 
                @endphp
                <tr>
                    <td><b>{{  $val->category->title }} </b></td>
                    <td><b>{{ $val->value }} </b></td>
                </tr>
            @endforeach
            <tr>
                <td style="text-align: right"><b>Total </b></td>
                <td><b>{{ $total }} </b></td>
            </tr>
        </table>
    </div>
    <div class="col-md-4">
         <h5><b>Total Salary</b> </h5>
        <table class="table table-bordered table-striped table-hover table-sm">
            <tr>
                <th>Salary</th>
                <th>Amount</th>
            </tr>
            @php
                $total = "0";
            @endphp
            @foreach ($salaryData as $val)
                @php
                    $total+= $val->value; 
                @endphp
                <tr>
                    <td><b>{{  $val->category->title }} </b></td>
                    <td><b>{{ $val->value }} </b></td>
                </tr>
            @endforeach
            <tr>
                <td style="text-align: right"><b>Total </b></td>
                <td><b>{{ $total }} </b></td>
            </tr>
        </table>
    </div>
    <div class="col-md-4">
        <h5><b>Total Expenses</b></h5>
       <table class="table table-bordered table-striped table-hover table-sm">
           <tr>
               <th>Expenses</th>
               <th>Amount</th>
           </tr>
           @php
               $total = "0";
           @endphp
           @foreach ($expanseData as $val)
               @php
                   $total+= $val->value; 
               @endphp
               <tr>
                   <td><b>{{  $val->category->title }} </b></td>
                   <td><b>{{ $val->value }} </b></td>
               </tr>
           @endforeach
           <tr>
               <td style="text-align: right"><b>Total </b></td>
               <td><b>{{ $total }} </b></td>
           </tr>
       </table>
   </div>

</div>
