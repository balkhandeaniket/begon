@php
    $rand = rand();
@endphp
<tr class="remove_category_{{ $rand }}">
    <td>
        <select name="category_id[]" class="form-control select2 category_id">
            <option value="">Select</option>
            @foreach ($getCategory as $val)
                <option value="{{ $val->id }}">{{ $val->title }}</option>
            @endforeach
        </select>
    </td>
    {{-- onkeypress="return isNumberKey(event)" --}}
    <td><input type="text"  placeholder="Value" class="category_value form-control" id="value"
            name="category_value[]"  onkeypress="return isNumberKey(event)" value=""></td>
    <td><button type="button" onclick="removeCategory({{ $rand }})" style="display:none"
            class="remove-btn btn btn-danger btn-sm">X</button>
    </td>
</tr>
