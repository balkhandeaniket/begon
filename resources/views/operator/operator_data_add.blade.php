<h4 class="modal-title">Add {{ $type }}</h4> <br>
<div class="row mark_attendance">
    <input type="hidden" value="{{ $getUser->id }}" id="operator_user_id">
    <div class="col-md-6">
        Name : {{ $getUser->name }}
    </div>
    <div class="col-md-4">
        Contact : {{ $getUser->contact }}
    </div>
    <hr>
    <input type="hidden" value="{{$type}}" id="type">
    <div class="col-md-12">
        <div class="form-group">
            <label for="date">Date</label>
            <input type="date" class="form-control" id="category_date" name="category_date" value="{{ date('Y-m-d') }}">
        </div>
    </div>
    <div class="col-md-12">
        <table class="table table-border table-striped">
            <thead>
               <tr>
                    <th><center>Name</center></th>
                    <th><center>Value</center></th>
                    <th> </th>
               </tr>
            </thead>
            <tbody class="dataAdd">
                
            </tbody>
        </table>
        <div class="clearfix">&nbsp;</div>
        <button type="button" onclick="add_data()" class="btn btn-info btn-sm">Add</button>
    </div>
   
    
    <div class="col-md-12">
        <div class="form-group">
            <label for="concern_person">Remark</label>
            <textarea name="remark" id="remark" class="form-control"></textarea>
        </div>
    </div>
</div>
<div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary"  onclick="addCategoryData()">Save</button>
</div>
@push('scripts')
    <script>
        function isNumberKey(event) {
            // Check if the pressed key is a number
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                event.preventDefault();
                return false;
            }
            return true;
        }
    </script>
@endpush

