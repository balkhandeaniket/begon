<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rodent Control Form</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            font-size: 14px;
        }
        .row {
            width: 950px;
            border: 4px solid #000;
            padding: 20px;
            box-sizing: border-box;
            background-color: #fff;
        }
        table {
            width: 100%;
            border-spacing: 0 10px;
        }
        td {
            width: 50%;
            vertical-align: top;
            padding: 1px;
        }
        .label {
            font-weight: bold;
            width: 120px; 
            display: inline-block;
        }
        .label1 {
            font-weight: bold;
            width: 20px; 
            display: inline-block;
        }
        .label2 {
            font-weight: bold;
            width: 70px; 
            display: inline-block;
        }
        .label3 {
            font-weight: bold;
            width: 170px; 
            display: inline-block;
        }
        .underline {
            display: inline-block;
            border-bottom: 1px solid black;
            width: 30px; 
            text-align: left;
        }
        .half-line {
            display: inline-block;
            border-bottom: 1px solid black;
            width: 150px;
            text-align: left;
        }
        .table-box {
            border: 1px solid black;
            border-collapse: collapse;
            width: 100%;
            margin-top: 10px;
        }
        .table-box th, .table-box td {
            border: 1px solid black;
            padding: 2px;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="row">
        <table>
            <tbody>
                <tr>
                    <td style="padding-right:20px">
                        <table>
                            <tr>
                                <td class="label"><b>Area sq. Ft.</b></td>
                                <td class="underline"></td>
                                <td class="label1"><b>Entire</b></td>
                                <td class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Booked By</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label3"><b>Value of Contract Rs.</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label3"><b>Payment Recd. Yes/No</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Balance Payment</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Cash / Cheque</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Concern Person</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Sign</b></td>
                                <td class="half-line"></td>
                                <td class="label1"><b>Date</b></td>
                                <td class="half-line"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Stop</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                        </table>
                        <style>
                            .table-box {
                                width: 100%;
                                border-collapse: collapse;
                                table-layout: fixed; /* Ensures that column widths are respected */
                            }
                            .table-box th, .table-box td {
                                border: 1px solid #000; /* Optional: Adds borders for better visualization */
                                text-align: left;
                                padding: 8px;
                                text-align: center;
                            }
                            .table-box th:nth-child(1),
                            .table-box td:nth-child(1) { width: 20%; }
                            .table-box th:nth-child(2),
                            .table-box td:nth-child(2) { width: 20%; }
                            .table-box th:nth-child(3),
                            .table-box td:nth-child(3) { width: 40%; }
                            .table-box th:nth-child(4),
                            .table-box td:nth-child(4) { width: 20%; }
                        </style>
                        
                        <table class="table-box">
                            <thead>
                                <tr>
                                    <th>DATE</th>
                                    <th>TIME</th>
                                    <th>REASON</th>
                                    <th>CON. PER</th>
                                </tr>
                            </thead>
                            <tbody>
                               @for ($i=0;$i<=6;$i++)
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                               @endfor
                            </tbody>
                        </table>
                        
                    </td>
                    <td style="padding-left:20px">
                        <table style="width: 100%; border: none;">
                            <tr>
                                <td style="width: 50%; text-align: center;">
                                    <img src="images/contract_form.jpg" alt="Logo" width="150" style="margin-bottom: 10px;">
                                </td>
                                {{-- <td style="width: 50%; text-align: auto;padding-top:20px">
                                    <b>CONTACT : 022 78888</b>
                                </td> --}}
                                <td style="width: 50%; text-align: auto;padding-top:20px">
                                    <div style="background-color: rgb(44, 43, 43); color: white; padding: 20px 20px; border-radius: 100px; display: inline-block;">
                                        <b>CONTACT : 022 28282833</b>
                                    </div>
                                </td>
                                
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td class="label"><b>Type of Service</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Name</b></td>
                                <td colspan="3" class="underline"> {{ ucfirst($customerDetails->customer->first_name) }} </td>
                            </tr>
                            <tr>
                                <td class="label"><b>Flat No. Floor</b></td>
                                <td class="half-line"></td>
                                <td class="label2"><b>Plot No.</b></td>
                                <td class="half-line"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Name of Bldg.</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Area</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Landmark</b></td>
                                <td colspan="3" class="underline"> {{ $customerDetails->landmark }}</td>
                            </tr>
                            <tr>
                                <td class="label"><b>Road Street</b></td>
                                <td colspan="3" class="underline"> {{ $customerDetails->road }} </td>
                            </tr>
                            <tr>
                                <td class="label"><b>Near</b></td>
                                <td colspan="3" class="underline">  {{ $customerDetails->near }} </td>
                            </tr>
                            <tr>
                                <td class="label"><b>Opp.</b></td>
                                <td colspan="3" class="underline">  {{ $customerDetails->opposite }} </td>
                            </tr>
                            <tr>
                                <td class="label"><b>Bus Stop</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Locality</b></td>
                                <td colspan="3" class="underline">{{ $customerDetails->locality }}</td>
                            </tr>

                            <tr>
                                <td class="label"><b>Mobile - 1</b></td>
                                <td colspan="3" class="underline">{{ $customerDetails->contact }}</td>
                            </tr>
                            <tr>
                                <td class="label"><b>Mobile - 2</b></td>
                                <td colspan="3" class="underline"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Tel. Resi.</b></td>
                                <td class="half-line"></td>
                                <td class="label2"><b>Office</b></td>
                                <td class="half-line"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>Contact No.</b></td>
                                <td class="half-line"></td>
                                <td class="label2"><b>Bill No.</b></td>
                                <td class="half-line"></td>
                            </tr>
                            <tr>
                                <td class="label"><b>From</b></td>
                                <td class="half-line"></td>
                                <td class="label2"><b>To</b></td>
                                <td class="half-line"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>
