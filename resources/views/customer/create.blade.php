@extends('main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Enquiry</h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">{{ $customer_action }}</h3>
                                <div class="card-tools">
                                    <ul class="nav nav-pills ml-auto">
                                        <li class="nav-item btn-sm">
                                            <button class="btn-sm btn-danger" onclick="history.back()">Back</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form id="enquiry-form" action="{{ $customer_action_url }}" method="post">
                                    {{-- @csrf --}}
                                    <input type="hidden" name="id" value="{{ $getCustomer->id ?? '' }}">
                                    <!-- General Details Section -->
                                    <div class="card card-secondary">
                                        <div class="card-header">
                                            <h3 class="card-title">Personal Details</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="first_name">First Name<span
                                                                class="text-danger">*</span></label>
                                                        <input type="text" value="{{ $getCustomer->first_name ?? '' }}"
                                                            name="first_name" id="first_name" class="form-control" required>
                                                    </div>
                                                    <small id="firstNameErr" class="text-danger"
                                                        style="display:none;">*First name is reuired.</small>
                                                </div>

                                                {{-- <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="contact">Contact<span
                                                                class="text-danger">*</span></label>
                                                        <input type="number" value="{{ $getCustomer->contact ?? '' }}"
                                                            name="contact" id="contact" required class="form-control"
                                                            maxlength="10">
                                                    </div>
                                                    <small id="contactErr" class="text-danger"
                                                        style="display:none;">*Contact is reuired.</small>
                                                </div> --}}
                                           
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="gst_no">GST No</label>
                                                        <input type="text" value="{{ $getCustomer->gst_no ?? '' }}"
                                                            name="gst_no" id="gst_no" class="form-control">
                                                    </div>
                                                    <small id="gstNOErr" class="text-danger"
                                                        style="display:none;">*GST no is reuired.</small>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="concern_person">Remark</label>
                                                        <textarea name="remark" class="form-control">{{ $getCustomer->remark ?? '' }}</textarea>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="card-footer" style="background-color:white !important;float: right;">
                                        <button type="submit" class="btn btn-success"
                                            >Submit</button>
                                        &nbsp;
                                        <a href="/customer"><button type="button"
                                                class="btn btn-secondary">Cancel</button></a>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@push('scripts')

@endpush
