@php

    use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice</title>
    <style>
        body {
            font-size: 18px;
        }

        .invoice {
            text-align: center;
            font-size: 20px;
            padding: 0px;
            margin: 0px;
        }

        table,
        th,
        td {
            border-collapse: collapse;
            padding: 7px;
        }

        .services_header{
            border:1;text-align:left;width:20%
        }
        .services_details_header{
            border:1;
            text-align:right;
            width:10%
        }

        .services{
            
            text-align:left;width:20%
        }
        .services_details{
            border-left:1;
            text-align:right;
           
        }
    </style>
</head>

<body>
    <div class="invoice"><b>
            @if ($invoice_type == 0 && $settings->gst_no != '')
                Tax
            @endif Invoice
        </b></div>
    <table>
        <tr style="border:1">
            <td style="width:50%;">
                <img src="images/begon-logo.png" width="300px" height="120px" alt="Logo" class="begon-image">
            </td>
            <td style="text-align:right;width:50%;border-right: 0;">
                <b>{{ $settings->name ?? '' }}</b><br>
                WE ALSO PROVIDE 100% HERBAL (ODOURLESS) SERVICES
                <span style="text-transform: uppercase;">{{ $settings->address ?? '' }}</span> <br>
                Mobile No: 9892072628/9324972638<br>
                9323669191/7303290130/28280898/28282833
                {{-- &nbsp;&nbsp;
                 Mobile No:{{ $settings->mobile_no ?? '' }}&nbsp;&nbsp; --}}<br>
                E-mail:{{ $settings->email ?? '' }}<br>Website:{{ $settings->website ?? '' }}<br>LIC
                No:{{ $settings->lic_no ?? '' }}
                <br>
                @if ($invoice_type == 0 && $settings->gst_no != '')
                    GSTIN:{{ $settings->gst_no ?? '' }}, <br>
                @endif
                State:{{ $settings->state ?? '' }}
            </td>
        </tr>
        <tr style="border:1">
            <td style="width:50%;border:1">
                <b>Bill To</b>
            </td>
            <td style="text-align:right;width:50%;border-right: 0;">
                <b>Invoice Details</b>
            </td>
        </tr>
        <tr style="border:1">
            <td style="width:50%;border:1">
                @if (isset($customerPayment->customerDetails->first_name))
                    <b> {{ $customerPayment->customerDetails->first_name }}</b>
                @else
                    -
                @endif
                </b>
                <br>

                @if (isset($customerAddress->address_line1))
                    @php
                        // Split the address by commas
                        $addressParts = explode(',', $customerAddress->address_line1);
                    @endphp

                    @foreach ($addressParts as $part)
                        {{ trim($part) }}<br>
                    @endforeach
                @endif

                @if (!empty($customerPayment->customerDetails->gst_no))
                    GSTIN
                    : {{ $customerPayment->customerDetails->gst_no ?? '' }}
                    <br>
                @endif
                Contact No.:{{ $customerAddress->contact ?? '' }}
                </div>
            </td>
            <td style="text-align:right;width:50%;vertical-align:top">
                <b>Invoice
                    No:{{ $customerPayment->invoice_no ?? '' }}</b><br>Date:
                {{ date('d/m/Y', strtotime($customerPayment->invoice_date)) ?? '' }}
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 0px;">
                <table style="width:100%">
                    <tr style="">
                        <th  class="services_header" style="">Service Description</th>
                        <th  class="services_details_header" style="">Period</th>
                        <th  class="services_details_header" style="">Contract From</th>
                        <th  class="services_details_header" style="">Contract To</th>
                        @if ($isContractShow == '1')
                             <th  class="services_details_header" style="">Contract Amt</th>
                        @endif
                        <th  class="services_details_header" style="">HSN Code</th>
                        @if ($invoice_type == 0)
                            <th  class="services_details_header" style="">Taxable Amount</th>
                            <th  class="services_details_header" style="">GST </th>
                        @endif
                        <th  class="services_details_header" style="">Amount </th>
                    </tr>
                    @php
                        $totalAmount = 0;
                        $totalGst = 0;
                        $gstTotalAmount = 0;
                        $getSgst = 0;
                    @endphp
                    @if (count($customerPayment->customerServices) > 0)

                        @foreach ($customerPayment->customerServices as $servies)
                            <tr>
                                <td  class="services">
                                    <b>{{ ucfirst($servies->service_id) ?? '' }} </b>
                                    @if (!empty($servies->remark))
                                        <br>
                                        <span>( {{ $servies->remark }}) </span>
                                    @endif
                                   
                                    @if (!empty($servies->service_due_text))
                                        <br> Service Due:
                                        {{ $servies->service_due_text }}
                                    @endif
                                    
                                </td>
                                <td class="services_details">
                                    {{ $servies->period ?? '' }}
                                    
                                </td>
                                <td class="services_details">
                                    {{ $servies->contract_from ?? '' }}
                                   
                                </td>
                                <td class="services_details">
                                    {{ $servies->contract_to ?? '' }}
                                    
                                </td>
                                @if ($isContractShow == '1')
                                    <td
                                        style="">
                                        {{ $servies->contract ?? '' }}
                                        
                                    </td>
                                @endif

                                <td class="services_details">
                                    {{ $servies->hsn ?? '' }}
                                   
                                </td>
                                @if ($invoice_type == 0)
                                    <td  class="services_details">
                                        &#8377; {{ $servies->amount ?? '' }}
                                       
                                    </td>

                                    @php   $getSgst = $servies->gst / 2;@endphp

                                    <td class="services_details">
                                        &#8377; {{ ($servies->amount * $servies->gst) / 100 }}<br>
                                        {{-- ({{ $servies->gst ?? '' }}&#37;) --}} @isset($servies->gst)
                                            (CGST:{{ $getSgst }}%,<br>SGST:{{ $getSgst }}%)
                                        @endisset
                                       
                                    </td>
                                @endif
                                <td class="services_details">
                                    &#8377; @if ($invoice_type == 0)
                                        {{ $servies->gst_amount }}
                                    @else
                                        {{ $servies->amount ?? '' }}
                                    @endif
                                   
                                </td>
                            </tr>
                            @php
                                $totalAmount += $servies->amount;
                                $totalGst += ($servies->amount * $servies->gst) / 100;
                                if ($invoice_type == 0) {
                                    $gstTotalAmount += $servies->gst_amount;
                                } else {
                                    $gstTotalAmount += $servies->amount;
                                }
                            @endphp
                        @endforeach

                    @endif

                    {{-- <tr>
                        @php
                            if ($isContractShow == '1') {
                                $colspanvalue = 5;
                                $colspanvalue2 = 3;
                                if ($invoice_type == 0) {
                                    $colspanvalue = 6;
                                    $colspanvalue2 = 4;
                                }

                                $colspanvalue3 = '4';
                            } else {
                                $colspanvalue = 4;
                                $colspanvalue2 = 2;
                                if ($invoice_type == 0) {
                                    $colspanvalue = 5;
                                    $colspanvalue2 = 3;
                                }
                                $colspanvalue3 = '3';
                            }

                        @endphp
                        <td colspan="{{ $colspanvalue }}"
                            style="">
                            <b>Total</b>
                        </td>
                        <td style=""></td>
                       
                        @if ($invoice_type == 0)
                            <td style=""><b>&#8377;
                                    {{ $totalGst }}</b></td>
                        @endif
                        <td style="">
                            <b>&#8377; {{ $gstTotalAmount }}</b>
                        </td>
                    </tr> --}}
                    {{-- <tr>
                    
                        <td style=""
                            colspan="4">
                            <br>
                            <b>Invoice Amount In Words</b><br>{{ numberInword($gstTotalAmount) }}<b>
                                <br>

                                @if (!empty($customerPayment->payment_mode))
                                    Payment Mode
                            </b><br>{{ $customerPayment->payment_mode }}
                            @endif
                            <br>
                            <br>
                        </td>

                        <td style=""
                            colspan="@if ($invoice_type == 0) 3 @else 2 @endif">
                            <table style="border: none;">
                             
                                <tr>
                                    <td
                                        style="">

                                        <span><b>&nbsp;&nbsp;&nbsp;Total &nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
                                        <span style="float:right">&nbsp;&#8377;
                                            {{ $gstTotalAmount }}</span>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr> --}}
                </table>
            </td>
        </tr>
        {{-- <tr>
            <td colspan="2" style="padding: 0px;">
                <table style="width:100%">
                    <tr style="">
                       <td style="border:1;vertical-align:top;width:30%">
                            @php $i =1 ; @endphp
                            <b>Terms and Conditions</b><br><br>
                            @foreach ($termAndCondition as $T_C)
                                <p>{{ $i }}){{ $T_C->content }}</p>
                                @php $i++; @endphp
                            @endforeach
                            <P>Note: Kindly NOTE THAT THE above No. of Services may get skip, if not taken within
                                stipulated due period of the Services.</P><br>
                            <b>Bank Details</b><br><br>
                            <p>Bank Name : {{ $settings->bank_name }}</p><br>
                            <p>Bank Acount No : {{ $settings->bank_account }}</p><br>
                            <p>Bank IFSC code : {{ $settings->ifsc_code }}</p><br>
                            <p>Acount holder's name : {{ $settings->account_holder_name }}</p><br>
                       </td>
                       <td style="border:1;">
                             <img width="100" height="100" src=data:image/png;base64,{{ $qrCode }} <br>
                           <img width="200" height="100" src="images/scan_qr.png" id="upi" alt="Logo"
                                class="brand-image img-circle" > 
                       </td>
                       <td style="border:1;">
                            <p style="vertical-align: center;">For, BE-GON PEST CONTROL SERVICES</p>
                            <p style="vertical-align: middle;">Authorized Signatory</p>
                       </td>
                    </tr>
                </table>
            </td>
        </tr> --}}
    </table>
</body>

</html>
