@extends('main')

@section('content')
    <style>
        .custom-border {
            border: 1px solid #ccc;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Customer</h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h1 class="card-title"> Customer Details</h3>
                                    <div class="card-tools">
                                        <ul class="nav nav-pills ml-auto">
                                            <li class="nav-item btn-sm">
                                                <a class="btn-sm btn-danger" href="/customer">Back</a>
                                            </li>
                                        </ul>
                                    </div>
                            </div>
                            <!-- /.card-header -->

                            <div class="card-body">
                                {{-- @csrf --}}
                                <input type="hidden" name="id" value="{{ $getCustomer->id ?? '' }}">
                                <!-- General Details Section -->
                                <div class="card card-secondary">
                                    <div class="card-header">
                                        <h3 class="card-title">Personal Details</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-4 ">
                                                <div class="form-group">
                                                    <label for="first_name">Name : </label>
                                                    {{ $getCustomer->first_name ?? '' }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <div class="form-group">
                                                    <label for="gst_no">GST No</label>
                                                    {{ $getCustomer->gst_no ?? '' }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <div class="form-group">
                                                    <label for="middle_name">Remark</label>
                                                    {{ $getCustomer->remark ?? '' }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Communication Details Section -->
                                <div class="card card-secondary">
                                    <div class="card-header">
                                        <h3 class="card-title">Address</h3>
                                    </div>
                                    <div class="card-body">
                                        <a href="{{ '/add-customer-address/' . $getCustomer->id }}"
                                            class="btn btn-sm btn-info" style="float: right">Create</a>
                                        <div class="clearfix">&nbsp;</div>
                                        <table  class="table table-striped table-responsive">
                                            <tr>
                                                {{-- <th></th> --}}
                                                <th>Contact</th>
                                                <th>Email</th>
                                                {{-- <th>Contact No 1</th>
                                                <th>Contact No 2</th> --}}
                                                <th>Address</th>
                                                {{-- <th>Address 2</th> --}}
                                                <th>Landmark</th>
                                                <th>Road</th>
                                                <th>Near</th>
                                                <th>Street</th>
                                                <th>Opposite</th>
                                                <th>Locality</th>
                                                <th>Pincode</th>
                                                <th>Action</th>
                                            </tr>
                                            @foreach ($getCustomerAddress as $val)
                                                <tr>
                                                    {{-- <td><input checked type="radio" name="address"
                                                            value="{{ $val->id }}"></td> --}}
                                                    <td><a href="/customer-address-edit/{{ $val->id }}"
                                                            title="Edit">{{ $val->contact }}</a></td>
                                                    <td> {{ $val->email }}</td>
                                                    {{-- <td> {{ $val->contact_no1 }}</td>
                                                    <td> {{ $val->contact_no2 }}</td> --}}
                                                    <td> {{ $val->address_line1 }}</td>
                                                    {{-- <td> {{ $val->address_line2 }}</td> --}}
                                                    <td> {{ $val->landmark }}</td>
                                                    <td> {{ $val->road }}</td>
                                                    <td> {{ $val->near }}</td>
                                                    <td> {{ $val->street }}</td>
                                                    <td> {{ $val->opposite }}</td>
                                                    <td> {{ $val->locality }}</td>
                                                    <td> {{ $val->pin_code }}</td>
                                                    <td> <a onclick="printCard({{ $val->id }})"
                                                        style="margin:5px"
                                                        class="btn btn-sm btn-update btn-warning"
                                                        href="javascript:void(0)">Card</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                                <!-- Communication Details Section -->
                                <!-- Payment Details Section -->
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title">Payment</h3>
                                    </div>
                                    <div class="card-body">
                                        <a href="{{ '/customer-payment-create/' . $getCustomer->id }}"
                                            class="btn btn-sm btn-info" style="float: right">Create</a>
                                        <br>
                                        <hr>
                                        <div class="row">
                                            @foreach ($getCustomerPayment as $payment_val)
                                                <div class="col-md-12">
                                                    <div class="card-header">
                                                        <a href="{{ '/customer-payment-update/'.$getCustomer->id.'/'.$payment_val->id.'/'.$payment_val->is_bill }}" style="float: right;margin:5px" class="btn btn-sm btn-update btn-danger">Edit</a>
                                                        @if ($payment_val->is_cancelled == '0')
                                                            @if ($payment_val->is_bill == '0')
                                                                Quotation
                                                            @else
                                                                Invoice
                                                            @endif
                                                             No : {{ $payment_val->invoice_no }}
                                                            <a onclick="printContractForm({{ $payment_val->id }})"
                                                                style="float: right;margin:5px"
                                                                class="btn btn-sm btn-update btn-primary"
                                                                href="javascript:void(0)">Contract</a>
                                                            <a onclick="printInvoice({{ $payment_val->id }},1)"
                                                                style="float: right;margin:5px"
                                                                class="btn btn-sm btn-update btn-warning"
                                                                href="javascript:void(0)"> Invoice</a>
                                                            <a onclick="printInvoice({{ $payment_val->id }},0)"
                                                                style="float: right;margin:5px"
                                                                class="btn btn-sm btn-update btn-warning"
                                                                href="javascript:void(0)"> GST Invoice</a>
                                                        @else
                                                            <strike style="color: red">Invoice No :
                                                                {{ $payment_val->invoice_no }} </strike>
                                                            <a style="float:right;margin:5px"
                                                                class="btn btn-sm btn-update btn-danger"
                                                                href="javascript:void(0)">Payment Cancelled</a>
                                                        @endif

                                                    </div>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Finance Year : </label>
                                                                    {{ $payment_val->finance_year ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Invoice Date : </label>
                                                                    {{ $payment_val->invoice_date ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Address : </label>
                                                                    {{ $payment_val->customerPaymentAddress->address_line1 ?? '' }}
                                                                    {{ $payment_val->customerPaymentAddress->address_line2 ?? '' }}
                                                                    {{ $payment_val->customerPaymentAddress->landmark ?? '' }}
                                                                    {{ $payment_val->customerPaymentAddress->road ?? '' }}
                                                                    {{ $payment_val->customerPaymentAddress->street ?? '' }}
                                                                    {{ $payment_val->customerPaymentAddress->near ?? '' }}
                                                                    {{ $payment_val->customerPaymentAddress->opposite ?? '' }}
                                                                    {{ $payment_val->customerPaymentAddress->pin_code ?? '' }}
                                                                </div>
                                                            </div>
                                                            @php
                                                                $finalAmount = 0;
                                                                $finalAmountGST = 0;
                                                                $finalAmountGSTTotal = 0;
                                                            @endphp
                                                            @foreach ($payment_val->customerServices as $service_val)
                                                                <div class="col-md-12">
                                                                    @php
                                                                        $finalAmount += $service_val->amount;
                                                                        $finalAmountGST += round(($service_val->amount / 100) * $service_val->gst);
                                                                        $finalAmountGSTTotal += $service_val->gst_amount;
                                                                    @endphp
                                                                    <table class="table table-bordered table-striped table-responsive">
                                                                        <tr>
                                                                            {{-- <th>Category </th> --}}
                                                                            <th>Item </th>
                                                                            <th>HSN Code </th>
                                                                            <th>Description </th>
                                                                            <th>Service Due </th>
                                                                            <th>Contract From </th>
                                                                            <th>Contract To </th>
                                                                            <th>Contract Amount </th>
                                                                            <th>Period </th>
                                                                            <th>Price </th>
                                                                            <th>Quantity </th>
                                                                            <th>Amount </th>
                                                                            <th>GST </th>
                                                                            <th>Total Amount</th>
                                                                        </tr>
                                                                        <tr>
                                                                            {{-- <td>{{ $service_val->category->title ?? '' }}
                                                                            </td> --}}
                                                                            <td> {{ $service_val->service_id ?? '' }}
                                                                            </td>
                                                                            <td> {{ $service_val->hsn ?? '' }}
                                                                            </td>
                                                                            <td> {{ $service_val->remark ?? '' }}
                                                                            </td>
                                                                            <td> {{ $service_val->service_due_text ?? '' }}
                                                                            </td>
                                                                            <td>{{ $service_val->contract_from ?? '' }}
                                                                            </td>
                                                                            <td> {{ $service_val->contract_to ?? '' }}
                                                                            </td>
                                                                            <td>{{ $service_val->contract ?? '' }}
                                                                            </td>
                                                                            <td>{{ $service_val->period ?? '' }}
                                                                            </td>
                                                                            <td>{{ $service_val->price ?? '' }}
                                                                            </td>
                                                                            <td>{{ $service_val->quantity ?? '' }}
                                                                            </td>
                                                                            <td>{{ $service_val->amount ?? '' }}
                                                                            </td>
                                                                            <td>{{ $service_val->gst ?? '' }}</td>
                                                                            <td>{{ $service_val->gst_amount ?? '' }}
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>

                                                                {{-- <div class="col-xs-12 col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="first_name">Remark : </label>
                                                                        {{ $service_val->remark ?? '' }}
                                                                    </div>
                                                                </div> --}}
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="first_name">Service Due Date :
                                                                        </label>
                                                                        @foreach ($service_val->serviceDueDate as $serviceDueDate_val)
                                                                            @php
                                                                                $due_date = date('d M Y', strtotime($serviceDueDate_val->service_due));
                                                                            @endphp
                                                                            @if ($serviceDueDate_val->is_completed == '1')
                                                                                <button
                                                                                    onclick="serviceDue({{ $serviceDueDate_val->id }})"
                                                                                    class="btn btn-sm btn-success">{{ $due_date }}</button>
                                                                                @elseif(date('Y-m-d') < $serviceDueDate_val->service_due)
                                                                                <button
                                                                                    onclick="serviceDue({{ $serviceDueDate_val->id }})"
                                                                                    class="btn btn-sm btn-warning">{{ $due_date }}</button>
                                                                            @else
                                                                                <button
                                                                                    onclick="serviceDue({{ $serviceDueDate_val->id }})"
                                                                                    class="btn btn-sm btn-danger">{{ $due_date }}</button>
                                                                            @endif
                                                                        @endforeach

                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Total Amount : </label>
                                                                    {{ $finalAmount ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Total GST Amount :
                                                                    </label>
                                                                    {{ $finalAmountGST ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Final Amount : </label>
                                                                    {{ $finalAmountGSTTotal ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Paid Amount : </label>
                                                                    {{ $payment_val->paid_amount ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Payment Mode : </label>
                                                                    {{ $payment_val->payment_mode ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Bank Name : </label>
                                                                    {{ $payment_val->bank_name ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Payment Refernce :
                                                                    </label>
                                                                    {{ $payment_val->payment_reference ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name"> Payment Discussion :
                                                                    </label>
                                                                    {{ $payment_val->paymentDisussion->name ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Remark : </label>
                                                                    {{ $payment_val->remark ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Created By : </label>
                                                                    {{ $payment_val->createdBy->name ?? '' }}
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-4 ">
                                                                <div class="form-group">
                                                                    <label for="first_name">Updeted By : </label>
                                                                    {{ $payment_val->updeted_by->name ?? '' }}
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <hr>
                                                    <hr>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                                <!-- Communication Details Section -->
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        function isNumberKey(event) {
            // Check if the pressed key is a number
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                event.preventDefault();
                return false;
            }
            return true;
        }

        function printInvoice(payment_id, type = 0) {
            //var selectedAddress = $("input[name='address']:checked").val();
            var selectedAddress = "0";
            var url = "/customer-payment-pdf/" + payment_id + '/' + selectedAddress + "/" + type;
            window.open(url, '_blank');
        }

        function printContractForm(payment_id, type = 0) {
            // var selectedAddress = $("input[name='address']:checked").val();
            var selectedAddress = "0";
            var url = "/customer-contract-form/" + payment_id + '/' + selectedAddress + "/";
            window.open(url, '_blank');
        }

        function serviceDue(id) {

            var postData = {
                id: id,
            };
            $.post('/servicedue-update-status', postData, function(response) {
                location.reload();
            });
        }
        $('.service_id').select2();

        function printCard(id){
            var selectedAddress = "0";
            var url = "/customer-card/" + id + '/';
            window.open(url, '_blank');
        }
    </script>
@endpush
