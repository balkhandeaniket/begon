@extends('main')

@section('content')
    <style>
        body {
            font-size: 14px;
        }

        .custom-border {
            border: 1px solid #ccc;
            padding: 15px;
        }

        .validiaton {
            color: red;
        }

        .textUpperCase {
            text-transform: uppercase;
        }
    </style>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h5>Customer</h5>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h1 class="card-title"> Add Party</h3>
                                    <div class="card-tools">
                                        <ul class="nav nav-pills ml-auto">
                                            <li class="nav-item btn-sm">
                                                <a class="btn-sm btn-danger" href="/customer">Back</a>
                                            </li>
                                        </ul>
                                    </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                {{-- @csrf --}}
                                {{-- <input type="hidden" name="customer_id" id="customer_id" value="{{ $getCustomer->id ?? '' }}"> --}}

                                <!-- Payment Details Section -->
                                <div class="card">
                                    {{-- <div class="card-header">
                                        <h3 class="card-title"> Add Party</h3>
                                    </div> --}}
                                    <div class="card-body">
                                        <input type="hidden" id="payment_id" value="{{ $payment_id ?? '' }}">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="first_name">Customer <span class="validiaton"> * </span>
                                                        <span class="validiaton" id="customer_id_err"> </span></label>
                                                    <select onchange="getAddress()" name="customer_id" id="customer_id"
                                                        class="form-control">
                                                        <option value="">Select Customer </option>
                                                        @if (!empty($getCustomer))
                                                            <option value="{{ $getCustomer->id }}" selected>
                                                                {{ $getCustomer->first_name }}
                                                            </option>
                                                        @endif
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    @if (empty($payment_id))
                                                        <button type="button" style="margin-top:30px"
                                                            class="btn btn-md btn-info" data-toggle="modal"
                                                            data-target="#modal-default">Add Party</button>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <input type="hidden" value="{{ $is_bill }}" id="is_bill">
                                                   
                                                        <span> <label style="color: red;"> 
                                                            Previous  @if ($is_bill == 1) 
                                                            Invoice   
                                                            @else
                                                                Quotation
                                                            @endif
                                                            No.
                                                         </label> : {{ $previous_invoice_no }} </span> <br>
                                                   
                                                   

                                                    <label for="invoice_date">@if ($is_bill != 0)Invoice @else Quotation @endif Date <span class="validiaton"> *
                                                        </span> <span class="validiaton" id="invoice_date_err">
                                                        </span></label>

                                                    <input type="text" readonly name="invoice_date" id="invoice_date"
                                                        class="form-control"
                                                        value="{{ $getCustomerPayment->invoice_date ?? date('Y-m-d') }}">
                                                       
                                                        <input type="text"  oninput="invoiceNoUnique()" name="invoice_no" id="invoice_no"
                                                            class="form-control" placeholder="Invoice No."
                                                            value="{{ $invoice_no }}">
                                                        <b><span class="validiaton" id="invoice_no_err"> 
                                                            </span></b>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    <label for="customer_address_id">Billing Address <span
                                                            class="validiaton"> * </span> <span class="validiaton"
                                                            id="customer_address_id_err"> </span> </label>
                                                    <select name="customer_address_id" id="customer_address_id"
                                                        class="form-control select2">
                                                        {{-- <option value="">Select  Address</option> --}}
                                                        @foreach ($getAddress as $val)
                                                            <option value="{{ $val->id }}"
                                                                @if (!empty($getCustomerPayment) && $getCustomerPayment->customer_address_id == $val->id) selected @endif>
                                                                {{ $val->address_line1 }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                               
                                            </div>
                                        
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="invoice_date">State Of Supply</label> : &nbsp; Maharashtra
                                                </div>

                                            </div>
                                            <div class="clearfix">&nbsp;</div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="invoice_date">Category</label>  &nbsp;  <select name="category_id" id="category_id"
                                                    class="form-control select2">
                                                    <option value="">Select  Category</option>
                                                    @foreach ($category as $val)
                                                        <option value="{{ $val->id }}"
                                                            @if (!empty($getCustomerPayment) && $getCustomerPayment->category_id == $val->id) selected @endif>
                                                            {{ $val->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                </div>

                                            </div>
                                            <div class="clearfix">&nbsp;</div>
                                            {{-- <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="first_name">Name : </label>
                                                    {{ $getCustomer->first_name ?? '' }}
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="middle_name">Remark : </label>
                                                    {{ $getCustomer->remark ?? '' }}
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="row">
                                            <table class="table table-bordered table-striped table-responsived">
                                                <thead>
                                                    <tr>
                                                        <th style="padding: 0px;">ITEM</th>
                                                        <th style="padding: 0px;">HSN CODE</th>
                                                        <th style="padding: 0px;">DESCRIPTION</th>
                                                        <th style="padding: 0px;">SERVICE DUE</th>
                                                        <th style="padding: 0px;">CONTRACT FROM</th>
                                                        <th style="padding: 0px;">CONTRACT TO</th>
                                                        <th style="padding: 0px;">CONTRACT AMOUNT</th>
                                                        <th style="padding: 0px;">PERIOD</th>
                                                        <th style="padding: 0px;">QTY</th>
                                                        <th style="padding: 0px;" width="80px !important">PRICE/UNIT WITHOUT
                                                            TAX</th>
                                                        <th style="padding: 0px;">GST IN %</th>
                                                        <th style="padding: 0px;">GST AMT</th>
                                                        <th style="padding: 0px;">AMT</th>
                                                        <th style="padding: 0px;display:none;">SERVICE DATE</th>
                                                        <th style="padding: 0px;">*</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="append_service">
                                                    @if (!empty($getCustomerPayment->customerServices))
                                                        @foreach ($getCustomerPayment->customerServices as $item)
                                                            @php
                                                                $rand = rand();
                                                            @endphp
                                                            <tr class="service_{{ $rand }}">

                                                                <td style="padding: 0px;">
                                                                    <input style="width: 200px !important;" type="hidden"
                                                                        class="services" value="{{ $rand }}">
                                                                    <input value="{{ $item->service_id }}" type="text"
                                                                        id="service_id_{{ $rand }}"
                                                                        name="service_id[]"
                                                                        class="service_id autocomplete form-control"
                                                                        list="suggestions_{{ $rand }}">
                                                                    <datalist onclick="testData()"
                                                                        id="suggestions_{{ $rand }}">
                                                                        <!-- Suggestions will be populated dynamically via JavaScript -->
                                                                    </datalist>
                                                                    {{-- <select style="width: 180px;"
                                                                        id="service_id_{{ $rand }}"
                                                                        name="service_id[]"
                                                                        class="form-control select2 service_id">
                                                                        <option value="">Select</option>
                                                                        @foreach ($services as $row)
                                                                            <option value="{{ $row->id }}"
                                                                                @if ($item->service_id == $row->id) selected @endif>
                                                                                {{ $row->title }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select> --}}
                                                                </td>
                                                                <td style="padding: 0px;"><input
                                                                        style="width: 80px !important;" type="text"
                                                                        id="hsn_{{ $rand }}" name="hsn[]"
                                                                        class="hsn form-control autocomplete"
                                                                        list="hsn_auto_{{ $rand }}"
                                                                        value="{{ $item->hsn ?? '998531' }}">
                                                                    {{-- <datalist id="hsn_auto_{{ $rand }}">
                                                                        @foreach ($hsn as $row)
                                                                            <option value="{{ $row->hsn }}"> {{ $row->title }}
                                                                            </option>
                                                                        @endforeach
                                                                    </datalist> --}}
                                                                </td>
                                                                <td style="padding: 0px;"><input
                                                                        style="width: 140px !important;" type="text"
                                                                        id="remark_{{ $rand }}" name="remark[]"
                                                                        class="remark form-control autocomplete"
                                                                        value="{{ $item->remark ?? '' }}"
                                                                        list="remark_auto_{{ $rand }}">
                                                                    <datalist id="remark_auto_{{ $rand }}">
                                                                        @foreach ($remark as $row)
                                                                            <option value="{{ $row->remark }}">
                                                                                {{ $row->title }}
                                                                            </option>
                                                                        @endforeach
                                                                    </datalist>
                                                                </td>
                                                                <td style="padding: 0px;"><input
                                                                        style="width: 140px !important;" type="text"
                                                                        id="service_due_text_{{ $rand }}"
                                                                        name="service_due_text[]"
                                                                        class="service_due_text form-control"
                                                                        value="{{ $item->service_due_text ?? '' }}"></td>
                                                                <td style="padding: 0px;">
                                                                    <input style="width: 100px !important;" type="text" autocomplete="off"
                                                                        id="contract_from_{{ $rand }}"
                                                                        name="contract_from[]"
                                                                        class="form-control contract_from"
                                                                        value="{{ $item->contract_from ?? '' }}">

                                                                </td>
                                                                <td style="padding: 0px;">

                                                                    <input style="width: 100px !important;" type="text" autocomplete="off"
                                                                        id="contract_to_{{ $rand }}"
                                                                        name="contract_to[]"
                                                                        class="form-control contract_to"
                                                                        value="{{ $item->contract_to ?? '' }}">
                                                                    @push('scripts')
                                                                        <script>
                                                                            $(function() {
                                                                                $("#contract_from_{{ $rand }}").datepicker({
                                                                                    dateFormat: 'dd/mm/yy',
                                                                                });
                                                                                $("#contract_to_{{ $rand }}").datepicker({
                                                                                    dateFormat: 'dd/mm/yy',
                                                                                });

                                                                                // updateSuggestions("","{{ $rand }}");
                                                                                $('#service_id_' + "{{ $rand }}").on('input', function() {
                                                                                    var inputValue = $(this).val();
                                                                                    updateSuggestions(inputValue, "{{ $rand }}");
                                                                                });

                                                                            });
                                                                        </script>
                                                                    @endpush
                                                                </td>
                                                                <td style="padding: 0px;"><input
                                                                        style="width: 80px !important;" type="text"
                                                                        id="contract_{{ $rand }}"
                                                                        name="contract[]" class="form-control contract"
                                                                        value="{{ $item->contract ?? '' }}"></td>
                                                                <td style="padding: 0px;"> <input
                                                                        style="width: 80px !important;" type="text"
                                                                        id="period_{{ $rand }}" name="period[]"
                                                                        class="period form-control"
                                                                        value="{{ $item->period ?? '' }}"></td>

                                                                <td style="padding: 0px;"><input
                                                                        style="width: 50px !important;"
                                                                        style="width:50px !important;" type="text"
                                                                        onkeypress="return isNumberKey(event)"
                                                                        oninput="handlePriceChange({{ $rand }})"
                                                                        id="quantity_{{ $rand }}"
                                                                        name="quantity[]" class="form-control quantity"
                                                                        value="{{ $item->quantity ?? '' }}"></td>
                                                                <td style="padding: 0px;">
                                                                    <input style="width: 80px !important;" type="text"
                                                                        onkeypress="return isNumberKey(event)"
                                                                        oninput="handlePriceChange({{ $rand }})"
                                                                        id="price_{{ $rand }}" name="price[]"
                                                                        class="price form-control"
                                                                        value="{{ $item->price ?? '' }}">
                                                                    <input type="hidden"
                                                                        onkeypress="return isNumberKey(event)" readonly
                                                                        id="amount_{{ $rand }}" name="amount[]"
                                                                        class="form-control amount" value="">
                                                                </td>
                                                                {{-- <td style="padding: 0px;"><label for="amount">Amount</label> <span id="err_amount_{{ $rand }}" class="validiaton">
                                                                    <input type="text" onkeypress="return isNumberKey(event)" readonly
                                                                        id="amount_{{ $rand }}" name="amount[]" class="form-control amount" value=""></td> --}}
                                                                <td style="padding: 0px;"> <input
                                                                        style="width: 50px !important;" type="text"
                                                                        onkeypress="return isNumberKey(event)"
                                                                        oninput="handlePriceChange({{ $rand }})"
                                                                        id="gst_{{ $rand }}" name="gst[]"
                                                                        class="form-control gst"
                                                                        value="{{ $item->gst ?? '' }}"></td>
                                                                <td style="padding: 0px;"><input
                                                                        style="width: 80px !important;" type="text"
                                                                        onkeypress="return isNumberKey(event)" readonly
                                                                        id="gst_cal_amount_{{ $rand }}"
                                                                        name="gst_cal_amount[]"
                                                                        class="form-control gst_cal_amount"
                                                                        value="{{ $item->gst_cal_amount ?? '' }}"></td>
                                                                <td style="padding: 0px;"> <input
                                                                        style="width: 80px !important;"
                                                                        type="text"
                                                                        onkeypress="return isNumberKey(event)"
                                                                        id="gst_amount_{{ $rand }}"
                                                                        name="gst_amount[]" oninput="calGstAmount({{ $rand }});"
                                                                        class="form-control gst_amount"
                                                                        value="{{ $item->gst_amount ?? '' }}"></td>
                                                                <td class="service_due_date" style="display:none;"><button
                                                                        class="btn btn-sm btn-info"
                                                                        onclick="addServiceDueDate({{ $rand }})">
                                                                        + </button> <span
                                                                        id="err_service_due_{{ $rand }}"
                                                                        class="validiaton">
                                                                        @if (!empty($item->serviceDueDate))
                                                                            @foreach ($item->serviceDueDate as $serviceDueDate)
                                                                                <input style="width: 140px !important;"
                                                                                    type="date"
                                                                                    id="service_due_{{ $rand }}"
                                                                                    name="service_due[]"
                                                                                    class="form-control service_due"
                                                                                    value="{{ $serviceDueDate->service_due }}">
                                                                            @endforeach
                                                                        @else
                                                                            <input style="width: 140px !important;"
                                                                                type="date"
                                                                                id="service_due_{{ $rand }}"
                                                                                name="service_due[]"
                                                                                class="form-control service_due"
                                                                                value="">
                                                                        @endif
                                                                </td>
                                                                <td style="padding: 0px;">

                                                                    <button class="btn btn-sm btn-danger removeButton"
                                                                        onclick="removeService({{ $rand }}); handlePriceChange({{ $rand }});"
                                                                        style="float:right">&times;</button>

                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif

                                                </tbody>
                                                <tfoot>
                                                    <td colspan="7"><button onclick="addServices()"
                                                            class="btn btn-sm btn-info" style="float: left">Add
                                                            Row</button></td>
                                                    <td style="padding: 0px;">-</td>
                                                    <td style="padding: 0px;"><b>Total</b></td>
                                                    <td style="padding: 0px;"> <input style="width: 80px !important;"
                                                            type="text" onkeypress="return isNumberKey(event)"
                                                            name="total_amount" readonly id="total_amount"
                                                            class="form-control" value=""></td>
                                                    <td style="padding: 0px;">-</td>
                                                    <td style="padding: 0px;"><input style="width: 80px !important;"
                                                            type="text" onkeypress="return isNumberKey(event)"
                                                            name="total_gst_amount" readonly class="form-control"
                                                            id="all_gst_amount_cal"></td>
                                                    <td> <input style="width: 80px !important;" type="text"
                                                            onkeypress="return isNumberKey(event)" name="total_gst_amount"
                                                            readonly id="total_gst_amount" class="form-control"
                                                            value=""></td>
                                                    <td style="padding: 0px;"></td>
                                                </tfoot>
                                            </table>
                                        </div>

                                        {{-- <div id="append_service">

                                        </div> --}}
                                        {{-- <div class="clearfix">&nbsp;</div>
                                        <div class="col-md-12">
                                            <button onclick="addServices()" class="btn btn-sm btn-info"
                                                style="float: left">Add Row</button>
                                        </div>
                                        --}}
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="row">


                                            {{-- <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="total_amount">Total Amount</label>
                                                    <input type="text" onkeypress="return isNumberKey(event)"
                                                        name="total_amount" readonly id="total_amount" class="form-control"
                                                        value="">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="total_gst_amount">Total Amount (+ GST)</label>
                                                    <input type="text" onkeypress="return isNumberKey(event)"
                                                        name="total_gst_amount" readonly id="total_gst_amount"
                                                        class="form-control" value="">
                                                </div>
                                            </div> --}}
                                            {{-- <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="paid_amount">Paid Amount</label>
                                                    <input type="text" onkeypress="return isNumberKey(event)"
                                                        name="paid_amount" id="paid_amount" class="form-control"
                                                        value="{{ $getCustomerPayment->paid_amount ?? '' }}">
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="payment_mode">Payment Mode</label>
                                                    <select name="payment_mode" id="payment_mode"
                                                        class="form-control select2">
                                                        <option value=""> Select </option>
                                                        @foreach ($paymentMode as $val)
                                                            <option value="{{ $val }}"
                                                                @if (!empty($getCustomerPayment) && $getCustomerPayment->payment_mode == $val) selected @endif>
                                                                {{ $val }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="bank_name">Bank Name</label>
                                                    <input type="text" name="bank_name" id="bank_name"
                                                        class="form-control"
                                                        value="{{ $getCustomerPayment->bank_name ?? '' }}">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="payment_reference">Payment Reference</label>
                                                    <input type="text" name="payment_reference" id="payment_reference"
                                                        class="form-control"
                                                        value="{{ $getCustomerPayment->payment_reference ?? '' }}">
                                                </div>
                                            </div> --}}
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="payment_disussion">Disussion Of Payment </label>
                                                    <select name="payment_disussion" id="payment_disussion"
                                                        class="form-control select2">
                                                        <option value="">Select User</option>
                                                        @foreach ($getUsers as $val)
                                                            <option value="{{ $val->id }}"
                                                                @if (!empty($getCustomerPayment) && $getCustomerPayment->payment_disussion == $val->id) selected @endif>
                                                                {{ $val->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="remark">Remark</label><br>
                                                    <textarea id="remark" name="remark">{{ $getCustomerPayment->remark ?? '' }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">&nbsp;</div>
                                        <button id="submitBtn" class="btn btn-sm btn-success" type="button"
                                            onclick="return submitPayment()" style="float: right"> Submit</button>
                                    </div>
                                </div>
                                <!-- Communication Details Section -->
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Party</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="customer-form" action="/customer-add" method="post">
                        {{-- @csrf --}}
                        <!-- General Details Section -->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label  autocomplete="off"  for="first_name">First Name<span class="text-danger">*</span></label>
                                    <input type="text" value="" name="first_name" id="first_name"
                                        class="form-control" required>
                                </div>
                                <spam id="firstNameErr" class="text-danger" style="display:none;">*First name is
                                    reuired.</spam>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contact">Contact<span class="text-danger">*</span></label>
                                    <input  autocomplete="off"  type="text" value="" name="contact" id="contact" required
                                        class="form-control">
                                </div>
                                <spam id="contactErr" class="text-danger" style="display:none;">*Contact is
                                    reuired.</spam>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="gst_no">GST No</label>
                                    <input type="text" value="" name="gst_no" autocomplete="off" id="gst_no"
                                        class="form-control textUpperCase">
                                </div>
                                <spam id="gstNOErr" class="text-danger" style="display:none;">*GST no invlaid .</spam>
                                <spam id="gstNoRight" style="color: green;display:none;" >*GST no <i class="fas fa-check"></i></spam>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="concern_person">Address <span class="text-danger">*</span></label>
                                    <textarea  autocomplete="off"  name="address" id="address" required class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="concern_person">Remark</label>
                                    <textarea name="remark" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <span class="text-danger" id="emptyErr" style="display:none">All * entries are
                            required.</span>
                            <span class="text-danger" id="numErr" style="display:none">*Please enter valid contact number.</span>
                        <span class="text-danger" id="numLenErr" style="display:none">*Customer already existed with this number.</span>
                        <div class="card-footer" style="background-color:white !important;float: right;">
                            <button type="button" id="checkNumberValidation" class="btn btn-success">Submit</button>
                            <button type="submit" id="addCustomerbtn" class="btn btn-success"
                                style="display:none;">Submit</button>
                            &nbsp;
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.content-wrapper -->
    <input type="hidden" id="getSuggestion" value="{{ json_encode($service_html) }}">
@endsection
@push('scripts')
    <script>
        function invoiceNoUnique() {

            is_bill = $("#is_bill").val()
            invoice_no = $("#invoice_no").val()
            payment_id = $("#payment_id").val()
           
            var postData = {
                "invoice_no": invoice_no,
                "payment_id": payment_id,
                "is_bill": is_bill
            };
            $.post('/customer-invoice-unique', postData, function(response) {
                if (parseInt(response) > 0) {
                    if (parseInt(is_bill) == 1) {
                        $("#invoice_no_err").text("Invoice no. should be unique")
                    }else{
                        $("#invoice_no_err").text("Quotation no. should be unique")
                    }
                    $("#submitBtn").attr('disabled',true)
                
                } else {
                    $("#invoice_no_err").text("")
                    $("#submitBtn").attr('disabled',false)
                }
            }).done(function() {

            });
            
        }
        invoiceNoUnique();

         function calGstAmount(id) {
            var totalAmount = $("#gst_amount_" + id).val();
            var gstRate = 18;

            var baseAmount = totalAmount / (1 + gstRate / 100);
            var gstAmount = totalAmount - baseAmount;

            // Round off the values
            baseAmount = Math.round(baseAmount);
            gstAmount = Math.round(gstAmount);

            $("#gst_" + id).val(gstRate)
            $("#price_" + id).val(baseAmount)
            $("#amount_" + id).val(baseAmount)
            $("#gst_cal_amount_" + id).val(gstAmount)

            amount_cal = 0;
            $(".price").each(function() {
                amount = $(this).val();
                amount_cal = parseInt(amount_cal) + parseInt(amount);
            })
            $("#total_amount").val(amount_cal)

            gst_amount_cal = 0;
            $(".gst_amount").each(function() {
                gst_amount = $(this).val();
                gst_amount_cal = parseInt(gst_amount_cal) + parseInt(gst_amount);
            })
            $("#total_gst_amount").val(gst_amount_cal)

            all_gst_amount_cal = 0;
            $(".gst_cal_amount").each(function() {
                gst_add_amount = $(this).val();
                if (gst_add_amount != "") {
                    all_gst_amount_cal = parseInt(all_gst_amount_cal) + parseInt(gst_add_amount);
                }
            })
            $("#all_gst_amount_cal").val(all_gst_amount_cal)
        }


        $(document).ready(function() {
            // Initialize Select2
            setTimeout(() => {
                $('#customer_id').select2({
                    width: '100%', // Set the width to 100%
                    placeholder: 'Search customer...', // Set the placeholder text
                    allowClear: true,
                    tokenSeparators: [','],
                    minimumInputLength: 2,
                    minimumResultsForSearch: 10,
                    ajax: {
                        // url: $('#search_types_value').attr('data-url'),
                        url: '/customer-filter-get',
                        dataType: "json",
                        type: "POST",
                        data: function(params) {
                            var queryParameters = {
                                term: params.term,
                                customer_id: $("#customer_id").val(),
                            }
                            return queryParameters;
                        },
                        processResults: function(data) {
                            // $("#customer_id").html(data.html);
                            return {
                                // alert(JSON.parse(data));
                                results: $.map(data.result, function(item) {
                                    return {
                                        text: item.text,
                                        id: item.id,
                                    }
                                })
                            };
                        }
                    }
                });
            }, 1000);
            // Listen for keyup event
        });
        $(function() {
            $("#invoice_date").datepicker({
                dateFormat: 'yy-mm-dd',
            });

        });
        // Sample suggestion data
        getSuggestion = $("#getSuggestion").val()
        var suggestions = JSON.parse(getSuggestion);

        // Function to update suggestions based on input value
        function updateSuggestions(inputValue, rand) {
            // Clear previous suggestions

            // Filter suggestions based on input value
            var filteredSuggestions = suggestions.filter(function(suggestion) {
                return suggestion.toLowerCase().includes(inputValue.toLowerCase());
            });
            // Populate datalist with filtered suggestions
            $("#suggestions_" + rand).html('')
            filteredSuggestions.forEach(function(suggestion) {

                $("#suggestions_" + rand).append('<option class="abcd" value="' + suggestion + '">' + suggestion +
                    '</option>');
            });
        }

        $("#pushMenuClose").click();

        function isNumberKey(event) {
            // Check if the pressed key is a number
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                event.preventDefault();
                return false;
            }
            return true;
        }

        function getServices(rand_id) {
            category_id = $("#category_id_" + rand_id).val()
            var postData = {
                "category_id": category_id
            };
            $.post('/customer-services', postData, function(response) {
                $('#service_id_' + rand_id).html(response.html);
            }).done(function() {

            });
        }

        function getAddress() {
            customer_id = $("#customer_id").val();

            if (customer_id != "") {
                var postData = {
                    "customer_id": customer_id
                };
                $.post('/get-customer-address', postData, function(response) {
                    $('#customer_address_id').html(response.html);
                }).done(function() {

                });
            }
        }
        //getAddress();

        function addServices() {
            var postData = {

            };
            $.post('/customer-add-services', postData, function(response) {
                $('#append_service').append(response.html);

                // $('#contract_form_date_' + response.random_no).datepicker();
                $(function() {
                    $("#contract_from_" + response.random_no).datepicker({
                        dateFormat: 'dd/mm/yy',
                    });
                    $("#contract_to_" + response.random_no).datepicker({
                        dateFormat: 'dd/mm/yy',
                    });
                });

                updateSuggestions("", response.random_no);
                $('#service_id_' + response.random_no).on('input', function() {
                    var inputValue = $(this).val();
                    updateSuggestions(inputValue, response.random_no);
                });
            }).done(function() {
                removeButton = $(".removeButton").length
                if (parseInt(removeButton) <= 1) {
                    $(".removeButton").hide()
                } else {
                    $(".removeButton").show()
                }
                //$('.select2').select2();
            });
        }

        payment_id = $("#payment_id").val();
        if (payment_id == "") {
            addServices();
        }

        removeButton = $(".removeButton").length
        if (parseInt(removeButton) <= 1) {
            $(".removeButton").hide()
        } else {
            $(".removeButton").show()
        }

        function removeService(classVal) {
            $(".service_" + classVal).html('')

            removeButton = $(".removeButton").length
            if (parseInt(removeButton) <= 1) {
                $(".removeButton").hide()
            } else {
                $(".removeButton").show()
            }
        }

        function removeServiceDueDate(rand_id, service_due_date) {
            $(".service_" + rand_id + " #service_due_date_" + service_due_date).html('')
        }

        function addServiceDueDate(rand_id) {
            var random = Math.floor(Math.random() * 1000000)
            $(".service_" + rand_id + " .service_due_date").append('<div id="service_due_date_' + random +
                '"><input type="date" name="service_due[]" id="" class="form-control service_due" value=""> <button class="btn btn-sm btn-danger removeButton" onclick="removeServiceDueDate(' +
                rand_id + ',' + random + ')">&times;</button></div>');
        }

        function handlePriceChange(rand_id,roundOFfNotWork = "") {

            price = $(".service_" + rand_id + " .price").val()
            if (price != "" && parseInt(price) > 0) {
                price = price;
            } else {
                price = 0;
            }

            quantity = $(".service_" + rand_id + " .quantity").val()
            if (quantity != "" && parseInt(quantity) > 0) {
                quantity = quantity;
            } else {
                quantity = 0;
            }

            if (parseInt(price) > 0 && parseInt(quantity) > 0) {
                amountCal = parseInt(price) * parseInt(quantity);

                $(".service_" + rand_id + " .amount").val(amountCal)

                gst = $(".service_" + rand_id + " .gst").val()
                if (gst != "" && parseInt(gst) > 0) {
                    gst = gst;
                } else {
                    gst = 0;
                }
                gstCal = 0;
                if (parseInt(gst) > 0) {
                    if (roundOFfNotWork == "roundOFfNotWork") {
                        gstCal = $(".service_" + rand_id + " .gst_cal_amount").val()
                    }else{
                        gstCal = Math.round(amountCal / 100 * parseInt(gst));
                    }
                    amountCal = parseInt(amountCal) + parseInt(gstCal);
                }

                $(".service_" + rand_id + " .gst_cal_amount").val(gstCal)
                $(".service_" + rand_id + " .gst_amount").val(amountCal)

            } else {
                $(".service_" + rand_id + " .amount").val('')
                $(".service_" + rand_id + " .gst_cal_amount").val('')
                $(".service_" + rand_id + " .gst_amount").val('')
            }

            amount_cal = 0;
            $(".amount").each(function() {
                amount = $(this).val();
                amount_cal = parseInt(amount_cal) + parseInt(amount);
            })
            $("#total_amount").val(amount_cal)

            gst_amount_cal = 0;
            $(".gst_amount").each(function() {
                gst_amount = $(this).val();
                gst_amount_cal = parseInt(gst_amount_cal) + parseInt(gst_amount);
            })
            $("#total_gst_amount").val(gst_amount_cal)

            all_gst_amount_cal = 0;
            $(".gst_cal_amount").each(function() {
                gst_add_amount = $(this).val();
                if (gst_add_amount != "") {
                    all_gst_amount_cal = parseInt(all_gst_amount_cal) + parseInt(gst_add_amount);
                }
            })
            $("#all_gst_amount_cal").val(all_gst_amount_cal)

        }

        payment_id = $("#payment_id").val();
        if (payment_id != "") {
            $(".services").each(function() {
                handlePriceChange($(this).val(),'roundOFfNotWork')

            })
        }

        function printInvoice(payment_id, type = 0) {
            //var selectedAddress = $("input[name='address']:checked").val();
            var selectedAddress = "0";
            var url = "/customer-payment-pdf/" + payment_id + '/' + selectedAddress + "/" + type;
            window.open(url, '_blank');
        }

        function submitPayment() {
            customer_id = $("#customer_id").val()
            if (customer_id == "") {
                $("#customer_id_err").text('Required')
                setTimeout(() => {
                    $("#customer_id_err").text('')
                }, 1000);
                return false;
            }

            customer_address_id = $("#customer_address_id").val();
            if (customer_address_id == "" || customer_address_id == null) {
                $("#customer_address_id_err").text('Required')
                setTimeout(() => {
                    $("#customer_address_id_err").text('')
                }, 1000);
                return false;
            }

            invoice_date = $("#invoice_date").val()
            if (invoice_date == "") {
                $("#invoice_date_err").text('Required')
                setTimeout(() => {
                    $("#invoice_date_err").text('')
                }, 1000);
                return false;
            }
            // category_id_err_count = 0;
            // $(".category_id").each(function() {
            //     if ($(this).val() == "") {
            //         category_id_err_count += 1;
            //     }
            // });

            // if (category_id_err_count >= 1) {
            //     alert('Please select category.')
            //     return false;
            // }

            service_id_err_count = 0;
            $(".service_id").each(function() {
                if ($(this).val() == "") {
                    service_id_err_count += 1;
                }
            });

            if (service_id_err_count >= 1) {
                alert('Please select item.')
                return false;
            }

            /*
                service_due_err_count = 0;
                $(".service_due").each(function() {
                    if ($(this).val() == "") {
                        service_due_err_count += 1;
                    }
                })
                if (service_due_err_count >= 1) {
                    alert('Please select service due date.')
                    return false;
                }


                contract_from_err_count = 0;
                $(".contract_from").each(function() {
                    if ($(this).val() == "") {
                        contract_from_err_count += 1;
                    }
                })
                if (contract_from_err_count >= 1) {
                    alert('Please select contract from date.')
                    return false;
                }

                contract_to_err_count = 0;
                $(".contract_to").each(function() {
                    if ($(this).val() == "") {
                        contract_to_err_count += 1;
                    }
                })
                if (contract_to_err_count >= 1) {
                    alert('Please select contract to date.')
                    return false;
                }
                 */

            price_err_count = 0;
            $(".price").each(function() {
                if ($(this).val() == "") {
                    price_err_count += 1;
                }
            })
            if (price_err_count >= 1) {
                alert('Required price.')
                return false;
            }

            quantity_err_count = 0;
            $(".quantity").each(function() {
                if ($(this).val() == "") {
                    quantity_err_count += 1;
                }
            })
            if (quantity_err_count >= 1) {
                alert('Required quantity.')
                return false;
            }

            var service_due_dates = [];
            $(".services").each(function() {
                service_div_id = $(this).val()

                service_wise_due_date = [];
                $(".service_" + service_div_id + " .service_due").each(function() {
                    service_due_date = $(this).val()
                    if (service_due_date != "") {
                        service_wise_due_date.push(service_due_date)
                    }
                })
                service_due_dates.push(service_wise_due_date)
            })
            // category_ids = [];
            // $(".category_id").each(function() {
            //     category_id = $(this).val()
            //     category_ids.push(category_id)
            // })

            service_ids = [];
            $(".service_id").each(function() {
                service_id = $(this).val()
                service_ids.push(service_id)
            })

            hsns = [];
            $(".hsn").each(function() {
                hsn = $(this).val()
                hsns.push(hsn)
            })

            remarks = [];
            $(".remark").each(function() {
                remark = $(this).val()
                remarks.push(remark)
            })

            service_due_texts = [];
            $(".service_due_text").each(function() {
                service_due_text = $(this).val()
                service_due_texts.push(service_due_text)
            })

            contract_froms = [];
            $(".contract_from").each(function() {
                contract_from = $(this).val()
                contract_froms.push(contract_from)
            })

            contract_tos = [];
            $(".contract_to").each(function() {
                contract_to = $(this).val()
                contract_tos.push(contract_to)
            })

            contracts = [];
            $(".contract").each(function() {
                contract = $(this).val()
                contracts.push(contract)
            })

            periods = [];
            $(".period").each(function() {
                period = $(this).val()
                periods.push(period)
            })
            prices = [];
            $(".price").each(function() {
                price = $(this).val()
                prices.push(price)
            })
            quantitys = [];
            $(".quantity").each(function() {
                quantity = $(this).val()
                quantitys.push(quantity)
            })
            amounts = [];
            $(".amount").each(function() {
                amount = $(this).val()
                amounts.push(amount)
            })
            gsts = [];
            $(".gst").each(function() {
                gst = $(this).val()
                gsts.push(gst)
            })

            gst_cal_amounts = [];
            $(".gst_cal_amount").each(function() {
                gst_cal_amount = $(this).val()
                gst_cal_amounts.push(gst_cal_amount)
            })
            gst_amounts = [];
            $(".gst_amount").each(function() {
                gst_amount = $(this).val()
                gst_amounts.push(gst_amount)
            })


            customer_id = $("#customer_id").val();
            invoice_date = $("#invoice_date").val();
            customer_address_id = $("#customer_address_id").val();
            total_amount = $("#total_amount").val();
            total_gst_amount = $("#total_gst_amount").val();
            paid_amount = $("#paid_amount").val();
            payment_mode = $("#payment_mode").val();
            bank_name = $("#bank_name").val();
            payment_reference = $("#payment_reference").val();
            payment_disussion = $("#payment_disussion").val();
            remark = $("#remark").val();
            payment_id = $("#payment_id").val();
            invoice_no = $("#invoice_no").val();
            category_id = $("#category_id").val();
            is_bill = $("#is_bill").val();
            var postData = {
                "customer_id": customer_id,
                //"category_ids":category_ids,
                "service_ids": service_ids,
                "hsns": hsns,
                "remarks": remarks,
                "service_due_texts": service_due_texts,
                "service_due_dates": service_due_dates,
                "contract_froms": contract_froms,
                "contract_tos": contract_tos,
                "contracts": contracts,
                "periods": periods,
                "prices": prices,
                "quantitys": quantitys,
                "amounts": amounts,
                "gsts": gsts,
                "gst_cal_amounts": gst_cal_amounts,
                "gst_amounts": gst_amounts,
                "invoice_date": invoice_date,
                "invoice_no": invoice_no,
                "customer_address_id": customer_address_id,
                "total_amount": total_amount,
                "total_gst_amount": total_gst_amount,
                "paid_amount": paid_amount,
                "payment_mode": payment_mode,
                "bank_name": bank_name,
                "payment_reference": payment_reference,
                "payment_disussion": payment_disussion,
                "remark": remark,
                "payment_id": payment_id,
                'category_id' :category_id,
                'is_bill' :is_bill,
            }

            $("#submitBtn").attr('disabled', true);
            $.post('/customer-submit-payment-action', postData, function(response) {
                printInvoice(response.payment_id, response.is_gst)
                window.location.href = "/customer-show/" + response.customer_id;

            }).done(function() {

            });
        }

        $('#gst_no').on('input', function() {
            var gstNumber = $('#gst_no').val()
            if (gstNumber != "") {
                var gstNumber = $('#gst_no').val().toUpperCase();  // Convert to uppercase for consistency
                var gstPattern = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}[Z]{1}[0-9A-Z]{1}$/;
                var result = gstPattern.test(gstNumber);
                if (!result) {
                    $('#gstNoRight').fadeOut();
                    $('#gstNOErr').fadeIn();
                }else{
                    $('#gstNOErr').fadeOut();
                    $("#gstNoRight").fadeIn();
                }
            }else{
                $('#gstNOErr').fadeOut();
                $("#gstNoRight").fadeOut();
            }
        });
       

        $('#checkNumberValidation').on('click', function() {
            first_name = $('#first_name').val();
            contact = $('#contact').val();
            address = $('#address').val();


            var postData = {
                "contact": $('#contact').val(),
            };
            if (first_name == "" || contact == "" || address == "") {
                $('#emptyErr').fadeIn();
                setTimeout(() => {
                    $('#emptyErr').fadeOut();

                }, 5000);
                return false;
            }

            var gstNumber = $('#gst_no').val()
            if (gstNumber != "") {
                var gstNumber = $('#gst_no').val().toUpperCase();  // Convert to uppercase for consistency
                var gstPattern = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}[Z]{1}[0-9A-Z]{1}$/;
                var result = gstPattern.test(gstNumber);
                if (!result) {
                    $('#gstNOErr').fadeIn();
                    setTimeout(() => {
                        $('#gstNOErr').fadeOut();
                    }, 5000);
                    return false;
                }
            }
           
            $.post('/customer-number-duplication-check', postData, function(response) {
                if(response == 1){
                     $('#numLenErr').fadeIn();
                    setTimeout(() => {
                        $('#numLenErr').fadeOut();
                    }, 5000);
                    //return false;
                }
            }).done(function() {
                $('#addCustomerbtn').click();
            });
        });
    </script>
@endpush
