@php

    use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice</title>
    <style>
        * {
            box-sizing: border-box;

        }

        /* Create two equal columns that floats next to each other */
        .column1 {
            float: left;
            width: 60%;
        }

        .column {
            float: left;
            width: 40%;
            height: 200px;
            display: flex;
            justify-content: center;
            /* Center horizontally */
            align-items: center;
            /* Center vertically */
        }



        .column2 {
            width: 50%;
            height: 100px;
            display: flex;
            float: left;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        table {
            border-collapse: collapse;
            /* width: 100%; */
        }


        table,
        th,
        td {

            border: 1px solid black;

            padding: 8px;
        }

        hr {
            color: black;
            height: 5px;
        }

        #upi {
            display: block;
            /* Ensures the image won't affect the layout of other elements */
            /* margin: 0 auto; */
            padding: 0px;
            /* Center the image horizontally in the cell */
        }
    </style>
</head>

<body>


    {{-- <div class="row" style="border:1px solid black;padding:1px">
        <div class="column">
            <img src="images/begon-logo.png" alt="Logo" style="padding:5%;margin-top:{{ $width }}">
        </div>
        <div class="column1" style="text-align:right;font-size:14px;">
            <b>{{ $settings->name ?? '' }}</b><br>
            WE ALSO PROVIDE 100% HERBAL (ODOURLESS) SERVICES <span
                style="text-transform: uppercase;">{{ $settings->address ?? '' }}</span><br>Mobile
            No:{{ $settings->mobile_no ?? '' }}<br>E-mail:{{ $settings->email ?? '' }}<br>Website:{{ $settings->website ?? '' }}<br>LIC
            No:{{ $settings->lic_no ?? '' }}
            <br>
            @if ($invoice_type == 0 && $settings->gst_no != '')
                GSTIN:{{ $settings->gst_no ?? '' }},
            @endif
            State:{{ $settings->state ?? '' }}
        </div>
    </div> --}}
    <div style="text-align:center;font-size:16px;margin-bottom:15px"><b>
            Payment Receipt
        </b></div>

    <table cellspacing="0" style="padding-top:-1%;border-top:unset !important;">
        <!-- First Row with Two Columns -->
        <tr>
            <td style="border-right:0px;">

                &nbsp; &nbsp; &nbsp;<img src="images/begon-logo.png" alt="Logo" width="300px" height="100px"
                    class="begon-image">

            </td>
            <td style="border-left:0px;text-align:right;font-size:16px">
                <b style="font-size:18px">{{ $settings->name ?? '' }}</b><br>
                WE ALSO PROVIDE 100% HERBAL (ODOURLESS) SERVICES <br>
                <span style="text-transform: uppercase;">{{ $settings->address ?? '' }}</span> <br>
                Mobile No: 9892072628/9324972638<br>
                9323669191/7303290130/28280898/28282833
                {{-- &nbsp;&nbsp;
                 Mobile No:{{ $settings->mobile_no ?? '' }}&nbsp;&nbsp; --}}<br>
                E-mail:{{ $settings->email ?? '' }}<br>Website:{{ $settings->website ?? '' }}<br>LIC
                No:{{ $settings->lic_no ?? '' }}
                , GSTIN:{{ $settings->gst_no ?? '' }}, <br>

                State:{{ $settings->state ?? '' }}
            </td>
        </tr>
        <!-- Second Row with Two Columns -->
        <tr>
            <td style="text-align:left;font-size:14px;border-top:unset;border-bottom:unset;border-right"><b>Received
                    From</b></td>
            <td style="border-left:0px;text-align:right;font-size:14px;border-top:unset;border-bottom:unset"><b>Receipt
                    Details</b></td>
        </tr>
        <tr>
            <td style="width:50%;border-right:unset;border-bottom:unset">
                <!-- Nested table with two rows in the second column -->

                <div style="font-size: 16px">
                    @if (isset($customerPayment->customerDetails->first_name))
                        <b> {{ $customerPayment->customerDetails->first_name }}</b>
                    @else
                        -
                    @endif
                    </b>
                    <br>
                    <br>
                    @php
                        $sentence = $customerAddress->address_line1;
                        // Define the width for the line break
                        $width = 40;
                        // Use wordwrap to break the sentence into lines of specified width
                        $wrapped_sentence = wordwrap($sentence, $width, "\n", true);
                    @endphp
                    {!! nl2br($wrapped_sentence) !!}
                    {{-- {!! nl2br($customerAddress->address_line1) !!} <br> --}}
                    {{-- @if (isset($customerAddress->address_line1))
                        @php
                            // Split the address by commas
                            $addressParts = explode(',', $customerAddress->address_line1);
                        @endphp

                        @foreach ($addressParts as $key => $part)
                            @if ($key % 2 == '0')
                                {{ trim($part) }} <br>
                            @else
                                {{ trim($part) }}
                            @endif

                        @endforeach
                    @else
                        {{ '' }}
                    @endif --}}
                    <br>
                    <br>
                    Contact No.:{{ $customerAddress->contact ?? '' }}
                    @if (!empty($customerPayment->customerDetails->gst_no))
                        <br>
                        <br>
                        GSTIN: {{ $customerPayment->customerDetails->gst_no ?? '' }}
                    @endif
                    {{-- <br>
                    <br>
                     State: 27 Maharashtra --}}
                </div>
                <div class="clearfix">&nbsp;</div>
            </td>
            <td style="text-align:right;width:50%;font-size:14px;margin:0px;border-bottom:unset;vertical-align:top">
                {{-- Place of supply:{{ $settings->state }} &nbsp;<br> --}}
                <b> Receipt No. : {{ $trasn_details->id }} </b><br><br>
                <b> Date:
                    {{ date('d/m/Y', strtotime($trasn_details->created_at)) ?? '' }}</b>
            </td>
        </tr>

        <tr style="border-top:1px solid black">
            <td style="border-bottom:unset;border-right:unset;">
                &nbsp;
            </td>
            <td style="font-size:16px;border-bottom:unset;">
                {{-- <b>Amounts</b><br>
                    Sub Total  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &#8377; {{ $gstTotalAmount }}<br>
                    <span style="border-top:1px solid black;"> --}}
                <b>Amounts</b> <br><br>
                Received
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b> &#8377; {{ $trasn_details->paid_amount }} </b>
                {{-- </span>
                    --}}

            </td>
        </tr>
        <tr>
            <td style="font-size:16px;text-align:center;border-bottom:unset;border-right:unset">
                <b>Invoice Amount In Words</b><br><br>{{ numberInword($trasn_details->paid_amount) }}
                <br><br>
                <b> Payment Mode</b> <br>
                <br>{{ $customerPayment->payment_mode }} &nbsp;

            </td>
            <td style="border-bottom:unset">&nbsp;</td>
        </tr>
        <tr>
            <td style="border-right:unset"></td>
            <td style="border-left:unset;text:align:center;padding-left:80px;">
                For, BE-GON PEST CONTROL SERVICES
                <br> <br> <br> <br> <br> <br> <br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Authorized
                    Signatory</b>
            </td>
        </tr>
    </table>

</body>

</html>
