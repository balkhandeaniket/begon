<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Form</title>

    <style>
        body {
            text-align: center;
            margin: 0;
            padding: 0;
            font-size: 14px;
        }

        .container {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }

        .logo {
            margin-top: 2px;
            margin-bottom: 1px;
            width: 40%;
            height: auto;
        }

        .address {
            font-size: 14px;
            margin-top: 1px;
        }

        .contract-heading {
            font-size: 15px;
            font-weight: bold;
            text-decoration: underline;
            margin-top: 2px;
        }

        .invoice-table {
            width: 100%;
            margin-top: 10px;
        }

        .invoice-table td {
            padding: 5px;
        }

        .invoice-no {
            text-align: left;
        }

        .date {
            text-align: right;
        }
        .service-frequencies td {
            border: 1px solid black; /* Add this to set border for cells in this section */
            width: 36%;
        }
    </style>
</head>

<body>

    <div class="container">
        <img src="images/contract_form.jpg" alt="Logo" class="logo">
        <p class="address">
            {{ $settings->address ?? '' }}<br>
            &bull; Mobile No:{{ $settings->mobile_no ?? '' }} &bull; LIC No:{{ $settings->lic_no ?? '' }}<br>
            &bull; Website:{{ $settings->website ?? '' }} &bull; Email:{{ $settings->email ?? '' }}
        </p>
        <div class="contract-heading">CONTRACT FORM</div>
        <table class="invoice-table">
            <tr>
                <td class="invoice-no">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Invoice No:</b>
                    @if($customerPayment->invoice_no != "")
                        <u>{{ $customerPayment->invoice_no }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
                    @else
                        _______________
                    @endif
                </td>
                <td class="date">
                    <b>Date: </b>
                    @if($customerPayment->invoice_date != "")
                        <u>{{ date('d-m-Y',strtotime($customerPayment->invoice_date)) }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
                    @else
                        _______________
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>1) Name:&nbsp;&nbsp; </b>
                    <u>
                        @if (isset($customerPayment->customerDetails->first_name))
                            {{ $customerPayment->customerDetails->first_name }}
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            @else _______________________________________________________________________
                        @endif
                    </u>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>   &nbsp;&nbsp;&nbsp;&nbsp;Name of the Society:&nbsp; </b>
                    <u>
                        @if (isset($customerAddress->address_line1))
                        {{ $customerAddress->address_line1 }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      @endif
                    </u>
                </td>
            </tr>
            <tr>
                <td >
                    <b>   &nbsp;&nbsp;&nbsp;&nbsp;Bldg No. :&nbsp; </b>
                    <u>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </u>
                </td>
                <td >
                    <b>   &nbsp;&nbsp;&nbsp;&nbsp;Road / Street:&nbsp; </b>
                    <u>
                        @if (isset($customerAddress->road))
                        {{ $customerAddress->road }}

                        @endif
                        @if (isset($customerAddress->street))
                        {{ $customerAddress->street }}

                        @endif
                    </u>
                </td>
            </tr>
            <tr>
                <td >
                    <b>   &nbsp;&nbsp;&nbsp;&nbsp;Tel.Resi:&nbsp; </b>
                    <u>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </u>
                </td>
                <td >
                    <b>   &nbsp;&nbsp;&nbsp;&nbsp;Mobile:&nbsp; </b>

                    <u>
                        @if (isset($customerAddress->contact))
                        {{ $customerAddress->contact }}&nbsp;&nbsp;&nbsp;&nbsp;
                        @endif

                    </u>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>   &nbsp;&nbsp;&nbsp;&nbsp;Near/Opp/Above:&nbsp; </b>
                    <u>
                        @if (isset($customerAddress->near))
                        {{ $customerAddress->near }}
                        @endif
                        @if (isset($customerAddress->opposite))
                        {{ $customerAddress->opposite }}
                        @endif &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                    </u>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>   &nbsp;&nbsp;&nbsp;&nbsp;Landmark:&nbsp; </b>
                    <u>
                        @if (isset($customerAddress->landmark))
                        {{ $customerAddress->landmark }}
                        @endif
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                    </u>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>   &nbsp;&nbsp;&nbsp;&nbsp;Locality:&nbsp; </b>
                    <u>
                        @if (isset($customerAddress->locality))
                        {{ $customerAddress->locality }}
                        @endif
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                    </u>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>2) Type of Services:&nbsp;&nbsp; </b>
                         @php $i = 1; $totalAmount = 0;     $gstTotalAmount = 0;@endphp
                        @if (count($customerPayment->customerServices) > 0)
                        @foreach ($customerPayment->customerServices as $servies)
                        {{ $i }}) <u> {{ $servies->service_id }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
                          @php $i++;

                          $totalAmount += $servies->amount;
                          $gstTotalAmount += $servies->gst_amount;
                          @endphp


                       @endforeach
                       @endif

                </td>
            </tr>
            <tr>
                <td>
                    <b>3)  Period of Contract Form :&nbsp;&nbsp; </b><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>

                </td>
                <td>
                <b>To :</b><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>

                </td>
            </tr>
        </table>
        <table class="invoice-table service-frequencies">
            <tr >
                <td colspan="3" style="border:none"><b>4) Frequency of Service:</b></td>
            </tr>
            <tr>
                <td style="border-top:none;border-left:none;border-right:none"><b>&nbsp;&nbsp;&nbsp;&nbsp;For Termite Control:</b></td>
                <td  style="border-top:none;none;border-right:none"><b>&nbsp;&nbsp;For General Disinfestation:</b></td>
                <td style="border-top:none;border-left:none;border-right:none"><b>&nbsp;&nbsp;For Rodent Control:</b></td>
            </tr>
            <tr>
                <td style="border-top:none;border-left:none;border-right:none"><b>&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                <td  style="border-top:none;none;border-right:none"><b>&nbsp;&nbsp;</b></td>
                <td style="border-top:none;border-left:none;border-right:none"><b>&nbsp;&nbsp;</b></td>
            </tr>
            <tr>
                <td style="border-top:none;border-left:none;border-right:none"><b>&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                <td  style="border-top:none;none;border-right:none"><b>&nbsp;&nbsp;</b></td>
                <td style="border-top:none;border-left:none;border-right:none"><b>&nbsp;&nbsp;</b></td>
            </tr>
        </table>
        <table class="invoice-table service-frequencies">
            <tr>
                <td colspan="3" style="border:none"></td>


            </tr>

            <tr>
                <td style="border-top:none;border-left:none;border-right:none;"><b>&nbsp;&nbsp;&nbsp;&nbsp;For Mosquito Control:</b></td>
                <td  style="border-top:none;none;border-right:none;"><b>&nbsp;&nbsp;For :</b></td>
                <td style="border-top:none;border-left:none;border-right:none;"><b>&nbsp;&nbsp;For :</b></td>
            </tr>
            <tr>
                <td style="border-top:none;border-left:none;border-right:none;"><b>&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                <td  style="border-top:none;none;border-right:none;"><b>&nbsp;&nbsp;</b></td>
                <td style="border-top:none;border-left:none;border-right:none;"><b>&nbsp;&nbsp;</b></td>
            </tr>
            <tr>
                <td style="border-top:none;border-left:none;border-right:none;"><b>&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                <td  style="border-top:none;none;border-right:none;"><b>&nbsp;&nbsp;</b></td>
                <td style="border-top:none;border-left:none;border-right:none;"><b>&nbsp;&nbsp;</b></td>
            </tr>
            <tr>
                <td colspan="3" style="border:none"></td>

            </tr>
            <tr>
                <td colspan="3" style="border:none"></td>

            </tr>
            <tr>
                <td colspan="3" style="border:none"><b>5) Total Amount Rs: &nbsp;&nbsp;</b><u>{{ $gstTotalAmount }}/-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> </td>

            </tr>
            <tr>
                <td colspan="3" style="border:none"><b> &nbsp;&nbsp;&nbsp;&nbsp;(In Words)Rupees: &nbsp;&nbsp;</b><u>{{ numberInword($gstTotalAmount) }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> </td>

            </tr>

            <tr>
                <td colspan="3" style="border:none"><b>6) Discussion of payment:</b> <u>@if (isset($customerPayment->paymentDisussion->name)) {{ $customerPayment->paymentDisussion->name }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@endif</u> </td>

            </tr>
            <tr>
                <td colspan="3" style="border:none"><b>7) Terms of payment: &nbsp;&nbsp;</b><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> </td>

            </tr>
            <tr>
                <td colspan="3" style="border:none"><b>8) Balance payment: &nbsp;&nbsp;</b><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> </td>

            </tr>
            <tr>
                <td colspan="3" style="border:none"><b>9) Premises under contract: &nbsp;&nbsp;</b><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> </td>

            </tr>
        </table>
    </div>

</body>

</html>
