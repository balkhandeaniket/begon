@extends('main')
<style>
    #fileErr {
        display: none;
    }
</style>

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Address</h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <form id="customer-form" action="{{ $customer_action_url }}" method="post"
                                enctype="multipart/form-data">
                                <div class="card-header">
                                    {{-- <h3 class="card-title">{{ $customer_action }}</h3> --}}
                                    <div class="card-tools">
                                        <input type="hidden" value="{{ $customerID }}" id="customer_id"
                                            name="customer_id">
                                        <ul class="nav nav-pills ml-auto">
                                            <li class="nav-item btn-sm">

                                            </li>
                                            <li class="nav-item btn-sm">
                                                <button class="btn btn-sm btn-danger" onclick="history.back()">Back</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">

                                    {{-- @csrf --}}

                                    <!-- General Details Section -->
                                    <div class="card card-secondary">
                                        <div class="card-header">
                                            <h3 class="card-title">Add Customer Address</h3>
                                        </div>
                                        <div class="card-body">
                                            <!-- Communication Details Section -->

                                            <div class="row">

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="contact">Contact<span
                                                                class="text-danger">*</span></label>
                                                        <input type="text"
                                                            value="{{ $getCustomerAddress->contact ?? '' }}" name="contact"
                                                            id="contact" required class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="address_line1">Address <span
                                                                class="text-danger">*</span></label>
                                                        <input required type="text"
                                                            value="{{ $getCustomerAddress->address_line1 ?? '' }}"
                                                            name="address_line1" id="address_line1" class="form-control"
                                                            required>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" value="{{ $getCustomerAddress->email ?? '' }}"
                                                            name="email" id="email" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="road">Road</label>
                                                        <input type="text" value="{{ $getCustomerAddress->road ?? '' }}"
                                                            name="road" id="road" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="near">Near</label>
                                                        <input type="text" value="{{ $getCustomerAddress->near ?? '' }}"
                                                            name="near" id="near" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="landmark">Landmark</label>
                                                        <input type="text"
                                                            value="{{ $getCustomerAddress->landmark ?? '' }}"
                                                            name="landmark" id="landmark" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="street">Street</label>
                                                        <input type="text"
                                                            value="{{ $getCustomerAddress->street ?? '' }}" name="street"
                                                            id="street" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="opposite">Opposite</label>
                                                        <input type="text"
                                                            value="{{ $getCustomerAddress->opposite ?? '' }}"
                                                            name="opposite" id="opposite" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="locality">Locality</label>
                                                        <input type="text"
                                                            value="{{ $getCustomerAddress->locality ?? '' }}"
                                                            name="locality" id="locality" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="pin_code">Pincode</label>
                                                        <input type="text"
                                                            value="{{ $getCustomerAddress->pin_code ?? '' }}"
                                                            name="pin_code" id="pin_code" class="form-control">
                                                    </div>
                                                </div>


                                            </div>
                                            <button type="submit"
                                                class="btn btn-sm btn-success" style="float: right;">
                                                @if (empty($getCustomerAddress))
                                                    Submit
                                                @else
                                                    Update
                                                @endif
                                            </button>

                                        </div>
                                    </div>


                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection


@push('scripts')
@endpush
