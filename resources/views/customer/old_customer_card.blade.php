<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rodent Control Form</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #f8f8f8;
            height: 100vh;
        }
        .row {
            width: 950px;
            border: 4px solid #000;
            padding: 20px;
            box-sizing: border-box;
            background-color: #fff; /* White background for printing clarity */
        }
        table {
            width: 100%; /* Table spans full width */
            border-spacing: 0 15px; /* Adds spacing between rows */
        }
        td {
            width: 50%; /* Two equal columns */
            vertical-align: top;
            padding: 10px;
        }
        .section {
            display: flex;
            justify-content: space-between;
            align-items: center;
            /* margin-bottom: 35px; */
        }
        .section label {
            font-weight: bold;
            white-space: nowrap; /* Ensures the label stays in one line */
        }
        .line {
            flex: 1; /* Line takes the remaining space */
            border-bottom: 1px solid #000;
            margin-left: 10px;
            width: 100%;
        }
        .half-line {
            display: inline-block;
            /* border-bottom: 1px solid #000; */
            width: 45%;
        }
        .table-box {
            border: 1px solid black;
            border-collapse: collapse;
            width: 97%;
            margin-top: 10px;
        }
        .table-box th, .table-box td {
            border: 1px solid black;
            padding: 9px;
            text-align: center;
        }

        .underline-box {
            display: inline-block;
            width: 200px; /* Adjust width as needed */
            border-bottom: 1px solid black; /* Creates the underline */
            text-align: left; /* Ensures text starts from left */
            padding-bottom: 2px; /* Adjust spacing between text and underline */
        }
       
    </style>
</head>
<body>
    <div class="row">
        <table>
            <tbody>
                <tr>
                    <!-- First Column -->
                    <td>
                        <div class="section">
                            <b>Area sq. Ft.</b>
                            <span class="half-line">_____________________</span>
                            <b>Entire</b>
                            <span class="half-line">_____________________</span>
                        </div>
                        <br>
                        <div class="section">
                            <b>Booked By</b>
                            <span class="line">__________________________________________________</span>
                        </div>
                        <br>
                        <div class="section">
                            <b>Value of Contact Rs.</b>
                            <span class="line">________________________________________</span>
                        </div>
                        <br>
                        <div class="section">
                            <b>Payment Recd. Yes/No</b>
                            <span class="line">______________________________________</span>
                        </div>
                        <br>
                        <div class="section">
                            <b>Balance Payment</b>
                            <span class="line">___________________________________________ </span>
                        </div>
                        <br>
                        <div class="section">
                            <b>Cash / Cheque</b>
                            <span class="line">______________________________________________</span>
                        </div>
                        <br>
                        <div class="section">
                            <b>Concern Person</b>
                            <span class="line">_____________________________________________</span>
                        </div><br>
                        <div class="section">
                            <b>Sign</b>
                            <span class="half-line">________________________</span>
                            <b>Date</b>
                            <span class="half-line">__________________________</span>
                        </div><br>
                        <div class="section">
                            <b>Stop</b>
                            <span class="line">________________________________________________________</span>
                        </div>
                        <table class="table-box">
                            <thead>
                                <tr>
                                    <th>DATE</th>
                                    <th>TIME</th>
                                    <th>REASON</th>
                                    <th>CON. PER</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                                <tr><td></td><td></td><td></td><td></td></tr>
                            </tbody>
                        </table>
                    </td>

                    <!-- Second Column -->
                    <td>
                        <div>
                            <img src="images/contract_form.jpg" alt="Logo" class="logo" width="130"  style="margin-bottom: 20px;margin-left:18%">
                        </div>


                        <div class="section">
                            <b>Type of Service</b>
                            <span class="line">_______________________________________________</span>
                        </div><br>
                        <div class="section">
                            <b>Name </b>
                            {{-- <span class="line"> 
                                {{ ucfirst($customerDetails->customer->first_name) }}
                                @php
                                    $a=0; 
                                    $a = 50- strlen($customerDetails->customer->first_name);
                                @endphp
                                @for ($i =0 ; $i <$a ; $i++)
                                    &nbsp;
                                @endfor
                            </span> --}}
                        </div><br>
                        <div class="section">
                            <b>Flat No. </b>
                            <span class="half-line">___________________</span>
                            <b>Plot No.</b>
                            <span class="half-line">_____________________</span>
                        </div><br>
                        <div class="section">
                            <b>Name of Bldg.</b>
                            <span class="line">_______________________________________________</span>
                        </div><br>
                        <div class="section">
                            <b>Area</b>
                              <span class="line">_________________________________________________________</span>
                        </div><br>
                        <div class="section">
                            <b>Landmark</b>
                              <span class="line">____________________________________________________</span>
                        </div><br>
                        <div class="section">
                            <b>Road Street</b>
                            <span class="line">__________________________________________________</span>
                        </div><br>
                        <div class="section">
                            <b>Near</b>
                            <span class="line">_________________________________________________________</span>
                        </div><br>
                        <div class="section">
                            <b>Opp.</b>
                            <span class="line">_________________________________________________________</span>
                        </div><br>
                        <div class="section">
                            <b>Bus Stop</b>
                            <span class="line">_____________________________________________________</span>
                        </div><br>
                        <div class="section">
                            <b>Locality</b>
                            <span class="line">______________________________________________________</span>
                        </div><br>
                        <div class="section">
                            <b>Mobile - 1</b>
                            <span class="half-line">___________________</span>
                            <b>Mobile - 2</b>
                            <span class="half-line">_____________________</span>
                        </div><br>
                        <div class="section">
                            <b>Tel. Resi</b>
                            <span class="half-line">_______________________</span>
                            <b>Office</b>
                            <span class="half-line">______________________</span>
                        </div><br>
                        <div class="section">
                            <b>Contact</b>
                            <span class="half-line">_______________________</span>
                            <b>Bill No.</b>
                            <span class="half-line">______________________</span>
                        </div><br>

                        <div class="section">
                            <b>From</b>
                            <span class="half-line">_________________________</span>
                            <b>To</b>
                            <span class="half-line">___________________________</span>
                        </div><br>

                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>
