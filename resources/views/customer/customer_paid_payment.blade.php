<div class="row">
    <input type="hidden" id="payment_id" value="{{ $getCustomerPayment->id }}">
    <div class="col-md-4">
        <div class="form-group">
            <label for="paid_amount">Name : {{ $payment->first_name }}</label>
           
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="paid_amount">Invice No. : {{ $payment->invoice_no }}</label>
           
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="paid_amount">Amount : {{ $payment->amount }}</label>
           
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="paid_amount">GST Amount: {{ $payment->gst_cal_amount }} </label>
           
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="paid_amount">Final Amount : {{ $payment->gst_amount }}</label>
           
        </div>
    </div>
    
    <div class="col-md-3">
        <div class="form-group">
            <label for="paid_amount">Paid Amount : {{ $payment->paid_amount }}</label>
           
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="paid_amount">balance : {{ $payment->balance }}</label>
           
        </div>
    </div>
    <hr>

    <div class="col-md-2">
        <div class="form-group">
            <label for="paid_amount">Paid Amount</label>
            <input type="text" onkeypress="return isNumberKey(event)" name="paid_amount" id="paid_amount"
                class="form-control" value="">
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="payment_mode">Payment Mode</label>
            <select name="payment_mode" id="payment_mode" class="form-control select2">
                <option value=""> Select </option>
                @foreach ($paymentMode as $val)
                    <option value="{{ $val }}" @if (!empty($getCustomerPayment) && $getCustomerPayment->payment_mode == $val) selected @endif>
                        {{ $val }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="bank_name">Bank Name</label>
            <input type="text" name="bank_name" id="bank_name" class="form-control"
                value="{{ $getCustomerPayment->bank_name ?? '' }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="payment_reference">Payment Reference</label>
            <input type="text" name="payment_reference" id="payment_reference" class="form-control"
                value="{{ $getCustomerPayment->payment_reference ?? '' }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="payment_disussion">Disussion Of Payment </label>
            <select name="payment_disussion" id="payment_disussion" class="form-control select2">
                <option value="">Select User</option>
                @foreach ($getUsers as $val)
                    <option value="{{ $val->id }}" @if (!empty($getCustomerPayment) && $getCustomerPayment->payment_disussion == $val->id) selected @endif>
                        {{ $val->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="remark">Remark</label><br>
            <textarea id="remark" name="remark">{{ $getCustomerPayment->remark ?? '' }}</textarea>
        </div>
    </div>

</div>
@push('scripts')
    <script>
        $("#payment_mode").select2()
    </script>
@endpush
