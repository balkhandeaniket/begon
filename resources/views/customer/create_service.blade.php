<tr class="service_{{ $rand }}">
    
    <td style="padding: 0px;">
        <input type="hidden" class="services" value="{{ $rand }}">
        <input style="width: 200px !important;" type="text" id="service_id_{{ $rand }}" name="service_id[]" class="autocomplete service_id  form-control" list="suggestions_{{ $rand }}"> 
        <datalist id="suggestions_{{ $rand }}">
            <!-- Suggestions will be populated dynamically via JavaScript -->
        </datalist>
        {{-- <select style="width: 180px;" id="service_id_{{ $rand }}" name="service_id[]" class="form-control select2 service_id">
            <option value="">Select</option>
            @foreach ($services as $row)
                <option value="{{ $row->id }}"> {{ $row->title }}
                </option>
            @endforeach
        </select> --}}
    </td>
    <td style="padding: 0px"><input style="width: 80px !important;" type="text" id="hsn_{{ $rand }}" name="hsn[]" class="hsn form-control autocomplete" list="hsn_auto_{{ $rand }}"
        value="{{ $getSetting->hsn_code }}">
        {{-- <datalist id="hsn_auto_{{ $rand }}">
            @foreach ($hsn as $row)
                <option value="{{ $row->hsn }}"> {{ $row->title }}
                </option>
            @endforeach
        </datalist> --}}
    </td>
    <td style="padding: 0px"><input style="width: 140px !important;" type="text" id="remark_{{ $rand }}" name="remark[]" class="remark form-control autocomplete"  list="remark_auto_{{ $rand }}"
        value="">
        <datalist id="remark_auto_{{ $rand }}">
            @foreach ($remark as $row)
                <option value="{{ $row->remark }}"> {{ $row->title }}
                </option>
            @endforeach
        </datalist>
    </td>
    <td style="padding: 0px"><input style="width: 140px !important;" type="text" id="service_due_text_{{ $rand }}" name="service_due_text[]" class="service_due_text form-control"
            value=""></td>
    <td style="padding: 0px">
        <input style="width: 100px !important;"  autocomplete="off"  type="text" id="contract_from_{{ $rand }}" name="contract_from[]" class="form-control contract_from" value="">
    </td>
    <td style="padding: 0px">
        <input style="width: 100px !important;"  autocomplete="off" type="text" id="contract_to_{{ $rand }}" name="contract_to[]"
        class="form-control contract_to" value="">
    </td>
    <td style="padding: 0px"><input style="width: 80px !important;" type="text" id="contract_{{ $rand }}"
        name="contract[]" class="form-control contract" value=""></td>
    <td style="padding: 0px"> <input style="width: 80px !important;" type="text" id="period_{{ $rand }}" name="period[]" class="period form-control" value=""></td>
    
    <td style="padding: 0px"><input  style="width: 50px !important;" style="width:50px !important;" type="text" onkeypress="return isNumberKey(event)"  oninput="handlePriceChange({{ $rand }})" id="quantity_{{ $rand }}"  name="quantity[]" class="form-control quantity" value="1"></td>
    <td style="padding: 0px"> 
        <input style="width: 80px !important;" type="text" onkeypress="return isNumberKey(event)"
            oninput="handlePriceChange({{ $rand }})" id="price_{{ $rand }}"
            name="price[]" class="price form-control" value="">
            <input type="hidden" onkeypress="return isNumberKey(event)" readonly
            id="amount_{{ $rand }}" name="amount[]" class="form-control amount" value="">
    </td>
    {{-- <td style="padding: 0px"><label for="amount">Amount</label> <span id="err_amount_{{ $rand }}" class="validiaton">
        <input type="text" onkeypress="return isNumberKey(event)" readonly
            id="amount_{{ $rand }}" name="amount[]" class="form-control amount" value=""></td> --}}
    <td style="padding: 0px"> <input style="width: 50px !important;" type="text" onkeypress="return isNumberKey(event)"
        oninput="handlePriceChange({{ $rand }})" id="gst_{{ $rand }}"
        name="gst[]" class="form-control gst" value=""></td>
    <td style="padding: 0px"><input style="width: 80px !important;" type="text" onkeypress="return isNumberKey(event)" readonly
        id="gst_cal_amount_{{ $rand }}" name="gst_cal_amount[]"
        class="form-control gst_cal_amount" value=""></td>
    <td style="padding: 0px"> <input style="width: 80px !important;" oninput="calGstAmount({{ $rand }});" type="text" onkeypress="return isNumberKey(event)"
        id="gst_amount_{{ $rand }}" name="gst_amount[]" class="form-control gst_amount"
        value=""></td>
    <td class="service_due_date" style="display:none;"><button class="btn btn-sm btn-info" onclick="addServiceDueDate({{ $rand }})"> + </button> <span  id="err_service_due_{{ $rand }}" class="validiaton">
            <input style="width: 140px !important;" type="date" id="service_due_{{ $rand }}" name="service_due[]"  class="form-control service_due" value=""></td>
    <td style="padding: 0px"><button class="btn btn-sm btn-danger removeButton"
        onclick="removeService({{ $rand }});handlePriceChange({{ $rand }});" style="float:right">&times;</button></td>
</tr>
{{-- <div class="service_{{ $rand }}">
    <input type="hidden" class="services" value="{{ $rand }}">
    <div class="row custom-border">
        <div class="col-md-12"><button class="btn btn-sm btn-danger removeButton"
                onclick="removeService({{ $rand }})" style="float:right">&times;</button></div>
        <div class="col-md-2 col-xs-12">
             <div class="form-group">
                <label for="category_id">Category</label> <span class="validiaton"> * </span> <span
                    id="err_category_id_{{ $rand }}" class="validiaton"></span>
                <select id="category_id_{{ $rand }}" name="category_id[]"
                    onchange="return getServices('{{ $rand }}')" class="form-control select2 category_id">
                    <option value="">Select</option>
                    @foreach ($categorys as $row)
                        <option value="{{ $row->id }}"> {{ $row->title }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="service_id">Service</label> <span class="validiaton"> * </span> <span
                    id="err_service_id_{{ $rand }}" class="validiaton"></span>
                <select id="service_id_{{ $rand }}" name="service_id[]" class="form-control select2 service_id">
                    <option value="">Select</option>
                </select>
            </div>
        </div> 
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label for="service_id">Item</label> <span class="validiaton"> * </span> <span
                    id="err_service_id_{{ $rand }}" class="validiaton"></span>
                <select id="service_id_{{ $rand }}" name="service_id[]" class="form-control select2 service_id">
                    <option value="">Select</option>
                    @foreach ($services as $row)
                        <option value="{{ $row->id }}"> {{ $row->title }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="remark">Description </label> <span id="err_remark_{{ $rand }}"
                    class="validiaton">
                    <input type="text" id="remark_{{ $rand }}" name="remark[]" class="remark form-control"
                        value="">
            </div>
        </div>

        <div class="col-md-2 col-xs-12 service_due_date">
            <div class="form-group">
                <label for="service_due">Service Due</label> <span class="validiaton"> * </span> <button
                    class="btn btn-sm btn-info" onclick="addServiceDueDate({{ $rand }})"> + </button> <span
                    id="err_service_due_{{ $rand }}" class="validiaton">
                    <input type="date" id="service_due_{{ $rand }}" name="service_due[]"
                        class="form-control service_due" value="">
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="contract_from">Contract From</label> <span class="validiaton"> * </span> <span
                    id="err_contract_from_{{ $rand }}" class="validiaton">
                    <input type="date" id="contract_from_{{ $rand }}" name="contract_from[]"
                        class="form-control contract_from" value="">
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="contract_to">Contract To</label> <span class="validiaton"> * </span> <span
                    id="err_contract_to_{{ $rand }}" class="validiaton">
                    <input type="date" id="contract_to_{{ $rand }}" name="contract_to[]"
                        class="form-control contract_to" value="">
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="contract">Contract Amount</label> <span id="err_contract_{{ $rand }}"
                    class="validiaton">
                    <input type="text" onkeypress="return isNumberKey(event)" id="contract_{{ $rand }}"
                        name="contract[]" class="form-control contract" value="">
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="period">Period</label> <span id="err_period_{{ $rand }}" class="validiaton">
                    <input type="text" id="period_{{ $rand }}" name="period[]" class="period form-control"
                        value="">
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="price">Price</label> <span class="validiaton"> * </span> <span
                    id="err_price_{{ $rand }}" class="validiaton">
                    <input type="text" onkeypress="return isNumberKey(event)"
                        oninput="handlePriceChange({{ $rand }})" id="price_{{ $rand }}"
                        name="price[]" class="price form-control" value="">
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="quantity">Quantity</label> <span class="validiaton"> * </span> <span
                    id="err_quantity_{{ $rand }}" class="validiaton">
                    <input type="number" onkeypress="return isNumberKey(event)"
                        oninput="handlePriceChange({{ $rand }})" id="quantity_{{ $rand }}"
                        name="quantity[]" class="form-control quantity" value="1">
            </div>
        </div>

        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="amount">Amount</label> <span id="err_amount_{{ $rand }}" class="validiaton">
                    <input type="text" onkeypress="return isNumberKey(event)" readonly
                        id="amount_{{ $rand }}" name="amount[]" class="form-control amount" value="">
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="gst">GST ( in %)</label> <span id="err_gst_{{ $rand }}" class="validiaton">
                    <input type="text" onkeypress="return isNumberKey(event)"
                        oninput="handlePriceChange({{ $rand }})" id="gst_{{ $rand }}"
                        name="gst[]" class="form-control gst" value="">
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="gst_cal_amount">GST Amount</label> <span id="err_gst_cal_amount_{{ $rand }}"
                    class="validiaton">
                    <input type="text" onkeypress="return isNumberKey(event)" readonly
                        id="gst_cal_amount_{{ $rand }}" name="gst_cal_amount[]"
                        class="form-control gst_cal_amount" value="">
            </div>
        </div>
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label for="gst_amount">Final Amount</label> <span id="err_gst_amount_{{ $rand }}"
                    class="validiaton">
                    <input readonly type="text" onkeypress="return isNumberKey(event)"
                        id="gst_amount_{{ $rand }}" name="gst_amount[]" class="form-control gst_amount"
                        value="">
            </div>
        </div>
    </div>
</div> --}}
