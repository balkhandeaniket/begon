@php

    use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice</title>

    <style>
        body {
            font-size: 80px;
            font-family: sans-serif;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }


        table,
        th,
        td {
            border: 1px solid black;
            padding: 20px; /* Increase padding as needed */
        }
        

        #upi {
            display: block;
            /* Ensures the image won't affect the layout of other elements */
            margin: 0 auto;
            padding: 50px;
            /* Center the image horizontally in the cell */
        }

        /* .begon-logo{
            position: absolute;
        } */

       
    </style>
</head>

<body>
    <center>
        
        <h4 style="text-align:center;font-size:16px">Tax Invoice</h4>
        <table style="width:800px" cellspacing="0" style="padding-top:-1%">
            <!-- First Row with Two Columns -->
            <tr>
                <td style="border-right:0px;width:100%">
                    
                     <img src="images/begon-logo.png" alt="Logo" width="580px" style="padding:20px" height="500px" class="begon-image">
             
                </td>
                <td style="border-left:0px;text-align:right;width:50%">
                    <b>{{ $settings->name ?? '' }}</b><br>
                    WE ALSO PROVIDE 100% HERBAL (ODOURLESS) SERVICES <span
                        style="text-transform: uppercase;">{{ $settings->address ?? '' }}</span>&nbsp;&nbsp;Mobile
                    No:{{ $settings->mobile_no ?? '' }}&nbsp;&nbsp;E-mail:{{ $settings->email ?? '' }}<br>Website:{{ $settings->website ?? '' }}<br>LIC
                    No:{{ $settings->lic_no ?? '' }}
                    <br>
                    @if ($invoice_type == 0 && $settings->gst_no != '')
                        GSTIN:{{ $settings->gst_no ?? '' }},
                    @endif
                    State:{{ $settings->state ?? '' }}
                </td>
            </tr>
            <!-- Second Row with Two Columns -->
            <tr>
                <td style="width:50%">
                    <!-- Nested table with two rows in the second column -->
                    <table style="border: none;">
                        <!-- First nested row with one column -->
                        <tr>
                            <td style="border-top:unset;border-right:unset;border-left:unset;text-align:left"><b>
                                    Bill To
                                </b></td>
                        </tr>
                        <!-- Second nested row with one column -->

                        <tr>
                            <td style="border-bottom:unset;border-right:unset;border-left:unset"><b>
                                    @if (isset($customerPayment->customerDetails->first_name))
                                        {{ $customerPayment->customerDetails->first_name }}
                                    @else
                                        -
                                    @endif
                                </b>
                                <br>
                                @if (isset($customerAddress->address_line1))
                                    @php
                                        // Split the address by commas
                                        $addressParts = explode(',', $customerAddress->address_line1);
                                    @endphp

                                    @foreach ($addressParts as $part)
                                        {{ trim($part) }},<br>
                                    @endforeach
                                @else
                                    {{ '' }}
                                @endif
                                {{-- {{ $customerAddress->address_line1 ?? '' }}<br> --}}
                                {{-- @if ($customerAddress->address_line2)
                                    {{ $customerAddress->address_line2 ?? '' }}
                                @else
                                @endif
                                @if ($customerAddress->landmark)
                                    ,{{ $customerAddress->landmark ?? '' }}
                                @else
                                @endif
                                @if ($customerAddress->road)
                                    ,{{ $customerAddress->road ?? '' }}
                                @else
                                @endif
                                @if ($customerAddress->street)
                                    ,{{ $customerAddress->street ?? '' }}
                                @else
                                @endif
                                @if ($customerAddress->near)
                                    ,{{ $customerAddress->near ?? '' }}
                                @else
                                @endif
                                @if ($customerAddress->opposite)
                                    ,{{ $customerAddress->opposite ?? '' }}
                                @else
                                @endif
                                <br>{{ $customerAddress->pin_code ?? '' }} --}}
                                @if (!empty($customerPayment->customerDetails->gst_no))
                                    GSTIN
                                    : {{ $customerPayment->customerDetails->gst_no ?? '' }}
                                    <br>
                                @endif
                                Contact No.:{{ $customerAddress->contact ?? '' }}

                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align:right;width:50%">Place of supply:{{ $settings->state }}<br> <b>Invoice
                        No:{{ $customerPayment->invoice_no ?? '' }}</b><br>Date:
                    {{ date('d/m/Y', strtotime($customerPayment->invoice_date)) ?? '' }}
                </td>
            </tr>
            <!-- Third Row with Two Columns -->
            <tr>
                <td colspan="2">
                    <!-- Nested table with two rows in the second column -->
                    <table style="border: none;">
                        <!-- First nested row with one column -->
                        <tr>
                            <td style="border-top:unset;border-left:unset;text-align:left;width:30%;"><b>Service
                                    Description</b></td>
                            <td style="border-top:unset;border-left:unset;text-align:right;width:10%; "><b>Period</b>
                            </td>
                            <td style="border-top:unset;border-left:unset;text-align:right;width:10%; "><b>Contract
                                    From</b></td>
                            <td style="border-top:unset;border-left:unset;text-align:right;width:10%; "><b>Contract
                                    To</b></td>
                            @if ($isContractShow == "1")
                                <td style="border-top:unset;border-left:unset;text-align:right;width:10%; "><b>Contract
                                Amt</b></td>
                            @endif
                            
                            <td style="border-top:unset;border-left:unset;text-align:right;width:10%; "><b>HSN
                                    Code</b></td>
                                    @if ($invoice_type == 0)
                            <td style="border-top:unset;border-left:unset;text-align:right;width:10%; "><b>Taxable
                                    Amount</b></td>

                                <td style="border-top:unset;border-left:unset;text-align:right;width:10%; "><b>GST</b>
                                </td>
                            @endif
                            <td
                                style="border-top:unset;border-left:unset;border-right:unset;text-align:right;width:15%; ">
                                <b>Amount</b>
                            </td>
                        </tr>
                        <!-- Second nested row with one column -->
                        @php
                            $totalAmount = 0;
                            $totalGst = 0;
                            $gstTotalAmount = 0;
                            $getSgst = 0;
                        @endphp

                        @if (count($customerPayment->customerServices) > 0)

                            @foreach ($customerPayment->customerServices as $servies)
                                @php
                                    $formattedDates = [];
                                @endphp
                                @foreach ($servies->serviceDueDate as $date)
                                    @php
                                        $formattedDates[] = \Carbon\Carbon::parse($date->service_due)->format('j M');

                                    @endphp
                                @endforeach

                                <tr>
                                    <td
                                        style="border-top:unset;border-bottom:unset;border-left:unset;text-align:left;width:30%;">
                                        <b>{{ ucfirst($servies->service_id) ?? '' }} </b>
                                        @if (!empty($servies->remark))
                                            <br>
                                            <span>( {{ $servies->remark }}) </span>
                                        @endif
                                        {{-- @if (!empty($servies->contract))
                                                <br>
                                                Contract Amount: {{ $servies->contract }}
                                            @endif --}}
                                        {{-- @if (count($formattedDates) > 0)
                                                <br> Service Due :
                                                {{ implode(', ', $formattedDates) }}
                                           @endif --}}
                                        @if (!empty($servies->service_due_text))
                                            <br> Service Due:
                                            {{ $servies->service_due_text }}
                                        @endif
                                        @if (count($customerPayment->customerServices) == '1')
                                            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                                        @else
                                            <br><br><br><br><br>
                                        @endif

                                        {{-- @if (count($customerPayment->customerServices) <= '1')
                                            <br><br> <br>
                                        @endif --}}
                                    </td>
                                    <td
                                        style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;width:10%; ">
                                        {{ $servies->period ?? '' }}
                                        @if (count($customerPayment->customerServices) == '1')
                                            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                                        @else
                                            <br><br><br><br><br>
                                        @endif
                                    </td>
                                    <td
                                        style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;width:10%;">
                                        {{ $servies->contract_from ?? '' }}
                                        @if (count($customerPayment->customerServices) == '1')
                                            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                                        @else
                                            <br><br><br><br><br>
                                        @endif
                                    </td>
                                    <td
                                        style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;width:10%;">
                                        {{ $servies->contract_to ?? '' }}
                                        @if (count($customerPayment->customerServices) == '1')
                                            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                                        @else
                                            <br><br><br><br><br>
                                        @endif
                                    </td>
                                    @if ($isContractShow == "1")
                                        <td style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;width:10%;">
                                            {{ $servies->contract ?? '' }}
                                            @if (count($customerPayment->customerServices) == '1')
                                                <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                                            @else
                                                <br><br>
                                            @endif
                                        </td>
                                    @endif
                                   
                                    <td
                                        style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;width:10%;">
                                        {{ $servies->hsn ?? '' }}
                                        @if (count($customerPayment->customerServices) == '1')
                                            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                                        @else
                                            <br><br><br><br><br>
                                        @endif
                                    </td>
                                    @if ($invoice_type == 0)
                                    <td
                                        style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;width:10%;">
                                        &#8377; {{ $servies->amount ?? '' }}
                                        @if (count($customerPayment->customerServices) == '1')
                                            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                                        @else
                                            <br><br><br><br><br>
                                        @endif
                                    </td>

                                        @php   $getSgst = $servies->gst / 2;@endphp

                                        <td
                                            style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;width:10%;">
                                            &#8377; {{ ($servies->amount * $servies->gst) / 100 }}<br>
                                            {{-- ({{ $servies->gst ?? '' }}&#37;) --}} @isset($servies->gst)
                                                (CGST:{{ $getSgst }}%,<br>SGST:{{ $getSgst }}%)
                                            @endisset
                                            @if (count($customerPayment->customerServices) == '1')
                                                <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                                            @else
                                                <br><br>
                                            @endif
                                        </td>
                                    @endif
                                    <td
                                        style="border-top:unset;border-left:unset;border-right:unset;border-bottom:unset;text-align:right;width:10%;">
                                        &#8377; @if ($invoice_type == 0)
                                            {{ $servies->gst_amount }}
                                        @else
                                            {{ $servies->amount ?? '' }}
                                        @endif
                                        @if (count($customerPayment->customerServices) == '1')
                                            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                                        @else
                                            <br><br><br><br><br>
                                        @endif
                                    </td>
                                </tr>
                                @php
                                    $totalAmount += $servies->amount;
                                    $totalGst += ($servies->amount * $servies->gst) / 100;
                                    if ($invoice_type == 0) {
                                        $gstTotalAmount += $servies->gst_amount;
                                    } else {
                                        $gstTotalAmount += $servies->amount;
                                    }
                                @endphp
                            @endforeach

                        @endif

                        <!-- Third nested row with one column -->
                        <tr>
                            @php
                                if ($isContractShow == "1"){
                                    $colspanvalue = 5;
                                    $colspanvalue2 = 3;
                                    if ($invoice_type == 0){
                                        $colspanvalue = 6;
                                        $colspanvalue2 = 4;
                                    }

                                    $colspanvalue3 = "4";
                                }else{
                                    $colspanvalue = 4;
                                    $colspanvalue2 = 2;
                                    if ($invoice_type == 0){
                                        $colspanvalue = 5;
                                        $colspanvalue2 = 3;
                                    }
                                    $colspanvalue3 = "3";
                                }
                               
                            @endphp
                            <td colspan="{{ $colspanvalue }}"
                                style="border-bottom:unset;border-left:unset;text-align:left;border-right:unset">
                                <b>Total</b>
                            </td>
                            <td style="border-bottom:unset;border-left:unset;text-align:right;"></td>
                            {{-- <td style="border-bottom:unset;border-left:unset;text-align:right;"><b>&#8377;
                                    {{ $totalAmount }}</b></td> --}}
                            @if ($invoice_type == 0)
                                <td style="border-left:unset;text-align:right;"><b>&#8377;
                                        {{ $totalGst }}</b></td>
                            @endif
                            <td style="border-left:unset;border-right:unset;text-align:right;">
                                <b>&#8377; {{ $gstTotalAmount }}</b>
                            </td>
                        </tr>
                        <tr>
                            {{-- <td style="border-bottom:unset;border-left:unset;text-align:center;" @if ($invoice_type == 0) colspan="9" @else colspan="8" @endif>
                                <b>Invoice Amount in word</b><br>{{ numberInword($gstTotalAmount) }}<br><b>Payment
                                    Mode</b><br>{{ $customerPayment->payment_mode }}
                            </td> --}}
                            <td style="border-bottom:unset;border-left:unset;text-align:center;padding:80px;" colspan="4">
                                <b>Invoice Amount In Words</b><br>{{ numberInword($gstTotalAmount) }}<br><b>Payment
                                    Mode</b><br>{{ $customerPayment->payment_mode }}
                            </td>

                            <td style="border-bottom:unset;border-left:unset;text-align:right;border-right:unset;"
                                colspan="@if ($invoice_type == 0)3 @else 2 @endif">
                                <table style="border: none;">
                                    <!-- First nested row with one column -->
                                    {{-- <tr>
                                        <td
                                            style="border-top:unset; border-right:unset; border-left:unset; text-align:left;">
                                            <p><b>Amounts:</b></p>
                                            <span>Sub Total</span>
                                            <span style="float:right">
                                                @if ($invoice_type == 0)
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                @endif

                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &#8377; {{ $gstTotalAmount }}
                                            </span>
                                        </td>
                                    </tr> --}}
                                    <!-- Second nested row with one column -->
                                    <tr>
                                        <td
                                            style="border-top:unset; border-right:unset; border-left:unset; text-align:left; border-bottom:unset;">

                                            <span><b>&nbsp;&nbsp;&nbsp;Total &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
                                            <span
                                                style="float:right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8377;
                                                {{ $gstTotalAmount }}</span>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td
                                style="border-top:1px solid white;border-bottom:1px solid white;border-left:1px solid white;">
                                @php $i =1 ; @endphp
                                <b>Terms and Conditions</b><br><br>
                                @foreach ($termAndCondition as $T_C)
                                    <p>{{ $i }}){{ $T_C->content }}</p>
                                    @php $i++; @endphp
                                @endforeach
                                <P>Note: Kindly NOTE THAT THE above No. of Services may get skip, if not taken within
                                    stipulated due period of the Services.</P><br>
                                <b>Bank Details</b><br><br>
                                <p>Bank Name : {{ $settings->bank_name }}</p><br>
                                <p>Bank Acount No : {{ $settings->bank_account }}</p><br>
                                <p>Bank IFSC code : {{ $settings->ifsc_code }}</p><br>
                                <p>Acount holder's name : {{ $settings->account_holder_name }}</p><br>
                            </td>
                            <td style="width:33%;border-top:1px solid white;border-bottom:1px solid white;">
                                {{-- <img  src="images/upi_image.png" id="upi" alt="Logo"
                                class="brand-image img-circle elevation-3" width="60%"> --}}
                                <img src=data:image/png;base64,{{ $qrCode }} <br>
                                <img src="images/scan_qr.png" id="upi" alt="Logo"
                                    class="brand-image img-circle elevation-5" width="70%">

                            </td>
                            <td
                                style="width: 33%; border-top: 1px solid white; border-bottom: 1px solid white; border-right: 1px solid white; text-align: center;">
                                <p style="margin: 0; vertical-align: top;">For, BE-GON PEST CONTROL SERVICES</p>
                                <p style="margin: 0; vertical-align: middle;">Authorized Signatory</p>
                            </td>


                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </center>
</body>

</html>
