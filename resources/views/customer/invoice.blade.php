@php

    use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice</title>
    <style>
        * {
            box-sizing: border-box;

        }

        /* Create two equal columns that floats next to each other */
        .column1 {
            float: left;
            width: 60%;
        }

        .column {
            float: left;
            width: 40%;
            height: 200px;
            display: flex;
            justify-content: center;
            /* Center horizontally */
            align-items: center;
            /* Center vertically */
        }



        .column2 {
            width: 50%;
            height: 100px;
            display: flex;
            float: left;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        table {
            border-collapse: collapse;
            /* width: 100%; */
        }


        table,
        th,
        td {

            border: 1px solid black;

            padding: 8px;
        }

        hr {
           color: black;
            height: 5px;
        }
        #upi {
            display: block;
            /* Ensures the image won't affect the layout of other elements */
            /* margin: 0 auto; */
            padding: 0;
            /* Center the image horizontally in the cell */
        }
    </style>
</head>

<body>


    {{-- <div class="row" style="border:1px solid black;padding:1px">
        <div class="column">
            <img src="images/begon-logo.png" alt="Logo" style="padding:5%;margin-top:{{ $width }}">
        </div>
        <div class="column1" style="text-align:right;font-size:14px;">
            <b>{{ $settings->name ?? '' }}</b><br>
            WE ALSO PROVIDE 100% HERBAL (ODOURLESS) SERVICES <span
                style="text-transform: uppercase;">{{ $settings->address ?? '' }}</span><br>Mobile
            No:{{ $settings->mobile_no ?? '' }}<br>E-mail:{{ $settings->email ?? '' }}<br>Website:{{ $settings->website ?? '' }}<br>LIC
            No:{{ $settings->lic_no ?? '' }}
            <br>
            @if ($invoice_type == 0 && $settings->gst_no != '')
                GSTIN:{{ $settings->gst_no ?? '' }},
            @endif
            State:{{ $settings->state ?? '' }}
        </div>
    </div> --}}
    <div style="text-align:center;font-size:14px;margin-bottom:15px"><b>
        @if ($customerPayment->is_bill == "0")
            Estimate 
        @else
            @if ($invoice_type == 0 && $settings->gst_no != '')Tax @endif Invoice 
        @endif
    </b></div>

    <table cellspacing="0" style="padding-top:-1%;border-top:unset !important;">
        <!-- First Row with Two Columns -->
        <tr>
            <td style="border-right:0px;">

                &nbsp; &nbsp; &nbsp;<img src="images/begon-logo.png" alt="Logo" width="320px" height="80px"
                    class="begon-image">

            </td>
            <td style="border-left:0px;text-align:right;font-size:14px">
                <b style="font-size:16px">{{ $settings->name ?? '' }}</b><br>
                WE ALSO PROVIDE 100% HERBAL (ODOURLESS) SERVICES <br>
                <span style="text-transform: uppercase;">{{ $settings->address ?? '' }}</span> <br>
                Mobile No: 9892072628/9324972638<br>
                9323669191/7303290130/28280898/28282833
                {{-- &nbsp;&nbsp;
                 Mobile No:{{ $settings->mobile_no ?? '' }}&nbsp;&nbsp; --}}<br>
                E-mail:{{ $settings->email ?? '' }}<br>Website:{{ $settings->website ?? '' }}<br>LIC
                No:{{ $settings->lic_no ?? '' }}
                ,
                @if ($invoice_type == 0 && $settings->gst_no != '')
                    GSTIN:{{ $settings->gst_no ?? '' }}, <br>
                @endif
                State:{{ $settings->state ?? '' }}
            </td>
        </tr>
        <!-- Second Row with Two Columns -->
        <tr>
            <td style="text-align:left;font-size:14px;border-top:unset;border-bottom:unset;border-right"><b>Bill To</b></td>
            <td style="border-left:0px;text-align:right;font-size:14px;border-top:unset;border-bottom:unset"><b>Invoice Details</b></td>
        </tr>
        <tr>
            <td style="width:50%;border-right:unset;border-bottom:unset">
                <!-- Nested table with two rows in the second column -->

                <div style="font-size: 14px">
                    @if (isset($customerPayment->customerDetails->first_name))
                      <b> {{ $customerPayment->customerDetails->first_name }}</b>
                    @else
                        -
                    @endif
                    </b>
                    <br>
                    <br>
                    @php
                        $sentence = $customerAddress->address_line1;
                        // Define the width for the line break
                        $width = 40;
                        // Use wordwrap to break the sentence into lines of specified width
                        $wrapped_sentence = wordwrap($sentence, $width, "\n", true);
                    @endphp
                    {!! nl2br($wrapped_sentence) !!}
                    {{-- {!! nl2br($customerAddress->address_line1) !!} <br> --}}
                    {{-- @if (isset($customerAddress->address_line1))
                        @php
                            // Split the address by commas
                            $addressParts = explode(',', $customerAddress->address_line1);
                        @endphp

                        @foreach ($addressParts as $key=>$part)
                            @if ($key % 2 == "0")
                                {{ trim($part) }} <br>
                            @else
                                {{ trim($part) }}
                            @endif

                        @endforeach
                    @else
                        {{ '' }}
                    @endif --}}
                    <br>
                    <br>
                    Contact No.:{{ $customerAddress->contact ?? '' }}
                    @if (!empty($customerPayment->customerDetails->gst_no))
                        <br>
                        <br>
                        GSTIN: {{ $customerPayment->customerDetails->gst_no ?? '' }}

                    @endif
                    {{-- <br>
                    <br>
                     State: 27 Maharashtra --}}
                </div>
                <div class="clearfix">&nbsp;</div>
            </td>
            <td style="text-align:right;width:50%;font-size:14px;padding:0px;margin:0px;border-bottom:unset">
                Place of supply:{{ $settings->state }} &nbsp;<br>
                @if ($customerPayment->is_bill == "1")
                    <b>Invoice No:{{ $customerPayment->invoice_no ?? '' }} &nbsp;</b><br>
                @else
                    <b>Quotation No:{{ $customerPayment->invoice_no ?? '' }} &nbsp;</b><br>
                @endif
                <b> Date:
                {{ date('d/m/Y', strtotime($customerPayment->invoice_date)) ?? '' }} &nbsp;</b>
            </td>
        </tr>
        <!-- Third Row with Two Columns -->
        <tr>
            <td colspan="2">
                <!-- Nested table with two rows in the second column -->
                <table style="border: none;width:100%;margin:-7px;">
                    <!-- First nested row with one column -->
                    @php
                        $width = "10%";
                        $servic_width = "30%"
                    @endphp
                    <tr style="">
                        <td style="border-top:unset;border-left:unset;text-align:left;font-size:14px;vertical-align:top;width:26%">
                            <b>Service
                                Description</b>
                        </td>
                        <td style="border-top:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:topvertical-align:top;width:100px">
                            <b>Period</b>
                        </td>
                        <td style="border-top:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:top;width:100px">
                            <b>Contract
                                From</b>
                        </td>
                        <td style="border-top:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:top;width:100px">
                            <b>Contract
                                To</b>
                        </td>
                        @if ($isContractShow == '1')
                            <td style="border-top:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:top;width:100px">
                                <b>Contract
                                    Amt</b>
                            </td>
                        @endif

                        <td style="border-top:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:top;width:100px"><b>HSN
                                Code</b></td>
                        @if ($invoice_type == 0)
                            <td style="border-top:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:top;width:100px">
                                <b>Taxable
                                    Amount</b>
                            </td>

                            <td style="border-top:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:top;width:100px">
                                <b>GST</b>
                            </td>
                        @endif
                        <td
                            style="border-top:unset;border-left:unset;border-right:unset;text-align:right;font-size:14px;vertical-align:top;width:100px">
                            <b>Amount</b>
                        </td>
                    </tr>
                    <!-- Second nested row with one column -->
                    @php
                        $totalAmount = 0;
                        $totalGst = 0;
                        $gstTotalAmount = 0;
                        $getSgst = 0;
                    @endphp

                    @if (count($customerPayment->customerServices) > 0)

                        @foreach ($customerPayment->customerServices as $servies)
                            @php
                                $formattedDates = [];
                            @endphp
                            @foreach ($servies->serviceDueDate as $date)
                                @php
                                    $formattedDates[] = \Carbon\Carbon::parse($date->service_due)->format('j M');

                                @endphp
                            @endforeach

                            <tr>
                                <td
                                    style="border-top:unset;border-bottom:unset;border-left:unset;text-align:left;font-size:14px;vertical-align:top">
                                    <b>{{ ucfirst($servies->service_id) ?? '' }} </b>
                                    @if (!empty($servies->remark))
                                        <br>
                                        <span>({{ $servies->remark }}) </span>
                                    @endif
                                    {{-- @if (!empty($servies->contract))
                                            <br>
                                            Contract Amount: {{ $servies->contract }}
                                        @endif --}}
                                    {{-- @if (count($formattedDates) > 0)
                                            <br> Service Due :
                                            {{ implode(', ', $formattedDates) }}
                                       @endif --}}
                                    @if (!empty($servies->service_due_text))
                                        <br> Service Due:
                                        {{ $servies->service_due_text }}
                                    @endif
                                    @if (count($customerPayment->customerServices) == '1')
                                        <br><br><br><br><br><br>
                                    @else
                                        <br><br>
                                    @endif

                                    {{-- @if (count($customerPayment->customerServices) <= '1')
                                        <br><br> <br>
                                    @endif --}}
                                </td>
                                <td
                                    style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right; font-size:14px;vertical-align:top">
                                    {{ $servies->period ?? '' }}
                                    @if (count($customerPayment->customerServices) == '1')
                                        <br><br><br><br><br><br>
                                    @else
                                        <br><br>
                                    @endif
                                </td>
                                <td
                                    style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:top">
                                    {{ $servies->contract_from ?? '' }}
                                    @if (count($customerPayment->customerServices) == '1')
                                        <br><br><br><br><br><br>
                                    @else
                                        <br><br>
                                    @endif
                                </td>
                                <td
                                    style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:top">
                                    {{ $servies->contract_to ?? '' }}
                                    @if (count($customerPayment->customerServices) == '1')
                                        <br><br><br><br><br><br>
                                    @else
                                        <br><br>
                                    @endif
                                </td>
                                @if ($isContractShow == '1')
                                    <td
                                        style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:top">
                                        {{ $servies->contract ?? '' }}
                                        @if (count($customerPayment->customerServices) == '1')
                                            <br><br><br><br><br><br>
                                        @else
                                            <br><br>
                                        @endif
                                    </td>
                                @endif

                                <td
                                    style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:top">
                                    {{ $servies->hsn ?? '' }}
                                    @if (count($customerPayment->customerServices) == '1')
                                        <br><br><br><br><br><br>
                                    @else
                                        <br><br>
                                    @endif
                                </td>
                                @if ($invoice_type == 0)
                                    <td style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:top">

                                        &#8377; {{ $servies->amount ?? '' }}
                                        @if (count($customerPayment->customerServices) == '1')
                                            <br><br><br><br><br><br>
                                        @else
                                            <br><br><br><br><br>
                                        @endif
                                    </td>

                                    @php   $getSgst = $servies->gst / 2;@endphp

                                    <td
                                        style="border-top:unset;border-bottom:unset;border-left:unset;text-align:right;font-size:14px;vertical-align:topvertical-align:top;">
                                        &#8377; {{ $servies->gst_cal_amount }}<br>
                                        {{ "(GST:".$servies->gst.'%)' }}
                                        {{-- ({{ $servies->gst ?? '' }}&#37;)  @isset($servies->gst)
                                            {{-- (CGST:{{ $getSgst }}%,<br>SGST:{{ $getSgst }}%)

                                        @endisset --}}
                                        @if (count($customerPayment->customerServices) == '1')
                                            <br><br><br><br><br><br>
                                        @else
                                            <br><br>
                                        @endif
                                    </td>
                                @endif
                                <td
                                    style="border-top:unset;border-left:unset;border-right:unset;border-bottom:unset;text-align:right;font-size:14px;vertical-align:top">
                                    &#8377; @if ($invoice_type == 0)
                                        {{ $servies->gst_amount }}
                                    @else
                                        {{ $servies->gst_amount ?? '' }}
                                    @endif
                                    @if (count($customerPayment->customerServices) == '1')
                                        <br><br><br><br><br><br>
                                    @else
                                        <br><br>
                                    @endif
                                </td>
                            </tr>
                            @php
                                $totalAmount += $servies->amount;

                                $totalGst += $servies->gst_cal_amount;
                            
                                $gstTotalAmount += $servies->gst_amount;
                            @endphp
                        @endforeach

                    @endif

                    <!-- Third nested row with one column -->
                    <tr>
                        @php
                            if ($isContractShow == '1') {
                                $colspanvalue = 5;
                                $colspanvalue2 = 3;
                                if ($invoice_type == 0) {
                                    $colspanvalue = 6;
                                    $colspanvalue2 = 4;
                                }

                                $colspanvalue3 = '4';
                            } else {
                                $colspanvalue = 4;
                                $colspanvalue2 = 2;
                                if ($invoice_type == 0) {
                                    $colspanvalue = 5;
                                    $colspanvalue2 = 3;
                                }
                                $colspanvalue3 = '3';
                            }

                        @endphp
                        <td colspan="{{ $colspanvalue }}"
                            style="border-bottom:unset;border-left:unset;text-align:left;border-right:unset;font-size:14px;">
                            <b>Total</b>
                        </td>
                        <td style="border-bottom:unset;border-left:unset;text-align:right;font-size:14px;"></td>
                        {{-- <td style="border-bottom:unset;border-left:unset;text-align:right;"><b>&#8377;
                                {{ $totalAmount }}</b></td> --}}
                        @if ($invoice_type == 0)
                            <td style="border-left:unset;border-bottom:unset;text-align:right;font-size:14px;"><b>&#8377;
                                    {{ $totalGst }}</b></td>
                        @endif
                        <td style="border-left:unset;border-right:unset;border-bottom:unset;text-align:right;font-size:14px;">
                            <b>&#8377; {{ $gstTotalAmount }}</b>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td style="border-bottom:unset;border-right:unset;border-top:unset;">
                &nbsp;
            </td>
            <td style="font-size:14px;border-bottom:unset;border-top:unset;">
                    <br><br><br>
                    {{-- <b>Amounts</b><br>
                    Sub Total  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &#8377; {{ $gstTotalAmount }}<br>
                    <span style="border-top:1px solid black;"> --}}
                        <b>Total</b>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> &#8377; {{ $gstTotalAmount }} </b>
                    {{-- </span>
                    --}}

            </td>
        </tr>
        <tr>
            <td style="font-size:14px;text-align:center;border-bottom:unset;border-right:unset">
                <b>Invoice Amount In Words</b><br><br>{{ numberInword($gstTotalAmount) }}
                    <br><br>
                <b> Payment  Mode</b> <br>
                <br>{{ $customerPayment->payment_mode }} &nbsp;

            </td>
            <td style="border-bottom:unset">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <table  cellspacing="0" style="margin:-8px;">
                    <tr>
                        <td style="width:40 %;border-top:1px solid white;border-bottom:1px solid white;vertical-align: top;font-size:14px;border-left:unset">

                            {{-- <img  src=data:image/png;base64,{{ $qrCode }}
                            <br>

                             <img src="images/scan_qr.png" id="upi" alt="Logo"
                                class="brand-image img-circle elevation-5" width="150px"> <br> --}}
                                <table  cellspacing="0" style="border-collapse: collapse; border: none;">
                                    <tr>
                                        <td colspan="2" style="border: unset; vertical-align: top;">
                                            <p><b>Bank Details</b></p><br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: unset; vertical-align: top;">

                                            <div style="margin-top: 20px;">
                                            <img  src=data:image/png;base64,{{ $qrCode }}
                                            <br>

                                             <img src="images/scan_qr.png" id="upi" alt="Logo"
                                                class="brand-image img-circle elevation-5" width="170px">
                                            </div>
                                        </td>
                                        <td style="border: unset">
                                            <div style="margin-top: 20px;">
                                            <p style="">Name : {{ $settings->bank_name }}</p><br>
                                            <p>Acount No :<br> {{ $settings->bank_account }}</p><br>
                                            <p>IFSC code : {{ $settings->ifsc_code }}</p><br>
                                            <p>Acount holder's name : {{ $settings->account_holder_name }}</p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                        </td>
                        <td style="width: 35%;border-top:1px solid white;border-bottom:1px solid white;border-left:1px solid white;font-size:14px;vertical-align: top;">
                            @php $i =1 ; @endphp

                            <p><b>Terms and Conditions</b></p><br>
                            @foreach ($termAndCondition as $T_C)
                                <p>{{ $i }}){{ $T_C->content }}</p>
                                @php $i++; @endphp
                            @endforeach
                            <div style="margin-top: 20px;">
                            <P>Note: Kindly NOTE THAT THE above No. of Services may get skip, if not taken within
                                stipulated due period of the Services.</P><br>
                            </div>
                        </td>
                        <td
                            style="width: 32%; border-top: 1px solid white; border-bottom: 1px solid white; border-right: 1px solid white; text-align: center;font-size:14px;vertical-align: top;border-left:unset">
                            For, BE-GON PEST CONTROL SERVICES
                            <br> <br> <br> <br> <br> <br> <br>
                                <b>Authorized Signatory</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</body>

</html>
