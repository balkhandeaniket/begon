@extends('main_datatable')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Party Details</h4>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-4">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Party List</h3>
                            </div>
                            <div class="card-body" id="party_list">
                                <table id="party-table" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Contact</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-8" id="party_detalis">

                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    <div id="append_data">

                    </div>
                    <div class="card-footer" style="background-color:white !important;float: right;">
                        <button type="button" onclick="submitPaidAmount()" class="btn btn-success">Submit</button>
                        &nbsp;
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('scripts')
    <script>
        function getParty() {
            $('#party-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '/party-getServerSide',
                    type: 'POST', // Specify POST method
                },
                columns: [{
                        data: 'name',
                        name: 'first_name'
                    },
                    {
                        data: 'contact',
                        name: 'contact'
                    },
                    {
                        data: 'amount',
                        name: 'amount'
                    },

                ]
            });
        }
        getParty();

        function showPartyDetails(id) {
            var postData = {
                "customer_id": id
            };
            $.post('/party-details', postData, function(response) {
                $('#party_detalis').html(response.html);
            }).done(function() {

                $('.select2').select2();
            });


        }

        function printInvoice(tran_id) {
            //var selectedAddress = $("input[name='address']:checked").val();
            var selectedAddress = "0";
            var url = "/party-payment-invoice/" + tran_id;
            window.open(url, '_blank');
        }

        function printContractForm(payment_id, type = 0) {
            // var selectedAddress = $("input[name='address']:checked").val();
            var selectedAddress = "0";
            var url = "/customer-contract-form/" + payment_id + '/' + selectedAddress + "/";
            window.open(url, '_blank');
        }

        function isNumberKey(event) {
            // Check if the pressed key is a number
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                event.preventDefault();
                return false;
            }
            return true;
        }
        function customerPaidAmount(payment_id) {
            var postData = {
                "payment_id" : payment_id
            };
            $.post('/customer-paid-amount', postData, function(response) {
                $('#append_data').html(response.html);
            }).done(function() {
                
               // $('.select2').select2();
            });
        }

        function submitPaidAmount(){
            payment_id = $("#payment_id").val();
            paid_amount = $("#append_data #paid_amount").val();
            payment_mode = $("#append_data #payment_mode").val();
            bank_name = $("#append_data #bank_name").val();
            payment_reference = $("#append_data #payment_reference").val();
            payment_disussion = $("#append_data #payment_disussion").val();
            remark = $("#append_data #remark").val();
          
            var postData = {
                "payment_id" : payment_id,
                "paid_amount": paid_amount,
                "payment_mode": payment_mode,
                "bank_name": bank_name,
                "payment_reference": payment_reference,
                "payment_disussion": payment_disussion,
                "remark": remark,
            };
            $.post('/customer-submit-paid-amount', postData, function(response) {
                $('#modal-default').modal('hide');
                showPartyDetails(response)
                
            }).done(function() {
            });
        }

        function deletePaymentTrans(trans_id,customer_id){

            var postData = {
                "trans_id" : trans_id,
                "customer_id" : customer_id,
            };
            $.post('/party-delete-transaction', postData, function(response) {
                showPartyDetails(customer_id)
            }).done(function() {
            });
        }

    </script>
@endpush
