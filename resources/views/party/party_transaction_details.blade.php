
<div class="card-body" id="transactionTbl">
    <table id="customer-table-transaction" class="table table-bordered">
        <thead>
            <tr>
                <th>Name</th>
                <th>Mobile</th>
                <th>Address</th>
                <th>Invoice No</th>
                <th>Invoice Date</th>
                <th>Amount</th>
                <th>GST Amount</th>
                <th>Total Amount</th>
                <th>Paid Amount</th>
                <th>Balance</th>
                {{-- <th>Payment Mode</th>
                <th>Payment Discussion</th> --}}
                <th>Action</th>
            </tr>
        </thead>
    </table>

</div>
@push('scripts')
    <script>
        $(document).ready(function(){
            $('#customer-table-transaction').DataTable({
    destroy: true,
    processing: true,
    serverSide: true,
    ajax: {
        url: '/party-getServerSide-transaction-tbl',
        type: 'POST', // Specify POST method
        data:postData,
    },
    columns: [
        {
            data: 'name',
            name: 'name'
        },
        {
            data: 'mobile',
            name: 'mobile'
        },
        {
            data: 'address',
            name: 'address'
        },
        {
            data: 'invoice_no_finance',
            name: 'invoice_no_finance'
        },
        {
            data: 'invoice_date',
            name: 'invoice_date'
        },

        {
            data: 'amount',
            name: 'amount'
        },
        {
            data: 'gst_cal_amount',
            name: 'gst_cal_amount'
        },
        {
            data: 'gst_amount',
            name: 'gst_amount'
        },
        {
            data: 'paid_amount',
            name: 'customer_payments.paid_amount'
        },
        {
            data: 'balance',
            name: 'balance'
        },
        // {
        //     data: 'payment_mode',
        //     name: 'customer_payments.payment_mode'
        // },
        // {
        //     data: 'paymentDiscussion',
        //     name: 'paymentDiscussion'
        // },
        {
            data: 'action',
            name: 'action'
        },
    ]
});
        })


</script>
