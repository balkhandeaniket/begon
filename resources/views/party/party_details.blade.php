<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Party Details</h3>
            </div>
            <div class="card-body">
                <!-- Heading -->
                <div>
                    <h5 class="card-title"><b>Name : {{ $customers->first_name }}</b></h5><br>
                    GST No.: {{ $customers->gst_no }}<br>
                    Remark : {{ $customers->remark }}<br>
                </div>
                <div>
                    {{-- <div class="col-6" style="margin-left: -39%;">
                        <p>{{ $customers->contact }}</p>
                    </div> --}}
                    <div class="clearfix">&nbsp;</div>
                    <div>
                        @foreach ($customersAddress as $val)
                            <p>Contact : {{ $val->contact }}</p>
                            <p>Address : {{ $val->address_line1 }}</p>
                        @endforeach
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<!-- Second Row in col-8 -->
<div class="row"> <!-- Added mt-3 for spacing between rows -->
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Transactional Details</h3>
            </div>
            <!-- Add content here -->
            <div class="party_transaction_detalis" id="party_transaction_details">
                <table class="table table-border table-striped" style="width:100%">
                    <thead>
                        <th> TYPE </th>
                        <th> INVOICE NO. </th>
                        <th> TOTAL AMOUNT </th>
                        <th> PAID AMOUNT </th>
                        <th> BALANCE </th>
                        <th> PAYMENT MODE </th>
                       {{-- <th> BANK NAME </th>
                        <th> PAYMENT REFERENCE </th>
                         <th> DISCUSSION OF PAYMENT </th>
                        <th> REMARK </th> --}}
                        <th> Action </th>
                    </thead>
                    <tbody>
                        @foreach ($payment as $val)
                            <tr>
                                <td>Sale</td>
                                <td>{{ $val->invoice_no }}</td>
                                <td>{{ $val->total_amount }}</td>
                                <td>{{ $val->paid_amount }}</td>
                                <td>{{ $val->total_amount - $val->paid_amount }}</td>
                                <td>{{ $val->payment_mode }}</td>
                                {{-- <td>{{ $val->total_amount }}</td>
                                <td>{{ $val->total_amount }}</td>
                                <td>{{ $val->total_amount }}</td>
                                <td>{{ $val->total_amount }}</td> --}}
                                <td>
                                    <button type="button" onclick="customerPaidAmount({{ $val->id }})"
                                        style="margin-top:30px" class="btn btn-sm btn-info" data-toggle="modal"
                                        data-target="#modal-default">Pay</button>
                                </td>
                            </tr>
                            @if (!empty($val->paidTransaction))
                                @foreach ($val->paidTransaction as $trans)
                                    <tr @if ($trans->is_delete =="1")  style="text-decoration: line-through;" @endif>
                                        <td>Payment-In</td>
                                        <td>{{  $val->invoice_no }}</td>
                                        <td> - </td>
                                        <td>{{ $trans->paid_amount }}</td>
                                        <td> - </td>
                                        <td>{{ $trans->payment_mode }}</td>
                                        <td> 
                                            @if ($trans->is_delete =="0")
                                                <button onclick="deletePaymentTrans({{ $trans->id }},{{$val->customer_id}})" class="btn btn-xs btn-danger"> <i class="fa fa-trash"></i> </button> 

                                                <a onclick="printInvoice({{ $trans->id }})"
                                                    style="margin:5px"
                                                    class="btn btn-sm btn-update btn-warning"
                                                    href="javascript:void(0)"> Receipt</a>
                                            @endif    
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
