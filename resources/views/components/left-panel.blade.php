 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/dashboard') }}" class="brand-link">
      <img src="/images/logo.jpg" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"> {{ auth()->user()->name ?? "" }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div> -->
     
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @php 
            $getMenus = getMenus(); 
            $url = url()->current();
            $urlSegemnt = explode('/', $url);
            $urlSegemntData =  explode('-', $urlSegemnt[3]); 
          @endphp
          @foreach($getMenus as $val)
          <li class="nav-item menu-open">
            <a href="{{ url($val->url) }}" class="nav-link @if ($urlSegemntData[0] == $val->url) active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                  {{ $val->title }} 
                <!-- <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>
          </li>
          @endforeach
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>