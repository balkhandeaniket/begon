@extends('main_datatable')

@section('content')
    <style>
        #customer_list {
            overflow-x: auto;
        }

        .total_anount_show {
            padding: 20px;
            border: 1px solid rgb(226, 220, 220);
        }

        th {
            text-align: center;
            /* Or right or left */
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Account</h4>
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-2">
                        <button onclick="exportData()" class="btn btn-info" style="float: right">Export</button>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="finance_year">Finance Year</label>
                            <select name="finance_year" id="finance_year" class="form-control select2">
                                <option value="All">All</option>
                                @foreach ($financeYear as $val)
                                    <option value="{{ $val }}">{{ $val }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="from_date">From Date</label>
                            <input type="date" name="from_date" id="from_date" class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="to_date">To Date</label>
                            <input type="date" name="to_date" id="to_date" class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" style="margin-top: 34px;">
                            <button onclick="collection()" class="btn btn-sm btn-info">Go</button>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Payment Collection</h3>
                            </div>

                            <!-- /.card-header -->
                            <div class="card-body" id="customer_list">
                                <div class="clearfix">&nbsp;</div>

                                <div class="col-md-12 total_anount_show">
                                    <label class=""> Total Amount</label> : &#8377;
                                    <label id="total_amount"> ... </label>
                                    </label> |
                                    <label class=""> Total GST</label> : &#8377;
                                    <span id="total_gst_cal_amount"> ... </span>
                                    </label> |
                                    <label class=""> Total Amount</label> : &#8377;
                                    <span id="total_gst_amount"> ... </span>
                                    </label> |
                                    <label class=""> Paid Amount</label> : &#8377;
                                    <span id="paid_amount"> ... </span>
                                    </label>
                                </div>

                                <div class="clearfix">&nbsp;</div>
                                <table id="customer-table" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            {{-- <th>Sr. No.</th> --}}
                                            <th>Customer Name</th>
                                            <th>Address</th>
                                            <th>Mobile</th>
                                            <th>Services</th>
                                            <th>Invoice No</th>
                                            {{-- <th>Amount</th>
                                            <th>GST Amount</th> --}}
                                            <th>Amount</th>
                                            {{-- <th>Paid Amount</th> --}}
                                            {{-- <th>Invoice Date</th> --}}
                                            <th>Contract</th>
                                            {{-- <th>Contract To</th> --}}

                                            {{-- <th>Payment Mode</th> --}}
                                            {{-- <th>Payment Discussion</th> --}}
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- /.content -->
    </div>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div id="append_data">

                    </div>
                    <div class="card-footer" style="background-color:white !important;float: right;">
                        <button type="button" onclick="submitPaidAmount()" class="btn btn-success">Submit</button>
                        &nbsp;
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        function isNumberKey(event) {
            // Check if the pressed key is a number
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                event.preventDefault();
                return false;
            }
            return true;
        }

        function customerPaidAmount(payment_id) {
            var postData = {
                "payment_id": payment_id
            };
            $.post('/customer-paid-amount', postData, function(response) {
                $('#append_data').html(response.html);
            }).done(function() {

                $('.select2').select2();
            });
        }

        function deletePayment(payment_id,flag="") {
            var result = window.confirm('Are you sure you want to proceed?');
            if (result) {

                var postData = {
                    "payment_id": payment_id,
                    "flag": flag,
                };
                $.post('/customer-payment-cancelled', postData, function(response) {
                    collection()
                }).done(function() {

                });
            }
        }

        function exportData() {
            finance_year =  $('#finance_year').val();
            from_date =  $('#from_date').val();
            to_date =  $('#to_date').val();

            // Create a temporary form
            var form = $('<form>', {
                method: 'POST',
                action: '/customer-download-csv'
            });

            // Add postData inputs to the form
            form.append($('<input>', {
                type: 'hidden',
                name: 'finance_year',
                value: finance_year
            }));
            form.append($('<input>', {
                type: 'hidden',
                name: 'from_date',
                value: from_date
            }));
            form.append($('<input>', {
                type: 'hidden',
                name: 'to_date',
                value: to_date
            }));

            // Append the form to the body and submit it
            form.appendTo('body').submit().remove();


        // var link = document.createElement('a');
        //     finance_year =  $('#finance_year').val();
        //     from_date =  $('#from_date').val();
        //     to_date =  $('#to_date').val();
        // link.href = '/customer-download-csv';
        // link.download = 'data.csv';

        // // Append to the document and trigger a click
        // document.body.appendChild(link);
        // link.click();

        // // Clean up
        // document.body.removeChild(link);
        // postData =  {
        //     finance_year: $('#finance_year').val(),
        //     from_date: $('#from_date').val(),
        //     to_date: $('#to_date').val(),
        // }

        // $.get('/customer-download-csv', postData, function(response) {

        // }).done(function() {

        // });
        }

        function submitPaidAmount() {
            payment_id = $("#payment_id").val();
            paid_amount = $("#append_data #paid_amount").val();
            payment_mode = $("#append_data #payment_mode").val();
            bank_name = $("#append_data #bank_name").val();
            payment_reference = $("#append_data #payment_reference").val();
            payment_disussion = $("#append_data #payment_disussion").val();
            remark = $("#append_data #remark").val();

            var postData = {
                "payment_id": payment_id,
                "paid_amount": paid_amount,
                "payment_mode": payment_mode,
                "bank_name": bank_name,
                "payment_reference": payment_reference,
                "payment_disussion": payment_disussion,
                "remark": remark,
            };
            $.post('/customer-submit-paid-amount', postData, function(response) {
                $('#modal-default').modal('hide');
                collection()
            }).done(function() {

            });
        }

        function collection() {
            postData = {
                finance_year: $('#finance_year').val(),
                from_date: $('#from_date').val(),
                to_date: $('#to_date').val(),
            }

            $.post('/collection-total-amount-cal', postData, function(response) {
                $("#total_amount").text(response.total_amount)
                $("#total_gst_amount").text(response.total_gst_amount)
                $("#total_gst_cal_amount").text(response.total_gst_cal_amount)
                $("#paid_amount").text(response.paid_amount)
            }).done(function() {

            });

            $('#customer-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '/collection-getServerSide',
                    type: 'POST', // Specify POST method
                    data: postData,
                },
                columns: [
                    // {
                    //     data: 'sr_no',
                    //     name: 'sr_no'
                    // },
                    {
                        data: 'name',
                        name: 'name',
                        render: function(data, type, row) {
                            if (row.is_return =='1') {
                                return '<span style="text-decoration: line-through;">' + data + '</span>';
                            }
                            return data;
                        }
                    },
                    {
                        data: 'address',
                        name: 'address',
                        render: function(data, type, row) {
                            if (row.is_return =='1') {
                                return '<span style="text-decoration: line-through;">' + data + '</span>';
                            }
                            return data;
                        }
                    },
                    {
                        data: 'mobile',
                        name: 'mobile',
                        render: function(data, type, row) {
                            if (row.is_return =='1') {
                                return '<span style="text-decoration: line-through;">' + data + '</span>';
                            }
                            return data;
                        }
                    },
                    {
                        data: 'service_id',
                        name: 'service_id',
                        render: function(data, type, row) {
                            if (row.is_return =='1') {
                                return '<span style="text-decoration: line-through;">' + data + '</span>';
                            }
                            return data;
                        }
                    },
                    /*
                    {
                        data: 'invoice_date',
                        name: 'invoice_date'
                    },
                    */
                    {
                        data: 'invoice_no_finance',
                        name: 'invoice_no_finance',
                        render: function(data, type, row) {
                            if (row.is_return =='1') {
                                return '<span style="text-decoration: line-through;">' + data + '</span>';
                            }
                            return data;
                        }
                    },
                    // {
                    //     data: 'amount',
                    //     name: 'amount'
                    // },
                    // {
                    //     data: 'gst_cal_amount',
                    //     name: 'gst_cal_amount'
                    // },
                    {
                        data: 'gst_amount',
                        name: 'gst_amount',
                        render: function(data, type, row) {
                            if (row.is_return =='1') {
                                return '<span style="text-decoration: line-through;">' + data + '</span>';
                            }
                            return data;
                        }
                    },
                    // {
                    //     data: 'paid_amount',
                    //     name: 'customer_payments.paid_amount'
                    // },
                    // {
                    //     data: 'invoice_date',
                    //     name: 'invoice_date'
                    // },
                    {
                        data: 'contract_from',
                        name: 'contract_from',
                        render: function(data, type, row) {
                            if (row.is_return =='1') {
                                return '<span style="text-decoration: line-through;">' + data + '</span>';
                            }
                            return data;
                        }
                    },
                    // {
                    //     data: 'contract_to',
                    //     name: 'contract_to'
                    // },

                    // {
                    //     data: 'payment_mode',
                    //     name: 'customer_payments.payment_mode'
                    // },
                    // {
                    //     data: 'paymentDiscussion',
                    //     name: 'paymentDiscussion'
                    // },
                    {
                        data: 'action',
                        name: 'action'
                    },
                ]
            });
        }
        collection();

        function printInvoice(payment_id, type = 0) {
            //var selectedAddress = $("input[name='address']:checked").val();
            var selectedAddress = "0";
            var url = "/customer-payment-pdf/" + payment_id + '/' + selectedAddress + "/" + type;
            window.open(url, '_blank');
        }

        function printContractForm(payment_id, type = 0) {
            // var selectedAddress = $("input[name='address']:checked").val();
            var selectedAddress = "0";
            var url = "/customer-contract-form/" + payment_id + '/' + selectedAddress + "/";
            window.open(url, '_blank');
        }

    </script>
@endpush
