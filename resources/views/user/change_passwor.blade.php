<div class="row">
    <div class="col-md-4">
        <input name="user_id" value="{{ $user->id }}" type="hidden">
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password"
                name="password" id="password" class="form-control">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="password-re">Re-entered password</label>
            <input type="password"
                name="password-re" id="password-re" class="form-control">
        </div>
    </div>
    <small class="text-danger passError">*Passwords do not match</small>
</div>
