@extends('main')
<style>
    .passError {
        display: none;
    }
</style>
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Users</h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <form id="user-form" action="{{ $user_action_url }}" method="post">
                                <div class="card-header">
                                    <h3 class="card-title" id="titleStaus"></h3>
                                    <div class="card-tools">

                                        {{-- <ul class="nav nav-pills ml-auto">
                                            <li class="nav-item btn-sm">
                                                <button type="submit" class="btn btn-sm btn-success">
                                                    @if (empty($getUser))
                                                        Submit
                                                    @else
                                                        Update
                                                    @endif
                                                </button>
                                            </li>
                                            <li class="nav-item btn-sm">
                                                <button class="btn-sm btn-danger" onclick="history.back()">Back</button>
                                            </li>

                                        </ul> --}}
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">

                                    {{-- @csrf --}}
                                    <input type="hidden" name="id" value="{{ $getUser->id ?? '' }}">
                                    <!-- General Details Section -->
                                    <div class="card card-secondary">
                                        <div class="card-header">
                                            <h3 class="card-title">{{ $user_action }}</h3>

                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="first_name">Name<span
                                                                class="text-danger">*</span></label>
                                                        <input type="text" value="{{ $getUser->name ?? '' }}"
                                                            name="first_name" id="first_name" class="form-control" required>
                                                    </div>
                                                    <small id="firstNameErr" class="text-danger"
                                                        style="display:none;">*First name is reuired.</small>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="contact">Contact<span
                                                                class="text-danger">*</span></label>
                                                        <input onkeypress="return isNumberKey(event)" type="text" value="{{ $getUser->contact ?? '' }}"
                                                            name="contact" id="contact" required class="form-control" 
                                                            maxlength="10">
                                                    </div>
                                                    <small id="contactErr" class="text-danger"
                                                        style="display:none;">*Contact is reuired.</small>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="email">Email<span
                                                                class="text-danger">*</span></label>
                                                        <input required type="email" value="{{ $getUser->email ?? '' }}"
                                                            required name="email" id="email" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="staus">Status</label>
                                                        <select name="status" id="status" class="form-control select2">
                                                            <option value="1"
                                                                @if (isset($getUser) && $getUser->status == 1) selected @endif> Active
                                                            </option>
                                                            <option value="0"
                                                                @if (isset($getUser) && $getUser->status == 0) selected @endif> In-Active
                                                            </option>
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="role_id">Role<span class="text-danger">*</span></label>
                                                        <select name="role_id" id="role_id" required
                                                            class="form-control select2">
                                                            <option value=""> Select Role</option>
                                                            @foreach ($roles as $row)
                                                                <option @if (isset($getUser) && $getUser->role_id == $row->id) selected @endif
                                                                    value="{{ $row->id }}"> {{ $row->title }}
                                                                </option>
                                                            @endforeach
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="password">Password</label>
                                                        <input @if ($user_action == 'Create') required @endif type="password" value=""
                                                            name="password" id="password" class="form-control">
                                                    </div>
                                                </div>
                                                {{-- @if ($user_action == 'Create')
                                                    
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="password-re">Re-entered password</label>
                                                            <input required type="password" name="password-re" id="password-re"
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                    <small class="text-danger passError">*Passwords do not match</small>
                                                @endif --}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-footer" style="background-color:white !important;float: right;">
                                        <button type="submit" class="btn btn-success btn-sm">
                                            @if (empty($getUser))
                                                Submit
                                            @else
                                                Update
                                            @endif
                                        </button>
                                        &nbsp;
                                        <button  onclick="history.back()" type="button"  class="btn btn-secondary btn-sm ">Cancel</button>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                            </form>
                        </div>

                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('scripts')
    <script>
        // document.getElementById('user-form').addEventListener('submit', function(event) {
        //     // Get the values of password and re-entered password
        //     var password = document.getElementById('password').value;
        //     var passwordRe = document.getElementById('password-re').value;
        //     var status = document.getElementById('titleStaus').value;
        //     // Check if the password field is not empty
        //     if (password !== '' && status === "Add") {
        //         // Check if the passwords match
        //         if (password !== passwordRe) {
        //             // Display an error message
        //             $('.passError').fadeIn();
        //             setTimeout(() => {
        //                 $('.passError').fadeOut();
        //             }, 2000);

        //             // Prevent the form submission
        //             event.preventDefault();
        //         }
        //     }
        // });

        function isNumberKey(event) {
            // Check if the pressed key is a number
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                event.preventDefault();
                return false;
            }
            return true;
        }
    </script>
@endpush
