@extends('main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Customer</h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h1 class="card-title"> Customer Details</h3>
                                    <div class="card-tools">
                                        <ul class="nav nav-pills ml-auto">
                                            <li class="nav-item btn-sm">
                                                <button class="btn-sm btn-danger" onclick="history.back()">Back</button>
                                            </li>
                                        </ul>
                                    </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                {{-- @csrf --}}
                                <input type="hidden" name="id" value="{{ $getCustomer->id ?? '' }}">
                                <!-- General Details Section -->
                                <div class="card card-secondary">
                                    <div class="card-header">
                                        <h3 class="card-title">Personal Details</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label for="first_name">Name : </label>
                                                    {{ $getCustomer->first_name ?? '' }}
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="middle_name">Remark</label>
                                                    {{ $getCustomer->remark ?? '' }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <!-- Communication Details Section -->
                                    <div class="card card-secondary">
                                        <div class="card-header">
                                            <h3 class="card-title">Address</h3>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-striped">
                                                <tr>
                                                    <th>Contact</th>
                                                    <th>Email</th>
                                                    <th>Contact No 1</th>
                                                    <th>Contact No 2</th>
                                                    <th>Address 1</th>
                                                    <th>Address 2</th>
                                                    <th>Landmark</th>
                                                    <th>Road</th>
                                                    <th>Near</th>
                                                    <th>Street</th>
                                                    <th>Opposite</th>
                                                    <th>Locality</th>
                                                    <th>Pincode</th>
                                                </tr>
                                                @foreach ($getCustomerAddress as $val)
                                                    <tr>
                                                        <td> {{ $val->contact }}</td>
                                                        <td> {{ $val->email }}</td>
                                                        <td> {{ $val->contact_no1 }}</td>
                                                        <td> {{ $val->contact_no2 }}</td>
                                                        <td> {{ $val->address_line1 }}</td>
                                                        <td> {{ $val->address_line2 }}</td>
                                                        <td> {{ $val->landmark }}</td>
                                                        <td> {{ $val->road }}</td>
                                                        <td> {{ $val->near }}</td>
                                                        <td> {{ $val->street }}</td>
                                                        <td> {{ $val->opposite }}</td>
                                                        <td> {{ $val->locality }}</td>
                                                        <td> {{ $val->pin_code }}</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                    <!-- Communication Details Section -->
                                    <!-- Payment Details Section -->
                                    <div class="card card-info">
                                        <div class="card-header">
                                            <h3 class="card-title">Payment</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="title">Invoice Date</label>
                                                        <input type="text" name="title" id="title"
                                                            class="form-control" value=""
                                                            required>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="title">Paid Amount</label>
                                                        <input type="text" name="title" id="title"
                                                            class="form-control" value=""
                                                            required>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="title">Payment Mode</label>
                                                        <input type="text" name="title" id="title"
                                                            class="form-control" value=""
                                                            required>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="title">Bank Name</label>
                                                        <input type="text" name="title" id="title"
                                                            class="form-control" value=""
                                                            required>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="title">Payment Reference</label>
                                                        <input type="text" name="title" id="title"
                                                            class="form-control" value=""
                                                            required>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="title">Remark</label>
                                                        <textarea id="remark" name="remark"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix">&nbsp</div>
                                            <button class="btn btn-sm btn-success" style="float: right"> Submit</button>
                                        </div>
                                    </div>
                                    <!-- Communication Details Section -->
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
