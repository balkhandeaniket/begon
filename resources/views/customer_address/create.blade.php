@extends('main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Enquiry</h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">{{ $customer_action }}</h3>
                                <div class="card-tools">
                                    <ul class="nav nav-pills ml-auto">
                                        <li class="nav-item btn-sm">
                                            <button class="btn-sm btn-danger" onclick="history.back()">Back</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form id="enquiry-form" action="{{ $customer_action_url }}" method="post">
                                    {{-- @csrf --}}
                                    <input type="hidden" name="id" value="{{ $getCustomer->id ?? '' }}">
                                    <!-- General Details Section -->
                                    <div class="card card-secondary">
                                        <div class="card-header">
                                            <h3 class="card-title">Personal Details</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="first_name">First Name<span
                                                                class="text-danger">*</span></label>
                                                        <input type="text" value="{{ $getCustomer->first_name ?? '' }}"
                                                            name="first_name" id="first_name" class="form-control" required>
                                                    </div>
                                                    <small id="firstNameErr" class="text-danger"
                                                        style="display:none;">*First name is reuired.</small>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="contact">Contact<span
                                                                class="text-danger">*</span></label>
                                                        <input type="number" value="{{ $getCustomer->contact ?? '' }}"
                                                            name="contact" id="contact" required class="form-control"
                                                            maxlength="10">
                                                    </div>
                                                    <small id="contactErr" class="text-danger"
                                                        style="display:none;">*Contact is reuired.</small>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="concern_person">Remark</label>
                                                        <textarea name="remark" class="form-control">{{ $getCustomer->remark ?? '' }}</textarea>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- Communication Details Section -->
                                    <div class="card card-secondary">
                                        <div class="card-header">
                                            <h3 class="card-title">Communication Details</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="contact_no1">Contact No 1</label>
                                                        <input type="number" value="{{ $getCustomer->contact_no1 ?? '' }}"
                                                            name="contact_no1" id="contact_no1" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="address_line2">Address Line 2</label>
                                                        <input type="text"
                                                            value="{{ $getCustomer->address_line2 ?? '' }}"
                                                            name="address_line2" id="address_line2" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="road">Road</label>
                                                        <input type="text" value="{{ $getCustomer->road ?? '' }}"
                                                            name="road" id="road" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="near">Near</label>
                                                        <input type="text" value="{{ $getCustomer->near ?? '' }}"
                                                            name="near" id="near" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="contact_no2">Contact No 2</label>
                                                        <input type="number"
                                                            value="{{ $getCustomer->contact_no2 ?? '' }}"
                                                            name="contact_no2" id="contact_no2" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="landmark">Landmark</label>
                                                        <input type="text" value="{{ $getCustomer->landmark ?? '' }}"
                                                            name="landmark" id="landmark" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="street">Street</label>
                                                        <input type="text" value="{{ $getCustomer->street ?? '' }}"
                                                            name="street" id="street" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="opposite">Opposite</label>
                                                        <input type="text" value="{{ $getCustomer->opposite ?? '' }}"
                                                            name="opposite" id="opposite" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="locality">Locality</label>
                                                        <input type="text" value="{{ $getCustomer->locality ?? '' }}"
                                                            name="locality" id="locality" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="pin_code">Pincode</label>
                                                    <input type="text" value="{{ $getCustomer->pin_code ?? '' }}"
                                                        name="pin_code" id="pin_code" class="form-control">
                                                </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="card-footer" style="background-color:white !important;float: right;">
                                        <button type="submit" class="btn btn-success"
                                            >Submit</button>
                                        &nbsp;
                                        <a href="/customer"><button type="button"
                                                class="btn btn-secondary">Cancel</button></a>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@push('scripts')
    <script>
        /*
        $('#submit-enquiry').click(function() {
            contact = $('#contact').val();
            first_name = $('#first_name').val();
            if (first_name == "") {
                $('#firstNameErr').fadeIn();
                setTimeout(function() {
                    $('#firstNameErr').fadeOut();
                }, 2000);
                return false;
            }
            if (contact == "") {
                $('#contactErr').fadeIn();
                setTimeout(function() {
                    $('#contactErr').fadeOut();
                }, 2000);
                return false;
            }


            var formData = $('#enquiry-form').serialize();

            // Send an AJAX request
            $.ajax({
                type: 'POST',
                url: $('#enquiry-form').attr('action'),
                data: formData,
                success: function(response) {
                    if (response.success) {
                        // Enquiry added successfully
                        alert('Enquiry added successfully!');
                        // Optionally, you can redirect to another page
                        window.location.href = '{{ route('enquiry.index') }}';
                    } else {
                        // Display validation errors if any
                        alert('Error: ' + response.message);
                    }
                },
                error: function(xhr, status, error) {
                    alert('Error: ' + error);
                }
            });
        })
        */
    </script>
@endpush
