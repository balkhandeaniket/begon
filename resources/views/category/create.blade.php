@extends('main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Category</h4>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                              
                                <div class="card-tools">
                                    <ul class="nav nav-pills ml-auto">
                                        <li class="nav-item btn-sm">
                                            <button class="btn-sm btn-danger" onclick="history.back()" >Back</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{ $data['action'] }}" method="post">
                                    @csrf

                                    <div class="card card-secondary">
                                        <div class="card-header">
                                            <h3 class="card-title">{{ $data['action_name'] }}</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="title">Name<span  class="text-danger">*</span></label>
                                                        <input type="text" name="title" id="title"  class="form-control" value="{{ $getCategory->title ?? "" }}" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="type">Type</label>
                                                        <select name="type" id="type" class="form-control select2">
                                                            <option @if (!empty($getCategory) && $getCategory->type == "Service") selected @endif value="Service">Service</option>
                                                            <option @if (!empty($getCategory) && $getCategory->type == "Work") selected @endif value="Work">Work</option>
                                                            <option @if (!empty($getCategory) && $getCategory->type == "Salary") selected @endif value="Salary">Salary</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="is_expense">Is Expense</label>
                                                        <select name="is_expense" id="is_expense" class="form-control select2">
                                                            <option @if (!empty($getCategory) && $getCategory->is_expense == "0") selected @endif value="0">No</option>
                                                            <option @if (!empty($getCategory) && $getCategory->is_expense == "1") selected @endif value="1">Yes</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="status">Status</label>
                                                        <select name="status" id="status" class="form-control select2">
                                                            <option @if (!empty($getCategory) && $getCategory->status == "Active") selected @endif value="Active">Active</option>
                                                            <option @if (!empty($getCategory) && $getCategory->status == "Inactive") selected @endif value="Inactive">Inactive</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix">&nbsp</div>
                                            <button class="btn btn-sm btn-success"> Submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
