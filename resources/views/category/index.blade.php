@extends('main_datatable')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Category </h4>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Category List</h3>
                                <div class="card-tools">
                                    <ul class="nav nav-pills ml-auto">
                                        <li class="nav-item">
                                            <a class="nav-link active btn-sm btn" href="{{ route('category.create') }}">Create</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                            <th>Created</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($getCategory as $val)
                                          <tr>
                                            <td>{{ ucfirst($val->title) }}</td>
                                            <td>{{ ucfirst($val->type) }}</td>
                                            @if ($val->status == "Active")
                                              <td> <span class="btn btn-success btn-sm"> {{ $val->status }} </span> </td>
                                            @else
                                              <td> <span class="btn btn-danger btn-sm"> {{ $val->status }} </span> </td>
                                            @endif
                                            <td>{{ dateFormat($val->created_at) }}</td>
                                            <td>
                                              <a class="btn btn-sm btn-update btn-warning" href="{{ url('/category-edit/'.$val->id) }}"> Edit</a>
                                            </td>
                                          </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
