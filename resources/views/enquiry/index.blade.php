@extends('main_datatable')
@section('content')
    <style>
        #enquiry_list {
            overflow-x: auto;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Enquiry</h4>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Enquiry List</h3>

                                <div class="card-tools">
                                    <ul class="nav nav-pills ml-auto">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="{{ route('enquiry.create') }}">Create</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" id="enquiry_list">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select name="status" id="status" class="form-control select2">
                                                <option value="">All</option>
                                                <option value="Open">Open</option>
                                                <option value="Close">Close</option>
                                                <option value="Converted">Converted To Customer</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="type">Type</label>
                                            <select name="type" id="type" class="form-control select2">
                                                <option value="">All</option>
                                                <option value="New">New</option>
                                                <option value="Renewal">Renewal</option>
                                                <option value="Society">Society</option>
                                                <option value="Enquiry">Enquiry</option>
                                                <option value="Due">Due</option>
                                                <option value="Residential">Residential</option>
                                                <option value="Commercial">Commercial</option>
                                                <option value="Old">Old</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="created_by">Created By</label>
                                            <select name="created_by" id="created_by" class="form-control select2">
                                                <option value="">All</option>
                                                @foreach ($getUsers as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="from_date">From Date</label>
                                            <input type="date"  name="from_date" id="from_date" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="to_date">To Date</label>
                                            <input type="date"  name="to_date" id="to_date" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top: 34px;">
                                            <button onclick="getEnquiry()" class="btn btn-sm btn-info">Go</button>
                                        </div>
                                    </div>
                                </div>
                                <table id="enquiry-table" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Contact</th>
                                            <th>Address</th>
                                            <th>Status</th>
                                            <th>Follow-up Date </th>
                                            <th>Remark</th>
                                            <th>Type</th>
                                            <th>Created By</th>
                                            <th>Created At</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Status</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="updateStatusAction()">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('scripts')
    <script>
        function updateStatus(id) {
            var postData = {
                id: id,
            };
            $.post('/enquiry-update-status', postData, function(response) {
                $('.modal-body').empty();
                $('.modal-body').html(response.html);

            });
        }

        function updateStatusAction() {
            $("#modal-default").modal('hide')
            var postData = {
                id: $(".update_status #status_update_id").val(),
                remark: $(".update_status #remark").val(),
                status: $(".update_status #status").val(),
                appointment_date: $(".update_status #appointment_date").val(),
            };
            $.post('/enquiry-update-status-action', postData, function(response) {
                getEnquiry()

            });
        }
        /*
        function getEnquiry(){
            var postData = {
                status: $('#status').val(),
            };

            $.post('/enquiry-list',postData,function(response) {
                $('#enquiry_data').empty();
                $('#enquiry_data').html(response);
            });
        }
        getEnquiry()

        setTimeout(() => {
            $("#example2").DataTable({
            "responsive": true, "lengthChange": false, "autoWidth": true,
            //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        }, 2000);
        */

        function getEnquiry() {
            $('#enquiry-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '/enquiry-getServerSide',
                    type: 'POST', // Specify POST method
                    data: {
                        status: $('#status').val(),
                        type: $('#type').val(),
                        created_by : $("#created_by").val(),
                        from_date: $('#from_date').val(),
                        to_date: $('#to_date').val(),
                    },
                },
                columns: [{
                        data: 'first_name',
                        name: 'first_name'
                    },
                    {
                        data: 'contact',
                        name: 'contact'
                    },
                    {
                        data: 'address_line1',
                        name: 'address_line1'
                    },
                    {
                        data: 'status_flag',
                        name: 'status_flag'
                    },
                    {
                        data: 'appointment_date',
                        name: 'appointment_date'
                    },
                    {
                        data: 'remark',
                        name: 'remark'
                    },
                    {
                        data: 'type',
                        name: 'type'
                    },
                    {
                        data: 'created_by',
                        name: 'created_by'
                    },
                    {
                        data: 'created',
                        name: 'created'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    }
                ]
            });
        }
        getEnquiry();
    </script>
@endpush
