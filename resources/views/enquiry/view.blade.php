@extends('main')
<style>
    #fileErr {
        display: none;
    }
    table {
        border-collapse: collapse;
        width: 100%;
    }

    table, th, td {
        border: 1px solid #ddd;
    }

    th, td {
        padding: 8px;
        text-align: left;
    }

    th {
        background-color: #f2f2f2;
    }

    .delete-icon {
        color: red;
        cursor: pointer;
    }
</style>

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Enquiry</h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            {{-- <form id="enquiry-form" action="{{ $enquiry_action_url }}" method="post" enctype="multipart/form-data"> --}}
                            <div class="card-header">
                                <h3 class="card-title">Enquiry Details</h3>
                                <div class="card-tools">
                                    <ul class="nav nav-pills ml-auto">
                                        <li class="nav-item btn-sm">
                                            <button class="btn btn-sm btn-danger" onclick="history.back()">Back</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">

                                {{-- @csrf --}}
                                <input readonly type="hidden" name="id" id="enquiry_id"
                                    value="{{ $getEnquiry->id ?? '' }}">
                                <!-- General Details Section -->
                                <div class="card card-secondary">
                                    <div class="card-header">
                                        <h3 class="card-title">Audio Recording and Upload</h3>
                                    </div>
                                    <div class="card-body">
                                        <button id="startRecording">Start Recording</button>
                                        <button id="stopRecording" disabled>Stop Recording</button>
                                        <button id="uploadRecording" disabled>Upload Recording</button>
                                        <audio controls id="audioPlayer" style="display: none;"></audio>
                                        <br><br>
                                        <div class="table-responsive">
                                        <table  class="table">
                                            <tr>
                                                <th>Recording</th>
                                                <th>Action</th>
                                            </tr>

                                            @if (count($getRecordings) > 0)
                                            @foreach ($getRecordings as $val)
                                                <tr>
                                                    <td>
                                                        <audio controls>
                                                            <source src="{{ url('/storage/recordings/' . $val->file_name) }}" type="audio/mpeg">
                                                            Your browser does not support the audio element.
                                                        </audio>
                                                    </td>
                                                    <td>
                                                        @if(auth()->check() && auth()->user()->role_id == 1)
                                                        <span class="delete-icon" title="Delete Recording" onclick="deleteRecording({{ $val->id }})"   >
                                                            <i class="fas fa-trash-alt"></i>
                                                        </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @else

                                            <tr>
                                                <td colspan="2">No recording available.</td>
                                            </tr>
                                            @endif
                                        </table>
                                        </div>


                                    </div>
                                </div>

                                <div class="card card-secondary">
                                    <div class="card-header">
                                        <h3 class="card-title">Personal Details</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="first_name">Name<span class="text-danger">*</span></label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->first_name ?? '' }}" name="first_name"
                                                        id="first_name" class="form-control" required>
                                                </div>
                                                <small id="firstNameErr" class="text-danger" style="display:none;">*Name is
                                                    reuired.</small>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="services">Services</label>
                                                    @php
                                                        $selected_services_array = explode(',', $getEnquiry->services ?? '');
                                                    @endphp
                                                    <select name="services[]" id="services" disabled
                                                        class="form-control select2" multiple>
                                                        @foreach ($services as $row)
                                                            <option @if (!empty($getEnquiry->services) && in_array($row->id, $selected_services_array)) selected @endif
                                                                value="{{ $row->id }}"> {{ $row->title }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                      
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="contact">Contact<span class="text-danger">*</span></label>
                                                    <input readonly type="number" value="{{ $getEnquiry->contact ?? '' }}"
                                                        name="contact" id="contact" required class="form-control"
                                                        maxlength="10">
                                                </div>
                                                <small id="contactErr" class="text-danger" style="display:none;">*Contact is
                                                    reuired.</small>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input readonly type="email" value="{{ $getEnquiry->email ?? '' }}"
                                                        name="email" id="email" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="address_line1">Address</label>
                                                    <textarea readonly name="address_line1" id="address_line1" class="form-control">{{ $getEnquiry->address_line1 ?? '' }}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group" style="margin-top: 6%;">

                                                    @if (isset($getEnquiry) && $getEnquiry->attachment)
                                                        <audio controls>
                                                            <source
                                                                src="{{ asset('storage/recordings/' . $getEnquiry->attachment) }}"
                                                                type="audio/mpeg">
                                                            Your browser does not support the audio element.
                                                            </source>
                                                        </audio>
                                                    @else
                                                        <p>No recording available.</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Communication Details Section -->
                                {{-- <div class="card card-secondary">
                                    <div class="card-header">
                                        <h3 class="card-title">Communication Details</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="contact_no1">Contact No 1</label>
                                                    <input readonly type="number"
                                                        value="{{ $getEnquiry->contact_no1 ?? '' }}" name="contact_no1"
                                                        id="contact_no1" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="address_line2">Address Line 2</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->address_line2 ?? '' }}"
                                                        name="address_line2" id="address_line2" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="road">Road</label>
                                                    <input readonly type="text" value="{{ $getEnquiry->road ?? '' }}"
                                                        name="road" id="road" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="near">Near</label>
                                                    <input readonly type="text" value="{{ $getEnquiry->near ?? '' }}"
                                                        name="near" id="near" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="contact_no2">Contact No 2</label>
                                                    <input readonly type="number"
                                                        value="{{ $getEnquiry->contact_no2 ?? '' }}" name="contact_no2"
                                                        id="contact_no2" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="landmark">Landmark</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->landmark ?? '' }}" name="landmark"
                                                        id="landmark" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="street">Street</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->street ?? '' }}" name="street"
                                                        id="street" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="opposite">Opposite</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->opposite ?? '' }}" name="opposite"
                                                        id="opposite" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="locality">Locality</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->locality ?? '' }}" name="locality"
                                                        id="locality" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="pin_code">Pincode</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->pin_code ?? '' }}" name="pin_code"
                                                        id="pin_code" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div> --}}

                                <!-- Payment Section -->
                                <div class="card card-secondary">
                                    <div class="card-header">
                                        <h3 class="card-title">Payment Details</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="area_sq_ft">Area Sq Ft</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->area_sq_ft ?? '' }}" name="area_sq_ft"
                                                        id="area_sq_ft" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="booked_by">Booked By</label>
                                                    <select name="booked_by" id="booked_by" disabled
                                                        class="form-control select2">
                                                        <option value=""> Select User</option>
                                                        @foreach ($getUsers as $row)
                                                            <option @if (!empty($getEnquiry->booked_by) && $getEnquiry->booked_by == $row->id) selected @endif
                                                                value="{{ $row->id }}"> {{ $row->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="entire">Entire</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->entire ?? '' }}" name="entire"
                                                        id="entire" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="value_of_contact">Value of Contact Rs</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->value_of_contact ?? '' }}"
                                                        name="value_of_contact" id="value_of_contact"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="payment_received">Payment Received</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->payment_received ?? '' }}"
                                                        name="payment_received" id="payment_received"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="balance_payment">Balance Payment</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->balance_payment ?? '' }}"
                                                        name="balance_payment" id="balance_payment" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="payment_mode">Payment Mode</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->payment_mode ?? '' }}" name="payment_mode"
                                                        id="payment_mode" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="concern_person">Concern Person</label>
                                                    <input readonly type="text"
                                                        value="{{ $getEnquiry->concern_person ?? '' }}"
                                                        name="concern_person" id="concern_person" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="concern_person">Remark</label>
                                                    <textarea readonly name="remark" class="form-control">{{ $getEnquiry->remark ?? '' }}</textarea>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var form = document.getElementById('enquiry-form');
            var fileInput = document.getElementById('attachment');

            form.addEventListener('submit', function(event) {
                if (!validateFile(fileInput)) {
                    event.preventDefault(); // Prevent form submission if validation fails
                }
            });

            function validateFile(input) {
                var allowedExtensions = /(\.mp3)$/i;
                var fileName = input.value;

                if (!allowedExtensions.exec(fileName)) {
                    $('#fileErr').fadeIn();
                    setTimeout(() => {
                        $('#fileErr').fadeOut();
                    }, 2500);
                    return false;
                }

                return true;
            }
        });


        $(document).ready(function() {
            const startRecordingBtn = $('#startRecording');
            const stopRecordingBtn = $('#stopRecording');
            const uploadRecordingBtn = $('#uploadRecording');
            const audioPlayer = $('#audioPlayer');
            let mediaRecorder;
            let audioChunks = [];

            navigator.mediaDevices.getUserMedia({
                    audio: true
                })
                .then(stream => {
                    mediaRecorder = new MediaRecorder(stream);

                    mediaRecorder.ondataavailable = event => {
                        if (event.data.size > 0) {
                            audioChunks.push(event.data);
                        }
                    };

                    mediaRecorder.onstop = () => {
                        const audioBlob = new Blob(audioChunks, {
                            type: 'audio/wav'
                        });
                        const audioUrl = URL.createObjectURL(audioBlob);
                        audioPlayer.attr('src', audioUrl).show();
                        uploadRecordingBtn.prop('disabled', false);
                    };

                    startRecordingBtn.click(function() {
                        mediaRecorder.start();
                        startRecordingBtn.prop('disabled', true);
                        stopRecordingBtn.prop('disabled', false);
                    });

                    stopRecordingBtn.click(function() {
                        mediaRecorder.stop();
                        startRecordingBtn.prop('disabled', false);
                        stopRecordingBtn.prop('disabled', true);
                    });

                    uploadRecordingBtn.click(function() {
                        const formData = new FormData();
                        formData.append('audio', new File(audioChunks, 'audio.wav'));
                        formData.append('enquiry_id', $("#enquiry_id").val());

                        $.ajax({
                            url: '/enquiry-recording-upload',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                setTimeout(function(){
                                    location.reload(true);
                                }, 1000);
                            },
                            error: function(error) {
                                console.error('Error uploading recording:', error);
                            }
                        });
                    });
                })
                .catch(error => {
                    console.error('Error accessing microphone:', error);
                });
        });

        function deleteRecording(recordingId) {
            var confirmDelete = window.confirm('Are you sure you want to delete this recording?');
            if (confirmDelete) {
            $.ajax({
                            url: '/enquiry-recording-delete/'+recordingId,
                            type: 'POST',
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                location.reload();
                            },
                            error: function(error) {
                                console.error('Error uploading recording:', error);
                            }
                        });
                    } else {
        // User clicked "Cancel" in the confirmation dialog
        console.log('Deletion canceled.');
        // You can add additional actions or simply do nothing
    }
    }
    </script>
@endpush
