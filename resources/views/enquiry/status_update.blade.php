<div class="row update_status">
    <input type="hidden" value="{{ $getEnquiry->id }}" id="status_update_id">
    <div class="col-md-6">
        Name : {{ $getEnquiry->first_name }}  {{ $getEnquiry->middle_name }}  {{ $getEnquiry->last_name }}
    </div>
    <div class="col-md-4">
        Contact : {{ $getEnquiry->contact }}
    </div>
    <hr>
    <div class="col-md-12">
        <div class="form-group">
            <label for="status">Status</label>
            <select name="status" id="status"
                class="form-control select2">
                <option @if ($getEnquiry->status =="Open") selected @endif value="Open"> Open </option>
                <option  @if ($getEnquiry->status =="Close") selected @endif value="Close"> Close </option>
                <option @if ($getEnquiry->status =="Converted") selected @endif value="Converted" value="Converted"> Converted To Customer </option>
            </select>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="appointment_date">Follow-up Customer</label>
            <input type="datetime-local" class="form-control" id="appointment_date" name="appointment_date" value="{{ $getEnquiry->appointment_date ?? "" }}">
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="form-group">
            <label for="concern_person">Remark</label>
            <textarea name="remark" id="remark" class="form-control">{{ $getEnquiry->remark ?? '' }}</textarea>
        </div>
    </div>
</div>

