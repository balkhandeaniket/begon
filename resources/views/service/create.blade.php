@extends('main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h4>Service</h4>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">

                                <div class="card-tools">
                                    <ul class="nav nav-pills ml-auto">
                                        <li class="nav-item btn-sm">
                                            <button class="btn-sm btn-danger" onclick="history.back()">Back</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{ $data['action'] }}" method="post">
                                    @csrf

                                    <div class="card card-secondary">
                                        <div class="card-header">
                                            <h3 class="card-title">{{ $data['action_name'] }}</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="status">Category <span class="text-danger">*</span></label>
                                                        <select name="category_id" id="category_id" class="form-control select2" required>
                                                            <option value="">Select</option>
                                                            @if (count($data['categoryList'])> 0)
                                                                @foreach ($data['categoryList'] as $category)
                                                                    <option value="{{ $category->id }}" @if(!empty($getService) && $getService->category_id == $category->id) selected @endif>{{ $category->title }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="title">Name<span class="text-danger">*</span></label>
                                                        <input type="text" name="title" id="title"
                                                            class="form-control" value="{{ $getService->title ?? '' }}"
                                                            required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="hsn">HSN No.</label>
                                                        <input type="text" name="hsn" id="hsn"
                                                            class="form-control" value="{{ $getService->hsn ?? '' }}"
                                                            >
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="remark">Description</label>
                                                        <input type="text" name="remark" id="remark"
                                                            class="form-control" value="{{ $getService->remark ?? '' }}"
                                                            >
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="status">Status</label>
                                                        <select name="status" id="status" class="form-control select2">
                                                            <option @if (!empty($getService) && $getService->status == 'Active') selected @endif
                                                                value="Active">Active</option>
                                                            <option @if (!empty($getService) && $getService->status == 'Inactive') selected @endif
                                                                value="Inactive">Inactive</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix">&nbsp</div>
                                            <button class="btn btn-sm btn-success"> Submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
