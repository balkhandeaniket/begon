<!-- resources/views/main.blade.php -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Be - Gon</title>
    <!-- <title>@yield('title')</title>
    @section('title', 'Home') -->
</head>

<body>

    <!-- Include Header Component -->
    @include('components.header')

    <!-- Include Left Panel Component -->
    @include('components.left-panel')

    <!-- Main Content -->
    <div class="main-content">
        @yield('content')
    </div>

    <!-- Include Footer Component -->
    @include('components.footer-datatable')
    @stack('scripts')
</body>

</html>
