<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;
use App\Http\Controllers\EnquiryController;
use App\Http\Controllers\UserDataController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CustomerController;
// use App\Http\Controllers\PdfController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ServiceDueController;
use App\Http\Controllers\CollectionController;
use App\Http\Controllers\OutstandingAmountController;
use App\Http\Controllers\PartyController;

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['check.ip'])->group(function () {
    // Your protected routes here
    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');

    // Service
    Route::get('/category', [CategoryController::class, 'index'])->name('category');
    Route::get('/category-create', [CategoryController::class, 'create'])->name('category.create');
    Route::post('/category-add-action', [CategoryController::class, 'store'])->name('category.store');
    Route::get('/category-edit/{id}', [CategoryController::class, 'edit']);
    Route::post('/category-update-action/{id}', [CategoryController::class, 'update'])->name('category.update');

    // Service
    Route::get('/service', [ServiceController::class, 'index'])->name('service');
    Route::get('/service-create', [ServiceController::class, 'create'])->name('service.create');
    Route::post('/service-add-action', [ServiceController::class, 'store'])->name('service.store');
    Route::get('/service-edit/{id}', [ServiceController::class, 'edit']);
    Route::post('/service-update-action/{id}', [ServiceController::class, 'update'])->name('service.update');

    // Enquiry
    Route::get('/enquiry', [EnquiryController::class, 'index'])->name('enquiry.index');
    Route::post('/enquiry-list', [EnquiryController::class, 'getEnquiry'])->name('enquiry.list');
    Route::get('/enquiry-create/{type?}', [EnquiryController::class, 'addEnquiry'])->name('enquiry.create');
    Route::post('/enquiry-add', [EnquiryController::class, 'addEnquiryAction'])->name('enquiry.store');
    Route::post('/enquiry-getServerSide', [EnquiryController::class, 'getServerSide'])->name('enquiry.getServerSide');
    Route::get('/enquiry-edit/{id}', [EnquiryController::class, 'edit']);
    Route::post('/enquiry-update-action/{id}', [EnquiryController::class, 'update']);
    Route::post('/enquiry-update-status', [EnquiryController::class, 'updateStatus']);
    Route::post('/enquiry-update-status-action', [EnquiryController::class, 'updateStatusAction']);
    Route::post('/enquiry-recording-upload', [EnquiryController::class, 'recordingUpload']);
    Route::get('/enquiry-show/{id}', [EnquiryController::class, 'show']);
    Route::post('/enquiry-recording-delete/{id}', [EnquiryController::class, 'deleteRecording']);

    // Customer
    Route::get('/customer', [CustomerController::class, 'index'])->name('customer.index');
    Route::post('/customer-getServerSide', [CustomerController::class, 'getServerSide'])->name('customer.getServerSide');
    Route::get('/customer-edit/{id}', [CustomerController::class, 'edit']);
    Route::post('/customer-add', [CustomerController::class, 'store'])->name('customer.store');
    Route::post('/customer-update-action/{id}', [CustomerController::class, 'update']);
    Route::get('/customer-show/{id}', [CustomerController::class, 'show']);
    Route::get('/customer-payment-create/{id?}/{is_bill?}', [CustomerController::class, 'createCustomerPayment']);
    Route::get('/customer-payment-update/{id?}/{payment_id?}/{is_bill?}', [CustomerController::class, 'updateCustomerPayment']);
    Route::post('/customer-add-services', [CustomerController::class, 'addService']);
    Route::post('/customer-submit-payment-action', [CustomerController::class, 'customerPaymentSubmit']);
    Route::get('/customer-payment-pdf/{payment_id}/{address_id}/{type}', [CustomerController::class, 'paymentPdf']);
    Route::get('/customer-contract-form/{payment_id}/{address_id}', [CustomerController::class, 'contractForm']);
    Route::get('/add-customer-address/{id}', [CustomerController::class, 'customerAddressForm']);
    Route::post('/customer-address-add-action', [CustomerController::class, 'customerAddressAddAction']);
    Route::get('/customer-address-edit/{id}', [CustomerController::class, 'editAddress']);
    Route::post('/customer-address-edit-action', [CustomerController::class, 'updateCustomerAddress']);
    Route::post('/customer-services', [CustomerController::class, 'getCustomerServices']);
    Route::post('/get-customer-address', [CustomerController::class, 'getCustomerAddress']);
    Route::post('/customer-paid-amount', [CustomerController::class, 'updateCustomerPaidPayment']);
    Route::post('/customer-submit-paid-amount', [CustomerController::class, 'submitCustomerPaidPayment']);
    Route::post('/customer-payment-cancelled', [CustomerController::class, 'paymentCancelled']);
    Route::post('/customer-filter-get', [CustomerController::class, 'getCustomer']);
    Route::post('/customer-number-duplication-check', [CustomerController::class, 'checkNumberDuplication']);
    Route::post('/customer-invoice-unique', [CustomerController::class, 'invoiceUnique']);
    Route::post('/customer-download-csv', [CustomerController::class, 'downloadCSV']);
    //customer card
    Route::get('/customer-card/{id}', [CustomerController::class, 'customerCard']);

    //Party
    Route::get('/party', [PartyController::class, 'index'])->name('party.index');
    Route::post('/party-getServerSide', [PartyController::class, 'getServerSide'])->name('party.getServerSide');
    Route::post('/party-details', [PartyController::class, 'getPartyDetails'])->name('party.getPartyDetails');
    Route::post('/party-delete-transaction', [PartyController::class, 'deletePaymentTrans'])->name('party.deletePaymentTrans');
    Route::get('/party-payment-invoice/{transaction_id}', [PartyController::class, 'paymentPdf']);

    // Users
    Route::get('/user', [UserController::class, 'index'])->name('user.index');
    Route::post('/user-getServerSide', [UserController::class, 'getServerSide'])->name('user.getServerSide');
    Route::get('/user-create', [UserController::class, 'create'])->name('user.create');
    Route::post('/user-add', [UserController::class, 'store']);
    Route::get('/user-edit/{id}', [UserController::class, 'edit']);
    Route::post('/user-update-action/{id}', [UserController::class, 'update']);
    Route::post('/user-change-pass', [UserController::class, 'updateStatus']);
    Route::post('/user-ip-save', [UserController::class, 'userIpSave']);

    // Service due
    Route::get('/servicedue', [ServiceDueController::class, 'index'])->name('servicedue.index');
    Route::post('/servicedue-getServerSide', [ServiceDueController::class, 'getServerSide'])->name('servicedue.getServerSide');
    Route::post('/servicedue-update-status', [ServiceDueController::class, 'updateStatus']);

    // Collection
    Route::get('/collection', [CollectionController::class, 'index'])->name('collection.index');
    Route::post('/collection-getServerSide', [CollectionController::class, 'getServerSide'])->name('collection.getServerSide');
    Route::post('/collection-total-amount-cal', [CollectionController::class, 'totalAmountCal']);

    // outstanding
    Route::get('/outstanding', [OutstandingAmountController::class, 'index'])->name('outstanding.index');
    Route::post('/outstanding-getServerSide', [OutstandingAmountController::class, 'getServerSide'])->name('outstanding.getServerSide');
    Route::post('/outstanding-total-amount-cal', [OutstandingAmountController::class, 'totalAmountCal']);

    // Operator
    Route::get('/operator', [UserDataController::class, 'index'])->name('operator.index');
    Route::post('/operator-getServerSide', [UserDataController::class, 'getServerSide'])->name('operator.getServerSide');
    Route::get('/operator-show/{id}/{type}', [UserDataController::class, 'show']);
    Route::post('/operator-mark-attendance', [UserDataController::class, 'markAttendance']);
    Route::post('/mark-attendance-action', [UserDataController::class, 'addAttendance']);
    Route::post('/operator-getServerSideAttendence', [UserDataController::class, 'getServerSideAttendence']);
    Route::post('/operator-getServerSideOperatorData', [UserDataController::class, 'getServerSideOperatorData']);
    Route::post('/operator-getServerSideOperatorWorkData', [UserDataController::class, 'getServerSideOperatorWorkData']);
    Route::post('/operator-summary-data', [UserDataController::class, 'summaryData']);
    Route::post('/user-sheet-data-add', [UserDataController::class, 'sheetDataAdd']);
    Route::post('/operator-add-more', [UserDataController::class, 'operatorAddMore']);
    Route::post('/operator-add-more-save', [UserDataController::class, 'operatorAddMoreSave']);
    Route::post('/operator-attendence-delete-data', [UserDataController::class, 'operatorAttendenceDeleteData']);
    Route::post('/operator-attendence-delete-salary-work', [UserDataController::class, 'operatorAttendenceDeleteSalaryWork']);
    Route::get('/salary-slip/{id}/{from_date}/{to_date}', [UserDataController::class, 'salarySlip']);

    Auth::routes();
});
