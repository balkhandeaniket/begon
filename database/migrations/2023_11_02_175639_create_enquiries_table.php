<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquiries', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            // $table->string('middle_name')->nullable();
            // $table->string('last_name')->nullable();
            $table->bigInteger('contact'); // Change data type to bigInteger
            $table->text('address_line1')->nullable();
            $table->string('email')->nullable();
            $table->string('attachment')->nullable();
            $table->string('services')->nullable();
            // $table->string('contact_no1')->nullable(); // Change data type to bigInteger
            // $table->string('contact_no2')->nullable(); // Change data type to bigInteger
            // $table->string('locality')->nullable();
            // $table->string('address_line2')->nullable();
            // $table->string('landmark')->nullable();
            // $table->string('road')->nullable();
            // $table->string('street')->nullable();
            // $table->string('near')->nullable();
            // $table->string('opposite')->nullable();
            // $table->string('pin_code')->nullable();
            $table->string('area_sq_ft')->nullable();
            $table->integer('booked_by')->nullable();
            $table->string('entire')->nullable();
            $table->string('value_of_contact')->nullable();
            $table->string('payment_received')->nullable();
            $table->string('balance_payment')->nullable();
            $table->string('payment_mode')->nullable();
            $table->string('concern_person')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->text('remark')->nullable();
            $table->dateTime('appointment_date')->nullable();
            $table->enum('status', ['Open','Close','Converted'])->default('Open');
            $table->enum('category', ['New','Renewal','Society'])->default('New');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enquiries');
    }
}
