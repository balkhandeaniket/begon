<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_payments', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id');
            $table->integer('customer_address_id');
            $table->string('finance_year')->nullable();
            $table->string('invoice_no')->nullable();
            $table->date('invoice_date')->nullable();
            //$table->string('state_of_supply')->nullable();
            //$table->double('total_amount', 8, 2)->nullable();
            $table->double('paid_amount')->nullable();
            $table->enum('payment_mode', ['Cash','Cheque','Bank Transfer','Card Payment','Online','UPI'])->default('Cash');
            $table->string('bank_name')->nullable();
            $table->string('payment_reference')->nullable();
            $table->integer('payment_disussion')->nullable();
            $table->text('remark')->nullable();
            $table->enum('is_cancelled', ['0','1'])->default('0');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_payments');
    }
}
