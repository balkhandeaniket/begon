<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_addresses', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id'); 
            $table->string('contact'); // Change data type to bigInteger
            $table->string('email')->nullable();
            // $table->string('contact_no1')->nullable(); // Change data type to bigInteger
            // $table->string('contact_no2')->nullable(); // Change data type to bigInteger
            $table->string('locality')->nullable();
            $table->string('address_line1')->nullable();
            $table->string('address_line2')->nullable();
            $table->string('landmark')->nullable();
            $table->string('road')->nullable();
            $table->string('street')->nullable();
            $table->string('near')->nullable();
            $table->string('opposite')->nullable();
            $table->string('pin_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_addresses');
    }
}
