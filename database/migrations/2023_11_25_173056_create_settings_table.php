<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('logo');
            $table->string('bank_name');
            $table->string('bank_account');
            $table->string('ifsc_code');
            $table->string('account_holder_name');
            $table->string('gst_no');
            $table->string('hsn_code');
            $table->string('lic_no');
            $table->text('address');
            $table->string('email');
            $table->string('website');
            $table->string('state');
            $table->string('mobile_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
