<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceDueDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_due_dates', function (Blueprint $table) {
            $table->id();
            $table->integer('service_id');
            $table->date('service_due');
            $table->date('completed_date')->nullable();
            $table->string('card_image')->nullable();
            $table->text('remark')->nullable(); 
            $table->enum('is_completed', ['0','1'])->default('0');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_due_dates');
    }
}
