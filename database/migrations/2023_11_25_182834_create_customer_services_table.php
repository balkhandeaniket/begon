<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_services', function (Blueprint $table) {
            $table->id();
            $table->integer('payment_id');
            $table->integer('category_id');
            $table->string('service_id');
            //$table->date('service_due')->nullable();
            $table->string('contract_from')->nullable();
            $table->string('contract_to')->nullable();
            $table->string('contract')->nullable();
            $table->string('period')->nullable();
            $table->integer('price')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('gst')->nullable();
            $table->integer('gst_cal_amount')->nullable();
            $table->integer('gst_amount')->nullable();
            $table->text('remark')->nullable();
            $table->string('service_due_text')->nullable();
            $table->enum('status', ['0','1'])->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_services');
    }
}
